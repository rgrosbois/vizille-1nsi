import random
import re

def define_env(env):
    "Hook function"

    env.variables['term_counter'] = 0
    env.variables['IDE_counter'] = 0
    env.variables['dnd_counter'] = 0
    INFTY_SYMBOL = '\u221e'
    from urllib.parse import unquote

    @env.macro
    def rg_dnd(question, liste, mel_prop=1, mel_empl=1) -> str:
        """
        Question de type glisser-déposer (Drag-and-Drop).

        La fonction génère le code HTML correspond à l'arbre suivant:
        * fieldset
        |-- legend (énoncé)
        |   |-- button (validation)
        |   |-- button (reset)
        |-- div (énoncé de la question)
        |-- div (pile de propositions)
        |   |-- div (proposition)
        |   |-- div (proposition)
        |   |-- ...
        |-- div (pile d'emplacements)
            |-- div (emplacement)
            |-- div (emplacement)
            |-- ...
        """

        html = f'<div class="enonce">{question}</div>'
        html += '<div class="pile-propositions">'

        # Liste de propositions (éventuellement mélangées)
        liste2 = liste.copy()
        if mel_prop==1:
            random.shuffle(liste2) 
        for src,dst in liste2:
            if src != '':
                html += f'<div class="proposition" id="i{env.variables["dnd_counter"]}" draggable="true" ondragstart="rg_dragstart(event)">{src}</div>'
                env.variables['dnd_counter'] += 1
            
        html += '</div><div class="pile-emplacements">'

        # Liste d'emplacements (éventuellement mélangés)
        if mel_empl==1:
            random.shuffle(liste) 
        for src,dst in liste:
            if dst != '':
                html += f'<div class="emplacement" ondragover="rg_dragover(event)" ondrop="rg_drop(event, \'{src}\')">{dst}</div>'

        html += '</div></fieldset>'

        dico = { key:val for val,key in liste}
        
        htmlStart = f'''<fieldset><legend><button class="validate" onclick="corriger_dnd(this.parentNode.parentNode, {str(dico)})">&#10003;</button>'''
        htmlStart += f'''<button class="reset" onclick="reset_dnd(this.parentNode.parentNode, {str(dico)})">&#x21bb;</button></legend>'''

        return htmlStart + html

    @env.macro
    def rg_qcm(question: str, liste: list[str]) -> str:
        """
        Créer une question à choix multiple (une ou plusieurs bonne réponses sont possibles).

        La fonction génère le code HTML correspondant à l'arbre suivant :
        * fieldset
        |-- legend
        |   |-- button (validation)
        |   |-- button (reset)
        |-- pre (uniquement si [verbatim]...[/verbatim] dans l'énoncé)
        |-- div (énoncé de la question)
        |-- figure (uniquement si ![width=xx]nomfichier.xx! dans l'énoncé)
        |   |-- img
        |-- ul (propositions de réponses)
            |-- li
            |-- li
            |-- ...
        """
        # Mélanger l'ordre des réponses
        melange = ([i for i in range(len(liste))])
        random.shuffle(melange)        
        bonnes_reponses = '['+','.join(['true' if liste[melange[i]][0]=='+' else 'false' for i in range(len(liste))])+']'

        # Légendes (boutons JavaScript)
        correct_clic = f'correction_qcm(this.parentNode.parentNode, {bonnes_reponses})'
        reset_clic = f'reinitialiser_qcm(this.parentNode.parentNode, {bonnes_reponses})'
        htmlStart = f'''<fieldset><legend><button class="validate" onclick="{correct_clic}">&#10003;</button>'''
        htmlStart += f'''<button class="reset" onclick="{reset_clic}">&#x21bb;</button></legend>'''

        ################
        # Énoncé de la question

        # Mode horizontal ?
        i_horiz = question.find('[horiz]')
        if i_horiz!=-1:
            horizontal = True
            question = question[i_horiz+len('[horiz]'):]
        else:
            horizontal = False

        # Code verbatim
        i_verb_start = question.find('[verbatim]')
        i_verb_end = question.find('[/verbatim]')
        if i_verb_start!=-1 and i_verb_end!=-1:
            tmp = question[:i_verb_start].rstrip()
            tmp += '<pre>'
            tmp += question[i_verb_start+len('[verbatim]'):i_verb_end].strip().replace('\n', '\n    ')
            tmp += '</pre>'
            tmp += question[i_verb_end+len('[/verbatim]'):]
            question = tmp

        # Image
        m = re.search(r"!\[[\w]+=(?P<largeur>\w+)\](?P<fichier>[^!]+)!", question)
        if m:
            tmp = question[:m.start()].rstrip()
            tmp += question[m.end():].lstrip()
            question = tmp

            htmlStart += '<div class="enonce">'+question.strip()+'</div>'
            htmlStart += '<figure style="display: flex; justify-content: center;"><img src="'+m.group('fichier')+'" style="width: '+m.group('largeur')+';"></figure>'
        else:
            htmlStart += '<div class="enonce">'+question.strip()+'</div>'

        ################
        # Propositions de réponses

        # Affichage (mélangé) des propositions
        ul = '<ul class="reponses">'
        for i in range(len(liste)):
            elem = liste[melange[i]][1:]
            if horizontal:
                ul += f'<li onclick="cocher(event)" style="display: inline;">{elem}</li>'
            else:
                ul += f'<li onclick="cocher(event)">{elem}</li>'
        ul += '</ul>'

        # Bouton de validation
        htmlEnd = '</fieldset>'

        resultat = htmlStart + ul + htmlEnd
        return resultat
    
    def ajouter_question(questions: list[tuple], enonce: str, propositions: list[str]):
        """
        Rajouter une question (=énonce + propositions) dans un questionnaire.
        """

        if len(propositions)==0: # question non valide
            return

        # Caractères spéciaux
        enonce = enonce.replace("'", "′")
        enonce = enonce.replace('"', " ")
        for i in range(len(propositions)):
            propositions[i] = propositions[i].replace("'", "′")
            propositions[i] = propositions[i].replace("'", " ")

        # Ajouter au questionnaire
        questions.append( (enonce, propositions) )

    @env.macro
    def rg_qnum(reponse, precision) -> str:
        """
        Créer une question attendant une réponse numérique.
        """
        largeur = 3+len(str(reponse))

        tmp = '<span class="rg-qnum-span">'
        tmp += '<button class="reset" onclick="reset_question(this.parentNode)">&#x21bb;</button>'
        tmp += f'<input size="{largeur}" type="number" '
        tmp += 'onkeypress="if (event.keyCode === 13) { event.preventDefault(); '
        tmp += f'correction_qnum(this.parentNode, {reponse}, {precision});'
        tmp += '}" class="rg-qnum reponse-vide" autocomplete="off" autocapitalize="none" spellcheck="false">'
        tmp += f'<button class="validate" onclick="correction_qnum(this.parentNode, {reponse}, {precision})">&#10003;</button>'
        tmp += '</span>'
        return tmp

    @env.macro
    def rg_qsa(reponses: list[str]) -> str:
        """
        Créer une question attendant une réponse textuelle courte.
        """
        # Prévoir une zone de saisie de 3 caractères de plus
        # que la réponse la plus longue
        max = 0
        for r in reponses:
            if len(r)>max:
                max = len(r)
        largeur = 3+max

        tmp = '<span class="rg-qnum-span">'
        tmp += '<button class="reset" onclick="reset_question(this.parentNode)">&#x21bb;</button>'
        tmp += f'<input size="{largeur}" type="text" '
        tmp += 'onkeypress="if (event.keyCode === 13) { event.preventDefault(); '
        tmp += f'correction_qsa(this.parentNode, {reponses});'
        tmp += '}" class="rg-qnum reponse-vide" autocomplete="off" autocapitalize="none" spellcheck="false">'
        tmp += f'<button class="validate" onclick="correction_qsa(this.parentNode, {reponses})">&#10003;</button>'
        tmp += '</span>'
        return tmp


    @env.macro
    def rg_generer_qcm(nom_fichier) -> str:
        """
        Créer un questionnaire à partir d'un fichier au format ressemblant à l'AMC-TXT.
        """
        questions = []
        enonce = ''
        propositions = []

        # Extraire du fichier la liste des questions et leurs propositions
        with open(nom_fichier, 'rt') as f:
            continuer = True
            while continuer:
                ligne = f.readline()

                if ligne=='': # fin de fichier
                    ajouter_question(questions, enonce, propositions)
                    # Réinitialisation des variables
                    enonce = ''
                    propositions = []
                    break

                ligne = ligne.strip() # supprimer la fin de ligne
                if ligne=='': # fin de question
                    ajouter_question(questions, enonce, propositions)
                    # Réinitialisation des variables
                    enonce = ''
                    propositions = []

                elif ligne[0]=='+' or ligne[0]=='-': # proposition de réponse
                    propositions.append(ligne)
                else: # ligne d'énoncé de question
                    idx = 0
                    while ligne[idx]=='*': # Supprimer les * en début de ligne
                        idx += 1
                    if len(enonce)==0:
                        enonce = ligne[idx:]
                    else:
                        enonce += '\n'+ligne[idx:]

        # Générer le code HTML
        code_html = ''

        counter = 0
        for q in questions:
            counter += 1
            code_html += '=== "Q' + str(counter) + '"\n\t'
            code_html += rg_qcm(q[0], q[1]) + "\n"

        return code_html    
    
    @env.macro
    def rg_corriger_page(titre="Corriger toute la page") -> str:
        """
        Créer un bouton permettant de lancer l'opération de correction
        de tous les exercices de la page Web.
        """
        return f'<button class="md-button md-button--primary" onclick="corriger_page(this)">{titre}</button>'

    @env.macro
    def rgpopup(titre, contenu) -> str:
        tmp = f'<button class="md-button" onclick="this.parentNode.nextSibling.style.display = \'block\';">{titre}</button>'
        tmp += '<div class="rgmodal" onclick="this.style.display = \'none\';"><div class="rgmodal-content" onclick="event.stopPropagation();">'
        tmp += '<span class="fermer" onclick="this.parentNode.parentNode.style.display = \'none\';">&times;</span>'
        tmp += f'<h1>{titre}</h1><p>{contenu}</p></div></div>'
        return tmp
        
    @env.macro
    def terminal() -> str:
        """   
        Purpose : Create a Python Terminal.
        Methods : Two layers to avoid focusing on the Terminal. 
        1) Fake Terminal using CSS 
        2) A click hides the fake terminal and triggers the actual Terminal.
        """        
        tc = env.variables['term_counter']
        env.variables['term_counter'] += 1
        return f"""<div onclick='start_term("id{tc}")' id="fake_id{tc}" class="terminal_f"><label class="terminal"><span>>>> </span></label></div><div id="id{tc}" class="hide"></div>"""

    def read_ext_file(nom_script : str, path : str, filetype : str = 'py') -> str:
        """
        Purpose : Read a Python file that is uploaded on the server.
        Methods : The content of the file is hidden in the webpage. Replacing \n by a string makes it possible
        to integrate the content in mkdocs admonitions.
        """
        docs_path = f"""docs/"""

        try: 
            if path == "":
                f = open(f"""{docs_path}/scripts/{nom_script}.{filetype}""")
            else:
                f = open(f"""{docs_path}/{path}/{nom_script}.{filetype}""")
            content = ''.join(f.readlines())
            f.close()
            content = content + "\n"
            # Hack to integrate code lines in admonitions in mkdocs
            # change backslash_newline by backslash-newline
            return content.replace('\n','bksl-nl').replace('_','py-und').replace('*','py-str')
        except :
            return ""
        
    def generate_content(nom_script : str, path : str, filetype : str = 'py') -> str:
        """
        Purpose : Return content and current number IDE {tc}.
        """
        content = read_ext_file(nom_script, path, filetype)

        if content not in [None, ""]:
            tc = hashlib.sha1(content.encode('utf-8')).hexdigest()
        else : # non-existent file, empty file
            tc = env.variables['IDE_counter']
            env.variables['IDE_counter'] += 1

        return content, str(tc).zfill(int(log10(MAX_EMPTY_IDE)))


    def create_upload_button(tc : str) -> str:
        """
        Purpose : Create upload button for a IDE number {tc}.
        Methods : Use an HTML input to upload a file from user. The user clicks on the button to fire a JS event
        that triggers the hidden input.
        """
        path_img = env.variables.page.abs_url.split('/')[1]
        return f"""<button class="tooltip" onclick="document.getElementById('input_editor_{tc}').click()"><img src="/{path_img}/images/buttons/icons8-upload-64.png"><span class="tooltiptext">Téléverser</span></button>\
                <input type="file" id="input_editor_{tc}" name="file" enctype="multipart/form-data" class="hide"/>"""

    def create_unittest_button(tc: str, nom_script: str, path : str, mode: str, MAX : int = 5) -> str:
        """
        Purpose : Generate the button for IDE {tc} to perform the unit tests if a valid test_script.py is present.
        Methods : Hide the content in a div that is called in the Javascript
        """
        stripped_nom_script = nom_script.split('/')[-1]
        relative_path = '/'.join(nom_script.split('/')[:-1])
        nom_script = f"{relative_path}/{stripped_nom_script}_test"
        content = read_ext_file(nom_script, path)
        # print(nom_script, path, content, content == "")
        if content != "":
            path_img = env.variables.page.abs_url.split('/')[1]
            return f"""<span id="test_term_editor_{tc}" class="hide">{content}</span>\
                <button class="tooltip" onclick=\'executeTest("{tc}","{mode}")\'>\
                <img src="/{path_img}/images/buttons/icons8-check-64.png">\
                <span class="tooltiptext">Valider</span></button><span class="compteur">\
                {MAX}/{MAX}\
                </span>"""
        else: 
            return ''

    def blank_space(s=0.3) -> str:
        """ 
        Purpose : Return 5em blank spaces. Use to spread the buttons evenly
        """
        return f"""<span style="display: inline-block; width:{s}em"></span>"""

    def get_max_from_file(content : str) -> tuple:#[str, int]: # compatibilité Python antérieur 3.8
        split_content = content.split('bksl-nl')
        max_var = split_content[0]
        if max_var[:4] != "#MAX":
            MAX = 5 
        else:
            value = max_var.split('=')[1].strip()
            MAX = int(value) if value not in ['+', 1000] else INFTY_SYMBOL
            i = 1
            while split_content[i] == '':
                i += 1
            content = 'bksl-nl'.join(split_content[i:])
        return content, MAX

    def test_style(nom_script : str, element : str) -> bool:
        guillemets = ["'", '"']
        ide_style = ["", "v"]
        styles = [f"""IDE{istyle}({i}{nom_script}{i}""" for i in guillemets for istyle in ide_style]
        return any([style for style in styles if style in element])

    def convert_url_to_utf8(nom : str) -> str:
        return unquote(nom, encoding='utf-8')
        

    @env.macro
    def IDEv(nom_script : str = '', MAX : int = 5, SANS : str = "") -> str:
        """
        Purpose : Easy macro to generate vertical IDE in Markdown mkdocs.
        Methods : Fire the IDE function with 'v' mode.
        """
        return IDE(nom_script, mode = 'v', MAX = MAX, SANS = SANS)

    def generate_key(path_file: str):
        try:
            f = open(f"docs/{path_file}/clef.txt", "r", encoding="utf8")
            clef = f.read()            
        except: 
            clef = "" # base case -> no clef.txt file
        return clef

    def tooltip_button(onclick_action : str, button_style : str):
        return f"""<button class="tooltip" onclick={onclick_action}>{button_style}</button>"""

    @env.macro
    def IDE(nom_script : str = '', mode : str = 'h', MAX : int = 5, SANS : str = "") -> str:
        """
        Purpose : Create an IDE (Editor+Terminal) on a Mkdocs document. {nom_script}.py is loaded on the editor if present. 
        Methods : Two modes are available : vertical or horizontal. Buttons are added through functional calls.
        Last span hides the code content of the IDE if loaded.
        """
        print("docs_dirs", env.conf['docs_dir'])
        path_img = convert_url_to_utf8(env.variables.page.abs_url).split('/')[1]
        print(path_img)
        path_file = '/'.join(filter(lambda folder: folder != "", convert_url_to_utf8(env.variables.page.abs_url).split('/')[2:-2]))
        print('P1','/'.join(filter(lambda folder: folder != "", convert_url_to_utf8(env.variables.page.url).split('/')[:-2])))
        print('P2','/'.join(filter(lambda folder: folder != "", convert_url_to_utf8(env.variables.page.abs_url).split('/')[2:-2])))

        clef = generate_key(path_file)

        content, tc = generate_content(nom_script, path_file)
        corr_content, _ = generate_content(f"""{'/'.join(nom_script.split('/')[:-1])}/{nom_script.split('/')[-1]}_corr""", path_file)

        content, max_from_file = get_max_from_file(content)
        MAX = max_from_file if MAX == 5 else MAX
        MAX = MAX if MAX not in ['+', 1000] else INFTY_SYMBOL

        SANS_formatted = ","+"".join(SANS.split(" ")) if len(SANS)>0 else ""
        div_edit = f'<div class="ide_classe" data-max={MAX} data-exclude={"eval,exec" + SANS_formatted} >'

        if mode == 'v':
            div_edit += f'<div class="wrapper"><span id="comment_editor_{tc}" class="comment">###</span><div class="interior_wrapper"><div id="editor_{tc}"></div></div><div id="term_editor_{tc}" class="term_editor"></div></div>'
        else:
            div_edit += f'<div class="wrapper_h"><span id="comment_editor_{tc}" class="comment">###</span><div class="line" id="editor_{tc}"></div><div id="term_editor_{tc}" class="term_editor_h terminal_f_h"></div></div>'

        div_edit += tooltip_button(f"""'interpretACE("editor_{tc}","{mode}")'""", f"""<img src="/{path_img}/images/buttons/icons8-play-64.png"><span class="tooltiptext">Lancer</span>""")
        div_edit += create_unittest_button(tc, nom_script, path_file, mode, MAX) + blank_space(1)
        div_edit += tooltip_button(f"""\'downloadFile("editor_{tc}","{nom_script}")\'""", f"""<img src="/{path_img}/images/buttons/icons8-download-64.png"><span class="tooltiptext">Télécharger</span>""")+ blank_space()
        div_edit += create_upload_button(tc) + blank_space(1)
        div_edit += tooltip_button(f"""\'reload("{tc}")\'""", f"""<img src="/{path_img}/images/buttons/icons8-restart-64.png"><span class="tooltiptext">Recharger</span>""") + blank_space()
        div_edit += tooltip_button(f"""\'saveEditor("{tc}")\'""", f"""<img src="/{path_img}/images/buttons/icons8-save-64.png"><span class="tooltiptext">Sauvegarder</span>""")
        div_edit += '</div>'

        div_edit += f"""<span id="content_editor_{tc}" class="hide">{content}</span>"""
        div_edit += f"""<span id="corr_content_editor_{tc}" class="hide" data-strudel="{str(clef)}">{corr_content}</span>"""
        
        elt_insertion = [elt for elt in env.page.markdown.split("\n") if test_style(nom_script, elt)]
        elt_insertion = elt_insertion[0] if len(elt_insertion) >=1 else ""
        indent = " "*(len(elt_insertion) - len(elt_insertion.lstrip()))
        if nom_script == '' : indent = " "  # to avoid conflict with empty IDEs
        if indent == "":
            div_edit += f'''
{indent}--8<--- "docs/xtra/start_REM.md"
'''
        div_edit += f'''
{indent}--8<--- "docs/{path_file if path_file != "" else 'scripts'}/{nom_script}_REM.md"''' if clef == "" else f""
        if indent == "":
            div_edit += f'''
{indent}--8<--- "docs/xtra/end_REM.md"
'''
        return div_edit    