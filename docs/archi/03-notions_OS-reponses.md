---
hide:
  - navigation
---

# Introduction aux systèmes d'exploitation

??? info "Historique"

    [![Historique OS](img/03-Histoire_OS.png)](https://medium.com/@scan.pratik/modern-operating-platforms-evolution-history-dbc094002aef){ target=_blank }

    (pas d'OS sur l'ENIAC)

    |      |      |         |
    |------|------|-------- |
    | 1956 | GM-NAA I/O sur calculateur IBM | exécution séquentielle de programmes sur cartes perforées + routines d'accès aux périphériques E/S|
    | 1967 | MutiCS (*Multiplexed Information and Computing Service*) : Bell Labs et GE | plusieurs programmes *en même temps* |
    | 1970-1990 | Unix aux Bell Labs (K. Thomson, D. Ritchie) : assembleur + langage C | multitâche et multi-utilisateurs |
    | 1960-1980 | divers OS différents d'Unix | ordinateurs IBM sur processeur x86 d'Intel. |
    | 1980-1990 | MS-DOS de Microsoft | processeur x86 d'Intel. |
    | 1990- | Windows  | initialement, surcouche graphique sur MS-DOS. | 
    | 1984-2001 | Ordinateurs Apple sur architecture Motorola puis PowerPC | MacOS (graphique). |
    | 1991- | Linus Torvald (étudiant à l'université d'Helsinki) développe Linux | Licence GPL. |
    | 2001- | MacOS | basé sur BSD. |
    | 2007- | iOS | Version MacOS pour téléphone portable |
    | 2008- | Google Android pour téléphone portable | basé sur Linux |

    Standard POSIX (*Portable Operating System Interface*): fonctions de bibliothèques que doivent offrir un OS.


Un système d'exploitation (en anglais: *Operating System*) est un programme qui 
gère en permanence les ressources matérielles[^1] pour de multiples applications.


- Il sert de *chef d'orchestre* pour accéder et partager les ressources.
- Il fournit des services aux applications : stockage, exécution, sécurité&hellip;


![Couche OS](img/03-couche_OS.svg){ width=75% }

[^1]: processeur, mémoire, entrées/sorties (écran, clavier, disque dur, clé USB&hellip;).

## I. Généralités

### 1. Constitution

- Noyau (*kernel*): c&oelig;ur du système.
- Bibliothèques (utilisées par les applications),
- Interpréteur de commande (*shell*),
- Un (ou plusieurs) système(s) de fichiers (Ext4, NTFS, FAT32&hellip;),
- &hellip;

### 2. Exemples

Il existe 2 principales familles de systèmes d'exploitation : 

```mermaid
flowchart 
    Multics --> UNIX
    UNIX --> Minix
    Minix --> GNU/Linux
    GNU/Linux --> Android
    UNIX --> Darwin/BSD --> MacOS
    MS-DOS --> Windows

    classDef temp fill:lightgray,stroke:black,stroke-dasharray: 5 5;
    class Multics,Minix temp;

    classDef actuel fill:#f96;
    class Linux,Windows,MacOS,Android actuel;
```

Certains sont des logiciels **libres**[^2] ([GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.fr.html)), d'autres sont 
**propriétaires**[^3] (Windows, MacOS).

!!! warning "Attention"

    libre &ne; gratuit

[^2]: Selon la FSF : possibilité d'exécuter, de copier, de distribuer, d'étudier, 
de modifier et d'améliorer le logiciel (via son code-source). 
[^3]: propriété exclusive de la société qui l'a conçu et qui le commercialise 
uniquement sous la forme exécutable.

*[FSF]: Free Software Foundation

??? info "Pour aller plus loin"

    - [Histoire d'UNIX](https://www.youtube.com/watch?v=Za6vGTLp-wg){ target=_blank }
    - Simulateurs OS Microsoft : [MS-DOS](https://copy.sh/v86/?profile=msdos){ target=_blank }, [Windows 3.1](https://copy.sh/v86/?profile=windows31){ target=_blank }, [Windows 95](https://copy.sh/v86/?profile=windows95){ target=blank }, [Windows 98](https://copy.sh/v86/?profile=windows98){ target=blank }, [Windows 2000](https://copy.sh/v86/?profile=windows2000){ target=blank }

    OS de périphériques réseau :

    - Cisco IOS, IOS XE, IOS XR
    - VRP (Huawei),
    - TiMOS (Alcatel-Lucent),
    - JUNOS (Jupiter),
    - &hellip;

## II. GNU/Linux


*[Linux]: Linux est le noyau du système d'exploitation GNU/Linux.

### 1. Distributions GNU/Linux

Elles contiennent le système d'exploitation [GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.fr.html) et une sélection d'applications (Gnome, 
LibreOffice, Firefox&hellip;).

|     | Ubuntu | Fedora | Raspian | &hellip;
|:---:|:---:|:---:|:---:|:---:|
| ![TuX](img/03-Tux.png) | [![Ubuntu](img/03-Ubuntu.png){ width=150 }](https://www.ubuntu-fr.org/) | [![Fedora](img/03-Fedora.png){ width=150 }](https://www.fedora-fr.org/) | [![Raspbian](img/03-Raspbian.png){ width=125 }](https://www.raspberrypi.org/software/operating-systems/) |     |   

### 2. Système de fichiers

Le stockage de données sur un périphérique se fait via des fichiers (blocs
d'octets non nécessairement contigus) qui possèdent un nom et souvent une
extension (indicative).

```{.console .rgconsole}
[nsi@localhost:~]$ ls -F
Documents/   Musique/   Consignes.pdf   Readme.txt  
```

!!! warning "GNU/Linux"

    Tout est fichier dans le système d'exploitation GNU/Linux : fichier standard, 
    périphérique, partition, répertoire&hellip;

#### 1. Propriétés

On peut visualiser les propriétés d'un fichier à l'aide de la commande `ls`
et de son option `-l` :

```{.console .rgconsole}
[nsi@localhost:Documents]$ ls -l
-rw-r--r--. 1 grosbois nsi      250 10 sept. 21:56 monfichier.txt
-rwxr-xr--. 1 grosbois nsi      130 11 sept. 11:32 monscript.sh
```

??? tip "Interprétation"

    Les droits sur le fichier `monfichier.txt` (utilisateur : `grosbois` 
    groupe : `nsi`),  d'une taille de 250 octets et modifié la dernière 
    fois le 10 septembre à 21h56, sont :

    - utilisateur : lecture et écriture (mais pas d'exécution)
    - groupe : lecture seule
    - autre : lecture seule

    Les droits sur le fichier `monscript.sh` (utilisateur : `grosbois` 
    groupe : `nsi`),  d'une taille de 130 octets et modifié la dernière 
    fois le 11 septembre à 11h32, sont : 

    - utilisateur : lecture, écriture et exécution
    - groupe : lecture et exécution (pas d'écriture)
    - autre : lecture seule.

Chaque fichier possède 9 permissions : 

![permissions](img/src/03-permissions_fichiers.svg){ width=600px }

- 3 types de permissions d'accès :

    - `r` : droit en lecture.
    - `w` : droit en écriture.
    - `x` : droit en exécution (pour un programme ou un répertoire).

- et 3 catégories d'utilisateurs :

    - `u`: le propriétaire du fichier (par défaut, la personne qui a créé le fichier),
    - `g`: le groupe d'utilisateurs qui possèdent des droits particuliers sur ce fichier. 
    - `o`: tous les autres utilisateurs.


??? info "Modifier les permissions d'un fichier"

    La commande `chmod` permet d'ajouter ou supprimer des droits sur un fichier : 

    `chmod <categorie_utilisateur>[+-]<type de droit> nom_de_fichier`

    Exemple:
    ```{.console .rgconsole}
    [nsi@localhost:Documents]$ chmod g+w monfichier.txt
    ```

    ??? tip "Interprétation"

        Le droit d'écriture sur le fichier `monfichier.txt` est rajouté au groupe.

#### 2. Structure hiérarchique

FHS : *Filesystem Hierarchy Standard*

Tous les fichiers s'organisent selon une *arborescence* avec un unique parent : la *racine* (**root**, notée `/`).

```mermaid
flowchart 

    root["/"] --- dev["dev/"] & etc["etc/"] & home["home/"] & proc["proc/"]
    root --- root2["root/"] & run["run/"] & var["var/"]
    root --- tmp["tmp/"] & usr["usr/"] 
    home --- nsi["nsi/"] & grosbois["grosbois/"]
    usr --- share["share/"] --- Python2["Python"] --- HelloNSI2["HelloNSI.py"]
    var --- www["www/"] --- html["html/"] --- index.html
    subgraph "~/"
        nsi --- Documents["Documents/"] & Download["Téléchargements/"] 
        Download --- escgame["escape_game/"]
        nsi --- Seance01["Seance01/"] --- Python["Python/"] --- HelloNSI.py
    end
    grosbois --- Seance013["Seance01/"] --- Python3["Python/"] --- Hello3["HelloNSI.py"]

```

*(Un répertoire peut contenir un ou plusieurs (sous-)répertoires et fichiers)*

??? example "Exercices"

    1. Depuis le répertoire `Téléchargements`, déterminer les chemins absolu
    et relatifs pour aller dans le répertoire `Documents`.

        ??? tip "Réponse" 

            Absolu : `/home/nsi/Documents`

            Relatif : `../Documents`

    2. Depuis le répertoire `www`, déterminer les chemins absolu
    et relatifs pour aller dans le répertoire `share`.

        ??? tip "Réponse" 

            Absolu : `/usr/share`

            Relatif : `../../usr/share`


    3. Dessiner l'arbre des fichiers/répertoires (connus) après les 
    commandes suivantes :

        ```{.console .rgconsole}
        [nsi@localhost:~]$ cd /home/nsi
        [nsi@localhost:~]$ mkdir Photos
        [nsi@localhost:~]$ mkdir Vidéo
        [nsi@localhost:~]$ cd Vidéo
        [nsi@localhost:Vidéo]$ mkdir Copines
        [nsi@localhost:Vidéo]$ cd ..
        [nsi@localhost:~]$ mkdir Vacances
        [nsi@localhost:~]$ mkdir Vacances/Avril
        [nsi@localhost:~]$ mkdir Vacances/Juillet
        [nsi@localhost:~]$ mkdir /home/nsi/Vidéo/Danse
        ```

    4. Citer un avantage d'un chemin relatif par rapport à un chemin absolu

        ??? tip "Réponse" 

            Il est généralement plus court

    5. Citer un avantage d'un chemin absolu par rapport à un chemin relatif

        ??? tip "Réponse" 

            Absolu : Il est unique


??? info "Répertoires particuliers"

    - `/`: la **racine**  (*root*) est un répertoire (fichier) contient tous les autres.
    - `~/`: le répertoire personnel (`/home/nsi/` pour l'utilisateur `nsi`).
    - `./`: répertoire courant
    - `../`: répertoire parent.

- Chemin d'un fichier :

    - Chemin d'accès **absolu** : liste des répertoires (séparés par `/`) 
    rencontrés depuis la racine (`/`) jusqu'à la destination. 
    
        > Il débute obligatoirement par le caractère `/`.

        ??? example "Exemples"

            ```
            /home/nsi/Seance01/Python/HelloNSI.py
            /usr/share/Python/HelloNSI.py
            ```
    
    - Chemin d'accès **relatif** : liste des répertoires (séparés par `/`)
    rencontrés depuis le répertoire courant (`./`). La notation `../` permet
    d'accéder à un répertoire parent.

        > Il débute par `./` (facultatif) ou `../`.

        ??? example "Exemples" 
        
            (depuis le répertoire `/home/nsi`) :
            ```
            Seance01/Python/HelloNSI.py
            ../grosbois/Seance01/Python/HelloNSI.py
            ../../var/www/html/index.html
            ```

### 3. Interpréteur de commandes (*shell*)

Il est accessible via une application de *terminal* (exemple: *LXTerminal*, 
*xterm*, *gnome-terminal*&hellip;) et permet d'interagir directement avec le 
noyau du système d'exploitation ou de lancer des programmes. 

*Exemple de shell : bash, tcsh&hellip;*

??? danger "Émulateur de terminal"

    Taper les commandes suivantes pour obtenir une configuration de base :

    ```{.console}
    adduser -D nsi
    su -l nsi
    export PS1='[\u@\h:\W]\$ '
    clear
    ```

    <iframe loading="lazy" 
        width="100%"
        scrolling="no"
        height="275"
        title="JSLinux"
        allow="fullscreen"
        src="https://bellard.org/jslinux/vm.html?url=alpine-x86.cfg&mem=192&rows=10">
        </iframe>


??? info "Invite de commande du shell (prompt)"

    ```{.console .rgconsole}
    [nsi@localhost:~]$ 
    ```

    ??? tip "Détails"

        - `nsi` : nom de l'utilisateur actuel
        - `localhost` : nom de l'ordinateur
        - `~` : nom du répertoire actuel (ici le répertoire personnel)
        - `$` : l'utilisateur actuel n'a pas de privilèges.

Lancer un programme :

```{.console .rgconsole}
[nsi@localhost:~]$ programme -option1 -option2 argument1 argument2
```

Deux possibilités :

1. Fournir le **chemin absolu ou relatif** du programme :

    ```{.console .rgconsole}
    [nsi@localhost:~]$ /usr/bin/nano
    ```

2. Fournir le **nom du programme** (il faut que le répertoire le contenant 
soit spécifié dans `PATH`).

    ??? tip "Variable d'environnement PATH"
        ```{.console .rgconsole}
        [nsi@localhost:~]$ echo $PATH
        /usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin
        ```

        ??? tip "Modifier PATH"
            ```{.console .rgconsole}
            [nsi@localhost:~]$ export PATH=$PATH:/home/nsi/bin/
            ```


    ```{.console .rgconsole}
    [nsi@localhost ~]$ lancer_sauvegarde.sh
    ```


??? tip "Quelques commandes/programmes utiles"

    | Commande | Signification |
    |:--------:|:--------------|
    | `man`    | Manuel d'utilisation d'une commande |
    | `pwd`    | Afficher le nom du répertoire de travail actuel |
    | `ls`     | Afficher le contenu de répertoires |
    | `cd`     | Changer de répertoire de travail |
    | `cp`     | Copier un fichier |
    | `rm`     | Supprimer un fichier |
    | `mv`     | Déplacer/renommer un fichier ou répertoire |
    | `mkdir`  | Créer un répertoire |
    | `rmdir`  | Supprimer un répertoire vide |
    | `echo`   | Afficher une ligne de texte |
    | `cat`    | Afficher sur la sortie standard |
    | `less`   | Affichage avancé d'un fichier texte |
    | `nano`   | Un éditeur de texte fonctionnant dans le terminal | 

??? tip "Caractère générique"

    Le caractère `*` permet de remplacer un ou plusieurs caractères quelconques. 

    Par exemple, pour lister tous les fichiers commençant par `fi` :

    ```{.console .rgconsole}
    [nsi@localhost:Documents]$ ls fi*
    ```

### 4. Types d'utilisateurs

2 catégories :

- Classique (ou *sans privilège*) : `nsi`&hellip;
    ```{.console .rgconsole}
    [nsi@localhost ~]$
    ```
- Administrateur : 
    - super-utilisateur (ou **root**)
        ```{.console .rgconsole}
        [root@localhost ~]# 
        ```
    - avec privilèges temporaires (*sudoers*)
        ```{.console .rgconsole}
        [nsi@localhost ~]$ sudo less /etc/shadow
        ```
