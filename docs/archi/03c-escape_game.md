# Escape Game Linux

Utiliser cet émulateur pour jouer à l'escape game:

{{ rgpopup('Emulateur de terminal Linux', '<iframe src="https://snlpdo.fr/weblinux/" width=100% height="550"></iframe>') }} 



Pour démarrer le jeu :

- Sauvegarder cette [archive compressée](img/03-escape_game-weblinux.tar.gz) du jeu sur votre PC (cliquer-droit et
  enregistrer la cible...).
- Charger cette archive compressée dans l'émulateur en cliquant sur le bouton
  ![upload](img/03-upload.png){ width=32px }
    - Décompresser l'archive en tapant la commande (vous pouvez utiliser le champ
      *clipboard* pour faire un copier-coller de cette commande):
      ```{.console .rgconsole}
        [alice@weblinux:~]$ tar xvzf 03-escape_game-weblinux.tar.gz
      ```

    - Commencer le jeu en entrant dans le répertoire `escape_game` :
      ```{.console .rgconsole}
        [alice@weblinux:~]$ cd escape_game
      ```

??? tip "Quelques conseils"

    - Lire le fichier README.txt lorsqu'il y en a un dans le répertoire courant.
    - Pour lancer un exécutable `monprogramme` qui se trouve dans le répertoire 
    courant, taper:
      ```{.console .rgconsole}
      [alice@weblinux:~]$ ./monprogramme
      ```

??? sucess "Solution"
    <!---
    Pour obtenir l'animation SVG:
    python -m termtosvg solution-escape_game.svg -t window_frame_js -D 3000
    -->

    Ouvrir l'image dans un nouvel onglet pour visualiser l'animation :
    
    ![Solution de l'escape game](img/solution-escape_game.svg)


