# Architectures matérielles et systèmes d'exploitation

- Leçon : [notions de systèmes d'exploitation](03-notions_OS.md) 
    - TP : [Installation de Linux sur un PC](03a-Installation_Fedora34.md).
- Leçon : [shell et fichiers](03-Fichiers_shell.md) (GNU/Linux)
    - TP : [Découverte et utilisation du shell Linux](03b-Shell_Linux.md).
    - Jeu : [Escape Game](03c-escape_game.md)
- Leçon : [L'ordinateur à transistors](01-transistor.md)
    - TP : [logique combinatoire](01b-logique_combinatoire.md)
    - Construction d'un ordinateur (nand game): [matériel](01c-nandgame-materiel.md) / [logiciel](01d-nandgame-logiciel.md)
    - TP découverte : [Von Neumann](02-architecture_ordinateur.md)
