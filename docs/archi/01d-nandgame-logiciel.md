---
hide:
  - navigation
---

# Construction logicielle d'un ordinateur

(basé sur le jeu [The Nandgame](https://nandgame.com) et le simulateur [https://logic.modulo-info.ch/](https://logic.modulo-info.ch/))

### 1. Bas niveau

#### a. Code machine

Les instructions sont des mots sur 16 bits:

| I15 | I14 | I13 | I12    | I11 | I10 | I9 | I8 | I7 | I6 | I5 | I4 | I3 | I2 | I1 | I0 |
|:---:|:---:|:---:|:------:|:---:|:---:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| `ci`|  -  |  -  |`a/✱a`  |  -  | `u` | `op1`| `op0`| `zx` | `sw` | `a`  | `d`  | `✱a` | `lt` | `eq` | `gt` |

??? tip "Signification des bits"

    `ci` (*computation instruction*): indique s'il s'agit d'une instruction d'ALU (`ci=1`) ou d'une adresse (`ci=0`).

    S'il s'agit d'une instruction d'ALU:

    - `a/✱a` indique si la deuxième entrée de l'ALU provient du registre `A` ou de la RAM (`✱A`).

    - `u` indique le mode et `op1op0` l'opération à effectuer :

        | u | Op1 | Op0 | opération |
        |:-:|:---:|:---:|:---------:|
        | 0 |  0  |  0  |   AND     |
        | 0 |  0  |  1  |   OR      |
        | 0 |  1  |  0  |   XOR     |
        | 0 |  1  |  1  |   NOT     |
        | 1 |  0  |  0  |   D+A     |
        | 1 |  0  |  1  |   D+1     |
        | 1 |  1  |  0  |   D-A     |
        | 1 |  1  |  1  |   D-1     |

    - `zx`: met à 0 le premier mot d'entrée.
    - `sw`: échange les 2 mots d'entrée.
    - `a`, `d` et `✱a` indique la destination du résultat (respectivement `A`, `D` ou la RAM à l'adresse spécifiée par `A`)
    - `lt`, `eq`, `gt` effectue un test sur le résultat (`<0`, `=0` ou `>0`) et indique le résultat sur le bit de sortie `j`.

Exemples:

| Commentaires                               | Instruction binaire `I` |
|:-------------------------------------------|:-----------------------:|
| Mise à 0 du registre `D`                   | `1 0000 000 00 010 000` |
| Mettre `2` dans le registre d'adresse `A`  | `0 0000 000 00 000 010` |
| Ajouter `1` au registre `D`                | `1 0000 101 00 010 000` |
| Saut inconditionnel                        | `1 0000 000 00 000 111` |

??? info "Détails"

    1. Mise à 0 du registre `D`:

        Instruction ALU (`ci=1`), ET logique avec 0 (`u,Op1,Op0=000`, `zx=0`) 

    2. Mettre `2` dans le registre d'adresse `A`: 

        `ci=0`, bits de poids faible indiquant l'adresse `2`

    3. Ajouter `1` au registre `D`

        Instruction ALU (`ci=1`), opération arithmétique d'incrémentation de la première entrée (`u,Op,1Op0=101`, `sw=0`), résultat dans `D` (`a,d,✱a=010`)

    4. Saut inconditionnel

        Instruction ALU (`ci=1`), saut inconditionnel (`lt,eq,gt=1,1,1`) 

#### b. Langage assembleur

Une instruction est du type `destination = opération ; saut`.

(Exemple: `D = D + 1; JNE`)

=== "Destinations"

    Les destinations du résultat de l'ALU sont définies par les bits `a`, `d` et `✱a`:

    | Opcode  | a d ✱a | 
    |:--------|:-------|
    | (blank) | 0 0 0  |
    | A =     | 1 0 0  |
    | D =     | 0 1 0  |
    | ✱a =    | 0 0 1  |
    | A, D =  | 1 1 0  |
    | D, ✱a = | 0 1 1  |
    | A,D,✱a =| 1 1 1  |

=== "Opérations"

    Elle sont définies par les bits `u`, `op1`, `op0`, `zx` et `sw`:

    | Opcode  | u op1 op0 zx sw | 
    |:--------|:----------------|
    | D + A   | 1   0   0  0  0 |
    | D - A   | 1   1   0  0  0 |
    | A - D   | 1   1   0  0  1 |
    | D + 1   | 1   0   1  0  0 |
    | A + 1   | 1   0   1  0  1 |
    | D - 1   | 1   1   1  0  0 |
    | A - 1   | 1   1   1  0  1 |
    |  -D     | 1   1   0  1  1 |
    |  -A     | 1   1   0  1  0 |
    |  -1     | 1   1   1  1  0 |
    |   1     | 1   0   1  1  0 |
    |   D     | 1   0   0  1  1 |
    |   A     | 1   0   0  1  0 |
    |  D&A    | 0   0   0  0  0 |
    |  D\|A   | 0   0   1  0  0 |
    |  ~D     | 0   1   1  0  0 |
    |  ~A     | 0   1   1  0  1 |
    |   0     | 0   0   0  1  0 |

=== "Conditions de saut"

    Les condition sont définies par les bits `lt`, `eq` et `gt`.

    | Opcode  | lt eq gt | 
    |:--------|:---------|
    | (blank) | 0  0  0  |
    | ; JLT   | 1  0  0  |
    | ; JEQ   | 0  1  0  |
    | ; JGT   | 0  0  1  |
    | ; JLE   | 1  1  0  |
    | ; JGE   | 0  1  1  |
    | ; JMP   | 1  1  1  |
    | ; JNE   | 1  0  1  |

#### c. Programme assembleur

Allumer (au moins) 3 fois une lampe (verte) dont l'adresse est 0x7FFF (bit 0: allumer, bit 1: éteindre):

=== "Solution"

    (ici une boucle infinie)

    ```asm
    DEFINE LAMPE 0x7FFF
    LABEL loop
    A = LAMPE
    # Allumer
    *A = 1
    # Éteindre
    *A = *A + 1
    A = loop
    JMP
    ```

#### d. Sortir du labyrinthe

À chaque itération :

- si pas d'obstacle: avancer
- sinon tourner à droite

=== "Solution"

    ```asm
    DEFINE ROBOT 0x7FFF
    DEFINE obstacle_front 0x100 
    DEFINE forward 0b100
    DEFINE right_turn 0x10
    LABEL loop
    ################
    # Détecter obstacle
    A = ROBOT
    D = *A
    A = obstacle_front
    D = D&A
    A = tourner
    D; JNE
    ################
    # Pas d'obstacle: avancer
    A = forward
    D = A
    A = ROBOT
    *A = D
    A = loop
    JMP
    ###################
    # Obstacle: tourner à droite
    LABEL tourner
    A = right_turn
    D = A
    A = ROBOT
    *A = D
    A = loop
    JMP
    ```

#### e. Affichage LCD

L'écran LCD est monochrome:

- Adresses d'écriture: de 0x4000 à 0x6000.
- Chaque adresse (mot de 16 bits) définit 16 pixels consécutifs.

Il faut déssiner un logo quelconque avec la seule contrainte que ses 2 dimensions (largeur et hauteur) soient supérieures à 16 pixels

=== "Solution"

    Logo 31x16 rempli uniquement avec une alternance de colonnes à 1 et 0.

    ```asm
    # Adr RAM pour pixel courant
    DEFINE pixel 0
    A = 0x4000
    D = A
    A = pixel
    *A = D
    DEFINE compteur 1
    A = 16
    D = A
    A = compteur
    *A = D
    LABEL loop
    A = 0x5555
    D = A
    # Récupérer adr pixel
    A = pixel
    A = *A
    # Dessiner à cette adresse et la suivante
    *A = D
    A = A + 1
    *A = D
    # Incrémenter l'adr pixel
    A = pixel
    D = *A
    A = 0x20
    D = D + A
    A = pixel
    *A = D
    # reboucler si compteur > 0
    A = compteur
    D = *A
    D = D - 1
    *A = D
    A = loop
    D; JNE
    ```

#### f. Réseau

Périphérique réseau à l'adresse 0x6001 :

- données sur bit 0
- synchro sur bit 1 (alternativement 1 et 0)

18 bits à recevoir:
- 1 bit de start à `1`
- 16 bit de données (à afficher sur l'écran monochrome).
- 1 bit de stop à  `0`.

=== "Solution (à finir)"

    ```asm
    DEFINE NETWORK 0x6001
    DEFINE MASK_SYNC 0b10
    DEFINE MASK_DATA 1

    INIT_STACK

    # Syncho initiale
    PUSH_VALUE 0

    LABEL loop
    # Lire synchro
    A = NETWORK
    D = *A
    A = MASK_SYNC
    D = D&A
    PUSH_D

    # Si != précédente
    POP_A
    A - D
    A = new_data
    D; JNE
    A = loop
    JMP

    LABEL new_data
    # lire donnée
    A = NETWORK
    D = *A
    A = MASK_DATA
    D = D&A
    ```

### 2. Macros de pile

#### a. INIT_STACK

Initialisation de la pile: on attribue l'adresse 0 de la RAM à `SP` (*Stack Pointer*) et on y écrit la valeur 256 (0x100).

=== "Solution"

    ```asm
    DEFINE SP 0
    A = 0x100
    D = A
    A = SP
    *A = D
    ```

#### b. PUSH_D

Empiler la valeur du registre D: 

1. Écrire `D` à l'adresse contenu dans `SP`.
2. Incrémenter la valeur contenue dans `SP`

=== "Solution"

    ```asm
    DEFINE SP 0
    # Récupérer sommet pile 
    A = SP
    A = *A
    # Écrire D
    *A = D
    # Incrémenter SP
    D = A + 1
    A = SP
    *A = D
    ```

#### c. POP_D

Dépiler et mettre dans le registre D: 

1. Décrémenter SP
2. Dépiler dans D

Il faut décrémenter SP avant de dépiler car cette première opération nécessite aussi le registre D.

=== "Solution"

    ```asm
    DEFINE SP 0
    A = SP
    # Décrémenter SP
    D = *A - 1
    A, *A = D
    # Dépiler
    D = *A
    ```

#### d. POP_A

Dépiler dans le registre A (sans utiliser le registre D)

=== "Solution"

    ```asm
    # Décrémenter SP
    A = SP
    *A = *A - 1
    # Dépiler
    A = *A
    A = *A
    ```

#### e. PUSH_VALUE

En utilisant la macro `PUSH_D`

=== "Solution"

    ```asm
    A = value
    D = A
    PUSH_D
    ```

#### f. ADD

Dépiler deux valeurs, les additionner et empiler le résultat.

=== "Solution"

    ```asm
    POP_D
    POP_A
    D = D + A
    PUSH_D
    ```

#### g. SUB

Dépiler deux valeurs, soustraire la deuxième à la premiere et empiler le résultat.

=== "Solution"

    ```asm
    POP_D
    POP_A
    D = A - D
    PUSH_D
    ```

#### h. NEG

Dépiler une valeur et rempiler son opposé.

=== "Solution"

    ```asm
    POP_D
    D = -D
    PUSH_D
    ```

#### i. AND

Dépiler deux valeurs, faire un ET logique bit à bit et empiler le résultat.

```asm
POP_D
POP_A
D = D&A
PUSH_D
```

#### j. OR

Dépiler deux valeurs, faire un OU logique bit à bit et empiler le résultat.

```asm
POP_D
POP_A
D = D|A
PUSH_D
```