---
hide:
  - navigation
---

# Introduction aux systèmes d'exploitation

![Couche OS](img/03-couche_OS.svg){ width=25% align=right }

Un système d'exploitation (*Operating System*) est un programme qui s'exécute sur un système informatique et qui
gère en permanence les ressources matérielles[^1] des applications.

- Il sert de *chef d'orchestre* pour accéder et partager les ressources.
- Il fournit des services aux applications : stockage, exécution, sécurité&hellip;


[^1]: processeur, mémoire, entrées/sorties (écran, clavier, disque dur, clé USB&hellip;).

## 1. Historique

[![Historique OS](img/03-Histoire_OS.png)](https://medium.com/@scan.pratik/modern-operating-platforms-evolution-history-dbc094002aef){ target=_blank }

??? tip "Dates clés"

    (pas d'OS sur l'ENIAC)

    |      |      |         |
    |------|------|-------- |
    | 1956 | GM-NAA I/O sur calculateur IBM | exécution séquentielle de programmes sur cartes perforées + routines d'accès aux périphériques E/S|
    | 1967 | MutiCS (*Multiplexed Information and Computing Service*) : Bell Labs et GE | plusieurs programmes *en même temps* |
    | 1970-1990 | Unix aux Bell Labs (K. Thomson, D. Ritchie) : assembleur + langage C | multitâche et multi-utilisateurs |
    | 1960-1980 | divers OS différents d'Unix | ordinateurs IBM sur processeur x86 d'Intel. |
    | 1980-1990 | MS-DOS de Microsoft | processeur x86 d'Intel. |
    | 1990- | Windows  | initialement, surcouche graphique sur MS-DOS. | 
    | 1984-2001 | Ordinateurs Apple sur architecture Motorola puis PowerPC | MacOS (graphique). |
    | 1991- | Linus Torvald (étudiant à l'université d'Helsinki) développe Linux | Licence GPL. |
    | 2001- | MacOS | basé sur BSD. |
    | 2007- | iOS | Version MacOS pour téléphone portable |
    | 2008- | Google Android pour téléphone portable | basé sur Linux |

    Standard POSIX (*Portable Operating System Interface*): fonctions de bibliothèques que doivent offrir un OS.


## 2. Composantes d'un système d'exploitation

- Noyau (*kernel*): c&oelig;ur du système (fonctions clés).
- Interpréteur de commande (*shell*): communication avec l'OS,
- Pilotes (*drivers*): gestion des périphériques,
- Un ou plusieurs système(s) de fichiers (*filesystem*): Ext4, NTFS, FAT32&hellip;,
- Bibliothèques (*libraries*): utilisées par les applications,
- &hellip;

## 3. Exemples

Il existe 2 principales familles de systèmes d'exploitation : 

```mermaid
flowchart 
    Multics --> UNIX
    UNIX --> Minix
    Minix --> GNU/Linux
    GNU/Linux --> Android
    UNIX --> Darwin/BSD --> MacOS
    MS-DOS --> Windows

    classDef temp fill:lightgray,stroke:black,stroke-dasharray: 5 5;
    class Multics,Minix temp;

    classDef actuel fill:#f96;
    class Linux,Windows,MacOS,Android actuel;
```

Certains sont des logiciels **libres**[^2] ([GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.fr.html)), d'autres sont 
**propriétaires**[^3] (Windows, MacOS).

!!! warning "Attention"

    libre &ne; gratuit

[^2]: Selon la FSF : possibilité d'exécuter, de copier, de distribuer, d'étudier, 
de modifier et d'améliorer le logiciel (via son code-source). 
[^3]: propriété exclusive de la société qui l'a conçu et qui le commercialise 
uniquement sous la forme exécutable.

*[FSF]: Free Software Foundation

??? info "Pour aller plus loin"

    - [Histoire d'UNIX](https://www.youtube.com/watch?v=Za6vGTLp-wg){ target=_blank }
    - Simulateurs OS Microsoft : [MS-DOS](https://copy.sh/v86/?profile=msdos){ target=_blank }, [Windows 3.1](https://copy.sh/v86/?profile=windows31){ target=_blank }, [Windows 95](https://copy.sh/v86/?profile=windows95){ target=blank }, [Windows 98](https://copy.sh/v86/?profile=windows98){ target=blank }, [Windows 2000](https://copy.sh/v86/?profile=windows2000){ target=blank }

    OS de périphériques réseau :

    - Cisco IOS, IOS XE, IOS XR
    - VRP (Huawei),
    - TiMOS (Alcatel-Lucent),
    - JUNOS (Jupiter),
    - &hellip;

*[Linux]: Linux est le noyau du système d'exploitation GNU/Linux.

## 4. Distributions GNU/Linux

Elles contiennent le système d'exploitation [GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.fr.html) et une sélection d'applications (Gnome, 
LibreOffice, Firefox&hellip;).

|     | Ubuntu | Fedora | Raspian | &hellip;
|:---:|:---:|:---:|:---:|:---:|
| ![TuX](img/03-Tux.png) | [![Ubuntu](img/03-Ubuntu.png){ width=150 }](https://www.ubuntu-fr.org/) | [![Fedora](img/03-Fedora.png){ width=150 }](https://www.fedora-fr.org/) | [![Raspbian](img/03-Raspbian.png){ width=125 }](https://www.raspberrypi.org/software/operating-systems/) |     |   

