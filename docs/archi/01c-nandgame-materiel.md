---
hide:
  - navigation
---

# Construction matérielle d'un ordinateur

(basé sur le jeu [The Nandgame](https://nandgame.com) et le simulateur [https://logic.modulo-info.ch/](https://logic.modulo-info.ch/))

### 1. Opérations logiques

??? tip "Écrire une équation logique depuis une table de vérité"

    2 méthodes de base:

    1. On identifie les cas où la variable de sortie vaut `1`: l’équation logique s’écrit avec `OU` logiques des combinaisons correspondantes.
    2. On identifie les cas où la variable de sortie vaut `0`. L’équation logique s’écrit avec `ET` logiques du complémet des combinaisons correspondantes.

#### a. NAND

On ne dispose que des 2 relais: *default on* et *default off*:

- le relais *default off* se comporte comme une porte `AND`.
- le relais *default on* (avec l'entrée `in` à `1`) se comporte comme une porte `NOT`.

=== "Table de vérité"

    | e1 | e2 | s |
    |:--:|:--:|:-:|
    | 0  | 0  | 1 |
    | 0  | 1  | 1 |
    | 1  | 0  | 1 |
    | 1  | 1  | 0 |

=== "Solution"

    <figure>

    ![Porte NAND](img/01c-nand.png){ width=250 }

    <figcaption>Porte NAND</figcaption>
    </figure>
    

=== "Symbole"

    <div style="width: 100%; height: 120px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [50, 25], id: 10, name: 'e1'},
            out0: {type: 'out', pos: [210, 60], id: 11, name: 's'},
            nand0: {type: 'nand', pos: [140, 60], in: [12, 13], out: 14},
            in1: {type: 'in', pos: [50, 95], id: 15, name: 'e2', val: 1},
            },
            wires: [[10, 12], [15, 13], [14, 11]]
        }
        </script>
    </logic-editor>
    </div>

#### b. NOT

=== "Table de vérité"

    | e | s |
    |:-:|:-:|
    | 0 | 1 | 
    | 1 | 0 | 

=== "Réalisation"

    On ne dispose que de la porte `NAND`:

    <iframe style="width: 100%; height: 130px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand&data=N4NwXAbANA9gDgFwM5mAXygYxgWzjAOwFMDlUBLAgBlQQE84iwByS5qfFAbQFYeoIPALpRyAEzBUoBAIY4mzIswwwArghrB6jFmoTtOYLgCYAnFMEjxYY9LkKkytGiA"></iframe>

=== "Solution"

    <div style="width: 100%; height: 60px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showOnly: ['nand'], showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [35, 30], id: 0, name: 'e'},
            out0: {type: 'out', pos: [270, 30], id: 2, name: 's'},
            nand0: {type: 'nand', pos: [150, 30], in: [1, 3], out: 4},
            },
            wires: [[0, 1], [0, 3], [4, 2]]
        }
        </script>
    </logic-editor>
    </div>


=== "Symbole"

    <div style="width: 100%; height: 80px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            not0: {type: 'not', pos: [115, 40], in: 8, out: 9},
            in0: {type: 'in', pos: [45, 40], id: 10, name: 'e'},
            out0: {type: 'out', pos: [185, 40], id: 11, name: 's'},
            },
            wires: [[10, 8], [9, 11]]
        }
        </script>
    </logic-editor>
    </div>

#### c. AND

=== "Table de vérité"

    | e1 | e2 | s |
    |:--:|:--:|:-:|
    | 0  | 0  | 0 |
    | 0  | 1  | 0 |
    | 1  | 0  | 0 |
    | 1  | 1  | 1 |

=== "Réalisation"

    On ne dispose que des portes `NAND` et `NOT`:

    <iframe style="width: 100%; height: 195px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand,not&data=N4NwXAbANAxg9gWwA5wHYFNUBcDOZgCWqADPlgJ5LpgDkRNUKeA2gJwCsUExAulAQBMwxKKgCGCajXQBGBiDEAbMDIC+-VDLKUp9RnBYcoMgMzs+glaIlT0AJnlKV6uAFcspYBSq03WBkxgzCasIhwWQnbWkrQ4NKoJQA"></iframe>

=== "Solution"

    <div style="width: 100%; height: 145px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showOnly: ['nand', 'not'], showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [55, 35], id: 0, name: 'e1', val: 1},
            in1: {type: 'in', pos: [55, 110], id: 1, name: 'e2', val: 1},
            out0: {type: 'out', pos: [305, 75], id: 2, name: 's'},
            nand0: {type: 'nand', pos: [155, 75], in: [3, 4], out: 5},
            not0: {type: 'not', pos: [235, 75], in: 6, out: 7},
            },
            wires: [[0, 3], [1, 4], [5, 6], [7, 2]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    <div style="width: 100%; height: 120px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [50, 25], id: 10, name: 'e1'},
            out0: {type: 'out', pos: [210, 60], id: 11, name: 's'},
            nand0: {type: 'and', pos: [140, 60], in: [12, 13], out: 14},
            in1: {type: 'in', pos: [50, 95], id: 15, name: 'e2', val: 1},
            },
            wires: [[10, 12], [15, 13], [14, 11]]
        }
        </script>
    </logic-editor>
    </div>


#### d. OR

=== "Table de vérité"

    | e1 | e2 | s |
    |:--:|:--:|:-:|
    | 0  | 0  | 0 |
    | 0  | 1  | 1 |
    | 1  | 0  | 1 |
    | 1  | 1  | 1 |

=== "Réalisation"

    On ne dispose que des portes `NAND`, `NOT` et `AND`:

    <iframe style="width: 100%; height: 195px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand,not,and&data=N4NwXAbANAxg9gWwA5wHYFNUBcDOZgCWqADPlgJ5LpgDkRNUKeA2gJwCsUExAulAQBMwxKKgCGCajXQBGBiDEAbMDIC+-VDLKUp9RnBYcoMgMzs+glaIlT0AJnlKV6uAFcspYBSq03WBkxgzCasIhwWQnbWkrQ4NKoJQA"></iframe>

    ??? tip "Aide"

        On identifie les cas où la variable de sortie vaut `0`:

        `s = not(not(e1) and not(e2)) = not(e1) nand not(e2)`

=== "Solution"

    <div style="width: 100%; height: 155px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showOnly: ['nand', 'not', 'and'], showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [50, 40], id: 0, name: 'e1', val: 1},
            in1: {type: 'in', pos: [50, 115], id: 1, name: 'e2', val: 1},
            out0: {type: 'out', pos: [295, 75], id: 2, name: 's'},
            not0: {type: 'not', pos: [120, 40], in: 3, out: 4},
            not1: {type: 'not', pos: [120, 115], in: 5, out: 6},
            nand0: {type: 'nand', pos: [225, 75], in: [10, 11], out: 12},
            },
            wires: [[0, 3], [1, 5], [4, 10], [12, 2], [6, 11]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    <div style="width: 100%; height: 120px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [50, 25], id: 10, name: 'e1'},
            out0: {type: 'out', pos: [210, 60], id: 11, name: 's'},
            nand0: {type: 'or', pos: [140, 60], in: [12, 13], out: 14},
            in1: {type: 'in', pos: [50, 95], id: 15, name: 'e2', val: 1},
            },
            wires: [[10, 12], [15, 13], [14, 11]]
        }
        </script>
    </logic-editor>
    </div>

#### d. XOR

=== "Table de vérité"

    | e1 | e2 | s |
    |:--:|:--:|:-:|
    | 0  | 0  | 0 |
    | 0  | 1  | 1 |
    | 1  | 0  | 1 |
    | 1  | 1  | 0 |

=== "Réalisation"

    On ne dispose que des portes `NAND`, `NOT`, `AND` et `OR`:

    <iframe style="width: 100%; height: 240px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand,not,and,or&data=N4NwXAbANAxg9gWwA5wHYFNUBcDOZgCWqADPlgJ5LpgDkRNUKeA2gJwCsUExAulAQBMwxKKgCGCajXQBGBiDEAbMDIC+-VDLKUp9RnBYcoMgMzs+glaIlT0AJnlKV6uAFcspYBSq03WBkxgzCasIhwWQnbWkrQ4NKoJQA"></iframe>

    ??? tip "Aide"

        On identifie les cas où la variable de sortie vaut `0`:

        `s = not(not(e1) and not(e2)) and not(e1 and e2) = (e1 or e2) et (e1 nand e2)`

=== "Solution"

    <div style="width: 100%; height: 130px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showOnly: ['nand', 'not', 'and', 'or'], showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [55, 25], id: 0, name: 'e1', val: 1},
            in1: {type: 'in', pos: [55, 105], id: 1, name: 'e2', val: 1},
            out0: {type: 'out', pos: [340, 65], id: 2, name: 's'},
            or0: {type: 'or', pos: [160, 35], in: [3, 4], out: 5},
            nand0: {type: 'nand', pos: [165, 95], in: [6, 7], out: 8},
            and0: {type: 'and', pos: [270, 65], in: [9, 10], out: 11},
            },
            wires: [[0, 3], [1, 4], [0, 6], [1, 7], [5, 9], [8, 10], [11, 2]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    <div style="width: 100%; height: 120px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [50, 25], id: 10, name: 'e1'},
            out0: {type: 'out', pos: [210, 60], id: 11, name: 's'},
            nand0: {type: 'xor', pos: [140, 60], in: [12, 13], out: 14},
            in1: {type: 'in', pos: [50, 95], id: 15, name: 'e2', val: 1},
            },
            wires: [[10, 12], [15, 13], [14, 11]]
        }
        </script>
    </logic-editor>
    </div>

### 2. Opérations arithémiques

#### a. Demi-additionneur

On additionne les bits `a` et `b` et on récupère un résultat sur 2 bits `c` (poids fort ou *retenue*) et `s` (poids faible).

=== "Équation logique"

    | a | b | c | s |
    |:-:|:-:|:-:|:-:|
    | 0 | 0 | 0 | 0 |
    | 0 | 1 | 0 | 1 |
    | 1 | 0 | 0 | 1 |
    | 1 | 1 | 1 | 0 |

=== "Réalisation"

    On ne dispose que des portes `NAND`, `NOT`, `AND`, `OR` et `XOR`:

    <iframe style="width: 100%; height: 300px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand,not,and,or,xor&data=N4NwXAbANA9gDgFwM5mEgFjA7gcQIYICmAKgJ5yEoIBOAroQL5QDGMAtnDAHaFfKoBLLgAZUCcoTAByIVKicUAbQCMAZmFQAHAFYAulAEATMBq542kqXjkg8AGzDKmQ5WInTZ8mErUbl2vQNjZSgzC2kAIykmGFoEUWBxCmlYhDkFMEVVbQ0dfSMwACZQ80skaNg410T3KVT070zsvwD841US8KlmaIYGIA"></iframe>

    ??? tip "Aide"

        On reconnait les tables de véritué du `ET` pour `h` et du `OU-exclusif` pour `l`.

=== "Solution"

    <div style="width: 100%; height: 150px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showOnly: ['nand', 'not', 'and', 'or', 'xor'], showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [45, 30], id: 0, name: 'a', val: 1},
            in1: {type: 'in', pos: [45, 120], id: 1, name: 'b'},
            out0: {type: 'out', pos: [265, 40], id: 2, name: 's'},
            out1: {type: 'out', pos: [265, 110], id: 3, name: 'c'},
            and0: {type: 'and', pos: [170, 110], in: [4, 5], out: 6},
            xor0: {type: 'xor', pos: [180, 40], in: [7, 8], out: 9},
            },
            wires: [[0, 4], [1, 5], [6, 3], [1, 8], [0, 7], [9, 2]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    <div style="width: 100%; height: 100px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [50, 30], id: 0, name: 'a', val: 1},
            in1: {type: 'in', pos: [50, 70], id: 1, name: 'b'},
            out0: {type: 'out', pos: [255, 30], id: 2, name: 's'},
            out1: {type: 'out', pos: [255, 70], id: 3, name: 'c'},
            hadder0: {type: 'halfadder', pos: [150, 50], in: [4, 5], out: [6, 7]},
            },
            wires: [[0, 4], [1, 5], [7, 3], [6, 2]]
        }
        </script>
    </logic-editor>
    </div>

#### b. Additionneur complet

On additionne les bits `a`, `b` et une retenue entrante `cin` et on récupère un résultat sur 2 bits `cout` (poids fort ou *retenue sortante*) et `s` (poids faible).

=== "Équation logique"

    | a | b | cin | cout | s |
    |:-:|:-:|:---:|:----:|:-:|
    | 0 | 0 | 0   |   0  | 0 |
    | 0 | 0 | 1   |   0  | 1 |
    | 0 | 1 | 0   |   0  | 1 |
    | 0 | 1 | 1   |   1  | 0 |
    | 1 | 0 | 0   |   0  | 1 |
    | 1 | 0 | 1   |   1  | 0 |
    | 1 | 1 | 0   |   1  | 0 |
    | 1 | 1 | 1   |   1  | 1 |

=== "Réalisation"

    On ne dispose que des portes `NAND`, `NOT`, `AND`, `OR`, `XOR` et *demi-additionneur*:

    <iframe style="width: 100%; height: 440px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand,not,and,or,xor,halfadder&data=N4NwXAbANA9gDgFwM5mEgFjA7gcQIYICmAKgJ5yEoIBOAroQL5QDGMAtnDAHaFfKoBLLgAZUCcoTAByIVKicUAbQCswqAEYAnMoC6UAQBMwarnjaSpeOSDwAbMOqZD1YidNnyYS1RoBMwvUMHKFNzaQAjKSYYWgRRYHEKaRiEOQUwRQBmAHY1dQgA-SNfELMLJCjYWJcEtykUtK8M3wKoFsKYagFeBGkKorBM0rCpVljKoV9XJJkuRqV1ABY1Rd1YLp6+uSDF4YtmDxt7RwYGIA"></iframe>

    ??? tip "Aide"

        On demi-additionne `a` et `b` puis:

        - le poids faible du résultat avec `cin` pour obtenir `s`.
        - les retenues des 2 demi-additions précédentes pour obtenir `cout`.

=== "Solution"

    <div style="width: 100%; height: 300px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [50, 190], id: 0, name: 'a'},
            in1: {type: 'in', pos: [50, 110], id: 1, name: 'b', val: 1},
            out0: {type: 'out', pos: [445, 110], id: 2, name: 's'},
            out1: {type: 'out', pos: [390, 255], orient: 's', id: 3, name: 'cout'},
            in2: {type: 'in', pos: [165, 45], orient: 's', id: 4, name: 'cin'},
            hadder0: {type: 'halfadder', pos: [140, 170], in: [5, 6], out: [7, 8]},
            hadder1: {type: 'halfadder', pos: [245, 130], in: [9, 10], out: [11, 12]},
            hadder2: {type: 'halfadder', pos: [325, 170], in: [13, 14], out: [15, 16]},
            },
            wires: [[0, 6], [1, 5], [11, 2], [15, 3], [12, 13], [8, 14], [4, 9], [7, 10]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    <div style="width: 100%; height: 250px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [45, 155], id: 0, name: 'a'},
            in1: {type: 'in', pos: [45, 95], id: 1, name: 'b', val: 1},
            out0: {type: 'out', pos: [210, 125], id: 2, name: 's'},
            out1: {type: 'out', pos: [140, 205], orient: 's', id: 3, name: 'cout'},
            in2: {type: 'in', pos: [140, 45], orient: 's', id: 4, name: 'cin'},
            adder0: {type: 'adder', pos: [140, 125], orient: 'n', in: '27-29', out: [30, 31]},
            },
            wires: [[1, 28], [0, 27], [4, 29], [31, 3], [30, 2]]
        }
        </script>
    </logic-editor>
    </div>

#### c. Additionneur multi-bits

*(exemple avec des mots de 2 bits)*

On additionne les mots `A=a1a0`, `B=b1b0` et une retenue entrante `cin` et on récupère un mot de sortie `S=s1s0`sur 2 bits et une retenue `cout`.

=== "Principe"

    |    |   |    |    |
    |:--:|--:|:--:|:--:|
    | (retenues) | cout | ? | cin |
    |   |   | a1 | a0 |
    |   | + | b1 | b0 |
    |   | = | s1 | s0 |

=== "Réalisation"

    !!! warning "Attention"

        Les bits de poids faibles se trouvent en haut des mots `A`, `B` et `S`.

    <iframe style="width: 100%; height: 510px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand,not,and,or,xor,halfadder,adder&data=N4NwXAbANA9gDgFwM5mEgFjA7gcQIYICmAKgJ5yEoIBOAroQL5QDGMAtnDAHaFfKoBLLgAZUCcoTAByIVKicUAbQAsAVigBGDcIC6UAQBMwi1etUQ9XPG0lSAgnIBGA-gCYoIPABtp2qUyENMQlpWXkYJTUoV209Q2NVAA4oVQBOS2tbACEnFxR3Tx8pYQ1-fS5XYIpQrjkFYw0IYSg1PRhqAV4EaSQ5eKaoKxtpZjDCsA0mGFoEUWBxaqlphDqI4wBmYWaNUzijRQh3CHW9ZzdBzOkAZTLloPmQpZnVpUbmzdU2jq6evqMIZQXYZSVjPBjgoA"></iframe>

    ??? tip "Aide"

        Il faut: 
        
        - additionner séparément les bits de poids faible et les bits de poids fort
        - les cascader via les retenues.

=== "Solution"

    <div style="width: 100%; height: 350px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [45, 110], id: [55, 56], name: 'A', bits: 2, val: '10'},
            in1: {type: 'in', pos: [45, 210], id: [58, 59], name: 'B', bits: 2, val: '01'},
            in2: {type: 'in', pos: [160, 45], orient: 's', id: 60, name: 'cin', val: 1},
            out0: {type: 'out', pos: [265, 160], id: [62, 63], bits: 2, name: 'S'},
            out1: {type: 'out', pos: [160, 305], orient: 's', id: 64, name: 'cout'},
            adder0: {type: 'adder', pos: [160, 125], orient: 'n', in: '69-71', out: [72, 73]},
            adder1: {type: 'adder', pos: [160, 225], orient: 'n', in: '74-76', out: [77, 78]},
            },
            wires: [[73, 76], [55, 69], [56, 74], [58, 70], [59, 75], [60, 71], [78, 64], [72, 62], [77, 63]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    <div style="width: 100%; height: 380px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            adder1: {type: 'adder-array', pos: [180, 180], in: '46-50', out: '51-53', bits: 2},
            in0: {type: 'in', pos: [70, 130], id: [55, 56], name: 'A', bits: 2},
            in1: {type: 'in', pos: [70, 230], id: [58, 59], name: 'B', bits: 2, val: '01'},
            in2: {type: 'in', pos: [180, 45], orient: 's', id: 60, name: 'cin'},
            out0: {type: 'out', pos: [290, 180], id: [62, 63], bits: 2, name: 'S'},
            out1: {type: 'out', pos: [180, 335], orient: 's', id: 64, name: 'cout'},
            },
            wires: [[60, 50], [51, 62], [52, 63], [55, 46], [56, 47], [58, 48], [59, 49], [53, 64]]
        }
        </script>
    </logic-editor>
    </div>

#### d. Incrément

*(exemple avec des mots de 16 bits)*

On incrémente la valeur du mot `A=a15a14...a0` et on récupère un mot de sortie `S=s15s14...s0` (on revient à `0` après la valeur maximale).

=== "Réalisation"

    !!! warning "Attention"

        Les bits de poids faibles se trouvent en haut des mots `A` et `S`.

    <iframe style="width: 100%; height: 510px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=adder-array&data=N4NwXAbANA9gDgFwM5mEgFjA7gcQIYICmAKgJ5yEoIBOAroQL5QDGMAtnDAHaFfKoBLLgGZUCcoTAByIVKicUAbQAsABigBGVQFYAulAEATaQE4A7AFoNGgExyueNpKkBBOQCMB-DdBB4ANtKqwSGhodZSTDC0CKpiEtLRCHIKYIo2ZtqaJnoGxlLWlhrCdlCe3tAOTtIAypEGXHHA4hTSsvIwSplQwtq5MNQCvAjSXHJGYDY2wlBVzgECbHJ+gRoM60A"></iframe>

=== "Solution"

    <div style="width: 100%; height: 390px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in3: {type: 'in', pos: [40, 105], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
            out0: {type: 'out', pos: [275, 195], id: '117-132', bits: 16, name: 'S'},
            adder0: {type: 'adder-array', pos: [165, 195], in: '147-179', out: '180-196', bits: 16},
            in0: {type: 'in', pos: [105, 210], id: 223, val: 1, isConstant: true},
            },
            wires: [[97, 147], [98, 148], [99, 149], [100, 150], [101, 151], [102, 152], [103, 153], [104, 154], [105, 155], [106, 156], [107, 157], [108, 158], [109, 159], [110, 160], [111, 161], [112, 162], [180, 117], [181, 118], [182, 119], [183, 120], [184, 121], [185, 122], [186, 123], [187, 124], [188, 125], [189, 126], [190, 127], [191, 128], [192, 129], [193, 130], [194, 131], [195, 132], [223, 163]]
        }
        </script>
    </logic-editor>
    </div>

#### d. Soustracteur multi-bits

*(exemple avec des mots de 16 bits)*

On effectue la soustraction `A-B` avec `A=a15...a0`, `B=b15...b0` et on récupère un mot de sortie `S=s15...s0`.

=== "Réalisation"

    !!! warning "Attention"

        - Les bits de poids faibles se trouvent en haut des mots `A`, `B` et `S`.
        - Le composant `Opp16` donne l'opposé d'un mot de 16 bits (en complément à 2).

    <iframe style="width: 100%; height: 450px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=adder-array&data=N4NwXAbANA9gDgFwM5mEgFjA7gcQIYICmAKgJ5yEoIBOAroQL5QAmhAZigNrACWzYAcnhwAjBAFQAxnkQ8YAO0EB5OKPFSe1SbR4JUkmAFs4CwvOSoe8gAyoE5QoKsSTXEQBZrUAOwAmAKwAurDUPGZ6AvISfGC+vgDMUPJ4ho4CAJI2EiB4ADZgIlA8SADCCkgIeOZgNPRMkvIwCLbA9hSCDU0AtHjU1HikLjBu-v5Q8QAc1sFWgt7xXRMAnBIwtHqcAkvWXUvqAiIiC4f7R8fx3gLBAEa6KGJMeMys1CJ2DoJPLz19A0NcvgmY3c3mmRUUB38vi6Igm7lW60EsP8XV81hEEluFgesHWb1aHyE63+YE48QgwNBM34m3cMPiEgEEDpEBWNzuBWgyVSgi6ACEBEwrL53u0BM4oK5Sf4vJMwTEBGjvKi4hJuWkBVAsfdoDl8gJrIajcaTRiGEwsJpKKTOMioO5graJtAglAnd4oBBHbCJj5vcsoBNvdsoEtg4URGDbUtfFBDsHEiJfMH3HH4sGxszg9AKcGPRAvW6REtfRBvMGlp6g260V5WY6lT50zXrL7vA6W5XvK7OL5Iz5C73Dj5yzWkz5q0PEt4w2PU1MGyIxhMRIvoBNk2OPRNm0PfXDF5WgQ20YHB3FChNR724oHJ3FEstHSGl5vOEsI-5dzG4-4O+-E1GZ9UyXQcljGJdrz2X9J0OQDZ1tDw4wgKNDggiBVyLMRkLfM5kN3I4QKzIt4nQnsjmgMRByOD0xGvB9kKDQJzWCAxjFMcwUF4eR4lFNIJSlTgZTjQ1qUEJZlUOXw1RSNIAEFMQ5bC9UEE01KNQ5BVxZo+MENYEBJIT3Ag48in4A4REk+JpK1JSuVkwQAGUtKsFo2n4qJJWGaVZUjMTIR2MR-BknkBE1bVOSgFSDXU9SzSkIw4DRXSBG0CojC6YQxEM3wQ1rGYIWZLoZwRCIpl2YLzQYIA"></iframe>

=== "Solution"

    <div style="width: 100%; height: 525px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {name: 'Soustracteur', showGateTypes: true},
            defs: [
            {id: 'opp16', caption: 'Opp16', circuit: {
                components: {
                in0: {type: 'in', pos: [140, 725], orient: 'n', id: 223, name: 'In0', val: 1, isConstant: true},
                cnot0: {type: 'cnot-array', pos: [155, 380], in: '73-89', out: ['90-96', '113-116', '133-137'], bits: 16},
                adder1: {type: 'adder-array', pos: [285, 470], in: '152-184', out: '185-201', bits: 16},
                out1: {type: 'out', pos: [365, 470], id: ['4-13', '64-69'], bits: 16, name: '-B'},
                in2: {type: 'in', pos: [50, 380], id: '207-222', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[185, 4], [186, 5], [187, 6], [188, 7], [189, 8], [190, 9], [191, 10], [192, 11], [193, 12], [194, 13], [195, 64], [196, 65], [197, 66], [198, 67], [199, 68], [200, 69], [207, 73], [208, 74], [209, 75], [210, 76], [211, 77], [212, 78], [213, 79], [214, 80], [215, 81], [216, 82], [217, 83], [218, 84], [219, 85], [220, 86], [221, 87], [222, 88], [223, 89], [90, 152], [91, 153], [92, 154], [93, 155], [94, 156], [95, 157], [96, 158], [113, 159], [114, 160], [115, 161], [116, 162], [133, 163], [134, 164], [135, 165], [136, 166], [137, 167], [223, 168]]
            }}
            ],
            components: {
            in3: {type: 'in', pos: [50, 100], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
            out0: {type: 'out', pos: [580, 185], id: '117-132', bits: 16, name: 'S'},
            in0: {type: 'in', pos: [50, 350], id: '150-165', name: 'B', bits: 16, val: '0000000000000001'},
            comp20: {type: 'custom-opp16', pos: [255, 350], in: '64-79', out: '80-95'},
            adder0: {type: 'adder-array', pos: [410, 185], in: '14-46', out: '47-63', bits: 16},
            disp0: {type: 'display', pos: [130, 100], id: [116, '133-147'], bits: 16},
            disp1: {type: 'display', pos: [130, 350], id: '172-187', bits: 16},
            disp2: {type: 'display', pos: [500, 185], id: '188-203', bits: 16},
            },
            wires: [[150, 64], [151, 65], [152, 66], [153, 67], [154, 68], [155, 69], [156, 70], [157, 71], [158, 72], [159, 73], [160, 74], [161, 75], [162, 76], [163, 77], [164, 78], [165, 79], [80, 30], [81, 31], [82, 32], [83, 33], [84, 34], [85, 35], [86, 36], [87, 37], [88, 38], [89, 39], [90, 40], [91, 41], [92, 42], [93, 43], [94, 44], [95, 45], [97, 14], [98, 15], [99, 16], [100, 17], [101, 18], [102, 19], [103, 20], [104, 21], [105, 22], [106, 23], [107, 24], [108, 25], [109, 26], [110, 27], [111, 28], [112, 29], [47, 117], [48, 118], [49, 119], [50, 120], [51, 121], [52, 122], [53, 123], [54, 124], [55, 125], [56, 126], [57, 127], [58, 128], [59, 129], [60, 130], [61, 131], [62, 132], [47, 188], [48, 189], [49, 190], [50, 191], [51, 192], [52, 193], [53, 194], [54, 195], [55, 196], [56, 197], [57, 198], [58, 199], [59, 200], [60, 201], [61, 202], [62, 203], [97, 116], [98, 133], [99, 134], [100, 135], [101, 136], [102, 137], [103, 138], [104, 139], [105, 140], [106, 141], [107, 142], [108, 143], [109, 144], [110, 145], [111, 146], [112, 147], [150, 172], [151, 173], [152, 174], [153, 175], [154, 176], [155, 177], [156, 178], [157, 179], [158, 180], [159, 181], [160, 182], [161, 183], [162, 184], [163, 185], [164, 186], [165, 187]]
        }
        </script>
    </logic-editor>
    </div>

#### e. Test de nullité

Teste si le mot d'entrée (ici de 4 bits) est nul.

=== "Réalisation"

    <iframe style="width: 100%; height: 300px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand,not,and,or,xor&data=N4NwXAbANA9gDgFwM5mEgFjA7gcQIYICmAKgJ5yEoIBOAroQL5QAmhAZigNrACWzYAcnhwAjBAFQAxnkQ8YAO0EB5OKPFSe1SbR4JUkmAFs4CwvOSoe8gAyoE5QoKsSTXEQBZrUAOwAmAKwAurDUPGZ6AvISfGC+vgDMUPJ4ho4CAJI2EiB4ADZgIlA8SADCCkgIeOZgNPRMkvIwCLbA9hSCDU0AtHjU1HikLjBu-v5Q8QAc1sFWgt7xXRMAnBIwtHqcAkvWXUvqAiIiC4f7R8fx3gLBAEa6KGJMeMys1CJ2DoJPLz19A0NcvgmY3c3mmRUUB38vi6Igm7lW60EsP8XV81hEEluFgesHWb1aHyE63+YE48QgwNBM34m3cMPiEgEEDpEBWNzuBWgyVSgi6ACEBEwrL53u0BM4oK5Sf4vJMwTEBGjvKi4hJuWkBVAsfdoDl8gJrIajcaTRiGEwsJpKKTOMioO5graJtAglAnd4oBBHbCJj5vcsoBNvdsoEtg4URGDbUtfFBDsHEiJfMH3HH4sGxszg9AKcGPRAvW6REtfRBvMGlp6g260V5WY6lT50zXrL7vA6W5XvK7OL5Iz5C73Dj5yzWkz5q0PEt4w2PU1MGyIxhMRIvoBNk2OPRNm0PfXDF5WgQ20YHB3FChNR724oHJ3FEstHSGl5vOEsI-5dzG4-4O+-E1GZ9UyXQcljGJdrz2X9J0OQDZ1tDw4wgKNDggiBVyLMRkLfM5kN3I4QKzIt4nQnsjmgMRByOD0xGvB9kKDQJzWCAxjFMcwUF4eR4lFNIJSlTgZR8V0FR2BkkhSNIAEFMQ5dwmDWZo+MEJSSU4dxhIXIp+FTdVBAAZUFc0gA"></iframe>

=== "Solution"

    <div style="width: 100%; height: 160px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in3: {type: 'in', pos: [50, 75], id: '0-3', name: 'A', bits: 4},
            out0: {type: 'out', pos: [450, 85], id: 4, name: 'S'},
            or0: {type: 'or', pos: [155, 45], in: [5, 6], out: 7},
            or1: {type: 'or', pos: [160, 115], in: [8, 9], out: 10},
            or2: {type: 'or', pos: [280, 85], in: [11, 12], out: 13},
            not0: {type: 'not', pos: [360, 85], in: 14, out: 15},
            },
            wires: [[13, 14], [7, 11], [0, 5], [1, 6], [2, 8], [3, 9], [10, 12], [15, 4]]
        }
        </script>
    </logic-editor>
    </div>

#### f. Test de négativité

Teste si le mot d'entrée (ici de 16 bits) est nul.

=== "Réalisation"

    !!! warning "Attention"

        Le bits de poids faible se trouve en haut du mot `A`.
        
    <iframe style="width: 100%; height: 410px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand,not,and,or,xor,in&data=N4NwXAbANA9gDgFwM5mEgFjA7gcQIYICmAKgJ5yEoIBOAroQL5QAmhAZigNrACWzYAcnhwAjBAFQAxnkQ8YAO0EB5OKPFSe1SbR4JUkmAFs4CwvOSoe8gAyoE5QoKsSTXEQBZrUAOwAmAKwAurDUPGZ6AvISfGC+vgDMUPJ4ho4CAJI2EiB4ADZgIlA8SADCCkgIeOZgNPRMkvIwCLbA9hSCDU0AtHjU1HikLjBu-v5Q8QAc1sFWgt7xXRMAnBIwtHqcAkvWXUvqAiIiC4f7R8fx3gLBAEa6KGJMeMys1CJ2DoJPLz19A0NcvgmY3c3mmRUUB38vi6Igm7lW60EsP8XV81hEEluFgesHWb1aHyE63+YE48QgwNBM34m3cMPiEgEEDpEBWNzuBWgyVSgi6ACEBEwrL53u0BM4oK5Sf4vJMwTEBGjvKi4hJuWkBVAsfdoDl8gJrIajcaTRiGEwsJpKKTOMioO5graJtAglAnd4oBBHbCJj5vcsoBNvdsoEtg4URGDbUtfFBDsHEiJfMH3HH4sGxszg9AKcGPRAvW6REtfRBvMGlp6g260V5WY6lT50zXrL7vA6W5XvK7OL5Iz5C73Dj5yzWkz5q0PEt4w2PU1MGyIxhMRIvoBNk2OPRNm0PfXDF5WgQ20YHB3FChNR724oHJ3FEstHSGl5vOEsI-5dzG4-4O+-E1GZ9UyXQcljGJdrz2X9J0OQDZ1tDw4wgKNDggiBVyLMRkLfM5kN3I4QKzIt4nQnsjmgMRByOD0xGvB9kKDQJzWCAxjFMcwUGANZmlFNIeJJMkZVDV0YlTdVBAAZUFcF4j4pwoklYZpQg6xRP4RU6XiFYkhSNIAEFMQ5B5zSAA"></iframe>

    ??? tip "Aide"

        En complément à 2, les mots négatifs ont leur bit de poids fort à `1`.


=== "Solution"

    <div style="width: 100%; height: 210px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            out0: {type: 'out', pos: [350, 95], id: 4, name: 'S'},
            in3: {type: 'in', pos: [55, 105], id: '24-39', name: 'A', bits: 16},
            },
            wires: [[39, 4]]
        }
        </script>
    </logic-editor>
    </div>

### 3. Sélecteurs

#### a. Multiplexeur

Un bit `s` permet de sélectionner parmi 2 bits d'entrée (`D=d1d0`):

- si `s=0`, on a `o = d0`.
- si `s=1`, on a `o = d1`.

=== "Équation logique"

    | s | d1 | d0 | o |
    |:-:|:--:|:--:|:-:|
    | 0 | 0  | 0  | 0 |
    | 0 | 0  | 1  | 1 |
    | 0 | 1  | 0  | 0 |
    | 0 | 1  | 1  | 1 |
    | 1 | 0  | 0  | 0 |
    | 1 | 0  | 1  | 0 |
    | 1 | 1  | 0  | 1 |
    | 1 | 1  | 1  | 1 |

=== "Réalisation"

    <iframe style="width: 100%; height: 350px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand,not,and,or,xor&data=N4NwXAbANAxg9gWwA5wHYFNUBcDOZgCWqADPlgJ5LpgDkRNUKeA2gIzECsUHxAulHABOBTFlo4GBACZhWADiioAhgmo0JAXyhFWZSmvqM4LHlABMAZj7aZzM1zMR+y1bQAiDAEYFcYM1BAlABtaYlYaLTgAVyxSYAoqWmisBiYwZgsAdmJzABYOfmk-TMUVNTgIjQ0gA"></iframe>

=== "Solution"

    <div style="width: 100%; height: 342px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [105, 50], orient: 's', id: 18, name: 's'},
            in1: {type: 'in', pos: [50, 230], id: [25, 26], name: 'D', bits: 2, val: '01'},
            out0: {type: 'out', pos: [370, 245], id: 27, name: 'o'},
            and0: {type: 'and', pos: [195, 205], in: [28, 29], out: 30},
            not0: {type: 'not', pos: [105, 120], orient: 's', in: 31, out: 32},
            and1: {type: 'and', pos: [200, 285], in: [33, 34], out: 35},
            or0: {type: 'or', pos: [300, 245], in: [36, 37], out: 38},
            },
            wires: [[18, 31], [18, 33, {via: [[160, 95, 's']]}], [26, 34], [32, 28], [25, 29], [30, 36], [35, 37], [38, 27]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    <div style="width: 100%; height: 172px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            mux0: {type: 'mux', pos: [90, 110], in: '14-16', out: 17, from: 2, to: 1},
            in0: {type: 'in', pos: [90, 50], orient: 's', id: 18, name: 's', val: 1},
            in1: {type: 'in', pos: [50, 110], id: [25, 26], name: 'D', bits: 2},
            out0: {type: 'out', pos: [140, 110], id: 27, name: 'o'},
            },
            wires: [[18, 16], [25, 14], [26, 15], [17, 27]]
        }
        </script>
    </logic-editor>
    </div>

#### b. Démultiplexeur

Un bit `s` permet d'affecter la valeur d'un bit `d` à une des 2 sorties (`O=o1o0`):

- si `s=0`, on a `o0 = d`.
- si `s=1`, on a `o1 = d`.

=== "Équation logique"

    | s | d | o1 | o0 |
    |:-:|:-:|:--:|:--:|
    | 0 | 0  | 0 | 0  |
    | 0 | 1  | 0 | 1  |
    | 1 | 0  | 0 | 0  |
    | 1 | 1  | 1 | 0  |

=== "Réalisation"

    <iframe style="width: 100%; height: 295px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=nand,not,and,or,xor&data=N4NwXAbANA9gDgFwM5mEgFjA7gcQIYICmAKgJ5yEoIBOAroQL5QDGMAtnDAHaFfKoBLLgAZUCcoTAByIVKicUAbQCMAFmFRVAVgC6sagN4JpSOQIAmYZQA4oXPG0lTTUEHgA2VpkOViJ02XkYJS0tKAAmLWE9CzAAZgBOOwcnczk3T2UmGFoEUWBxCmkchDkFMEU48I1wuN0oWMV1TWU9ACMBfnDkx2kAeSkGIaA"></iframe>
    
=== "Solution"

    <div style="width: 100%; height: 352px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [140, 45], orient: 's', id: 18, name: 's', val: 1},
            in1: {type: 'in', pos: [55, 250], id: 39, name: 'd', val: 1},
            out0: {type: 'out', pos: [320, 235], id: [40, 41], bits: 2, name: 'O'},
            and0: {type: 'and', pos: [200, 210], in: [42, 43], out: 44},
            and1: {type: 'and', pos: [195, 300], in: [45, 46], out: 47},
            not0: {type: 'not', pos: [110, 145], orient: 's', in: 48, out: 49},
            },
            wires: [[49, 42], [39, 43], [44, 40], [18, 48], [18, 45], [39, 46], [47, 41]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    <div style="width: 100%; height: 172px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [115, 40], orient: 's', id: 18, name: 's', val: 1},
            in1: {type: 'in', pos: [45, 125], id: 39, name: 'd', val: 1},
            out0: {type: 'out', pos: [175, 125], id: [40, 41], bits: 2, name: 'O'},
            demux0: {type: 'demux', pos: [115, 125], in: [14, 15], out: [16, 17], from: 1, to: 2},
            },
            wires: [[16, 40], [17, 41], [18, 15], [39, 14]]
        }
        </script>
    </logic-editor>
    </div>

### 4. Unité Arithmétique et Logique

#### a. Unité Logique (UL)

Les deux bits `Op1` et `Op0` permettent de sélectionner parmi les 4 opérations logiques à effectuer sur les mots de 16 bits `A` et `B`:

| Op1 | Op0 | opération |
|:---:|:---:|:---------:|
|  0  |  0  |   AND     |
|  0  |  1  |   OR      |
|  1  |  0  |   XOR     |
|  1  |  1  |   NOT     |

=== "Réalisation"

    !!! info

        La porte `Inv16` permet d'inverser (`NOT`) un mot de 16 bits.

    <iframe style="width: 100%; height: 1025px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=mux,and-array,or-array,xor-array,pass-4&data=N4NwXAbANA9gDgFwM5mEgFjA7gcQIYICmAKgJ5yEoIBOAroVAF4wwC2YA7AAwC+UAJoQBmKANrAAlvzAByCQDsQARggyoAYzyIJMebICSilWvUTq62hISp1bOLsLzkqeTARdUCcoVmuEa+zEODgBWKCUATi4AXSgFMCUAJi5YWmskpT4-JU9vXzcAmCCAFjDE5Nj4pMTU9MSAZiy3RNyKfP8oQLBRYLLSyr0k4tqExJCmhHrWnxk-QpKy7gHR6Bg00Y4J4un2+e7eqHquGLjBxIAOEaSIiaUQndmCzqL90sOx5buUtfSQzKg-PdgF42o8Ol0etxDiEQp8jld6v8-BAHnNniUUvVzicqvUaj8EvVGgC3BxUU8IcEUsUlDjBvVhgSlPVxiSEOdyeCXj1itTeXDVutmZs2RFOXtIWFiktToTLkz6jc2bTxejXikQsdPryrjTbjlgXkwRKqVAQkdtfihcVidkWobQWjKTqQhA6QliozrazslMHTMndzTSFzrDZUpioL0tLbtt-bs1TyUhA7tr5dalT8PPGZD8JRE3VBeTjpDIuABaO5qABGVhQKgBeFYMwA9ABBGR8BQGkEzBQSkIRKVcMNSboNpQcKAySLlvEyWLyJszDtQWvOBsgPAAG1kSn3x0PR6Pnb4WDMlG6ogyUBO14a4Vi97KT6SU-qr8SESLr7+ZtfiJQBAAH1FAHAAWE5wAVOEQAd+tKvjS4RKIhoFJIhYTMohU4Roh8Fhte0BJHek7hOUr7wYkxRPskZHAVAoiJEoZFQQx5ThJqNFoRabHDMyiQ0ZhDI0UR9T0YxOFYjRlwRneX7hMUAkMfCEbUcpzERuJeIKVB0Q8DwsS2Kw9jyI4ziSPI2a9rI-aJkMGonDA1ASGZshIGoY5KJcS7NrIADycBKGoW67v8Cj2tZcjyPmURmo5zmuTI7lxNIf4+TMAVcJ2px+pFtkQsyYQRFElSlua5YQOcajpbIABCNZ1gk0AhXu+5te1HVBV2Rhug8FhIAgbDlgoyiqImbqYWMo56DOISJJWEAcGoBIzpVlbnPU2Xdg8+XcoVRYwqVsh4hEc6etVy6yKu671s1O6tR1XCdfu2VZg8eZ2USGr9Clx2RnOKYNRu0A1TIADKp48EAA"></iframe>

=== "Solution"

    <div style="width: 100%; height: 1007.9999999999999px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true, zoom: 70},
            defs: [
            {id: 'inv16', caption: 'Inv16', circuit: {
                components: {
                not0: {type: 'not', pos: [775, 190], in: 120, out: 121},
                not1: {type: 'not', pos: [745, 220], in: 122, out: 123},
                not2: {type: 'not', pos: [775, 245], in: 124, out: 125},
                not3: {type: 'not', pos: [745, 270], in: 126, out: 127},
                not4: {type: 'not', pos: [775, 300], in: 128, out: 129},
                not15: {type: 'not', pos: [745, 325], in: 150, out: 151},
                not5: {type: 'not', pos: [770, 355], in: 130, out: 131},
                not6: {type: 'not', pos: [740, 380], in: 132, out: 133},
                not7: {type: 'not', pos: [770, 410], in: 134, out: 135},
                not8: {type: 'not', pos: [740, 440], in: 136, out: 137},
                not9: {type: 'not', pos: [775, 470], in: 138, out: 139},
                not10: {type: 'not', pos: [740, 500], in: 140, out: 141},
                not11: {type: 'not', pos: [770, 530], in: 142, out: 143},
                not12: {type: 'not', pos: [740, 560], in: 144, out: 145},
                not13: {type: 'not', pos: [770, 585], in: 146, out: 147},
                not14: {type: 'not', pos: [740, 615], in: 148, out: 149},
                out0: {type: 'out', pos: [960, 400], id: '0-15', bits: 16, name: '/A'},
                in1: {type: 'in', pos: [595, 405], id: [16, 17, '19-32'], name: 'A', bits: 16, val: '1110000000000000'},
                },
                wires: [[121, 0], [123, 1], [125, 2], [127, 3], [129, 4], [151, 5], [131, 6], [133, 7], [135, 8], [137, 9], [139, 10], [141, 11], [143, 12], [145, 13], [147, 14], [149, 15], [16, 120], [17, 122], [19, 124], [20, 126], [21, 128], [22, 150], [23, 130], [24, 132], [25, 134], [26, 136], [27, 138], [28, 140], [29, 142], [30, 144], [31, 146], [32, 148]]
            }}
            ],
            components: {
            in0: {type: 'in', pos: [1240, 250], orient: 's', id: 18, name: 'Op1', val: 1},
            in2: {type: 'in', pos: [995, 245], orient: 's', id: 51, name: 'Op0'},
            in3: {type: 'in', pos: [135, 990], id: '53-68', name: 'B', bits: 16, val: '1111111111111111'},
            inv160: {type: 'custom-inv16', pos: [605, 1255], in: '152-167', out: '168-183'},
            in1: {type: 'in', pos: [135, 455], id: '329-344', name: 'A', bits: 16, val: '1111111011111111'},
            out0: {type: 'out', pos: [1330, 545], id: '346-361', bits: 16, name: 'S'},
            mux0: {type: 'mux', pos: [1240, 545], in: '391-423', out: '424-439', from: 32, to: 16},
            mux1: {type: 'mux', pos: [940, 455], in: '447-479', out: '480-495', from: 32, to: 16},
            mux2: {type: 'mux', pos: [990, 990], in: '503-535', out: '536-551', from: 32, to: 16},
            array0: {type: 'and-array', pos: [680, 195], in: ['37-50', 52, '69-85'], out: '86-101', bits: 16},
            array2: {type: 'xor-array', pos: [695, 905], in: ['12-17', '19-36', '102-109'], out: ['142-151', '184-189'], bits: 16},
            array1: {type: 'or-array', pos: [565, 545], in: '190-221', out: '222-237', bits: 16},
            pass0: {type: 'pass', pos: [345, 455], in: '282-297', out: '298-313', bits: 16, slant: 'up'},
            pass1: {type: 'pass', pos: [365, 105], in: ['322-328', 345, '362-369'], out: '370-385', bits: 16, slant: 'up'},
            pass3: {type: 'pass', pos: [365, 815], in: ['8-11', '110-121'], out: '122-137', bits: 16, slant: 'up'},
            pass4: {type: 'pass', pos: [365, 1255], in: '242-257', out: '258-273', bits: 16, slant: 'up'},
            pass5: {type: 'pass', pos: [165, 990], in: ['138-141', '238-241', '274-281'], out: ['314-321', '386-390', '440-442'], bits: 16, slant: 'up'},
            pass6: {type: 'pass', pos: [185, 285], in: '581-596', out: '597-612', bits: 16, slant: 'up'},
            pass7: {type: 'pass', pos: [185, 635], in: '621-636', out: '637-652', bits: 16, slant: 'up'},
            pass9: {type: 'pass', pos: [735, 195], in: ['573-580', '613-620'], out: ['653-660', '693-700'], bits: 16, slant: 'down'},
            pass10: {type: 'pass', pos: [755, 370], in: '709-724', out: '725-740', bits: 16, slant: 'down'},
            pass2: {type: 'pass', pos: [745, 1255], in: ['443-446', '496-502', '552-556'], out: '557-572', bits: 16, slant: 'up'},
            pass8: {type: 'pass', pos: [765, 1075], in: '669-684', out: ['685-692', '701-708'], bits: 16, slant: 'up'},
            pass11: {type: 'pass', pos: [1025, 990], in: ['661-668', '741-748'], out: '749-764', bits: 16, slant: 'up'},
            pass12: {type: 'pass', pos: [1045, 630], in: '773-788', out: '789-804', bits: 16, slant: 'up'},
            },
            wires: [[424, 346], [425, 347], [426, 348], [427, 349], [428, 350], [429, 351], [430, 352], [431, 353], [432, 354], [433, 355], [434, 356], [435, 357], [436, 358], [437, 359], [438, 360], [439, 361], [480, 391], [481, 392], [482, 393], [483, 394], [484, 395], [485, 396], [486, 397], [487, 398], [488, 399], [489, 400], [490, 401], [491, 402], [492, 403], [493, 404], [494, 405], [495, 406], [18, 423], [51, 479], [51, 535], [142, 503], [143, 504], [144, 505], [145, 506], [146, 507], [147, 508], [148, 509], [149, 510], [150, 511], [151, 512], [184, 513], [185, 514], [186, 515], [187, 516], [188, 517], [189, 518], [222, 463], [223, 464], [224, 465], [225, 466], [226, 467], [227, 468], [228, 469], [229, 470], [230, 471], [231, 472], [232, 473], [233, 474], [234, 475], [235, 476], [236, 477], [237, 478], [329, 282], [330, 283], [331, 284], [332, 285], [333, 286], [334, 287], [335, 288], [336, 289], [337, 290], [338, 291], [339, 292], [340, 293], [341, 294], [342, 295], [343, 296], [344, 297], [298, 322], [299, 323], [300, 324], [301, 325], [302, 326], [303, 327], [304, 328], [305, 345], [306, 362], [307, 363], [308, 364], [309, 365], [310, 366], [311, 367], [312, 368], [313, 369], [298, 8], [299, 9], [300, 10], [301, 11], [302, 110], [303, 111], [304, 112], [305, 113], [306, 114], [307, 115], [308, 116], [309, 117], [310, 118], [311, 119], [312, 120], [313, 121], [298, 242], [299, 243], [300, 244], [301, 245], [302, 246], [303, 247], [304, 248], [305, 249], [306, 250], [307, 251], [308, 252], [309, 253], [310, 254], [311, 255], [312, 256], [313, 257], [370, 37], [371, 38], [372, 39], [373, 40], [374, 41], [375, 42], [376, 43], [377, 44], [378, 45], [379, 46], [380, 47], [381, 48], [382, 49], [383, 50], [384, 52], [385, 69], [122, 12], [123, 13], [124, 14], [125, 15], [126, 16], [127, 17], [128, 19], [129, 20], [130, 21], [131, 22], [132, 23], [133, 24], [134, 25], [135, 26], [136, 27], [137, 28], [258, 152], [259, 153], [260, 154], [261, 155], [262, 156], [263, 157], [264, 158], [265, 159], [266, 160], [267, 161], [268, 162], [269, 163], [270, 164], [271, 165], [272, 166], [273, 167], [53, 138], [54, 139], [55, 140], [56, 141], [57, 238], [58, 239], [59, 240], [60, 241], [61, 274], [62, 275], [63, 276], [64, 277], [65, 278], [66, 279], [67, 280], [68, 281], [314, 581], [315, 582], [316, 583], [317, 584], [318, 585], [319, 586], [320, 587], [321, 588], [386, 589], [387, 590], [388, 591], [389, 592], [390, 593], [440, 594], [441, 595], [442, 596], [597, 70], [598, 71], [599, 72], [600, 73], [601, 74], [602, 75], [603, 76], [604, 77], [605, 78], [606, 79], [607, 80], [608, 81], [609, 82], [610, 83], [611, 84], [612, 85], [314, 621], [315, 622], [316, 623], [317, 624], [318, 625], [319, 626], [320, 627], [321, 628], [386, 629], [387, 630], [388, 631], [389, 632], [390, 633], [440, 634], [441, 635], [442, 636], [637, 206], [638, 207], [639, 208], [640, 209], [641, 210], [642, 211], [643, 212], [644, 213], [645, 214], [646, 215], [647, 216], [648, 217], [649, 218], [650, 219], [651, 220], [652, 221], [86, 573], [87, 574], [88, 575], [89, 576], [90, 577], [91, 578], [92, 579], [93, 580], [94, 613], [95, 614], [96, 615], [97, 616], [98, 617], [99, 618], [100, 619], [101, 620], [653, 709], [654, 710], [655, 711], [656, 712], [657, 713], [658, 714], [659, 715], [660, 716], [693, 717], [694, 718], [695, 719], [696, 720], [697, 721], [698, 722], [699, 723], [700, 724], [314, 29], [315, 30], [316, 31], [317, 32], [318, 33], [319, 34], [320, 35], [321, 36], [386, 102], [387, 103], [388, 104], [389, 105], [390, 106], [440, 107], [441, 108], [442, 109], [298, 190], [299, 191], [300, 192], [301, 193], [302, 194], [303, 195], [304, 196], [305, 197], [306, 198], [307, 199], [308, 200], [309, 201], [310, 202], [311, 203], [312, 204], [313, 205], [725, 447], [726, 448], [727, 449], [728, 450], [729, 451], [730, 452], [731, 453], [732, 454], [733, 455], [734, 456], [735, 457], [736, 458], [737, 459], [738, 460], [739, 461], [740, 462], [168, 443], [169, 444], [170, 445], [171, 446], [172, 496], [173, 497], [174, 498], [175, 499], [176, 500], [177, 501], [178, 502], [179, 552], [180, 553], [181, 554], [182, 555], [183, 556], [557, 669], [558, 670], [559, 671], [560, 672], [561, 673], [562, 674], [563, 675], [564, 676], [565, 677], [566, 678], [567, 679], [568, 680], [569, 681], [570, 682], [571, 683], [572, 684], [685, 519], [686, 520], [687, 521], [688, 522], [689, 523], [690, 524], [691, 525], [692, 526], [701, 527], [702, 528], [703, 529], [704, 530], [705, 531], [706, 532], [707, 533], [708, 534], [536, 661], [537, 662], [538, 663], [539, 664], [540, 665], [541, 666], [542, 667], [543, 668], [544, 741], [545, 742], [546, 743], [547, 744], [548, 745], [549, 746], [550, 747], [551, 748], [749, 773], [750, 774], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [789, 407], [790, 408], [791, 409], [792, 410], [793, 411], [794, 412], [795, 413], [796, 414], [797, 415], [798, 416], [799, 417], [800, 418], [801, 419], [802, 420], [803, 421], [804, 422]]
        }
        </script>
    </logic-editor>
    </div>

#### b. Unité Arithmértique (UA)

Les deux bits `Op1` et `Op0` permettent de sélectionner parmi les 4 opérations arithmétiques à effectuer sur les mots de 16 bits `A` et `B`:

| Op1 | Op0 | opération |
|:---:|:---:|:---------:|
|  0  |  0  |   A+B     |
|  0  |  1  |   A+1     |
|  1  |  0  |   A-B     |
|  1  |  1  |   A-1     |

=== "Réalisation"

    <div style="width: 100%; height: 1027px">
    <logic-editor mode="design">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: { showGateTypes: true },
            defs: [
            {id: 'opp16', caption: 'Opp16', circuit: {
                components: {
                in0: {type: 'in', pos: [140, 725], orient: 'n', id: 223, name: 'In0', val: 1, isConstant: true},
                cnot0: {type: 'cnot-array', pos: [155, 380], in: '73-89', out: ['90-96', '113-116', '133-137'], bits: 16},
                adder1: {type: 'adder-array', pos: [285, 470], in: '152-184', out: '185-201', bits: 16},
                out1: {type: 'out', pos: [365, 470], id: ['4-13', '64-69'], bits: 16, name: '-B'},
                in2: {type: 'in', pos: [50, 380], id: '207-222', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[185, 4], [186, 5], [187, 6], [188, 7], [189, 8], [190, 9], [191, 10], [192, 11], [193, 12], [194, 13], [195, 64], [196, 65], [197, 66], [198, 67], [199, 68], [200, 69], [207, 73], [208, 74], [209, 75], [210, 76], [211, 77], [212, 78], [213, 79], [214, 80], [215, 81], [216, 82], [217, 83], [218, 84], [219, 85], [220, 86], [221, 87], [222, 88], [223, 89], [90, 152], [91, 153], [92, 154], [93, 155], [94, 156], [95, 157], [96, 158], [113, 159], [114, 160], [115, 161], [116, 162], [133, 163], [134, 164], [135, 165], [136, 166], [137, 167], [223, 168]]
            }},
            {id: 'sous16', caption: 'Sous16', circuit: {
                components: {
                adder0: {type: 'adder-array', pos: [525, 350], in: '14-46', out: '47-63', bits: 16},
                comp20: {type: 'custom-opp16', pos: [370, 515], in: '64-79', out: '80-95'},
                out0: {type: 'out', pos: [695, 350], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [165, 265], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                in0: {type: 'in', pos: [165, 515], id: '150-165', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[47, 117], [48, 118], [49, 119], [50, 120], [51, 121], [52, 122], [53, 123], [54, 124], [55, 125], [56, 126], [57, 127], [58, 128], [59, 129], [60, 130], [61, 131], [62, 132], [97, 14], [98, 15], [99, 16], [100, 17], [101, 18], [102, 19], [103, 20], [104, 21], [105, 22], [106, 23], [107, 24], [108, 25], [109, 26], [110, 27], [111, 28], [112, 29], [80, 30], [81, 31], [82, 32], [83, 33], [84, 34], [85, 35], [86, 36], [87, 37], [88, 38], [89, 39], [90, 40], [91, 41], [92, 42], [93, 43], [94, 44], [95, 45], [150, 64], [151, 65], [152, 66], [153, 67], [154, 68], [155, 69], [156, 70], [157, 71], [158, 72], [159, 73], [160, 74], [161, 75], [162, 76], [163, 77], [164, 78], [165, 79]]
            }}
            ],
            components: {
            sous160: {type: 'custom-sous16', pos: [570, 670], in: '242-273', out: '274-289'},
            in0: {type: 'in', pos: [40, 300], id: '470-485', name: 'A', bits: 16, val: '0000000000000110'},
            in1: {type: 'in', pos: [40, 745], id: '486-501', name: 'B', bits: 16, val: '0000000000000010'},
            out0: {type: 'out', pos: [990, 335], id: '622-637', bits: 16, name: 'S'},
            in2: {type: 'in', pos: [85, 840], id: 638, val: 1, isConstant: true},
            in3: {type: 'in', pos: [885, 50], orient: 's', id: 807, name: 'Op1'},
            in4: {type: 'in', pos: [170, 45], orient: 's', id: 808, name: 'Op0'},
            }
        }
        </script>
    </logic-editor>
    </div>

=== "Solution"

    <div style="width: 100%; height: 1037px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {name: 'Soustracteur', showGateTypes: true},
            defs: [
            {id: 'opp16', caption: 'Opp16', circuit: {
                components: {
                in0: {type: 'in', pos: [140, 725], orient: 'n', id: 223, name: 'In0', val: 1, isConstant: true},
                cnot0: {type: 'cnot-array', pos: [155, 380], in: '73-89', out: ['90-96', '113-116', '133-137'], bits: 16},
                adder1: {type: 'adder-array', pos: [285, 470], in: '152-184', out: '185-201', bits: 16},
                out1: {type: 'out', pos: [365, 470], id: ['4-13', '64-69'], bits: 16, name: '-B'},
                in2: {type: 'in', pos: [50, 380], id: '207-222', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[185, 4], [186, 5], [187, 6], [188, 7], [189, 8], [190, 9], [191, 10], [192, 11], [193, 12], [194, 13], [195, 64], [196, 65], [197, 66], [198, 67], [199, 68], [200, 69], [207, 73], [208, 74], [209, 75], [210, 76], [211, 77], [212, 78], [213, 79], [214, 80], [215, 81], [216, 82], [217, 83], [218, 84], [219, 85], [220, 86], [221, 87], [222, 88], [223, 89], [90, 152], [91, 153], [92, 154], [93, 155], [94, 156], [95, 157], [96, 158], [113, 159], [114, 160], [115, 161], [116, 162], [133, 163], [134, 164], [135, 165], [136, 166], [137, 167], [223, 168]]
            }},
            {id: 'sous16', caption: 'Sous16', circuit: {
                components: {
                adder0: {type: 'adder-array', pos: [525, 350], in: '14-46', out: '47-63', bits: 16},
                comp20: {type: 'custom-opp16', pos: [370, 515], in: '64-79', out: '80-95'},
                out0: {type: 'out', pos: [695, 350], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [165, 265], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                in0: {type: 'in', pos: [165, 515], id: '150-165', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[47, 117], [48, 118], [49, 119], [50, 120], [51, 121], [52, 122], [53, 123], [54, 124], [55, 125], [56, 126], [57, 127], [58, 128], [59, 129], [60, 130], [61, 131], [62, 132], [97, 14], [98, 15], [99, 16], [100, 17], [101, 18], [102, 19], [103, 20], [104, 21], [105, 22], [106, 23], [107, 24], [108, 25], [109, 26], [110, 27], [111, 28], [112, 29], [80, 30], [81, 31], [82, 32], [83, 33], [84, 34], [85, 35], [86, 36], [87, 37], [88, 38], [89, 39], [90, 40], [91, 41], [92, 42], [93, 43], [94, 44], [95, 45], [150, 64], [151, 65], [152, 66], [153, 67], [154, 68], [155, 69], [156, 70], [157, 71], [158, 72], [159, 73], [160, 74], [161, 75], [162, 76], [163, 77], [164, 78], [165, 79]]
            }}
            ],
            components: {
            adder0: {type: 'adder-array', pos: [500, 245], in: '192-224', out: '225-241', bits: 16},
            sous160: {type: 'custom-sous16', pos: [570, 670], in: '242-273', out: '274-289'},
            mux0: {type: 'mux', pos: [885, 335], in: '297-329', out: '330-345', from: 32, to: 16},
            mux1: {type: 'mux', pos: [135, 830], in: '353-385', out: '386-401', from: 32, to: 16},
            in0: {type: 'in', pos: [40, 300], id: '470-485', name: 'A', bits: 16, val: '0000000000000110'},
            in1: {type: 'in', pos: [40, 745], id: '486-501', name: 'B', bits: 16, val: '0000000000000010'},
            pass0: {type: 'pass', pos: [90, 300], in: '510-525', out: '526-541', bits: 16, slant: 'up'},
            pass1: {type: 'pass', pos: [110, 150], in: '550-565', out: '566-581', bits: 16, slant: 'up'},
            pass2: {type: 'pass', pos: [110, 510], in: '590-605', out: '606-621', bits: 16, slant: 'up'},
            out0: {type: 'out', pos: [990, 335], id: '622-637', bits: 16, name: 'S'},
            in2: {type: 'in', pos: [85, 840], id: 638, val: 1, isConstant: true},
            pass3: {type: 'pass', pos: [275, 830], in: '647-662', out: '663-678', bits: 16, slant: 'up'},
            pass4: {type: 'pass', pos: [295, 335], in: '687-702', out: '703-718', bits: 16, slant: 'up'},
            pass6: {type: 'pass', pos: [660, 670], in: '735-750', out: '751-766', bits: 16, slant: 'up'},
            pass7: {type: 'pass', pos: [680, 420], in: '775-790', out: '791-806', bits: 16, slant: 'up'},
            in3: {type: 'in', pos: [885, 50], orient: 's', id: 807, name: 'Op1', val: 1},
            in4: {type: 'in', pos: [135, 590], orient: 's', id: 808, name: 'Op0', val: 1},
            },
            wires: [[470, 510], [471, 511], [472, 512], [473, 513], [474, 514], [475, 515], [476, 516], [477, 517], [478, 518], [479, 519], [480, 520], [481, 521], [482, 522], [483, 523], [484, 524], [485, 525], [526, 550], [527, 551], [528, 552], [529, 553], [530, 554], [531, 555], [532, 556], [533, 557], [534, 558], [535, 559], [536, 560], [537, 561], [538, 562], [539, 563], [540, 564], [541, 565], [566, 192], [567, 193], [568, 194], [569, 195], [570, 196], [571, 197], [572, 198], [573, 199], [574, 200], [575, 201], [576, 202], [577, 203], [578, 204], [579, 205], [580, 206], [581, 207], [526, 590], [527, 591], [528, 592], [529, 593], [530, 594], [531, 595], [532, 596], [533, 597], [534, 598], [535, 599], [536, 600], [537, 601], [538, 602], [539, 603], [540, 604], [541, 605], [606, 242], [607, 243], [608, 244], [609, 245], [610, 246], [611, 247], [612, 248], [613, 249], [614, 250], [615, 251], [616, 252], [617, 253], [618, 254], [619, 255], [620, 256], [621, 257], [486, 353], [487, 354], [488, 355], [489, 356], [490, 357], [491, 358], [492, 359], [493, 360], [494, 361], [495, 362], [496, 363], [497, 364], [498, 365], [499, 366], [500, 367], [501, 368], [663, 258], [664, 259], [665, 260], [666, 261], [667, 262], [668, 263], [669, 264], [670, 265], [671, 266], [672, 267], [673, 268], [674, 269], [675, 270], [676, 271], [677, 272], [678, 273], [386, 647], [387, 648], [388, 649], [389, 650], [390, 651], [391, 652], [392, 653], [393, 654], [394, 655], [395, 656], [396, 657], [397, 658], [398, 659], [399, 660], [400, 661], [401, 662], [663, 687], [664, 688], [665, 689], [666, 690], [667, 691], [668, 692], [669, 693], [670, 694], [671, 695], [672, 696], [673, 697], [674, 698], [675, 699], [676, 700], [677, 701], [678, 702], [703, 208], [704, 209], [705, 210], [706, 211], [707, 212], [708, 213], [709, 214], [710, 215], [711, 216], [712, 217], [713, 218], [714, 219], [715, 220], [716, 221], [717, 222], [718, 223], [274, 735], [275, 736], [276, 737], [277, 738], [278, 739], [279, 740], [280, 741], [281, 742], [282, 743], [283, 744], [284, 745], [285, 746], [286, 747], [287, 748], [288, 749], [289, 750], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [765, 789], [766, 790], [225, 297], [226, 298], [227, 299], [228, 300], [229, 301], [230, 302], [231, 303], [232, 304], [233, 305], [234, 306], [235, 307], [236, 308], [237, 309], [238, 310], [239, 311], [240, 312], [791, 313], [792, 314], [793, 315], [794, 316], [795, 317], [796, 318], [797, 319], [798, 320], [799, 321], [800, 322], [801, 323], [802, 324], [803, 325], [804, 326], [805, 327], [806, 328], [330, 622], [331, 623], [332, 624], [333, 625], [334, 626], [335, 627], [336, 628], [337, 629], [338, 630], [339, 631], [340, 632], [341, 633], [342, 634], [343, 635], [344, 636], [345, 637], [807, 329], [808, 385], [638, 369]]
        }
        </script>
    </logic-editor>
    </div>

#### c. UAL

L'Unité Arithmétique et Logique combine, en premier lieu, les opérations de l'UL et de l'AL: 

- les bits `Op0` et `Op1` ont les mêmes significations que précédemment.
- Un bit `u` permet de choisir l'unité (`0`: UL, `1`: AL).

2 autres bits ajoutent des fonctionnalités supplémentaires :

- `sw` échange les opérandes si `1`.
- `zx` annule l'opérande de gauche si `1`.

=== "Réalisation"

    <div style="width: 100%; height: 1040.8999999999999px">
    <logic-editor mode="design">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true, zoom: 70},
            defs: [
            {id: 'inv16', caption: 'Inv16', circuit: {
                components: {
                not0: {type: 'not', pos: [775, 190], in: 120, out: 121},
                not1: {type: 'not', pos: [745, 220], in: 122, out: 123},
                not2: {type: 'not', pos: [775, 245], in: 124, out: 125},
                not3: {type: 'not', pos: [745, 270], in: 126, out: 127},
                not4: {type: 'not', pos: [775, 300], in: 128, out: 129},
                not15: {type: 'not', pos: [745, 325], in: 150, out: 151},
                not5: {type: 'not', pos: [770, 355], in: 130, out: 131},
                not6: {type: 'not', pos: [740, 380], in: 132, out: 133},
                not7: {type: 'not', pos: [770, 410], in: 134, out: 135},
                not8: {type: 'not', pos: [740, 440], in: 136, out: 137},
                not9: {type: 'not', pos: [775, 470], in: 138, out: 139},
                not10: {type: 'not', pos: [740, 500], in: 140, out: 141},
                not11: {type: 'not', pos: [770, 530], in: 142, out: 143},
                not12: {type: 'not', pos: [740, 560], in: 144, out: 145},
                not13: {type: 'not', pos: [770, 585], in: 146, out: 147},
                not14: {type: 'not', pos: [740, 615], in: 148, out: 149},
                out0: {type: 'out', pos: [960, 400], id: '0-15', bits: 16, name: '/A'},
                in1: {type: 'in', pos: [595, 405], id: [16, 17, '19-32'], name: 'A', bits: 16, val: '1110000000000000'},
                },
                wires: [[121, 0], [123, 1], [125, 2], [127, 3], [129, 4], [151, 5], [131, 6], [133, 7], [135, 8], [137, 9], [139, 10], [141, 11], [143, 12], [145, 13], [147, 14], [149, 15], [16, 120], [17, 122], [19, 124], [20, 126], [21, 128], [22, 150], [23, 130], [24, 132], [25, 134], [26, 136], [27, 138], [28, 140], [29, 142], [30, 144], [31, 146], [32, 148]]
            }},
            {id: 'ul', caption: 'ul', circuit: {
                components: {
                mux0: {type: 'mux', pos: [1240, 545], in: '391-423', out: '424-439', from: 32, to: 16},
                mux1: {type: 'mux', pos: [940, 455], in: '447-479', out: '480-495', from: 32, to: 16},
                mux2: {type: 'mux', pos: [990, 990], in: '503-535', out: '536-551', from: 32, to: 16},
                pass5: {type: 'pass', pos: [165, 990], in: ['138-141', '238-241', '274-281'], out: ['314-321', '386-390', '440-442'], bits: 16, slant: 'up'},
                pass0: {type: 'pass', pos: [345, 455], in: '282-297', out: '298-313', bits: 16, slant: 'up'},
                pass11: {type: 'pass', pos: [1025, 990], in: ['661-668', '741-748'], out: '749-764', bits: 16, slant: 'up'},
                pass6: {type: 'pass', pos: [185, 285], in: '581-596', out: '597-612', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [185, 635], in: '621-636', out: '637-652', bits: 16, slant: 'up'},
                array2: {type: 'xor-array', pos: [695, 905], in: ['12-17', '19-36', '102-109'], out: ['142-151', '184-189'], bits: 16},
                pass1: {type: 'pass', pos: [365, 105], in: ['322-328', 345, '362-369'], out: '370-385', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [365, 815], in: ['8-11', '110-121'], out: '122-137', bits: 16, slant: 'up'},
                pass4: {type: 'pass', pos: [365, 1255], in: '242-257', out: '258-273', bits: 16, slant: 'up'},
                array1: {type: 'or-array', pos: [565, 545], in: '190-221', out: '222-237', bits: 16},
                pass12: {type: 'pass', pos: [1045, 630], in: '773-788', out: '789-804', bits: 16, slant: 'up'},
                array0: {type: 'and-array', pos: [680, 195], in: ['37-50', 52, '69-85'], out: '86-101', bits: 16},
                inv160: {type: 'custom-inv16', pos: [605, 1255], in: '152-167', out: '168-183'},
                pass9: {type: 'pass', pos: [735, 195], in: ['573-580', '613-620'], out: ['653-660', '693-700'], bits: 16, slant: 'down'},
                pass2: {type: 'pass', pos: [745, 1255], in: ['443-446', '496-502', '552-556'], out: '557-572', bits: 16, slant: 'up'},
                pass10: {type: 'pass', pos: [755, 370], in: '709-724', out: '725-740', bits: 16, slant: 'down'},
                pass8: {type: 'pass', pos: [765, 1075], in: '669-684', out: ['685-692', '701-708'], bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [1330, 545], id: '346-361', bits: 16, name: 'S'},
                in1: {type: 'in', pos: [135, 455], id: '329-344', name: 'A', bits: 16, val: '1111111011111111'},
                in3: {type: 'in', pos: [135, 990], id: '53-68', name: 'B', bits: 16, val: '1111111111111111'},
                in0: {type: 'in', pos: [1240, 250], orient: 's', id: 18, name: 'Op1', val: 1},
                in2: {type: 'in', pos: [995, 245], orient: 's', id: 51, name: 'Op0'},
                },
                wires: [[424, 346], [425, 347], [426, 348], [427, 349], [428, 350], [429, 351], [430, 352], [431, 353], [432, 354], [433, 355], [434, 356], [435, 357], [436, 358], [437, 359], [438, 360], [439, 361], [480, 391], [481, 392], [482, 393], [483, 394], [484, 395], [485, 396], [486, 397], [487, 398], [488, 399], [489, 400], [490, 401], [491, 402], [492, 403], [493, 404], [494, 405], [495, 406], [789, 407], [790, 408], [791, 409], [792, 410], [793, 411], [794, 412], [795, 413], [796, 414], [797, 415], [798, 416], [799, 417], [800, 418], [801, 419], [802, 420], [803, 421], [804, 422], [18, 423], [725, 447], [726, 448], [727, 449], [728, 450], [729, 451], [730, 452], [731, 453], [732, 454], [733, 455], [734, 456], [735, 457], [736, 458], [737, 459], [738, 460], [739, 461], [740, 462], [222, 463], [223, 464], [224, 465], [225, 466], [226, 467], [227, 468], [228, 469], [229, 470], [230, 471], [231, 472], [232, 473], [233, 474], [234, 475], [235, 476], [236, 477], [237, 478], [51, 479], [142, 503], [143, 504], [144, 505], [145, 506], [146, 507], [147, 508], [148, 509], [149, 510], [150, 511], [151, 512], [184, 513], [185, 514], [186, 515], [187, 516], [188, 517], [189, 518], [685, 519], [686, 520], [687, 521], [688, 522], [689, 523], [690, 524], [691, 525], [692, 526], [701, 527], [702, 528], [703, 529], [704, 530], [705, 531], [706, 532], [707, 533], [708, 534], [51, 535], [53, 138], [54, 139], [55, 140], [56, 141], [57, 238], [58, 239], [59, 240], [60, 241], [61, 274], [62, 275], [63, 276], [64, 277], [65, 278], [66, 279], [67, 280], [68, 281], [329, 282], [330, 283], [331, 284], [332, 285], [333, 286], [334, 287], [335, 288], [336, 289], [337, 290], [338, 291], [339, 292], [340, 293], [341, 294], [342, 295], [343, 296], [344, 297], [536, 661], [537, 662], [538, 663], [539, 664], [540, 665], [541, 666], [542, 667], [543, 668], [544, 741], [545, 742], [546, 743], [547, 744], [548, 745], [549, 746], [550, 747], [551, 748], [314, 581], [315, 582], [316, 583], [317, 584], [318, 585], [319, 586], [320, 587], [321, 588], [386, 589], [387, 590], [388, 591], [389, 592], [390, 593], [440, 594], [441, 595], [442, 596], [314, 621], [315, 622], [316, 623], [317, 624], [318, 625], [319, 626], [320, 627], [321, 628], [386, 629], [387, 630], [388, 631], [389, 632], [390, 633], [440, 634], [441, 635], [442, 636], [122, 12], [123, 13], [124, 14], [125, 15], [126, 16], [127, 17], [128, 19], [129, 20], [130, 21], [131, 22], [132, 23], [133, 24], [134, 25], [135, 26], [136, 27], [137, 28], [314, 29], [315, 30], [316, 31], [317, 32], [318, 33], [319, 34], [320, 35], [321, 36], [386, 102], [387, 103], [388, 104], [389, 105], [390, 106], [440, 107], [441, 108], [442, 109], [298, 322], [299, 323], [300, 324], [301, 325], [302, 326], [303, 327], [304, 328], [305, 345], [306, 362], [307, 363], [308, 364], [309, 365], [310, 366], [311, 367], [312, 368], [313, 369], [298, 8], [299, 9], [300, 10], [301, 11], [302, 110], [303, 111], [304, 112], [305, 113], [306, 114], [307, 115], [308, 116], [309, 117], [310, 118], [311, 119], [312, 120], [313, 121], [298, 242], [299, 243], [300, 244], [301, 245], [302, 246], [303, 247], [304, 248], [305, 249], [306, 250], [307, 251], [308, 252], [309, 253], [310, 254], [311, 255], [312, 256], [313, 257], [298, 190], [299, 191], [300, 192], [301, 193], [302, 194], [303, 195], [304, 196], [305, 197], [306, 198], [307, 199], [308, 200], [309, 201], [310, 202], [311, 203], [312, 204], [313, 205], [637, 206], [638, 207], [639, 208], [640, 209], [641, 210], [642, 211], [643, 212], [644, 213], [645, 214], [646, 215], [647, 216], [648, 217], [649, 218], [650, 219], [651, 220], [652, 221], [749, 773], [750, 774], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [370, 37], [371, 38], [372, 39], [373, 40], [374, 41], [375, 42], [376, 43], [377, 44], [378, 45], [379, 46], [380, 47], [381, 48], [382, 49], [383, 50], [384, 52], [385, 69], [597, 70], [598, 71], [599, 72], [600, 73], [601, 74], [602, 75], [603, 76], [604, 77], [605, 78], [606, 79], [607, 80], [608, 81], [609, 82], [610, 83], [611, 84], [612, 85], [258, 152], [259, 153], [260, 154], [261, 155], [262, 156], [263, 157], [264, 158], [265, 159], [266, 160], [267, 161], [268, 162], [269, 163], [270, 164], [271, 165], [272, 166], [273, 167], [86, 573], [87, 574], [88, 575], [89, 576], [90, 577], [91, 578], [92, 579], [93, 580], [94, 613], [95, 614], [96, 615], [97, 616], [98, 617], [99, 618], [100, 619], [101, 620], [168, 443], [169, 444], [170, 445], [171, 446], [172, 496], [173, 497], [174, 498], [175, 499], [176, 500], [177, 501], [178, 502], [179, 552], [180, 553], [181, 554], [182, 555], [183, 556], [653, 709], [654, 710], [655, 711], [656, 712], [657, 713], [658, 714], [659, 715], [660, 716], [693, 717], [694, 718], [695, 719], [696, 720], [697, 721], [698, 722], [699, 723], [700, 724], [557, 669], [558, 670], [559, 671], [560, 672], [561, 673], [562, 674], [563, 675], [564, 676], [565, 677], [566, 678], [567, 679], [568, 680], [569, 681], [570, 682], [571, 683], [572, 684]]
            }},
            {id: 'opp16', caption: 'Opp16', circuit: {
                components: {
                in0: {type: 'in', pos: [140, 725], orient: 'n', id: 223, name: 'In0', val: 1, isConstant: true},
                cnot0: {type: 'cnot-array', pos: [155, 380], in: '73-89', out: ['90-96', '113-116', '133-137'], bits: 16},
                adder1: {type: 'adder-array', pos: [285, 470], in: '152-184', out: '185-201', bits: 16},
                out1: {type: 'out', pos: [365, 470], id: ['4-13', '64-69'], bits: 16, name: '-B'},
                in2: {type: 'in', pos: [50, 380], id: '207-222', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[185, 4], [186, 5], [187, 6], [188, 7], [189, 8], [190, 9], [191, 10], [192, 11], [193, 12], [194, 13], [195, 64], [196, 65], [197, 66], [198, 67], [199, 68], [200, 69], [207, 73], [208, 74], [209, 75], [210, 76], [211, 77], [212, 78], [213, 79], [214, 80], [215, 81], [216, 82], [217, 83], [218, 84], [219, 85], [220, 86], [221, 87], [222, 88], [223, 89], [90, 152], [91, 153], [92, 154], [93, 155], [94, 156], [95, 157], [96, 158], [113, 159], [114, 160], [115, 161], [116, 162], [133, 163], [134, 164], [135, 165], [136, 166], [137, 167], [223, 168]]
            }},
            {id: 'sous16', caption: 'Sous16', circuit: {
                components: {
                adder0: {type: 'adder-array', pos: [525, 350], in: '14-46', out: '47-63', bits: 16},
                comp20: {type: 'custom-opp16', pos: [370, 515], in: '64-79', out: '80-95'},
                out0: {type: 'out', pos: [695, 350], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [165, 265], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                in0: {type: 'in', pos: [165, 515], id: '150-165', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[47, 117], [48, 118], [49, 119], [50, 120], [51, 121], [52, 122], [53, 123], [54, 124], [55, 125], [56, 126], [57, 127], [58, 128], [59, 129], [60, 130], [61, 131], [62, 132], [97, 14], [98, 15], [99, 16], [100, 17], [101, 18], [102, 19], [103, 20], [104, 21], [105, 22], [106, 23], [107, 24], [108, 25], [109, 26], [110, 27], [111, 28], [112, 29], [80, 30], [81, 31], [82, 32], [83, 33], [84, 34], [85, 35], [86, 36], [87, 37], [88, 38], [89, 39], [90, 40], [91, 41], [92, 42], [93, 43], [94, 44], [95, 45], [150, 64], [151, 65], [152, 66], [153, 67], [154, 68], [155, 69], [156, 70], [157, 71], [158, 72], [159, 73], [160, 74], [161, 75], [162, 76], [163, 77], [164, 78], [165, 79]]
            }},
            {id: 'al', caption: 'al', circuit: {
                components: {
                in2: {type: 'in', pos: [85, 840], id: 638, name: 'In0', val: 1, isConstant: true},
                pass0: {type: 'pass', pos: [90, 300], in: '510-525', out: '526-541', bits: 16, slant: 'up'},
                mux1: {type: 'mux', pos: [135, 830], in: '353-385', out: '386-401', from: 32, to: 16},
                mux0: {type: 'mux', pos: [885, 335], in: '297-329', out: '330-345', from: 32, to: 16},
                pass1: {type: 'pass', pos: [110, 150], in: '550-565', out: '566-581', bits: 16, slant: 'up'},
                pass2: {type: 'pass', pos: [110, 510], in: '590-605', out: '606-621', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [275, 830], in: '647-662', out: '663-678', bits: 16, slant: 'up'},
                adder0: {type: 'adder-array', pos: [500, 245], in: '192-224', out: '225-241', bits: 16},
                sous160: {type: 'custom-sous16', pos: [570, 670], in: '242-273', out: '274-289'},
                pass4: {type: 'pass', pos: [295, 335], in: '687-702', out: '703-718', bits: 16, slant: 'up'},
                pass6: {type: 'pass', pos: [660, 670], in: '735-750', out: '751-766', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [680, 420], in: '775-790', out: '791-806', bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [990, 335], id: '622-637', bits: 16, name: 'S'},
                in0: {type: 'in', pos: [40, 300], id: '470-485', name: 'A', bits: 16, val: '0000000000000110'},
                in1: {type: 'in', pos: [40, 745], id: '486-501', name: 'B', bits: 16, val: '0000000000000010'},
                in3: {type: 'in', pos: [885, 50], orient: 's', id: 807, name: 'Op1', val: 1},
                in4: {type: 'in', pos: [135, 590], orient: 's', id: 808, name: 'Op0', val: 1},
                },
                wires: [[330, 622], [331, 623], [332, 624], [333, 625], [334, 626], [335, 627], [336, 628], [337, 629], [338, 630], [339, 631], [340, 632], [341, 633], [342, 634], [343, 635], [344, 636], [345, 637], [470, 510], [471, 511], [472, 512], [473, 513], [474, 514], [475, 515], [476, 516], [477, 517], [478, 518], [479, 519], [480, 520], [481, 521], [482, 522], [483, 523], [484, 524], [485, 525], [486, 353], [487, 354], [488, 355], [489, 356], [490, 357], [491, 358], [492, 359], [493, 360], [494, 361], [495, 362], [496, 363], [497, 364], [498, 365], [499, 366], [500, 367], [501, 368], [638, 369], [808, 385], [225, 297], [226, 298], [227, 299], [228, 300], [229, 301], [230, 302], [231, 303], [232, 304], [233, 305], [234, 306], [235, 307], [236, 308], [237, 309], [238, 310], [239, 311], [240, 312], [791, 313], [792, 314], [793, 315], [794, 316], [795, 317], [796, 318], [797, 319], [798, 320], [799, 321], [800, 322], [801, 323], [802, 324], [803, 325], [804, 326], [805, 327], [806, 328], [807, 329], [526, 550], [527, 551], [528, 552], [529, 553], [530, 554], [531, 555], [532, 556], [533, 557], [534, 558], [535, 559], [536, 560], [537, 561], [538, 562], [539, 563], [540, 564], [541, 565], [526, 590], [527, 591], [528, 592], [529, 593], [530, 594], [531, 595], [532, 596], [533, 597], [534, 598], [535, 599], [536, 600], [537, 601], [538, 602], [539, 603], [540, 604], [541, 605], [386, 647], [387, 648], [388, 649], [389, 650], [390, 651], [391, 652], [392, 653], [393, 654], [394, 655], [395, 656], [396, 657], [397, 658], [398, 659], [399, 660], [400, 661], [401, 662], [566, 192], [567, 193], [568, 194], [569, 195], [570, 196], [571, 197], [572, 198], [573, 199], [574, 200], [575, 201], [576, 202], [577, 203], [578, 204], [579, 205], [580, 206], [581, 207], [703, 208], [704, 209], [705, 210], [706, 211], [707, 212], [708, 213], [709, 214], [710, 215], [711, 216], [712, 217], [713, 218], [714, 219], [715, 220], [716, 221], [717, 222], [718, 223], [606, 242], [607, 243], [608, 244], [609, 245], [610, 246], [611, 247], [612, 248], [613, 249], [614, 250], [615, 251], [616, 252], [617, 253], [618, 254], [619, 255], [620, 256], [621, 257], [663, 258], [664, 259], [665, 260], [666, 261], [667, 262], [668, 263], [669, 264], [670, 265], [671, 266], [672, 267], [673, 268], [674, 269], [675, 270], [676, 271], [677, 272], [678, 273], [663, 687], [664, 688], [665, 689], [666, 690], [667, 691], [668, 692], [669, 693], [670, 694], [671, 695], [672, 696], [673, 697], [674, 698], [675, 699], [676, 700], [677, 701], [678, 702], [274, 735], [275, 736], [276, 737], [277, 738], [278, 739], [279, 740], [280, 741], [281, 742], [282, 743], [283, 744], [284, 745], [285, 746], [286, 747], [287, 748], [288, 749], [289, 750], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [765, 789], [766, 790]]
            }}
            ],
            components: {
            ul0: {type: 'custom-ul', pos: [1115, 525], in: ['0-7', '765-772', '805-822'], out: '823-838'},
            al0: {type: 'custom-al', pos: [1195, 1105], in: '8-41', out: '42-57'},
            in0: {type: 'in', pos: [205, 200], id: '62-77', name: 'A', bits: 16, val: '0000000000000100'},
            in1: {type: 'in', pos: [220, 1185], id: '82-97', name: 'B', bits: 16, val: '0000000000000001'},
            in2: {type: 'in', pos: [1195, 75], orient: 's', id: [104, 105], name: 'Op', bits: 2},
            in3: {type: 'in', pos: [1470, 80], orient: 's', id: 170, name: 'u'},
            in4: {type: 'in', pos: [660, 70], orient: 's', id: 479, name: 'sw'},
            in5: {type: 'in', pos: [860, 75], orient: 's', id: 593, name: 'zx'},
            out0: {type: 'out', pos: [1555, 615], id: ['598-607', '640-645'], bits: 16, name: 'S'},
            }
        }
        </script>
    </logic-editor>
    </div>    

=== "Solution"

    <div style="width: 100%; height: 1044px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true, zoom: 70},
            defs: [
            {id: 'inv16', caption: 'Inv16', circuit: {
                components: {
                not0: {type: 'not', pos: [775, 190], in: 120, out: 121},
                not1: {type: 'not', pos: [745, 220], in: 122, out: 123},
                not2: {type: 'not', pos: [775, 245], in: 124, out: 125},
                not3: {type: 'not', pos: [745, 270], in: 126, out: 127},
                not4: {type: 'not', pos: [775, 300], in: 128, out: 129},
                not15: {type: 'not', pos: [745, 325], in: 150, out: 151},
                not5: {type: 'not', pos: [770, 355], in: 130, out: 131},
                not6: {type: 'not', pos: [740, 380], in: 132, out: 133},
                not7: {type: 'not', pos: [770, 410], in: 134, out: 135},
                not8: {type: 'not', pos: [740, 440], in: 136, out: 137},
                not9: {type: 'not', pos: [775, 470], in: 138, out: 139},
                not10: {type: 'not', pos: [740, 500], in: 140, out: 141},
                not11: {type: 'not', pos: [770, 530], in: 142, out: 143},
                not12: {type: 'not', pos: [740, 560], in: 144, out: 145},
                not13: {type: 'not', pos: [770, 585], in: 146, out: 147},
                not14: {type: 'not', pos: [740, 615], in: 148, out: 149},
                out0: {type: 'out', pos: [960, 400], id: '0-15', bits: 16, name: '/A'},
                in1: {type: 'in', pos: [595, 405], id: [16, 17, '19-32'], name: 'A', bits: 16, val: '1110000000000000'},
                },
                wires: [[121, 0], [123, 1], [125, 2], [127, 3], [129, 4], [151, 5], [131, 6], [133, 7], [135, 8], [137, 9], [139, 10], [141, 11], [143, 12], [145, 13], [147, 14], [149, 15], [16, 120], [17, 122], [19, 124], [20, 126], [21, 128], [22, 150], [23, 130], [24, 132], [25, 134], [26, 136], [27, 138], [28, 140], [29, 142], [30, 144], [31, 146], [32, 148]]
            }},
            {id: 'ul', caption: 'ul', circuit: {
                components: {
                mux0: {type: 'mux', pos: [1240, 545], in: '391-423', out: '424-439', from: 32, to: 16},
                mux1: {type: 'mux', pos: [940, 455], in: '447-479', out: '480-495', from: 32, to: 16},
                mux2: {type: 'mux', pos: [990, 990], in: '503-535', out: '536-551', from: 32, to: 16},
                pass5: {type: 'pass', pos: [165, 990], in: ['138-141', '238-241', '274-281'], out: ['314-321', '386-390', '440-442'], bits: 16, slant: 'up'},
                pass0: {type: 'pass', pos: [345, 455], in: '282-297', out: '298-313', bits: 16, slant: 'up'},
                pass11: {type: 'pass', pos: [1025, 990], in: ['661-668', '741-748'], out: '749-764', bits: 16, slant: 'up'},
                pass6: {type: 'pass', pos: [185, 285], in: '581-596', out: '597-612', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [185, 635], in: '621-636', out: '637-652', bits: 16, slant: 'up'},
                array2: {type: 'xor-array', pos: [695, 905], in: ['12-17', '19-36', '102-109'], out: ['142-151', '184-189'], bits: 16},
                pass1: {type: 'pass', pos: [365, 105], in: ['322-328', 345, '362-369'], out: '370-385', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [365, 815], in: ['8-11', '110-121'], out: '122-137', bits: 16, slant: 'up'},
                pass4: {type: 'pass', pos: [365, 1255], in: '242-257', out: '258-273', bits: 16, slant: 'up'},
                array1: {type: 'or-array', pos: [565, 545], in: '190-221', out: '222-237', bits: 16},
                pass12: {type: 'pass', pos: [1045, 630], in: '773-788', out: '789-804', bits: 16, slant: 'up'},
                array0: {type: 'and-array', pos: [680, 195], in: ['37-50', 52, '69-85'], out: '86-101', bits: 16},
                inv160: {type: 'custom-inv16', pos: [605, 1255], in: '152-167', out: '168-183'},
                pass9: {type: 'pass', pos: [735, 195], in: ['573-580', '613-620'], out: ['653-660', '693-700'], bits: 16, slant: 'down'},
                pass2: {type: 'pass', pos: [745, 1255], in: ['443-446', '496-502', '552-556'], out: '557-572', bits: 16, slant: 'up'},
                pass10: {type: 'pass', pos: [755, 370], in: '709-724', out: '725-740', bits: 16, slant: 'down'},
                pass8: {type: 'pass', pos: [765, 1075], in: '669-684', out: ['685-692', '701-708'], bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [1330, 545], id: '346-361', bits: 16, name: 'S'},
                in1: {type: 'in', pos: [135, 455], id: '329-344', name: 'A', bits: 16, val: '1111111011111111'},
                in3: {type: 'in', pos: [135, 990], id: '53-68', name: 'B', bits: 16, val: '1111111111111111'},
                in0: {type: 'in', pos: [1240, 250], orient: 's', id: 18, name: 'Op1', val: 1},
                in2: {type: 'in', pos: [995, 245], orient: 's', id: 51, name: 'Op0'},
                },
                wires: [[424, 346], [425, 347], [426, 348], [427, 349], [428, 350], [429, 351], [430, 352], [431, 353], [432, 354], [433, 355], [434, 356], [435, 357], [436, 358], [437, 359], [438, 360], [439, 361], [480, 391], [481, 392], [482, 393], [483, 394], [484, 395], [485, 396], [486, 397], [487, 398], [488, 399], [489, 400], [490, 401], [491, 402], [492, 403], [493, 404], [494, 405], [495, 406], [789, 407], [790, 408], [791, 409], [792, 410], [793, 411], [794, 412], [795, 413], [796, 414], [797, 415], [798, 416], [799, 417], [800, 418], [801, 419], [802, 420], [803, 421], [804, 422], [18, 423], [725, 447], [726, 448], [727, 449], [728, 450], [729, 451], [730, 452], [731, 453], [732, 454], [733, 455], [734, 456], [735, 457], [736, 458], [737, 459], [738, 460], [739, 461], [740, 462], [222, 463], [223, 464], [224, 465], [225, 466], [226, 467], [227, 468], [228, 469], [229, 470], [230, 471], [231, 472], [232, 473], [233, 474], [234, 475], [235, 476], [236, 477], [237, 478], [51, 479], [142, 503], [143, 504], [144, 505], [145, 506], [146, 507], [147, 508], [148, 509], [149, 510], [150, 511], [151, 512], [184, 513], [185, 514], [186, 515], [187, 516], [188, 517], [189, 518], [685, 519], [686, 520], [687, 521], [688, 522], [689, 523], [690, 524], [691, 525], [692, 526], [701, 527], [702, 528], [703, 529], [704, 530], [705, 531], [706, 532], [707, 533], [708, 534], [51, 535], [53, 138], [54, 139], [55, 140], [56, 141], [57, 238], [58, 239], [59, 240], [60, 241], [61, 274], [62, 275], [63, 276], [64, 277], [65, 278], [66, 279], [67, 280], [68, 281], [329, 282], [330, 283], [331, 284], [332, 285], [333, 286], [334, 287], [335, 288], [336, 289], [337, 290], [338, 291], [339, 292], [340, 293], [341, 294], [342, 295], [343, 296], [344, 297], [536, 661], [537, 662], [538, 663], [539, 664], [540, 665], [541, 666], [542, 667], [543, 668], [544, 741], [545, 742], [546, 743], [547, 744], [548, 745], [549, 746], [550, 747], [551, 748], [314, 581], [315, 582], [316, 583], [317, 584], [318, 585], [319, 586], [320, 587], [321, 588], [386, 589], [387, 590], [388, 591], [389, 592], [390, 593], [440, 594], [441, 595], [442, 596], [314, 621], [315, 622], [316, 623], [317, 624], [318, 625], [319, 626], [320, 627], [321, 628], [386, 629], [387, 630], [388, 631], [389, 632], [390, 633], [440, 634], [441, 635], [442, 636], [122, 12], [123, 13], [124, 14], [125, 15], [126, 16], [127, 17], [128, 19], [129, 20], [130, 21], [131, 22], [132, 23], [133, 24], [134, 25], [135, 26], [136, 27], [137, 28], [314, 29], [315, 30], [316, 31], [317, 32], [318, 33], [319, 34], [320, 35], [321, 36], [386, 102], [387, 103], [388, 104], [389, 105], [390, 106], [440, 107], [441, 108], [442, 109], [298, 322], [299, 323], [300, 324], [301, 325], [302, 326], [303, 327], [304, 328], [305, 345], [306, 362], [307, 363], [308, 364], [309, 365], [310, 366], [311, 367], [312, 368], [313, 369], [298, 8], [299, 9], [300, 10], [301, 11], [302, 110], [303, 111], [304, 112], [305, 113], [306, 114], [307, 115], [308, 116], [309, 117], [310, 118], [311, 119], [312, 120], [313, 121], [298, 242], [299, 243], [300, 244], [301, 245], [302, 246], [303, 247], [304, 248], [305, 249], [306, 250], [307, 251], [308, 252], [309, 253], [310, 254], [311, 255], [312, 256], [313, 257], [298, 190], [299, 191], [300, 192], [301, 193], [302, 194], [303, 195], [304, 196], [305, 197], [306, 198], [307, 199], [308, 200], [309, 201], [310, 202], [311, 203], [312, 204], [313, 205], [637, 206], [638, 207], [639, 208], [640, 209], [641, 210], [642, 211], [643, 212], [644, 213], [645, 214], [646, 215], [647, 216], [648, 217], [649, 218], [650, 219], [651, 220], [652, 221], [749, 773], [750, 774], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [370, 37], [371, 38], [372, 39], [373, 40], [374, 41], [375, 42], [376, 43], [377, 44], [378, 45], [379, 46], [380, 47], [381, 48], [382, 49], [383, 50], [384, 52], [385, 69], [597, 70], [598, 71], [599, 72], [600, 73], [601, 74], [602, 75], [603, 76], [604, 77], [605, 78], [606, 79], [607, 80], [608, 81], [609, 82], [610, 83], [611, 84], [612, 85], [258, 152], [259, 153], [260, 154], [261, 155], [262, 156], [263, 157], [264, 158], [265, 159], [266, 160], [267, 161], [268, 162], [269, 163], [270, 164], [271, 165], [272, 166], [273, 167], [86, 573], [87, 574], [88, 575], [89, 576], [90, 577], [91, 578], [92, 579], [93, 580], [94, 613], [95, 614], [96, 615], [97, 616], [98, 617], [99, 618], [100, 619], [101, 620], [168, 443], [169, 444], [170, 445], [171, 446], [172, 496], [173, 497], [174, 498], [175, 499], [176, 500], [177, 501], [178, 502], [179, 552], [180, 553], [181, 554], [182, 555], [183, 556], [653, 709], [654, 710], [655, 711], [656, 712], [657, 713], [658, 714], [659, 715], [660, 716], [693, 717], [694, 718], [695, 719], [696, 720], [697, 721], [698, 722], [699, 723], [700, 724], [557, 669], [558, 670], [559, 671], [560, 672], [561, 673], [562, 674], [563, 675], [564, 676], [565, 677], [566, 678], [567, 679], [568, 680], [569, 681], [570, 682], [571, 683], [572, 684]]
            }},
            {id: 'opp16', caption: 'Opp16', circuit: {
                components: {
                in0: {type: 'in', pos: [140, 725], orient: 'n', id: 223, name: 'In0', val: 1, isConstant: true},
                cnot0: {type: 'cnot-array', pos: [155, 380], in: '73-89', out: ['90-96', '113-116', '133-137'], bits: 16},
                adder1: {type: 'adder-array', pos: [285, 470], in: '152-184', out: '185-201', bits: 16},
                out1: {type: 'out', pos: [365, 470], id: ['4-13', '64-69'], bits: 16, name: '-B'},
                in2: {type: 'in', pos: [50, 380], id: '207-222', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[185, 4], [186, 5], [187, 6], [188, 7], [189, 8], [190, 9], [191, 10], [192, 11], [193, 12], [194, 13], [195, 64], [196, 65], [197, 66], [198, 67], [199, 68], [200, 69], [207, 73], [208, 74], [209, 75], [210, 76], [211, 77], [212, 78], [213, 79], [214, 80], [215, 81], [216, 82], [217, 83], [218, 84], [219, 85], [220, 86], [221, 87], [222, 88], [223, 89], [90, 152], [91, 153], [92, 154], [93, 155], [94, 156], [95, 157], [96, 158], [113, 159], [114, 160], [115, 161], [116, 162], [133, 163], [134, 164], [135, 165], [136, 166], [137, 167], [223, 168]]
            }},
            {id: 'sous16', caption: 'Sous16', circuit: {
                components: {
                adder0: {type: 'adder-array', pos: [525, 350], in: '14-46', out: '47-63', bits: 16},
                comp20: {type: 'custom-opp16', pos: [370, 515], in: '64-79', out: '80-95'},
                out0: {type: 'out', pos: [695, 350], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [165, 265], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                in0: {type: 'in', pos: [165, 515], id: '150-165', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[47, 117], [48, 118], [49, 119], [50, 120], [51, 121], [52, 122], [53, 123], [54, 124], [55, 125], [56, 126], [57, 127], [58, 128], [59, 129], [60, 130], [61, 131], [62, 132], [97, 14], [98, 15], [99, 16], [100, 17], [101, 18], [102, 19], [103, 20], [104, 21], [105, 22], [106, 23], [107, 24], [108, 25], [109, 26], [110, 27], [111, 28], [112, 29], [80, 30], [81, 31], [82, 32], [83, 33], [84, 34], [85, 35], [86, 36], [87, 37], [88, 38], [89, 39], [90, 40], [91, 41], [92, 42], [93, 43], [94, 44], [95, 45], [150, 64], [151, 65], [152, 66], [153, 67], [154, 68], [155, 69], [156, 70], [157, 71], [158, 72], [159, 73], [160, 74], [161, 75], [162, 76], [163, 77], [164, 78], [165, 79]]
            }},
            {id: 'al', caption: 'al', circuit: {
                components: {
                in2: {type: 'in', pos: [85, 840], id: 638, name: 'In0', val: 1, isConstant: true},
                pass0: {type: 'pass', pos: [90, 300], in: '510-525', out: '526-541', bits: 16, slant: 'up'},
                mux1: {type: 'mux', pos: [135, 830], in: '353-385', out: '386-401', from: 32, to: 16},
                mux0: {type: 'mux', pos: [885, 335], in: '297-329', out: '330-345', from: 32, to: 16},
                pass1: {type: 'pass', pos: [110, 150], in: '550-565', out: '566-581', bits: 16, slant: 'up'},
                pass2: {type: 'pass', pos: [110, 510], in: '590-605', out: '606-621', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [275, 830], in: '647-662', out: '663-678', bits: 16, slant: 'up'},
                adder0: {type: 'adder-array', pos: [500, 245], in: '192-224', out: '225-241', bits: 16},
                sous160: {type: 'custom-sous16', pos: [570, 670], in: '242-273', out: '274-289'},
                pass4: {type: 'pass', pos: [295, 335], in: '687-702', out: '703-718', bits: 16, slant: 'up'},
                pass6: {type: 'pass', pos: [660, 670], in: '735-750', out: '751-766', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [680, 420], in: '775-790', out: '791-806', bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [990, 335], id: '622-637', bits: 16, name: 'S'},
                in0: {type: 'in', pos: [40, 300], id: '470-485', name: 'A', bits: 16, val: '0000000000000110'},
                in1: {type: 'in', pos: [40, 745], id: '486-501', name: 'B', bits: 16, val: '0000000000000010'},
                in3: {type: 'in', pos: [885, 50], orient: 's', id: 807, name: 'Op1', val: 1},
                in4: {type: 'in', pos: [135, 590], orient: 's', id: 808, name: 'Op0', val: 1},
                },
                wires: [[330, 622], [331, 623], [332, 624], [333, 625], [334, 626], [335, 627], [336, 628], [337, 629], [338, 630], [339, 631], [340, 632], [341, 633], [342, 634], [343, 635], [344, 636], [345, 637], [470, 510], [471, 511], [472, 512], [473, 513], [474, 514], [475, 515], [476, 516], [477, 517], [478, 518], [479, 519], [480, 520], [481, 521], [482, 522], [483, 523], [484, 524], [485, 525], [486, 353], [487, 354], [488, 355], [489, 356], [490, 357], [491, 358], [492, 359], [493, 360], [494, 361], [495, 362], [496, 363], [497, 364], [498, 365], [499, 366], [500, 367], [501, 368], [638, 369], [808, 385], [225, 297], [226, 298], [227, 299], [228, 300], [229, 301], [230, 302], [231, 303], [232, 304], [233, 305], [234, 306], [235, 307], [236, 308], [237, 309], [238, 310], [239, 311], [240, 312], [791, 313], [792, 314], [793, 315], [794, 316], [795, 317], [796, 318], [797, 319], [798, 320], [799, 321], [800, 322], [801, 323], [802, 324], [803, 325], [804, 326], [805, 327], [806, 328], [807, 329], [526, 550], [527, 551], [528, 552], [529, 553], [530, 554], [531, 555], [532, 556], [533, 557], [534, 558], [535, 559], [536, 560], [537, 561], [538, 562], [539, 563], [540, 564], [541, 565], [526, 590], [527, 591], [528, 592], [529, 593], [530, 594], [531, 595], [532, 596], [533, 597], [534, 598], [535, 599], [536, 600], [537, 601], [538, 602], [539, 603], [540, 604], [541, 605], [386, 647], [387, 648], [388, 649], [389, 650], [390, 651], [391, 652], [392, 653], [393, 654], [394, 655], [395, 656], [396, 657], [397, 658], [398, 659], [399, 660], [400, 661], [401, 662], [566, 192], [567, 193], [568, 194], [569, 195], [570, 196], [571, 197], [572, 198], [573, 199], [574, 200], [575, 201], [576, 202], [577, 203], [578, 204], [579, 205], [580, 206], [581, 207], [703, 208], [704, 209], [705, 210], [706, 211], [707, 212], [708, 213], [709, 214], [710, 215], [711, 216], [712, 217], [713, 218], [714, 219], [715, 220], [716, 221], [717, 222], [718, 223], [606, 242], [607, 243], [608, 244], [609, 245], [610, 246], [611, 247], [612, 248], [613, 249], [614, 250], [615, 251], [616, 252], [617, 253], [618, 254], [619, 255], [620, 256], [621, 257], [663, 258], [664, 259], [665, 260], [666, 261], [667, 262], [668, 263], [669, 264], [670, 265], [671, 266], [672, 267], [673, 268], [674, 269], [675, 270], [676, 271], [677, 272], [678, 273], [663, 687], [664, 688], [665, 689], [666, 690], [667, 691], [668, 692], [669, 693], [670, 694], [671, 695], [672, 696], [673, 697], [674, 698], [675, 699], [676, 700], [677, 701], [678, 702], [274, 735], [275, 736], [276, 737], [277, 738], [278, 739], [279, 740], [280, 741], [281, 742], [282, 743], [283, 744], [284, 745], [285, 746], [286, 747], [287, 748], [288, 749], [289, 750], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [765, 789], [766, 790]]
            }}
            ],
            components: {
            ul0: {type: 'custom-ul', pos: [1115, 525], in: ['0-7', '765-772', '805-822'], out: '823-838'},
            al0: {type: 'custom-al', pos: [1195, 1105], in: '8-41', out: '42-57'},
            in0: {type: 'in', pos: [205, 200], id: '62-77', name: 'A', bits: 16, val: '0000000000000100'},
            in1: {type: 'in', pos: [220, 1185], id: '82-97', name: 'B', bits: 16, val: '0000000000000001'},
            mux1: {type: 'mux', pos: [1475, 615], in: ['58-61', '78-81', '108-114', '136-153'], out: '154-169', from: 32, to: 16},
            in3: {type: 'in', pos: [1470, 80], orient: 's', id: 170, name: 'u'},
            pass2: {type: 'pass', pos: [1285, 700], in: '179-194', out: '195-210', bits: 16, slant: 'up'},
            pass3: {type: 'pass', pos: [1265, 1105], in: '219-234', out: '235-250', bits: 16, slant: 'up'},
            mux0: {type: 'mux', pos: [715, 280], in: '258-290', out: '291-306', from: 32, to: 16},
            mux2: {type: 'mux', pos: [660, 1265], in: ['122-135', '171-178', '211-218', '251-253'], out: ['254-257', '307-318'], from: 32, to: 16},
            pass4: {type: 'pass', pos: [460, 200], in: '327-342', out: '343-358', bits: 16, slant: 'down'},
            pass5: {type: 'pass', pos: [480, 1350], in: '367-382', out: '383-398', bits: 16, slant: 'down'},
            pass6: {type: 'pass', pos: [270, 1185], in: '407-422', out: '423-438', bits: 16, slant: 'up'},
            pass7: {type: 'pass', pos: [290, 365], in: '447-462', out: '463-478', bits: 16, slant: 'up'},
            in4: {type: 'in', pos: [660, 70], orient: 's', id: 479, name: 'sw'},
            pass9: {type: 'pass', pos: [720, 1265], in: '528-543', out: '544-559', bits: 16, slant: 'up'},
            pass11: {type: 'pass', pos: [740, 685], in: '608-623', out: '624-639', bits: 16, slant: 'up'},
            pass8: {type: 'pass', pos: [925, 945], in: ['320-326', '359-366', 399], out: ['400-406', '439-446', 480], bits: 16, slant: 'down'},
            pass10: {type: 'pass', pos: [905, 365], in: '481-496', out: '497-512', bits: 16, slant: 'down'},
            mux3: {type: 'mux', pos: [860, 365], in: [319, '513-527', '560-576'], out: '577-592', from: 32, to: 16},
            in5: {type: 'in', pos: [860, 75], orient: 's', id: 593, name: 'zx'},
            out0: {type: 'out', pos: [1555, 615], id: ['598-607', '640-645'], bits: 16, name: 'S'},
            in2: {type: 'in', pos: [1165, 95], orient: 's', id: 98, name: 'Op0'},
            in6: {type: 'in', pos: [1200, 95], orient: 's', id: 99, name: 'Op1'},
            },
            wires: [[170, 153], [823, 58], [824, 59], [825, 60], [826, 61], [827, 78], [828, 79], [829, 80], [830, 81], [831, 108], [832, 109], [833, 110], [834, 111], [835, 112], [836, 113], [837, 114], [838, 136], [195, 137], [196, 138], [197, 139], [198, 140], [199, 141], [200, 142], [201, 143], [202, 144], [203, 145], [204, 146], [205, 147], [206, 148], [207, 149], [208, 150], [209, 151], [210, 152], [235, 179], [236, 180], [237, 181], [238, 182], [239, 183], [240, 184], [241, 185], [242, 186], [243, 187], [244, 188], [245, 189], [246, 190], [247, 191], [248, 192], [249, 193], [250, 194], [42, 219], [43, 220], [44, 221], [45, 222], [46, 223], [47, 224], [48, 225], [49, 226], [50, 227], [51, 228], [52, 229], [53, 230], [54, 231], [55, 232], [56, 233], [57, 234], [62, 258], [63, 259], [64, 260], [65, 261], [66, 262], [67, 263], [68, 264], [69, 265], [70, 266], [71, 267], [72, 268], [73, 269], [74, 270], [75, 271], [76, 272], [77, 273], [62, 327], [63, 328], [64, 329], [65, 330], [66, 331], [67, 332], [68, 333], [69, 334], [70, 335], [71, 336], [72, 337], [73, 338], [74, 339], [75, 340], [76, 341], [77, 342], [343, 367], [344, 368], [345, 369], [346, 370], [347, 371], [348, 372], [349, 373], [350, 374], [351, 375], [352, 376], [353, 377], [354, 378], [355, 379], [356, 380], [357, 381], [358, 382], [383, 173], [384, 174], [385, 175], [386, 176], [387, 177], [388, 178], [389, 211], [390, 212], [391, 213], [392, 214], [393, 215], [394, 216], [395, 217], [396, 218], [397, 251], [398, 252], [82, 122], [83, 123], [84, 124], [85, 125], [86, 126], [87, 127], [88, 128], [89, 129], [90, 130], [91, 131], [92, 132], [93, 133], [94, 134], [95, 135], [96, 171], [97, 172], [82, 407], [83, 408], [84, 409], [85, 410], [86, 411], [87, 412], [88, 413], [89, 414], [90, 415], [91, 416], [92, 417], [93, 418], [94, 419], [95, 420], [96, 421], [97, 422], [423, 447], [424, 448], [425, 449], [426, 450], [427, 451], [428, 452], [429, 453], [430, 454], [431, 455], [432, 456], [433, 457], [434, 458], [435, 459], [436, 460], [437, 461], [438, 462], [463, 274], [464, 275], [465, 276], [466, 277], [467, 278], [468, 279], [469, 280], [470, 281], [471, 282], [472, 283], [473, 284], [474, 285], [475, 286], [476, 287], [477, 288], [478, 289], [479, 253], [479, 290], [254, 528], [254, 24], [255, 529], [255, 25], [256, 530], [256, 26], [257, 531], [257, 27], [307, 532], [307, 28], [308, 533], [308, 29], [309, 534], [309, 30], [310, 535], [310, 31], [311, 536], [311, 32], [312, 537], [312, 33], [313, 538], [313, 34], [314, 539], [314, 35], [315, 540], [315, 36], [316, 541], [316, 37], [317, 542], [317, 38], [318, 543], [318, 39], [544, 608], [545, 609], [546, 610], [547, 611], [548, 612], [549, 613], [550, 614], [551, 615], [552, 616], [553, 617], [554, 618], [555, 619], [556, 620], [557, 621], [558, 622], [559, 623], [624, 805], [625, 806], [626, 807], [627, 808], [628, 809], [629, 810], [630, 811], [631, 812], [632, 813], [633, 814], [634, 815], [635, 816], [636, 817], [637, 818], [638, 819], [639, 820], [400, 8], [401, 9], [402, 10], [403, 11], [404, 12], [405, 13], [406, 14], [439, 15], [440, 16], [441, 17], [442, 18], [443, 19], [444, 20], [445, 21], [446, 22], [480, 23], [497, 320], [498, 321], [499, 322], [500, 323], [501, 324], [502, 325], [503, 326], [504, 359], [505, 360], [506, 361], [507, 362], [508, 363], [509, 364], [510, 365], [511, 366], [512, 399], [291, 319], [292, 513], [293, 514], [294, 515], [295, 516], [296, 517], [297, 518], [298, 519], [299, 520], [300, 521], [301, 522], [302, 523], [303, 524], [304, 525], [305, 526], [306, 527], [577, 0], [578, 1], [579, 2], [580, 3], [581, 4], [582, 5], [583, 6], [584, 7], [585, 765], [586, 766], [587, 767], [588, 768], [589, 769], [590, 770], [591, 771], [592, 772], [577, 481], [578, 482], [579, 483], [580, 484], [581, 485], [583, 487], [584, 488], [586, 490], [588, 492], [589, 493], [593, 576], [154, 598], [155, 599], [156, 600], [157, 601], [158, 602], [159, 603], [160, 604], [161, 605], [162, 606], [163, 607], [164, 640], [165, 641], [166, 642], [167, 643], [168, 644], [169, 645], [98, 822], [99, 821, {via: [[1200, 190, 'w']]}], [99, 40], [98, 41]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    <iframe style="width: 100%; height: 517px; border: 0" src="https://logic.modulo-info.ch/?mode=tryout&data=N4NwXAbANAxg9gWwA5wHYFNUBcDOZgCWqADPlgJ5LpgDkRNUKeA2gBzFQCsnAulHACcCmLLRwMCAEzAB2AExRUAQwTUaAVwYglAGzABGAL5Qi+spTX1GcFnP0dufQcOxiJ0mQGZFKtQHkkYhpjIjlzKlorJjBmABZOKDtiPilaGQBOAFp02IZlVVoAQQYAIwJcA2htPRpiOvqGhvtgk1RPcMtUBmi4hM905JNpGnSs-X1cnwKaACFS8rx9Kt1aRrX65uM4dSxSYAoImm2sbpsYuViOTzqU4fGxz0myiqXjXXU958XodAAPUSwAnU6CgBzU71OLAmfRurRitUy+hkDBoMlimRkrBR6U42SxUBo41x43xhM8xMuNCcO1oE30iM46RaRFiHUiXWsUJkCUc-CEIjcQ0qHHy-iQ+mZqE4bLoHJ6+nJXF4fJcoho4iFS30UzFcmCxgA7gQBOgWMx5FAFek+ObvAqIDaMlBBmwOPobaxtXIPQpPB7vLEPbEuB6Eg6oGxoDIPTIoKwPawoNaI6x0paXQNLe6I+ltfpvTmFAqbek7YGc8H9MrmKNLeHmPY3dGI-ZtWibfYFNyO8RvDJ6-ZgzJmw3iAlMT3oDie7H0gPiIn0iP7Gn0vGW-Ys9Xxnmlh381nlwqs+uGxNLeSO7Fd8mG7Ei3IXRM7XYr5W5AW7wl836W7FoPm5Z3rG+bbrEib5gOsRpvmy6cG6cinlWeZyLeVZFtcHacHanjZg2nCVp4n5Vt+ni-vhAGPB2EBup4y4QHmnjxjwhhAA"></iframe>

    !!! warning "Attention"

        Cette ALU ne propose 8 opérations dans chaque mode (certaines différentes avec la réalisation précédente) et ne possède (directement) les fonctionnalités `sw` ni `zx`.

#### d. Condition (de saut)

Ce bloc vérifie les conditions suivantes sur le mot `A`:

- `lt`: `A` est strictement négatif,
- `eq`: `A` est nul,
- `gt`: `A` est strictement positif.

Une sortie `j` à `1` signifie qu'au moins une conditions, parmi celle(s)  demandée(s) est remplie (nous verrons que l'ordinateur effectuera alors un saut)

=== "Réalisation"

    <div style="width: 100%; height: 825px">
    <logic-editor mode="design">
        <script type="application/json">
        { // JSON5
            v: 6,
            defs: [
            {id: 'iszero', caption: 'isZero', circuit: {
                components: {
                or0: {type: 'or', pos: [305, 95], in: [39, 40], out: 41},
                or1: {type: 'or', pos: [305, 140], in: [42, 43], out: 44},
                or2: {type: 'or', pos: [305, 185], in: [45, 46], out: 47},
                or3: {type: 'or', pos: [305, 230], in: [48, 49], out: 50},
                or4: {type: 'or', pos: [305, 270], in: [51, 52], out: 53},
                or5: {type: 'or', pos: [305, 310], in: [54, 55], out: 56},
                or7: {type: 'or', pos: [305, 350], in: [60, 61], out: 62},
                or6: {type: 'or', pos: [305, 390], in: [57, 58], out: 59},
                or8: {type: 'or', pos: [410, 120], in: [63, 64], out: 65},
                or9: {type: 'or', pos: [410, 205], in: [66, 67], out: 68},
                or10: {type: 'or', pos: [410, 290], in: [69, 70], out: 71},
                or11: {type: 'or', pos: [405, 375], in: [72, 73], out: 74},
                or12: {type: 'or', pos: [510, 165], in: [75, 76], out: 77},
                or13: {type: 'or', pos: [510, 330], in: [78, 79], out: 80},
                or14: {type: 'or', pos: [610, 235], in: [81, 82], out: 83},
                not0: {type: 'not', pos: [690, 235], in: 84, out: 85},
                out1: {type: 'out', pos: [760, 235], id: 88, name: 'nul?'},
                in0: {type: 'in', pos: [185, 215], id: '4-19', name: 'A', bits: 16, val: '0000000000000010'},
                },
                wires: [[85, 88], [4, 39], [5, 40], [6, 42], [7, 43], [8, 45], [9, 46], [10, 48], [11, 49], [12, 51], [13, 52], [14, 54], [15, 55], [16, 60], [17, 61], [18, 57], [19, 58], [41, 63], [44, 64], [47, 66], [50, 67], [53, 69], [56, 70], [62, 72], [59, 73], [65, 75], [68, 76], [71, 78], [74, 79], [77, 81], [80, 82], [83, 84]]
            }},
            {id: 'isneg', caption: 'isNeg', circuit: {
                components: {
                out1: {type: 'out', pos: [570, 660], id: 118, name: 'Out0'},
                in0: {type: 'in', pos: [405, 635], id: '119-134', name: 'In0', bits: 16, val: '0000000001001010'},
                },
                wires: [[134, 118]]
            }}
            ],
            components: {
            in1: {type: 'in', pos: [50, 360], id: 20, name: 'lt'},
            in2: {type: 'in', pos: [50, 395], id: 21, name: 'eq', val: 1},
            in3: {type: 'in', pos: [70, 760], id: 22, name: 'gt'},
            out0: {type: 'out', pos: [905, 510], id: 23, name: 'j'},
            iszero0: {type: 'custom-iszero', pos: [400, 575], in: '89-104', out: 105},
            isneg0: {type: 'custom-isneg', pos: [395, 180], in: '135-150', out: 151},
            in0: {type: 'in', pos: [50, 130], id: '276-291', name: 'A', bits: 16, val: '0000000000000010'},
            }
        }
        </script>
    </logic-editor>
    </div>

=== "Solution"

    <div style="width: 100%; height: 788px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            defs: [
            {id: 'iszero', caption: 'isZero', circuit: {
                components: {
                or0: {type: 'or', pos: [305, 95], in: [39, 40], out: 41},
                or1: {type: 'or', pos: [305, 140], in: [42, 43], out: 44},
                or2: {type: 'or', pos: [305, 185], in: [45, 46], out: 47},
                or3: {type: 'or', pos: [305, 230], in: [48, 49], out: 50},
                or4: {type: 'or', pos: [305, 270], in: [51, 52], out: 53},
                or5: {type: 'or', pos: [305, 310], in: [54, 55], out: 56},
                or7: {type: 'or', pos: [305, 350], in: [60, 61], out: 62},
                or6: {type: 'or', pos: [305, 390], in: [57, 58], out: 59},
                or8: {type: 'or', pos: [410, 120], in: [63, 64], out: 65},
                or9: {type: 'or', pos: [410, 205], in: [66, 67], out: 68},
                or10: {type: 'or', pos: [410, 290], in: [69, 70], out: 71},
                or11: {type: 'or', pos: [405, 375], in: [72, 73], out: 74},
                or12: {type: 'or', pos: [510, 165], in: [75, 76], out: 77},
                or13: {type: 'or', pos: [510, 330], in: [78, 79], out: 80},
                or14: {type: 'or', pos: [610, 235], in: [81, 82], out: 83},
                not0: {type: 'not', pos: [690, 235], in: 84, out: 85},
                out1: {type: 'out', pos: [760, 235], id: 88, name: 'nul?'},
                in0: {type: 'in', pos: [185, 215], id: '4-19', name: 'A', bits: 16, val: '0000000000000010'},
                },
                wires: [[85, 88], [4, 39], [5, 40], [6, 42], [7, 43], [8, 45], [9, 46], [10, 48], [11, 49], [12, 51], [13, 52], [14, 54], [15, 55], [16, 60], [17, 61], [18, 57], [19, 58], [41, 63], [44, 64], [47, 66], [50, 67], [53, 69], [56, 70], [62, 72], [59, 73], [65, 75], [68, 76], [71, 78], [74, 79], [77, 81], [80, 82], [83, 84]]
            }},
            {id: 'isneg', caption: 'isNeg', circuit: {
                components: {
                out1: {type: 'out', pos: [570, 660], id: 118, name: 'Out0'},
                in0: {type: 'in', pos: [405, 635], id: '119-134', name: 'In0', bits: 16, val: '0000000001001010'},
                },
                wires: [[134, 118]]
            }}
            ],
            components: {
            in1: {type: 'in', pos: [50, 360], id: 20, name: 'lt'},
            in2: {type: 'in', pos: [50, 395], id: 21, name: 'eq', val: 1},
            in3: {type: 'in', pos: [70, 760], id: 22, name: 'gt'},
            out0: {type: 'out', pos: [905, 510], id: 23, name: 'j'},
            iszero0: {type: 'custom-iszero', pos: [400, 575], in: '89-104', out: 105},
            isneg0: {type: 'custom-isneg', pos: [395, 180], in: '135-150', out: 151},
            in0: {type: 'in', pos: [120, 180], id: '156-171', name: 'A', bits: 16},
            pass0: {type: 'pass', pos: [165, 180], in: '180-195', out: '196-211', bits: 16, slant: 'down'},
            pass1: {type: 'pass', pos: [185, 575], in: '220-235', out: '236-251', bits: 16, slant: 'down'},
            and0: {type: 'and', pos: [550, 350], in: [252, 253], out: 254},
            and1: {type: 'and', pos: [550, 405], in: [255, 256], out: 257},
            or0: {type: 'or', pos: [655, 375], in: [258, 259], out: 260},
            nor0: {type: 'or', pos: [545, 565], in: [261, 262], out: 263},
            not0: {type: 'not', pos: [625, 565], in: 264, out: 265},
            and2: {type: 'and', pos: [715, 750], in: [266, 267], out: 268},
            or1: {type: 'or', pos: [835, 510], in: [269, 270], out: 271},
            },
            wires: [[156, 135], [157, 136], [158, 137], [159, 138], [160, 139], [161, 140], [162, 141], [163, 142], [164, 143], [165, 144], [166, 145], [167, 146], [168, 147], [169, 148], [170, 149], [171, 150], [236, 89], [237, 90], [238, 91], [239, 92], [240, 93], [241, 94], [242, 95], [243, 96], [244, 97], [245, 98], [246, 99], [247, 100], [248, 101], [249, 102], [250, 103], [251, 104], [156, 180], [157, 181], [158, 182], [159, 183], [160, 184], [161, 185], [162, 186], [163, 187], [164, 188], [165, 189], [166, 190], [167, 191], [168, 192], [169, 193], [170, 194], [171, 195], [196, 220], [197, 221], [198, 222], [199, 223], [200, 224], [201, 225], [202, 226], [203, 227], [204, 228], [205, 229], [206, 230], [207, 231], [208, 232], [209, 233], [210, 234], [211, 235], [20, 253], [21, 255], [151, 252], [105, 256], [254, 258], [257, 259], [151, 261], [105, 262], [263, 264], [265, 266], [22, 267], [260, 269], [268, 270], [271, 23]]
        }
        </script>
    </logic-editor>
    </div>

### 5. Mémoire

#### a. Bascule

Lorsque `st=1` (*store*), le sélecteur recopie la donnée `d`. La valeur est mémorisée en rebouclant la sortie sur l'entrée sélectionnée par `st=0`.

=== "Solution"

    <div style="width: 100%; height: 163px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            components: {
            in0: {type: 'in', pos: [105, 135], orient: 'n', id: 18, name: 'st'},
            in1: {type: 'in', pos: [40, 85], id: 19, name: 'd'},
            pass1: {type: 'pass', pos: [125, 35], orient: 'n', in: 2, out: 3},
            pass2: {type: 'pass', pos: [80, 25], orient: 'w', in: 4, out: 5},
            pass3: {type: 'pass', pos: [70, 55], orient: 's', in: 6, out: 7},
            out0: {type: 'out', pos: [170, 75], id: 8},
            mux0: {type: 'mux', pos: [105, 75], in: '9-11', out: 12, from: 2, to: 1, bottom: true},
            },
            wires: [[18, 11], [19, 10], [3, 4], [5, 6], [7, 9], [12, 2], [12, 8]]
        }
        </script>
    </logic-editor>
    </div>

#### b. Bascule flip-flop

La donnée est mémorisée lorsque `st=1` et que survient un front montant de l'horloge (passage de 0 à 1).

Quand `st=1`:

- lorsque `clk=0`, une première bascule (intermédiaire) mémorise la donnée.
- lorsque `clk=1`, la deuxième bascule (finale) mémorise la sortie de la première bascule.

=== "Solution"

    <div style="width: 100%; height: 318px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            defs: [
            {id: 'bascule', caption: 'bascule', circuit: {
                components: {
                mux0: {type: 'mux', pos: [105, 75], in: '9-11', out: 12, from: 2, to: 1, bottom: true},
                pass1: {type: 'pass', pos: [125, 35], orient: 'n', in: 2, out: 3},
                pass2: {type: 'pass', pos: [80, 25], orient: 'w', in: 4, out: 5},
                pass3: {type: 'pass', pos: [70, 55], orient: 's', in: 6, out: 7},
                out0: {type: 'out', pos: [170, 75], id: 8, name: 'Out0'},
                in1: {type: 'in', pos: [40, 85], id: 19, name: 'd'},
                in0: {type: 'in', pos: [105, 135], orient: 'n', id: 18, name: 'st'},
                },
                wires: [[12, 8], [7, 9], [19, 10], [18, 11], [12, 2], [3, 4], [5, 6]]
            }}
            ],
            components: {
            bascule0: {type: 'custom-bascule', pos: [470, 70], in: [15, 16], out: 17},
            bascule1: {type: 'custom-bascule', pos: [315, 70], in: [0, 1], out: 2},
            in0: {type: 'in', pos: [70, 70], id: 3, name: 'd', val: 1},
            in1: {type: 'in', pos: [105, 280], orient: 'n', id: 4, name: 'clk'},
            not0: {type: 'not', pos: [145, 205], in: 5, out: 6},
            in2: {type: 'in', pos: [70, 145], id: 7, name: 'st'},
            and0: {type: 'and', pos: [225, 195], in: [8, 9], out: 10},
            and1: {type: 'and', pos: [400, 240], in: [11, 12], out: 13},
            out0: {type: 'out', pos: [600, 70], id: 14},
            },
            wires: [[4, 5], [3, 0], [6, 9], [7, 8, {via: [[185, 145]]}], [10, 1, {via: [[315, 195, 'n']]}], [4, 12], [7, 11, {via: [[360, 145]]}], [13, 16, {via: [[470, 240, 'n']]}], [2, 15], [17, 14]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    Les entrées `Pre` et `Clr` permettent de forcer la sortie `Q` respectivement à `1` ou `0`.

    <iframe style="width: 100%; height: 230px; border: 0" src="https://logic.modulo-info.ch/?mode=tryout&data=N4NwXAbANAxg9gWwA5wHYFNUBcDOZgBmBADPlgJ5LpgDkRAtACY1Qp4DaAjACwCsUnTrwC6UAJapaxegGYWcAK5Yw7blBEBfcalLAKVWhJZsVAdn6cZI8Y0hRUAQwTUaAYQA2cGAGsWYnAAKCjgAFgBCSlhoYFgATgroWhKcZJQuRqxwHDwWABzWcLFimMo0qH62pvZOLh6xfoHB4ZHRcQlJqABMqQY0GSZcfFB8ooXF2LQ4FWC51c60AbHoDUGhEVhRkm2J2jI96eWZHOZQAJzWYrancy4AIjRaili6+i5PxlkqnUJnF7aCN1oAEUHlAnik9GlaO8jl8fpY-mBOJ1ATQgYBQYAeWgA7mIlhx2NBiKJ2FVOiTZpwSdcZCS1IISRZycINEA"></iframe>

#### c. Registre

Les bascules flip-flop étant synchronisées par l'horloge, il est possible de mémoriser plusieurs bits en même temps et obtenir un registre.

*(ici un registre 2 bits)*

=== "Solution"

    <div style="width: 100%; height: 263px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            defs: [
            {id: 'bascule', caption: 'bascule', circuit: {
                components: {
                mux0: {type: 'mux', pos: [105, 75], in: '9-11', out: 12, from: 2, to: 1, bottom: true},
                pass1: {type: 'pass', pos: [125, 35], orient: 'n', in: 2, out: 3},
                pass2: {type: 'pass', pos: [80, 25], orient: 'w', in: 4, out: 5},
                pass3: {type: 'pass', pos: [70, 55], orient: 's', in: 6, out: 7},
                out0: {type: 'out', pos: [170, 75], id: 8, name: 'Out0'},
                in1: {type: 'in', pos: [40, 85], id: 19, name: 'd'},
                in0: {type: 'in', pos: [105, 135], orient: 'n', id: 18, name: 'st'},
                },
                wires: [[12, 8], [7, 9], [19, 10], [18, 11], [12, 2], [3, 4], [5, 6]]
            }},
            {id: 'bff', caption: 'b-ff', circuit: {
                components: {
                bascule1: {type: 'custom-bascule', pos: [315, 70], in: [0, 1], out: 2},
                not0: {type: 'not', pos: [145, 205], in: 5, out: 6},
                and1: {type: 'and', pos: [400, 240], in: [11, 12], out: 13},
                and0: {type: 'and', pos: [225, 195], in: [8, 9], out: 10},
                bascule0: {type: 'custom-bascule', pos: [470, 70], in: [15, 16], out: 17},
                out0: {type: 'out', pos: [600, 70], id: 14, name: 'Out0'},
                in0: {type: 'in', pos: [70, 70], id: 3, name: 'd', val: 1},
                in2: {type: 'in', pos: [70, 145], id: 7, name: 'st'},
                in1: {type: 'in', pos: [105, 280], orient: 'n', id: 4, name: 'clk'},
                },
                wires: [[17, 14], [3, 0], [10, 1, {via: [[315, 195, 'n']]}], [4, 5], [7, 11, {via: [[360, 140]]}], [4, 12], [7, 8, {via: [[185, 145]]}], [6, 9], [2, 15], [13, 16, {via: [[470, 240, 'n']]}]]
            }}
            ],
            components: {
            bff0: {type: 'custom-bff', pos: [270, 55], in: '18-20', out: 21},
            bff1: {type: 'custom-bff', pos: [270, 155], in: '0-2', out: 3},
            in0: {type: 'in', pos: [75, 55], id: [5, 6], name: 'd', bits: 2},
            in1: {type: 'in', pos: [60, 165], id: 4, name: 'st', val: 1},
            in2: {type: 'in', pos: [205, 225], orient: 'n', id: 7, name: 'clk', val: 1},
            out0: {type: 'out', pos: [390, 65], id: [9, 10], bits: 2},
            },
            wires: [[5, 18], [6, 0, {via: [[140, 65], [140, 145]]}], [4, 1], [4, 19, {via: [[185, 165], [185, 65, 'n']]}], [7, 2], [7, 20, {via: [[205, 95, 'n']]}], [21, 9], [3, 10, {via: [[310, 75, 'n']]}]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    Les entrées `Pre` et `Clr` permettent de forcer la sortie `Q` respectivement à `11` ou `00`.

    <iframe style="width: 100%; height: 290px; border: 0" src="https://logic.modulo-info.ch/?mode=tryout&data=N4NwXAbANAxg9gWwA5wHYFNUBcDOZgBO6A5gAz4BGAlrmAExRYCeS6YA5Ece1CngNoBGCKSiCALAFYAulCqoOdcQFo6ADh5wArljD86ATigBmUtIC+c1OWDNWHeTz57hoyTKhwCVTLvY4eKgATMGNBKFQAQwQ2dgAFIkCcOK0cAAsAIR0sNDAsAi10S3lBfDtYx144AVcoOmMPLx9sDlRAkOMGKJiOAGEAGwIklPSsrByFfMLi1Doylgq2qoEDUQkPYL1jAHYTNVlu2IARHmpaOhnjeft2Sud+NTW1DY6jQ77+uBgAa2HUzOyuSmRU8Ohs5Q42iwTmqenqayksk2-HE0HE21kZzwXWisQAiuxzJYAO5UIgCfhhOoeSkMOgQWSU3Z0DFQSlqOr7NnGIxKRmGKCoxmmQUYixAA"></iframe>

    !!! warning "Attention"

        Ce symbole ne possède pas de bit d'écriture `st`.

#### d. Compteur

Il s'agit d'un mot qui s'incrémente à chaque front montant de l'horloge. L'entrée `st` permet de charger un mot `X` de départ.

=== "Solution"

    En utilisant le registre (16 bit) et le circuit d'incrémentation construits précédemment.

    <div style="width: 100%; height: 648px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            defs: [
            {id: 'bascule', caption: 'bascule', circuit: {
                components: {
                mux0: {type: 'mux', pos: [105, 75], in: '9-11', out: 12, from: 2, to: 1, bottom: true},
                pass1: {type: 'pass', pos: [125, 35], orient: 'n', in: 2, out: 3},
                pass2: {type: 'pass', pos: [80, 25], orient: 'w', in: 4, out: 5},
                pass3: {type: 'pass', pos: [70, 55], orient: 's', in: 6, out: 7},
                out0: {type: 'out', pos: [170, 75], id: 8, name: 'Out0'},
                in1: {type: 'in', pos: [40, 85], id: 19, name: 'd'},
                in0: {type: 'in', pos: [105, 135], orient: 'n', id: 18, name: 'st'},
                },
                wires: [[12, 8], [7, 9], [19, 10], [18, 11], [12, 2], [3, 4], [5, 6]]
            }},
            {id: 'bff', caption: 'b-ff', circuit: {
                components: {
                bascule1: {type: 'custom-bascule', pos: [315, 70], in: [0, 1], out: 2},
                not0: {type: 'not', pos: [145, 205], in: 5, out: 6},
                and1: {type: 'and', pos: [400, 240], in: [11, 12], out: 13},
                and0: {type: 'and', pos: [225, 195], in: [8, 9], out: 10},
                bascule0: {type: 'custom-bascule', pos: [470, 70], in: [15, 16], out: 17},
                out0: {type: 'out', pos: [600, 70], id: 14, name: 'Out0'},
                in0: {type: 'in', pos: [70, 70], id: 3, name: 'd', val: 1},
                in2: {type: 'in', pos: [70, 145], id: 7, name: 'st'},
                in1: {type: 'in', pos: [105, 280], orient: 'n', id: 4, name: 'clk'},
                },
                wires: [[17, 14], [3, 0], [10, 1, {via: [[315, 195, 'n']]}], [4, 5], [7, 11, {via: [[360, 140]]}], [4, 12], [7, 8, {via: [[185, 145]]}], [6, 9], [2, 15], [13, 16, {via: [[470, 240, 'n']]}]]
            }},
            {id: 'reg16', caption: 'Reg16', circuit: {
                components: {
                bff0: {type: 'custom-bff', pos: [670, 55], in: '18-20', out: 21},
                bff1: {type: 'custom-bff', pos: [670, 135], in: '0-2', out: 3},
                bff2: {type: 'custom-bff', pos: [670, 215], in: '4-6', out: 7},
                bff3: {type: 'custom-bff', pos: [670, 295], in: '8-10', out: 11},
                bff4: {type: 'custom-bff', pos: [670, 375], in: '12-14', out: 15},
                bff5: {type: 'custom-bff', pos: [670, 455], in: [16, 17, 22], out: 23},
                bff6: {type: 'custom-bff', pos: [670, 535], in: '24-26', out: 27},
                bff7: {type: 'custom-bff', pos: [670, 615], in: '28-30', out: 31},
                bff8: {type: 'custom-bff', pos: [670, 695], in: '32-34', out: 35},
                bff9: {type: 'custom-bff', pos: [670, 775], in: '36-38', out: 39},
                bff10: {type: 'custom-bff', pos: [670, 855], in: '40-42', out: 43},
                bff11: {type: 'custom-bff', pos: [670, 935], in: '44-46', out: 47},
                bff12: {type: 'custom-bff', pos: [670, 1015], in: '48-50', out: 51},
                bff13: {type: 'custom-bff', pos: [670, 1095], in: '52-54', out: 55},
                bff14: {type: 'custom-bff', pos: [670, 1175], in: '56-58', out: 59},
                bff15: {type: 'custom-bff', pos: [670, 1255], in: '60-62', out: 63},
                out0: {type: 'out', pos: [915, 565], id: [64, 65, '86-99'], bits: 16, name: 'Out0'},
                in0: {type: 'in', pos: [435, 565], id: '70-85', name: 'd', bits: 16, val: '0000000000000010'},
                in1: {type: 'in', pos: [395, 980], id: 66, name: 'st', val: 1},
                in2: {type: 'in', pos: [600, 1370], orient: 'n', id: 67, name: 'clk', val: 1},
                },
                wires: [[21, 64], [3, 65], [7, 86], [11, 87], [15, 88], [23, 89], [27, 90], [31, 91], [35, 92], [39, 93], [43, 94], [47, 95], [51, 96], [55, 97], [59, 98], [63, 99], [70, 18], [66, 19], [67, 20, {via: [[600, 95, 'n']]}], [71, 0], [66, 1], [67, 2, {via: [[600, 175, 'n']]}], [72, 4], [66, 5], [67, 6, {via: [[600, 255, 'n']]}], [73, 8], [66, 9], [67, 10, {via: [[600, 335, 'n']]}], [74, 12], [66, 13], [67, 14, {via: [[600, 415, 'n']]}], [75, 16], [66, 17], [67, 22, {via: [[600, 495, 'n']]}], [76, 24], [66, 25], [67, 26, {via: [[600, 575, 'n']]}], [77, 28], [66, 29], [67, 30, {via: [[600, 655, 'n']]}], [78, 32], [66, 33], [67, 34, {via: [[600, 735, 'n']]}], [79, 36], [66, 37], [67, 38, {via: [[600, 815, 'n']]}], [80, 40], [66, 41], [67, 42, {via: [[600, 895, 'n']]}], [81, 44], [66, 45], [67, 46, {via: [[600, 975, 'n']]}], [82, 48], [66, 49], [67, 50, {via: [[600, 1055, 'n']]}], [83, 52], [66, 53], [67, 54, {via: [[600, 1135, 'n']]}], [84, 56], [66, 57], [67, 58, {via: [[600, 1215, 'n']]}], [85, 60], [66, 61], [67, 62, {via: [[600, 1295]]}]]
            }},
            {id: 'inc16', caption: 'Inc16', circuit: {
                components: {
                in0: {type: 'in', pos: [300, 335], id: 223, name: 'In0', val: 1, isConstant: true},
                adder0: {type: 'adder-array', pos: [360, 320], in: '147-179', out: '180-196', bits: 16},
                out0: {type: 'out', pos: [470, 320], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [235, 230], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                },
                wires: [[180, 117], [181, 118], [182, 119], [183, 120], [184, 121], [185, 122], [186, 123], [187, 124], [188, 125], [189, 126], [190, 127], [191, 128], [192, 129], [193, 130], [194, 131], [195, 132], [97, 147], [98, 148], [99, 149], [100, 150], [101, 151], [102, 152], [103, 153], [104, 154], [105, 155], [106, 156], [107, 157], [108, 158], [109, 159], [110, 160], [111, 161], [112, 162], [223, 163]]
            }}
            ],
            components: {
            reg160: {type: 'custom-reg16', pos: [360, 370], in: '100-117', out: '118-133'},
            inc160: {type: 'custom-inc16', pos: [510, 370], in: '0-15', out: '16-31'},
            pass0: {type: 'pass', pos: [580, 370], in: '137-152', out: '153-168', bits: 16, slant: 'up'},
            pass1: {type: 'pass', pos: [730, 100], orient: 'w', in: '177-192', out: '193-208', bits: 16, slant: 'down'},
            pass2: {type: 'pass', pos: [215, 100], orient: 'w', in: '217-232', out: '233-248', bits: 16, slant: 'up'},
            pass3: {type: 'pass', pos: [130, 190], orient: 's', in: '257-272', out: '273-288', bits: 16, slant: 'up'},
            mux0: {type: 'mux', pos: [255, 360], in: ['32-45', '95-99', '134-136', '169-176', '209-211'], out: ['212-216', '249-256', '289-291'], from: 32, to: 16, bottom: true},
            in0: {type: 'in', pos: [55, 580], id: 292, name: 'st'},
            in1: {type: 'in', pos: [60, 445], id: '294-309', name: 'X', bits: 16, val: '0000000000000001'},
            in2: {type: 'in', pos: [360, 615], orient: 'n', id: 46, name: 'clk'},
            in3: {type: 'in', pos: [285, 530], id: 47, val: 1, isConstant: true},
            out0: {type: 'out', pos: [810, 370], id: '52-67', bits: 16, name: 'Out'},
            },
            wires: [[118, 0], [119, 1], [120, 2], [121, 3], [122, 4], [123, 5], [124, 6], [125, 7], [126, 8], [127, 9], [128, 10], [129, 11], [130, 12], [131, 13], [132, 14], [133, 15], [16, 137], [17, 138], [18, 139], [19, 140], [20, 141], [21, 142], [22, 143], [23, 144], [24, 145], [25, 146], [26, 147], [27, 148], [28, 149], [29, 150], [30, 151], [31, 152], [153, 177], [154, 178], [155, 179], [156, 180], [157, 181], [158, 182], [159, 183], [160, 184], [161, 185], [162, 186], [163, 187], [164, 188], [165, 189], [166, 190], [167, 191], [168, 192], [193, 217], [194, 218], [195, 219], [196, 220], [197, 221], [198, 222], [199, 223], [200, 224], [201, 225], [202, 226], [203, 227], [204, 228], [205, 229], [206, 230], [207, 231], [208, 232], [233, 257], [234, 258], [235, 259], [236, 260], [237, 261], [238, 262], [239, 263], [240, 264], [241, 265], [242, 266], [243, 267], [244, 268], [245, 269], [246, 270], [247, 271], [248, 272], [273, 32], [274, 33], [275, 34], [276, 35], [277, 36], [278, 37], [279, 38], [280, 39], [281, 40], [282, 41], [283, 42], [284, 43], [285, 44], [286, 45], [287, 95], [288, 96], [292, 211, {via: [[255, 580]]}], [294, 97], [295, 98], [296, 99], [297, 134], [298, 135], [299, 136], [300, 169], [301, 170], [302, 171], [303, 172], [304, 173], [305, 174], [306, 175], [307, 176], [308, 209], [309, 210], [212, 100], [213, 101], [214, 102], [215, 103], [216, 104], [249, 105], [250, 106], [251, 107], [252, 108], [253, 109], [254, 110], [255, 111], [256, 112], [289, 113], [290, 114], [291, 115], [46, 117], [47, 116], [16, 52], [17, 53], [18, 54], [19, 55], [20, 56], [21, 57], [22, 58], [23, 59], [24, 60], [25, 61], [26, 62], [27, 63], [28, 64], [29, 65], [30, 66], [31, 67]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    L'entrée `Clr` remet le compteur à 0 et la sortie `V` (*overflow*) indique un dépassement de capacité.

    <iframe style="width: 100%; height: 278px; border: 0" src="https://logic.modulo-info.ch/?mode=tryout&data=N4NwXAbANAxg9gWwA5wHYFNUBcDOZjwCu26ATgAz5YCeS6YA5ESaQ1CngNoCMALAKxRu3fgF0oAS1RhO-Qfwji4hLI34B2ALTqAzGwBGE3GG4QAvlGVZKwGnUZW2HGQCZu5IeTGSAJo3UAHJoAnHpQhsamFlbcVLT0DI7scFwuvB4uLt4SfsG8UKgAhggJAGoMFlI2dglSTikyfIJp5EqkEpiqDKhsOWDBgkUljADCADaskjgACoQ4ABYAQipYaGBYpITolaixtvGMdclc6h7cwa2+-dBDCeNwMADWvTNzSytrG1tmFgDuEqR0FxZOooIFxLIAmDghD+MEoAFLpwIB4AtwIRBuAiXBiXAidBidAjeBj8gFvMjBAFFFBkdAAuoMaCAgEMVCAjDaRB4RcIacoMF0bT1FjgjjhXjQnyiXkIQMoAo5dA5KJRGYgA"></iframe>

#### e. RAM

Il s'agit de *registres* de 16 bits adressables et inscriptibles.

(ici une RAM de 2 mots)

=== "Solution"

    Le bit `ad` indique le registre à lire ou écrire. Le bit `st` autorise l'écriture. Toutes les opérations sont synchronisés par l'horloge `clk`.

    <div style="width: 100%; height: 950px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            defs: [
            {id: 'bascule', caption: 'bascule', circuit: {
                components: {
                mux0: {type: 'mux', pos: [105, 75], in: '9-11', out: 12, from: 2, to: 1, bottom: true},
                pass1: {type: 'pass', pos: [125, 35], orient: 'n', in: 2, out: 3},
                pass2: {type: 'pass', pos: [80, 25], orient: 'w', in: 4, out: 5},
                pass3: {type: 'pass', pos: [70, 55], orient: 's', in: 6, out: 7},
                out0: {type: 'out', pos: [170, 75], id: 8, name: 'Out0'},
                in1: {type: 'in', pos: [40, 85], id: 19, name: 'd'},
                in0: {type: 'in', pos: [105, 135], orient: 'n', id: 18, name: 'st'},
                },
                wires: [[12, 8], [7, 9], [19, 10], [18, 11], [12, 2], [3, 4], [5, 6]]
            }},
            {id: 'bff', caption: 'b-ff', circuit: {
                components: {
                bascule1: {type: 'custom-bascule', pos: [315, 70], in: [0, 1], out: 2},
                not0: {type: 'not', pos: [145, 205], in: 5, out: 6},
                and1: {type: 'and', pos: [400, 240], in: [11, 12], out: 13},
                and0: {type: 'and', pos: [225, 195], in: [8, 9], out: 10},
                bascule0: {type: 'custom-bascule', pos: [470, 70], in: [15, 16], out: 17},
                out0: {type: 'out', pos: [600, 70], id: 14, name: 'Out0'},
                in0: {type: 'in', pos: [70, 70], id: 3, name: 'd', val: 1},
                in2: {type: 'in', pos: [70, 145], id: 7, name: 'st'},
                in1: {type: 'in', pos: [105, 280], orient: 'n', id: 4, name: 'clk'},
                },
                wires: [[17, 14], [3, 0], [10, 1, {via: [[315, 195, 'n']]}], [4, 5], [7, 11, {via: [[360, 140]]}], [4, 12], [7, 8, {via: [[185, 145]]}], [6, 9], [2, 15], [13, 16, {via: [[470, 240, 'n']]}]]
            }},
            {id: 'reg16', caption: 'Reg16', circuit: {
                components: {
                bff0: {type: 'custom-bff', pos: [670, 55], in: '18-20', out: 21},
                bff1: {type: 'custom-bff', pos: [670, 135], in: '0-2', out: 3},
                bff2: {type: 'custom-bff', pos: [670, 215], in: '4-6', out: 7},
                bff3: {type: 'custom-bff', pos: [670, 295], in: '8-10', out: 11},
                bff4: {type: 'custom-bff', pos: [670, 375], in: '12-14', out: 15},
                bff5: {type: 'custom-bff', pos: [670, 455], in: [16, 17, 22], out: 23},
                bff6: {type: 'custom-bff', pos: [670, 535], in: '24-26', out: 27},
                bff7: {type: 'custom-bff', pos: [670, 615], in: '28-30', out: 31},
                bff8: {type: 'custom-bff', pos: [670, 695], in: '32-34', out: 35},
                bff9: {type: 'custom-bff', pos: [670, 775], in: '36-38', out: 39},
                bff10: {type: 'custom-bff', pos: [670, 855], in: '40-42', out: 43},
                bff11: {type: 'custom-bff', pos: [670, 935], in: '44-46', out: 47},
                bff12: {type: 'custom-bff', pos: [670, 1015], in: '48-50', out: 51},
                bff13: {type: 'custom-bff', pos: [670, 1095], in: '52-54', out: 55},
                bff14: {type: 'custom-bff', pos: [670, 1175], in: '56-58', out: 59},
                bff15: {type: 'custom-bff', pos: [670, 1255], in: '60-62', out: 63},
                out0: {type: 'out', pos: [915, 565], id: [64, 65, '86-99'], bits: 16, name: 'Out0'},
                in0: {type: 'in', pos: [435, 565], id: '70-85', name: 'd', bits: 16, val: '0000000000000010'},
                in1: {type: 'in', pos: [395, 980], id: 66, name: 'st', val: 1},
                in2: {type: 'in', pos: [600, 1370], orient: 'n', id: 67, name: 'clk'},
                },
                wires: [[21, 64], [3, 65], [7, 86], [11, 87], [15, 88], [23, 89], [27, 90], [31, 91], [35, 92], [39, 93], [43, 94], [47, 95], [51, 96], [55, 97], [59, 98], [63, 99], [70, 18], [66, 19], [67, 20, {via: [[600, 95, 'n']]}], [71, 0], [66, 1], [67, 2, {via: [[600, 175, 'n']]}], [72, 4], [66, 5], [67, 6, {via: [[600, 255, 'n']]}], [73, 8], [66, 9], [67, 10, {via: [[600, 335, 'n']]}], [74, 12], [66, 13], [67, 14, {via: [[600, 415, 'n']]}], [75, 16], [66, 17], [67, 22, {via: [[600, 495, 'n']]}], [76, 24], [66, 25], [67, 26, {via: [[600, 575, 'n']]}], [77, 28], [66, 29], [67, 30, {via: [[600, 655, 'n']]}], [78, 32], [66, 33], [67, 34, {via: [[600, 735, 'n']]}], [79, 36], [66, 37], [67, 38, {via: [[600, 815, 'n']]}], [80, 40], [66, 41], [67, 42, {via: [[600, 895, 'n']]}], [81, 44], [66, 45], [67, 46, {via: [[600, 975, 'n']]}], [82, 48], [66, 49], [67, 50, {via: [[600, 1055, 'n']]}], [83, 52], [66, 53], [67, 54, {via: [[600, 1135, 'n']]}], [84, 56], [66, 57], [67, 58, {via: [[600, 1215, 'n']]}], [85, 60], [66, 61], [67, 62, {via: [[600, 1295]]}]]
            }},
            {id: 'inc16', caption: 'Inc16', circuit: {
                components: {
                in0: {type: 'in', pos: [300, 335], id: 223, name: 'In0', val: 1, isConstant: true},
                adder0: {type: 'adder-array', pos: [360, 320], in: '147-179', out: '180-196', bits: 16},
                out0: {type: 'out', pos: [470, 320], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [235, 230], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                },
                wires: [[180, 117], [181, 118], [182, 119], [183, 120], [184, 121], [185, 122], [186, 123], [187, 124], [188, 125], [189, 126], [190, 127], [191, 128], [192, 129], [193, 130], [194, 131], [195, 132], [97, 147], [98, 148], [99, 149], [100, 150], [101, 151], [102, 152], [103, 153], [104, 154], [105, 155], [106, 156], [107, 157], [108, 158], [109, 159], [110, 160], [111, 161], [112, 162], [223, 163]]
            }}
            ],
            components: {
            in0: {type: 'in', pos: [35, 180], id: '38-53', bits: 16, val: '0000000000000101'},
            out0: {type: 'out', pos: [730, 275], id: '55-70', bits: 16},
            in1: {type: 'in', pos: [315, 910], orient: 'n', id: 72, name: 'clk', val: 1},
            in2: {type: 'in', pos: [80, 845], id: 73, name: 'ad'},
            pass0: {type: 'pass', pos: [110, 180], in: '84-99', out: '134-149', bits: 16, slant: 'down'},
            pass1: {type: 'pass', pos: [130, 560], in: '158-173', out: '174-189', bits: 16, slant: 'down'},
            pass2: {type: 'pass', pos: [450, 570], in: '232-247', out: '248-263', bits: 16, slant: 'up'},
            pass3: {type: 'pass', pos: [470, 360], in: '272-287', out: '288-303', bits: 16, slant: 'up'},
            mux0: {type: 'mux', pos: [665, 275], in: ['34-37', 54, 71, 76, '224-231', '264-271', '304-313'], out: '314-329', from: 32, to: 16, bottom: true},
            reg160: {type: 'custom-reg16', pos: [375, 190], in: ['77-83', '150-157', '190-192'], out: '193-208'},
            reg161: {type: 'custom-reg16', pos: [375, 570], in: ['209-223', '330-332'], out: '333-348'},
            demux0: {type: 'demux', pos: [275, 720], in: [367, 368], out: [369, 370], from: 1, to: 2, bottom: true},
            in3: {type: 'in', pos: [80, 720], id: 371, name: 'st', val: 1},
            },
            wires: [[72, 332, {via: [[315, 755, 'n']]}], [72, 192, {via: [[315, 375, 'n']]}], [38, 84], [39, 85], [40, 86], [41, 87], [42, 88], [43, 89], [44, 90], [45, 91], [46, 92], [47, 93], [48, 94], [49, 95], [50, 96], [51, 97], [52, 98], [53, 99], [134, 158], [135, 159], [136, 160], [137, 161], [138, 162], [139, 163], [140, 164], [141, 165], [142, 166], [143, 167], [144, 168], [145, 169], [146, 170], [147, 171], [148, 172], [149, 173], [174, 209], [175, 210], [176, 211], [177, 212], [178, 213], [179, 214], [180, 215], [181, 216], [182, 217], [183, 218], [184, 219], [185, 220], [186, 221], [187, 222], [188, 223], [189, 330], [38, 77], [39, 78], [40, 79], [41, 80], [42, 81], [43, 82], [44, 83], [45, 150], [46, 151], [47, 152], [48, 153], [49, 154], [50, 155], [51, 156], [52, 157], [53, 190], [193, 34], [194, 35], [195, 36], [196, 37], [197, 54], [198, 71], [199, 76], [200, 224], [201, 225], [202, 226], [203, 227], [204, 228], [205, 229], [206, 230], [207, 231], [208, 264], [333, 232], [334, 233], [335, 234], [336, 235], [337, 236], [338, 237], [339, 238], [340, 239], [341, 240], [342, 241], [343, 242], [344, 243], [345, 244], [346, 245], [347, 246], [348, 247], [248, 272], [249, 273], [250, 274], [251, 275], [252, 276], [253, 277], [254, 278], [255, 279], [256, 280], [257, 281], [258, 282], [259, 283], [260, 284], [261, 285], [262, 286], [263, 287], [288, 265], [289, 266], [290, 267], [291, 268], [292, 269], [293, 270], [294, 271], [295, 304], [296, 305], [297, 306], [298, 307], [299, 308], [300, 309], [301, 310], [302, 311], [303, 312], [314, 55], [315, 56], [316, 57], [317, 58], [318, 59], [319, 60], [320, 61], [321, 62], [322, 63], [323, 64], [324, 65], [325, 66], [326, 67], [327, 68], [328, 69], [329, 70], [73, 313, {via: [[665, 845]]}], [73, 368, {via: [[205, 845], [275, 845, 'n']]}], [370, 331], [369, 191, {via: [[295, 350, 'n']]}], [371, 367]]
        }
        </script>
    </logic-editor>
    </div>

=== "Symbole"

    Pour une RAM de 4 mots de 16 bits. Le bit `clr` permet d'effacer toute la RAM.

    <iframe style="width: 100%; height: 363px; border: 0" src="https://logic.modulo-info.ch/?mode=tryout&data=N4NwXAbANAxg9gWwA5wHYFNUBcDOZgBOAhggAz5YCeS6YA5MQnVCngNoCMEpUHA7KQC6UAJap6AVgAsAWj5TmcAK5Z6fCTICcpZgCMRuMFygAbMejxSAvqNTlgVGvTHNWYNlJ78hogCb1NDQ4OHSh9Q2MQIhN6Ujj4hPiQnRtlLHtHWjo01zh2ACYADi8BYRF-OmCggGZQ8LwuGzEOCmoslxY89y4eT2E4AhFMVTocZnLu6oleaohhVBIsgEFfPQM8fKbUfNanOg63Nm4oavyfCY5qvigFhCyYEwBrcZwABSUcAAsAIRUsNDAWAISnQW2qu3aqFy7A4nhOIX6g2G9ChfiM1UKN0W9BwWDoWykEOcqMOHGK8J8AyG2BR438l00WLu9AeBBe7y+vyw-3EQJBVhsAHcRAQLO42IEoBI+MIJdAJIVZZprhJNErMdwlYyIBxZckoBB8nrSBwDdVjfkDVJjdUDRJjVIDXMoJxSNMIDKXSFoBBFV7SNcIGr-ZjSv7GXxdV6QlA+EaXepeBx7QnoMFnWw+Ndgp7M5jgn7M4zgsG2OSOGdZYVTRWo2XLRX42XbRXzS7Co6K9b29MKymy2n8hnCtn8rnCvmilXi-lS9oZj5OFNY22l9B5HqrlLu0vMRJ+wypXNBFYgA"></iframe>

!!! tip "ROM"

    Une ROM (*Read Only Memory*) peut être vue (en première approximation) comme une RAM où il n'est pas possible d'écrire (il faut néanmoins une méthode de préchargement du contenu).

### 6. Processeur

#### a. Mémoires combinées

Ce bloc gère l'écriture d'un mot de 16 bits dans dans la RAM (`✱A`) ou dans les 2 registres:

- `D`: pour le bus de données (première entrée de l'ALU)
- `A`: pour le bus d'adresses (deuxième entrée de l'ALU, en compétition avec la RAM)

De plus:

- Les bits `a`, `d` et `✱a` indiquent où écrire (resp. dans les registres `A` et/ou `D` et/ou dans la RAM `✱A`).
- Dans le cas de la RAM, l'adresse concernée est donnée par le registre `A`.

=== "Solution"

    <div style="width: 100%; height: 1038px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            defs: [
            {id: 'bascule', caption: 'bascule', circuit: {
                components: {
                mux0: {type: 'mux', pos: [105, 75], in: '9-11', out: 12, from: 2, to: 1, bottom: true},
                pass1: {type: 'pass', pos: [125, 35], orient: 'n', in: 2, out: 3},
                pass2: {type: 'pass', pos: [80, 25], orient: 'w', in: 4, out: 5},
                pass3: {type: 'pass', pos: [70, 55], orient: 's', in: 6, out: 7},
                out0: {type: 'out', pos: [170, 75], id: 8, name: 'Out0'},
                in1: {type: 'in', pos: [40, 85], id: 19, name: 'd'},
                in0: {type: 'in', pos: [105, 135], orient: 'n', id: 18, name: 'st'},
                },
                wires: [[12, 8], [7, 9], [19, 10], [18, 11], [12, 2], [3, 4], [5, 6]]
            }},
            {id: 'bff', caption: 'b-ff', circuit: {
                components: {
                bascule1: {type: 'custom-bascule', pos: [315, 70], in: [0, 1], out: 2},
                not0: {type: 'not', pos: [145, 205], in: 5, out: 6},
                and1: {type: 'and', pos: [400, 240], in: [11, 12], out: 13},
                and0: {type: 'and', pos: [225, 195], in: [8, 9], out: 10},
                bascule0: {type: 'custom-bascule', pos: [470, 70], in: [15, 16], out: 17},
                out0: {type: 'out', pos: [600, 70], id: 14, name: 'Out0'},
                in0: {type: 'in', pos: [70, 70], id: 3, name: 'd', val: 1},
                in2: {type: 'in', pos: [70, 145], id: 7, name: 'st'},
                in1: {type: 'in', pos: [105, 280], orient: 'n', id: 4, name: 'clk'},
                },
                wires: [[17, 14], [3, 0], [10, 1, {via: [[315, 195, 'n']]}], [4, 5], [7, 11, {via: [[360, 140]]}], [4, 12], [7, 8, {via: [[185, 145]]}], [6, 9], [2, 15], [13, 16, {via: [[470, 240, 'n']]}]]
            }},
            {id: 'reg16', caption: 'Reg16', circuit: {
                components: {
                bff0: {type: 'custom-bff', pos: [670, 55], in: '18-20', out: 21},
                bff1: {type: 'custom-bff', pos: [670, 135], in: '0-2', out: 3},
                bff2: {type: 'custom-bff', pos: [670, 215], in: '4-6', out: 7},
                bff3: {type: 'custom-bff', pos: [670, 295], in: '8-10', out: 11},
                bff4: {type: 'custom-bff', pos: [670, 375], in: '12-14', out: 15},
                bff5: {type: 'custom-bff', pos: [670, 455], in: [16, 17, 22], out: 23},
                bff6: {type: 'custom-bff', pos: [670, 535], in: '24-26', out: 27},
                bff7: {type: 'custom-bff', pos: [670, 615], in: '28-30', out: 31},
                bff8: {type: 'custom-bff', pos: [670, 695], in: '32-34', out: 35},
                bff9: {type: 'custom-bff', pos: [670, 775], in: '36-38', out: 39},
                bff10: {type: 'custom-bff', pos: [670, 855], in: '40-42', out: 43},
                bff11: {type: 'custom-bff', pos: [670, 935], in: '44-46', out: 47},
                bff12: {type: 'custom-bff', pos: [670, 1015], in: '48-50', out: 51},
                bff13: {type: 'custom-bff', pos: [670, 1095], in: '52-54', out: 55},
                bff14: {type: 'custom-bff', pos: [670, 1175], in: '56-58', out: 59},
                bff15: {type: 'custom-bff', pos: [670, 1255], in: '60-62', out: 63},
                out0: {type: 'out', pos: [915, 565], id: [64, 65, '86-99'], bits: 16, name: 'Out0'},
                in0: {type: 'in', pos: [435, 565], id: '70-85', name: 'd', bits: 16, val: '0000000000000010'},
                in1: {type: 'in', pos: [395, 980], id: 66, name: 'st', val: 1},
                in2: {type: 'in', pos: [600, 1370], orient: 'n', id: 67, name: 'clk', val: 1},
                },
                wires: [[21, 64], [3, 65], [7, 86], [11, 87], [15, 88], [23, 89], [27, 90], [31, 91], [35, 92], [39, 93], [43, 94], [47, 95], [51, 96], [55, 97], [59, 98], [63, 99], [70, 18], [66, 19], [67, 20, {via: [[600, 95, 'n']]}], [71, 0], [66, 1], [67, 2, {via: [[600, 175, 'n']]}], [72, 4], [66, 5], [67, 6, {via: [[600, 255, 'n']]}], [73, 8], [66, 9], [67, 10, {via: [[600, 335, 'n']]}], [74, 12], [66, 13], [67, 14, {via: [[600, 415, 'n']]}], [75, 16], [66, 17], [67, 22, {via: [[600, 495, 'n']]}], [76, 24], [66, 25], [67, 26, {via: [[600, 575, 'n']]}], [77, 28], [66, 29], [67, 30, {via: [[600, 655, 'n']]}], [78, 32], [66, 33], [67, 34, {via: [[600, 735, 'n']]}], [79, 36], [66, 37], [67, 38, {via: [[600, 815, 'n']]}], [80, 40], [66, 41], [67, 42, {via: [[600, 895, 'n']]}], [81, 44], [66, 45], [67, 46, {via: [[600, 975, 'n']]}], [82, 48], [66, 49], [67, 50, {via: [[600, 1055, 'n']]}], [83, 52], [66, 53], [67, 54, {via: [[600, 1135, 'n']]}], [84, 56], [66, 57], [67, 58, {via: [[600, 1215, 'n']]}], [85, 60], [66, 61], [67, 62, {via: [[600, 1295]]}]]
            }},
            {id: 'inc16', caption: 'Inc16', circuit: {
                components: {
                in0: {type: 'in', pos: [300, 335], id: 223, name: 'In0', val: 1, isConstant: true},
                adder0: {type: 'adder-array', pos: [360, 320], in: '147-179', out: '180-196', bits: 16},
                out0: {type: 'out', pos: [470, 320], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [235, 230], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                },
                wires: [[180, 117], [181, 118], [182, 119], [183, 120], [184, 121], [185, 122], [186, 123], [187, 124], [188, 125], [189, 126], [190, 127], [191, 128], [192, 129], [193, 130], [194, 131], [195, 132], [97, 147], [98, 148], [99, 149], [100, 150], [101, 151], [102, 152], [103, 153], [104, 154], [105, 155], [106, 156], [107, 157], [108, 158], [109, 159], [110, 160], [111, 161], [112, 162], [223, 163]]
            }}
            ],
            components: {
            reg160: {type: 'custom-reg16', pos: [380, 190], in: '34-51', out: '52-67'},
            reg161: {type: 'custom-reg16', pos: [470, 800], in: '68-85', out: '86-101'},
            in0: {type: 'in', pos: [45, 470], id: '4-19', name: 'X', bits: 16, val: '0000000000000100'},
            in1: {type: 'in', pos: [60, 1010], id: 0, name: 'clk'},
            in2: {type: 'in', pos: [45, 350], id: 1, name: 'a'},
            in3: {type: 'in', pos: [60, 960], id: 2, name: 'd'},
            in4: {type: 'in', pos: [50, 590], id: 3, name: '✱a'},
            pass0: {type: 'pass', pos: [130, 470], in: ['28-33', '102-111'], out: ['112-116', '156-166'], bits: 16, slant: 'up'},
            pass1: {type: 'pass', pos: [150, 180], in: '175-190', out: '191-206', bits: 16, slant: 'up'},
            pass2: {type: 'pass', pos: [150, 790], in: '215-230', out: '231-246', bits: 16, slant: 'up'},
            out0: {type: 'out', pos: [795, 190], id: ['21-27', '167-174', 207], bits: 16, name: 'A'},
            out1: {type: 'out', pos: [795, 470], id: ['212-214', '247-259'], bits: 16, name: '✱A'},
            out2: {type: 'out', pos: [795, 800], id: '264-279', bits: 16, name: 'D'},
            ram0: {type: 'ram', pos: [645, 470], in: [20, '208-211', '260-263', '280-300'], out: '301-316', bits: 16, lines: 2048},
            },
            wires: [[52, 290, {via: [[695, 115]]}], [53, 291, {via: [[685, 125, 's']]}], [54, 292, {via: [[675, 135, 's']]}], [55, 293, {via: [[665, 145, 's']]}], [4, 210], [5, 211], [6, 260], [7, 261], [8, 262], [9, 263], [10, 280], [11, 281], [12, 282], [13, 283], [14, 284], [15, 285], [16, 286], [17, 287], [18, 288], [19, 289], [0, 51, {via: [[380, 1010, 'n']]}], [1, 50], [2, 84], [3, 208, {via: [[625, 595]]}], [0, 85, {via: [[470, 1010]]}], [0, 209, {via: [[665, 1010]]}], [4, 28], [5, 29], [6, 30], [7, 31], [8, 32], [9, 33], [10, 102], [11, 103], [12, 104], [13, 105], [14, 106], [15, 107], [16, 108], [17, 109], [18, 110], [19, 111], [112, 175], [113, 176], [114, 177], [115, 178], [116, 179], [156, 180], [157, 181], [158, 182], [159, 183], [160, 184], [161, 185], [162, 186], [163, 187], [164, 188], [165, 189], [166, 190], [191, 34], [192, 35], [193, 36], [194, 37], [195, 38], [196, 39], [197, 40], [198, 41], [199, 42], [200, 43], [201, 44], [202, 45], [203, 46], [204, 47], [205, 48], [206, 49], [112, 215], [113, 216], [114, 217], [115, 218], [116, 219], [156, 220], [157, 221], [158, 222], [159, 223], [160, 224], [161, 225], [162, 226], [163, 227], [164, 228], [165, 229], [166, 230], [231, 68], [232, 69], [233, 70], [234, 71], [235, 72], [236, 73], [237, 74], [238, 75], [239, 76], [240, 77], [241, 78], [242, 79], [243, 80], [244, 81], [245, 82], [246, 83], [52, 21], [53, 22], [54, 23], [55, 24], [56, 25], [57, 26], [58, 27], [59, 167], [60, 168], [61, 169], [62, 170], [63, 171], [64, 172], [65, 173], [66, 174], [67, 207], [301, 212], [302, 213], [303, 214], [304, 247], [305, 248], [306, 249], [307, 250], [308, 251], [309, 252], [310, 253], [311, 254], [312, 255], [313, 256], [314, 257], [315, 258], [316, 259], [86, 264], [87, 265], [88, 266], [89, 267], [90, 268], [91, 269], [92, 270], [93, 271], [94, 272], [95, 273], [96, 274], [97, 275], [98, 276], [99, 277], [100, 278], [101, 279], [56, 294, {via: [[655, 155]]}], [57, 295, {via: [[645, 165]]}], [58, 296, {via: [[635, 175]]}], [59, 297, {via: [[625, 185]]}], [60, 298, {via: [[615, 195]]}], [61, 299, {via: [[605, 205]]}], [62, 300, {via: [[595, 215]]}]]
        }
        </script>
    </logic-editor>
    </div>

    !!! warning "Attention"

        Le simulateur ne supporte pas plus de 4096 lignes dans la RAM (d'où l'adressage sur 12 bits).

#### b. Instruction d'ALU

Les 11 bits de poids faible de l'instruction `I` indiquent les opérations à effectuer sur l'ALU, le bloc Condition (et la mémoire combinée):

| I10 | I9 | I8 | I7 | I6 | I5 | I4 | I3 | I2 | I1 | I0 |
|:---:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| u   | op1| op0| zx | sw | a  | d  | ✱a | lt | eq | gt |

- `D` contient le mot pour la première entrée de l'ALU.
- Le bit I12 indique si la deuxième entrée de l'ALU est `A` (`I12=0`) ou `✱A` (`I12=1`).
- Les bits `a`, `d` et `✱a` indique la destination du résultat `R`.
- Le bloc teste la condition le résultat `R` de l'ALU et indique le résultat dans le bit `j`.

=== "Solution"

    <div style="width: 100%; height: 813px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true, zoom: 80},
            defs: [
            {id: 'inv16', caption: 'Inv16', circuit: {
                components: {
                not0: {type: 'not', pos: [775, 190], in: 120, out: 121},
                not1: {type: 'not', pos: [745, 220], in: 122, out: 123},
                not2: {type: 'not', pos: [775, 245], in: 124, out: 125},
                not3: {type: 'not', pos: [745, 270], in: 126, out: 127},
                not4: {type: 'not', pos: [775, 300], in: 128, out: 129},
                not15: {type: 'not', pos: [745, 325], in: 150, out: 151},
                not5: {type: 'not', pos: [770, 355], in: 130, out: 131},
                not6: {type: 'not', pos: [740, 380], in: 132, out: 133},
                not7: {type: 'not', pos: [770, 410], in: 134, out: 135},
                not8: {type: 'not', pos: [740, 440], in: 136, out: 137},
                not9: {type: 'not', pos: [775, 470], in: 138, out: 139},
                not10: {type: 'not', pos: [740, 500], in: 140, out: 141},
                not11: {type: 'not', pos: [770, 530], in: 142, out: 143},
                not12: {type: 'not', pos: [740, 560], in: 144, out: 145},
                not13: {type: 'not', pos: [770, 585], in: 146, out: 147},
                not14: {type: 'not', pos: [740, 615], in: 148, out: 149},
                out0: {type: 'out', pos: [960, 400], id: '0-15', bits: 16, name: '/A'},
                in1: {type: 'in', pos: [595, 405], id: [16, 17, '19-32'], name: 'A', bits: 16, val: '1110000000000000'},
                },
                wires: [[121, 0], [123, 1], [125, 2], [127, 3], [129, 4], [151, 5], [131, 6], [133, 7], [135, 8], [137, 9], [139, 10], [141, 11], [143, 12], [145, 13], [147, 14], [149, 15], [16, 120], [17, 122], [19, 124], [20, 126], [21, 128], [22, 150], [23, 130], [24, 132], [25, 134], [26, 136], [27, 138], [28, 140], [29, 142], [30, 144], [31, 146], [32, 148]]
            }},
            {id: 'ul', caption: 'ul', circuit: {
                components: {
                mux0: {type: 'mux', pos: [1240, 545], in: '391-423', out: '424-439', from: 32, to: 16},
                mux1: {type: 'mux', pos: [940, 455], in: '447-479', out: '480-495', from: 32, to: 16},
                mux2: {type: 'mux', pos: [990, 990], in: '503-535', out: '536-551', from: 32, to: 16},
                pass5: {type: 'pass', pos: [165, 990], in: ['138-141', '238-241', '274-281'], out: ['314-321', '386-390', '440-442'], bits: 16, slant: 'up'},
                pass0: {type: 'pass', pos: [345, 455], in: '282-297', out: '298-313', bits: 16, slant: 'up'},
                pass11: {type: 'pass', pos: [1025, 990], in: ['661-668', '741-748'], out: '749-764', bits: 16, slant: 'up'},
                pass6: {type: 'pass', pos: [185, 285], in: '581-596', out: '597-612', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [185, 635], in: '621-636', out: '637-652', bits: 16, slant: 'up'},
                array2: {type: 'xor-array', pos: [695, 905], in: ['12-17', '19-36', '102-109'], out: ['142-151', '184-189'], bits: 16},
                pass1: {type: 'pass', pos: [365, 105], in: ['322-328', 345, '362-369'], out: '370-385', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [365, 815], in: ['8-11', '110-121'], out: '122-137', bits: 16, slant: 'up'},
                pass4: {type: 'pass', pos: [365, 1255], in: '242-257', out: '258-273', bits: 16, slant: 'up'},
                array1: {type: 'or-array', pos: [565, 545], in: '190-221', out: '222-237', bits: 16},
                pass12: {type: 'pass', pos: [1045, 630], in: '773-788', out: '789-804', bits: 16, slant: 'up'},
                array0: {type: 'and-array', pos: [680, 195], in: ['37-50', 52, '69-85'], out: '86-101', bits: 16},
                inv160: {type: 'custom-inv16', pos: [605, 1255], in: '152-167', out: '168-183'},
                pass9: {type: 'pass', pos: [735, 195], in: ['573-580', '613-620'], out: ['653-660', '693-700'], bits: 16, slant: 'down'},
                pass2: {type: 'pass', pos: [745, 1255], in: ['443-446', '496-502', '552-556'], out: '557-572', bits: 16, slant: 'up'},
                pass10: {type: 'pass', pos: [755, 370], in: '709-724', out: '725-740', bits: 16, slant: 'down'},
                pass8: {type: 'pass', pos: [765, 1075], in: '669-684', out: ['685-692', '701-708'], bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [1330, 545], id: '346-361', bits: 16, name: 'S'},
                in1: {type: 'in', pos: [135, 455], id: '329-344', name: 'A', bits: 16, val: '1111111011111111'},
                in3: {type: 'in', pos: [135, 990], id: '53-68', name: 'B', bits: 16, val: '1111111111111111'},
                in0: {type: 'in', pos: [1240, 250], orient: 's', id: 18, name: 'Op1', val: 1},
                in2: {type: 'in', pos: [995, 245], orient: 's', id: 51, name: 'Op0'},
                },
                wires: [[424, 346], [425, 347], [426, 348], [427, 349], [428, 350], [429, 351], [430, 352], [431, 353], [432, 354], [433, 355], [434, 356], [435, 357], [436, 358], [437, 359], [438, 360], [439, 361], [480, 391], [481, 392], [482, 393], [483, 394], [484, 395], [485, 396], [486, 397], [487, 398], [488, 399], [489, 400], [490, 401], [491, 402], [492, 403], [493, 404], [494, 405], [495, 406], [789, 407], [790, 408], [791, 409], [792, 410], [793, 411], [794, 412], [795, 413], [796, 414], [797, 415], [798, 416], [799, 417], [800, 418], [801, 419], [802, 420], [803, 421], [804, 422], [18, 423], [725, 447], [726, 448], [727, 449], [728, 450], [729, 451], [730, 452], [731, 453], [732, 454], [733, 455], [734, 456], [735, 457], [736, 458], [737, 459], [738, 460], [739, 461], [740, 462], [222, 463], [223, 464], [224, 465], [225, 466], [226, 467], [227, 468], [228, 469], [229, 470], [230, 471], [231, 472], [232, 473], [233, 474], [234, 475], [235, 476], [236, 477], [237, 478], [51, 479], [142, 503], [143, 504], [144, 505], [145, 506], [146, 507], [147, 508], [148, 509], [149, 510], [150, 511], [151, 512], [184, 513], [185, 514], [186, 515], [187, 516], [188, 517], [189, 518], [685, 519], [686, 520], [687, 521], [688, 522], [689, 523], [690, 524], [691, 525], [692, 526], [701, 527], [702, 528], [703, 529], [704, 530], [705, 531], [706, 532], [707, 533], [708, 534], [51, 535], [53, 138], [54, 139], [55, 140], [56, 141], [57, 238], [58, 239], [59, 240], [60, 241], [61, 274], [62, 275], [63, 276], [64, 277], [65, 278], [66, 279], [67, 280], [68, 281], [329, 282], [330, 283], [331, 284], [332, 285], [333, 286], [334, 287], [335, 288], [336, 289], [337, 290], [338, 291], [339, 292], [340, 293], [341, 294], [342, 295], [343, 296], [344, 297], [536, 661], [537, 662], [538, 663], [539, 664], [540, 665], [541, 666], [542, 667], [543, 668], [544, 741], [545, 742], [546, 743], [547, 744], [548, 745], [549, 746], [550, 747], [551, 748], [314, 581], [315, 582], [316, 583], [317, 584], [318, 585], [319, 586], [320, 587], [321, 588], [386, 589], [387, 590], [388, 591], [389, 592], [390, 593], [440, 594], [441, 595], [442, 596], [314, 621], [315, 622], [316, 623], [317, 624], [318, 625], [319, 626], [320, 627], [321, 628], [386, 629], [387, 630], [388, 631], [389, 632], [390, 633], [440, 634], [441, 635], [442, 636], [122, 12], [123, 13], [124, 14], [125, 15], [126, 16], [127, 17], [128, 19], [129, 20], [130, 21], [131, 22], [132, 23], [133, 24], [134, 25], [135, 26], [136, 27], [137, 28], [314, 29], [315, 30], [316, 31], [317, 32], [318, 33], [319, 34], [320, 35], [321, 36], [386, 102], [387, 103], [388, 104], [389, 105], [390, 106], [440, 107], [441, 108], [442, 109], [298, 322], [299, 323], [300, 324], [301, 325], [302, 326], [303, 327], [304, 328], [305, 345], [306, 362], [307, 363], [308, 364], [309, 365], [310, 366], [311, 367], [312, 368], [313, 369], [298, 8], [299, 9], [300, 10], [301, 11], [302, 110], [303, 111], [304, 112], [305, 113], [306, 114], [307, 115], [308, 116], [309, 117], [310, 118], [311, 119], [312, 120], [313, 121], [298, 242], [299, 243], [300, 244], [301, 245], [302, 246], [303, 247], [304, 248], [305, 249], [306, 250], [307, 251], [308, 252], [309, 253], [310, 254], [311, 255], [312, 256], [313, 257], [298, 190], [299, 191], [300, 192], [301, 193], [302, 194], [303, 195], [304, 196], [305, 197], [306, 198], [307, 199], [308, 200], [309, 201], [310, 202], [311, 203], [312, 204], [313, 205], [637, 206], [638, 207], [639, 208], [640, 209], [641, 210], [642, 211], [643, 212], [644, 213], [645, 214], [646, 215], [647, 216], [648, 217], [649, 218], [650, 219], [651, 220], [652, 221], [749, 773], [750, 774], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [370, 37], [371, 38], [372, 39], [373, 40], [374, 41], [375, 42], [376, 43], [377, 44], [378, 45], [379, 46], [380, 47], [381, 48], [382, 49], [383, 50], [384, 52], [385, 69], [597, 70], [598, 71], [599, 72], [600, 73], [601, 74], [602, 75], [603, 76], [604, 77], [605, 78], [606, 79], [607, 80], [608, 81], [609, 82], [610, 83], [611, 84], [612, 85], [258, 152], [259, 153], [260, 154], [261, 155], [262, 156], [263, 157], [264, 158], [265, 159], [266, 160], [267, 161], [268, 162], [269, 163], [270, 164], [271, 165], [272, 166], [273, 167], [86, 573], [87, 574], [88, 575], [89, 576], [90, 577], [91, 578], [92, 579], [93, 580], [94, 613], [95, 614], [96, 615], [97, 616], [98, 617], [99, 618], [100, 619], [101, 620], [168, 443], [169, 444], [170, 445], [171, 446], [172, 496], [173, 497], [174, 498], [175, 499], [176, 500], [177, 501], [178, 502], [179, 552], [180, 553], [181, 554], [182, 555], [183, 556], [653, 709], [654, 710], [655, 711], [656, 712], [657, 713], [658, 714], [659, 715], [660, 716], [693, 717], [694, 718], [695, 719], [696, 720], [697, 721], [698, 722], [699, 723], [700, 724], [557, 669], [558, 670], [559, 671], [560, 672], [561, 673], [562, 674], [563, 675], [564, 676], [565, 677], [566, 678], [567, 679], [568, 680], [569, 681], [570, 682], [571, 683], [572, 684]]
            }},
            {id: 'opp16', caption: 'Opp16', circuit: {
                components: {
                in0: {type: 'in', pos: [140, 725], orient: 'n', id: 223, name: 'In0', val: 1, isConstant: true},
                cnot0: {type: 'cnot-array', pos: [155, 380], in: '73-89', out: ['90-96', '113-116', '133-137'], bits: 16},
                adder1: {type: 'adder-array', pos: [285, 470], in: '152-184', out: '185-201', bits: 16},
                out1: {type: 'out', pos: [365, 470], id: ['4-13', '64-69'], bits: 16, name: '-B'},
                in2: {type: 'in', pos: [50, 380], id: '207-222', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[185, 4], [186, 5], [187, 6], [188, 7], [189, 8], [190, 9], [191, 10], [192, 11], [193, 12], [194, 13], [195, 64], [196, 65], [197, 66], [198, 67], [199, 68], [200, 69], [207, 73], [208, 74], [209, 75], [210, 76], [211, 77], [212, 78], [213, 79], [214, 80], [215, 81], [216, 82], [217, 83], [218, 84], [219, 85], [220, 86], [221, 87], [222, 88], [223, 89], [90, 152], [91, 153], [92, 154], [93, 155], [94, 156], [95, 157], [96, 158], [113, 159], [114, 160], [115, 161], [116, 162], [133, 163], [134, 164], [135, 165], [136, 166], [137, 167], [223, 168]]
            }},
            {id: 'sous16', caption: 'Sous16', circuit: {
                components: {
                adder0: {type: 'adder-array', pos: [525, 350], in: '14-46', out: '47-63', bits: 16},
                comp20: {type: 'custom-opp16', pos: [370, 515], in: '64-79', out: '80-95'},
                out0: {type: 'out', pos: [695, 350], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [165, 265], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                in0: {type: 'in', pos: [165, 515], id: '150-165', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[47, 117], [48, 118], [49, 119], [50, 120], [51, 121], [52, 122], [53, 123], [54, 124], [55, 125], [56, 126], [57, 127], [58, 128], [59, 129], [60, 130], [61, 131], [62, 132], [97, 14], [98, 15], [99, 16], [100, 17], [101, 18], [102, 19], [103, 20], [104, 21], [105, 22], [106, 23], [107, 24], [108, 25], [109, 26], [110, 27], [111, 28], [112, 29], [80, 30], [81, 31], [82, 32], [83, 33], [84, 34], [85, 35], [86, 36], [87, 37], [88, 38], [89, 39], [90, 40], [91, 41], [92, 42], [93, 43], [94, 44], [95, 45], [150, 64], [151, 65], [152, 66], [153, 67], [154, 68], [155, 69], [156, 70], [157, 71], [158, 72], [159, 73], [160, 74], [161, 75], [162, 76], [163, 77], [164, 78], [165, 79]]
            }},
            {id: 'al', caption: 'al', circuit: {
                components: {
                in2: {type: 'in', pos: [85, 840], id: 638, name: 'In0', val: 1, isConstant: true},
                pass0: {type: 'pass', pos: [90, 300], in: '510-525', out: '526-541', bits: 16, slant: 'up'},
                mux1: {type: 'mux', pos: [135, 830], in: '353-385', out: '386-401', from: 32, to: 16},
                mux0: {type: 'mux', pos: [885, 335], in: '297-329', out: '330-345', from: 32, to: 16},
                pass1: {type: 'pass', pos: [110, 150], in: '550-565', out: '566-581', bits: 16, slant: 'up'},
                pass2: {type: 'pass', pos: [110, 510], in: '590-605', out: '606-621', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [275, 830], in: '647-662', out: '663-678', bits: 16, slant: 'up'},
                adder0: {type: 'adder-array', pos: [500, 245], in: '192-224', out: '225-241', bits: 16},
                sous160: {type: 'custom-sous16', pos: [570, 670], in: '242-273', out: '274-289'},
                pass4: {type: 'pass', pos: [295, 335], in: '687-702', out: '703-718', bits: 16, slant: 'up'},
                pass6: {type: 'pass', pos: [660, 670], in: '735-750', out: '751-766', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [680, 420], in: '775-790', out: '791-806', bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [990, 335], id: '622-637', bits: 16, name: 'S'},
                in0: {type: 'in', pos: [40, 300], id: '470-485', name: 'A', bits: 16, val: '0000000000000110'},
                in1: {type: 'in', pos: [40, 745], id: '486-501', name: 'B', bits: 16, val: '0000000000000010'},
                in3: {type: 'in', pos: [885, 50], orient: 's', id: 807, name: 'Op1', val: 1},
                in4: {type: 'in', pos: [135, 590], orient: 's', id: 808, name: 'Op0', val: 1},
                },
                wires: [[330, 622], [331, 623], [332, 624], [333, 625], [334, 626], [335, 627], [336, 628], [337, 629], [338, 630], [339, 631], [340, 632], [341, 633], [342, 634], [343, 635], [344, 636], [345, 637], [470, 510], [471, 511], [472, 512], [473, 513], [474, 514], [475, 515], [476, 516], [477, 517], [478, 518], [479, 519], [480, 520], [481, 521], [482, 522], [483, 523], [484, 524], [485, 525], [486, 353], [487, 354], [488, 355], [489, 356], [490, 357], [491, 358], [492, 359], [493, 360], [494, 361], [495, 362], [496, 363], [497, 364], [498, 365], [499, 366], [500, 367], [501, 368], [638, 369], [808, 385], [225, 297], [226, 298], [227, 299], [228, 300], [229, 301], [230, 302], [231, 303], [232, 304], [233, 305], [234, 306], [235, 307], [236, 308], [237, 309], [238, 310], [239, 311], [240, 312], [791, 313], [792, 314], [793, 315], [794, 316], [795, 317], [796, 318], [797, 319], [798, 320], [799, 321], [800, 322], [801, 323], [802, 324], [803, 325], [804, 326], [805, 327], [806, 328], [807, 329], [526, 550], [527, 551], [528, 552], [529, 553], [530, 554], [531, 555], [532, 556], [533, 557], [534, 558], [535, 559], [536, 560], [537, 561], [538, 562], [539, 563], [540, 564], [541, 565], [526, 590], [527, 591], [528, 592], [529, 593], [530, 594], [531, 595], [532, 596], [533, 597], [534, 598], [535, 599], [536, 600], [537, 601], [538, 602], [539, 603], [540, 604], [541, 605], [386, 647], [387, 648], [388, 649], [389, 650], [390, 651], [391, 652], [392, 653], [393, 654], [394, 655], [395, 656], [396, 657], [397, 658], [398, 659], [399, 660], [400, 661], [401, 662], [566, 192], [567, 193], [568, 194], [569, 195], [570, 196], [571, 197], [572, 198], [573, 199], [574, 200], [575, 201], [576, 202], [577, 203], [578, 204], [579, 205], [580, 206], [581, 207], [703, 208], [704, 209], [705, 210], [706, 211], [707, 212], [708, 213], [709, 214], [710, 215], [711, 216], [712, 217], [713, 218], [714, 219], [715, 220], [716, 221], [717, 222], [718, 223], [606, 242], [607, 243], [608, 244], [609, 245], [610, 246], [611, 247], [612, 248], [613, 249], [614, 250], [615, 251], [616, 252], [617, 253], [618, 254], [619, 255], [620, 256], [621, 257], [663, 258], [664, 259], [665, 260], [666, 261], [667, 262], [668, 263], [669, 264], [670, 265], [671, 266], [672, 267], [673, 268], [674, 269], [675, 270], [676, 271], [677, 272], [678, 273], [663, 687], [664, 688], [665, 689], [666, 690], [667, 691], [668, 692], [669, 693], [670, 694], [671, 695], [672, 696], [673, 697], [674, 698], [675, 699], [676, 700], [677, 701], [678, 702], [274, 735], [275, 736], [276, 737], [277, 738], [278, 739], [279, 740], [280, 741], [281, 742], [282, 743], [283, 744], [284, 745], [285, 746], [286, 747], [287, 748], [288, 749], [289, 750], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [765, 789], [766, 790]]
            }},
            {id: 'alu', caption: 'ALU', circuit: {
                components: {
                mux0: {type: 'mux', pos: [715, 280], in: '258-290', out: '291-306', from: 32, to: 16},
                pass4: {type: 'pass', pos: [460, 200], in: '327-342', out: '343-358', bits: 16, slant: 'down'},
                mux2: {type: 'mux', pos: [660, 1265], in: ['122-135', '171-178', '211-218', '251-253'], out: ['254-257', '307-318'], from: 32, to: 16},
                pass6: {type: 'pass', pos: [270, 1185], in: '407-422', out: '423-438', bits: 16, slant: 'up'},
                mux1: {type: 'mux', pos: [1475, 615], in: ['58-61', '78-81', '108-114', '136-153'], out: '154-169', from: 32, to: 16},
                mux3: {type: 'mux', pos: [860, 365], in: [319, '513-527', '560-576'], out: '577-592', from: 32, to: 16},
                ul0: {type: 'custom-ul', pos: [1115, 525], in: ['0-7', '765-772', '805-822'], out: '823-838'},
                al0: {type: 'custom-al', pos: [1195, 1105], in: '8-41', out: '42-57'},
                pass5: {type: 'pass', pos: [480, 1350], in: '367-382', out: '383-398', bits: 16, slant: 'down'},
                pass9: {type: 'pass', pos: [720, 1265], in: '528-543', out: '544-559', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [290, 365], in: '447-462', out: '463-478', bits: 16, slant: 'up'},
                pass10: {type: 'pass', pos: [905, 365], in: '481-496', out: '497-512', bits: 16, slant: 'down'},
                pass3: {type: 'pass', pos: [1265, 1105], in: '219-234', out: '235-250', bits: 16, slant: 'up'},
                pass11: {type: 'pass', pos: [740, 685], in: '608-623', out: '624-639', bits: 16, slant: 'up'},
                pass8: {type: 'pass', pos: [925, 945], in: ['320-326', '359-366', 399], out: ['400-406', '439-446', 480], bits: 16, slant: 'down'},
                pass2: {type: 'pass', pos: [1285, 700], in: '179-194', out: '195-210', bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [1555, 615], id: ['598-607', '640-645'], bits: 16, name: 'S'},
                in0: {type: 'in', pos: [205, 200], id: '62-77', name: 'A', bits: 16, val: '0000000000000100'},
                in1: {type: 'in', pos: [220, 1185], id: '82-97', name: 'B', bits: 16, val: '0000000000000001'},
                in3: {type: 'in', pos: [1470, 80], orient: 's', id: 170, name: 'u'},
                in6: {type: 'in', pos: [1200, 95], orient: 's', id: 99, name: 'Op1'},
                in2: {type: 'in', pos: [1165, 95], orient: 's', id: 98, name: 'Op0'},
                in5: {type: 'in', pos: [860, 75], orient: 's', id: 593, name: 'zx'},
                in4: {type: 'in', pos: [660, 70], orient: 's', id: 479, name: 'sw'},
                },
                wires: [[154, 598], [155, 599], [156, 600], [157, 601], [158, 602], [159, 603], [160, 604], [161, 605], [162, 606], [163, 607], [164, 640], [165, 641], [166, 642], [167, 643], [168, 644], [169, 645], [62, 258], [63, 259], [64, 260], [65, 261], [66, 262], [67, 263], [68, 264], [69, 265], [70, 266], [71, 267], [72, 268], [73, 269], [74, 270], [75, 271], [76, 272], [77, 273], [463, 274], [464, 275], [465, 276], [466, 277], [467, 278], [468, 279], [469, 280], [470, 281], [471, 282], [472, 283], [473, 284], [474, 285], [475, 286], [476, 287], [477, 288], [478, 289], [479, 290], [62, 327], [63, 328], [64, 329], [65, 330], [66, 331], [67, 332], [68, 333], [69, 334], [70, 335], [71, 336], [72, 337], [73, 338], [74, 339], [75, 340], [76, 341], [77, 342], [82, 122], [83, 123], [84, 124], [85, 125], [86, 126], [87, 127], [88, 128], [89, 129], [90, 130], [91, 131], [92, 132], [93, 133], [94, 134], [95, 135], [96, 171], [97, 172], [383, 173], [384, 174], [385, 175], [386, 176], [387, 177], [388, 178], [389, 211], [390, 212], [391, 213], [392, 214], [393, 215], [394, 216], [395, 217], [396, 218], [397, 251], [398, 252], [479, 253], [82, 407], [83, 408], [84, 409], [85, 410], [86, 411], [87, 412], [88, 413], [89, 414], [90, 415], [91, 416], [92, 417], [93, 418], [94, 419], [95, 420], [96, 421], [97, 422], [823, 58], [824, 59], [825, 60], [826, 61], [827, 78], [828, 79], [829, 80], [830, 81], [831, 108], [832, 109], [833, 110], [834, 111], [835, 112], [836, 113], [837, 114], [838, 136], [195, 137], [196, 138], [197, 139], [198, 140], [199, 141], [200, 142], [201, 143], [202, 144], [203, 145], [204, 146], [205, 147], [206, 148], [207, 149], [208, 150], [209, 151], [210, 152], [170, 153], [291, 319], [292, 513], [293, 514], [294, 515], [295, 516], [296, 517], [297, 518], [298, 519], [299, 520], [300, 521], [301, 522], [302, 523], [303, 524], [304, 525], [305, 526], [306, 527], [593, 576], [577, 0], [578, 1], [579, 2], [580, 3], [581, 4], [582, 5], [583, 6], [584, 7], [585, 765], [586, 766], [587, 767], [588, 768], [589, 769], [590, 770], [591, 771], [592, 772], [624, 805], [625, 806], [626, 807], [627, 808], [628, 809], [629, 810], [630, 811], [631, 812], [632, 813], [633, 814], [634, 815], [635, 816], [636, 817], [637, 818], [638, 819], [639, 820], [99, 821, {via: [[1200, 190, 'w']]}], [98, 822], [400, 8], [401, 9], [402, 10], [403, 11], [404, 12], [405, 13], [406, 14], [439, 15], [440, 16], [441, 17], [442, 18], [443, 19], [444, 20], [445, 21], [446, 22], [480, 23], [254, 24], [255, 25], [256, 26], [257, 27], [307, 28], [308, 29], [309, 30], [310, 31], [311, 32], [312, 33], [313, 34], [314, 35], [315, 36], [316, 37], [317, 38], [318, 39], [99, 40], [98, 41], [343, 367], [344, 368], [345, 369], [346, 370], [347, 371], [348, 372], [349, 373], [350, 374], [351, 375], [352, 376], [353, 377], [354, 378], [355, 379], [356, 380], [357, 381], [358, 382], [254, 528], [255, 529], [256, 530], [257, 531], [307, 532], [308, 533], [309, 534], [310, 535], [311, 536], [312, 537], [313, 538], [314, 539], [315, 540], [316, 541], [317, 542], [318, 543], [423, 447], [424, 448], [425, 449], [426, 450], [427, 451], [428, 452], [429, 453], [430, 454], [431, 455], [432, 456], [433, 457], [434, 458], [435, 459], [436, 460], [437, 461], [438, 462], [577, 481], [578, 482], [579, 483], [580, 484], [581, 485], [583, 487], [584, 488], [586, 490], [588, 492], [589, 493], [42, 219], [43, 220], [44, 221], [45, 222], [46, 223], [47, 224], [48, 225], [49, 226], [50, 227], [51, 228], [52, 229], [53, 230], [54, 231], [55, 232], [56, 233], [57, 234], [544, 608], [545, 609], [546, 610], [547, 611], [548, 612], [549, 613], [550, 614], [551, 615], [552, 616], [553, 617], [554, 618], [555, 619], [556, 620], [557, 621], [558, 622], [559, 623], [497, 320], [498, 321], [499, 322], [500, 323], [501, 324], [502, 325], [503, 326], [504, 359], [505, 360], [506, 361], [507, 362], [508, 363], [509, 364], [510, 365], [511, 366], [512, 399], [235, 179], [236, 180], [237, 181], [238, 182], [239, 183], [240, 184], [241, 185], [242, 186], [243, 187], [244, 188], [245, 189], [246, 190], [247, 191], [248, 192], [249, 193], [250, 194]]
            }},
            {id: 'iszero', caption: 'isZero', circuit: {
                components: {
                or0: {type: 'or', pos: [305, 95], in: [39, 40], out: 41},
                or1: {type: 'or', pos: [305, 140], in: [42, 43], out: 44},
                or2: {type: 'or', pos: [305, 185], in: [45, 46], out: 47},
                or3: {type: 'or', pos: [305, 230], in: [48, 49], out: 50},
                or4: {type: 'or', pos: [305, 270], in: [51, 52], out: 53},
                or5: {type: 'or', pos: [305, 310], in: [54, 55], out: 56},
                or7: {type: 'or', pos: [305, 350], in: [60, 61], out: 62},
                or6: {type: 'or', pos: [305, 390], in: [57, 58], out: 59},
                or8: {type: 'or', pos: [410, 120], in: [63, 64], out: 65},
                or9: {type: 'or', pos: [410, 205], in: [66, 67], out: 68},
                or10: {type: 'or', pos: [410, 290], in: [69, 70], out: 71},
                or11: {type: 'or', pos: [405, 375], in: [72, 73], out: 74},
                or12: {type: 'or', pos: [510, 165], in: [75, 76], out: 77},
                or13: {type: 'or', pos: [510, 330], in: [78, 79], out: 80},
                or14: {type: 'or', pos: [610, 235], in: [81, 82], out: 83},
                not0: {type: 'not', pos: [690, 235], in: 84, out: 85},
                out1: {type: 'out', pos: [760, 235], id: 88, name: 'nul?'},
                in0: {type: 'in', pos: [185, 215], id: '4-19', name: 'A', bits: 16, val: '0000000000000010'},
                },
                wires: [[85, 88], [4, 39], [5, 40], [6, 42], [7, 43], [8, 45], [9, 46], [10, 48], [11, 49], [12, 51], [13, 52], [14, 54], [15, 55], [16, 60], [17, 61], [18, 57], [19, 58], [41, 63], [44, 64], [47, 66], [50, 67], [53, 69], [56, 70], [62, 72], [59, 73], [65, 75], [68, 76], [71, 78], [74, 79], [77, 81], [80, 82], [83, 84]]
            }},
            {id: 'isneg', caption: 'isNeg', circuit: {
                components: {
                out1: {type: 'out', pos: [570, 660], id: 118, name: 'Out0'},
                in0: {type: 'in', pos: [405, 635], id: '119-134', name: 'In0', bits: 16, val: '0000000001001010'},
                },
                wires: [[134, 118]]
            }},
            {id: 'condition', caption: 'Condition', circuit: {
                components: {
                and0: {type: 'and', pos: [550, 350], in: [252, 253], out: 254},
                and1: {type: 'and', pos: [550, 405], in: [255, 256], out: 257},
                and2: {type: 'and', pos: [715, 750], in: [266, 267], out: 268},
                isneg0: {type: 'custom-isneg', pos: [395, 180], in: '135-150', out: 151},
                pass0: {type: 'pass', pos: [165, 180], in: '180-195', out: '196-211', bits: 16, slant: 'down'},
                or0: {type: 'or', pos: [655, 375], in: [258, 259], out: 260},
                or1: {type: 'or', pos: [835, 510], in: [269, 270], out: 271},
                nor0: {type: 'or', pos: [545, 565], in: [261, 262], out: 263},
                pass1: {type: 'pass', pos: [185, 575], in: '220-235', out: '236-251', bits: 16, slant: 'down'},
                not0: {type: 'not', pos: [625, 565], in: 264, out: 265},
                iszero0: {type: 'custom-iszero', pos: [400, 575], in: '89-104', out: 105},
                out0: {type: 'out', pos: [905, 510], id: 23, name: 'Verif'},
                in0: {type: 'in', pos: [120, 180], id: '156-171', name: 'A', bits: 16},
                in1: {type: 'in', pos: [50, 360], id: 20, name: 'lt'},
                in2: {type: 'in', pos: [50, 395], id: 21, name: 'eq', val: 1},
                in3: {type: 'in', pos: [70, 760], id: 22, name: 'gt'},
                },
                wires: [[271, 23], [151, 252], [20, 253], [21, 255], [105, 256], [265, 266], [22, 267], [156, 135], [157, 136], [158, 137], [159, 138], [160, 139], [161, 140], [162, 141], [163, 142], [164, 143], [165, 144], [166, 145], [167, 146], [168, 147], [169, 148], [170, 149], [171, 150], [156, 180], [157, 181], [158, 182], [159, 183], [160, 184], [161, 185], [162, 186], [163, 187], [164, 188], [165, 189], [166, 190], [167, 191], [168, 192], [169, 193], [170, 194], [171, 195], [254, 258], [257, 259], [260, 269], [268, 270], [151, 261], [105, 262], [196, 220], [197, 221], [198, 222], [199, 223], [200, 224], [201, 225], [202, 226], [203, 227], [204, 228], [205, 229], [206, 230], [207, 231], [208, 232], [209, 233], [210, 234], [211, 235], [263, 264], [236, 89], [237, 90], [238, 91], [239, 92], [240, 93], [241, 94], [242, 95], [243, 96], [244, 97], [245, 98], [246, 99], [247, 100], [248, 101], [249, 102], [250, 103], [251, 104]]
            }}
            ],
            components: {
            alu0: {type: 'custom-alu', pos: [830, 330], orient: 's', in: ['100-107', '115-121', '594-597', '646-663'], out: '664-679'},
            condition0: {type: 'custom-condition', pos: [300, 660], orient: 's', in: '0-18', out: 19},
            in0: {type: 'in', pos: [40, 205], id: '24-39', name: 'I', bits: 16, val: '0000000000000010'},
            out0: {type: 'out', pos: [1275, 155], id: 40, name: '✱a'},
            out1: {type: 'out', pos: [1340, 170], id: 41, name: 'd'},
            out2: {type: 'out', pos: [1275, 185], id: 42, name: 'a'},
            out3: {type: 'out', pos: [830, 705], orient: 's', id: '43-58', bits: 16, name: 'R'},
            in1: {type: 'in', pos: [755, 55], orient: 's', id: '63-78', name: 'A', bits: 16, val: '0000000001000010'},
            in2: {type: 'in', pos: [990, 55], orient: 's', id: '83-98', name: 'D', bits: 16, val: '0000000000000100'},
            in3: {type: 'in', pos: [585, 55], orient: 's', id: ['111-114', '122-133'], name: '✱A', bits: 16, val: '0000000000000010'},
            out4: {type: 'out', pos: [300, 755], orient: 's', id: 135, name: 'j'},
            pass0: {type: 'pass', pos: [830, 410], orient: 's', in: ['79-82', 99, '108-110', 134, '136-142'], out: '143-158', bits: 16, slant: 'down'},
            pass1: {type: 'pass', pos: [330, 430], orient: 's', in: '167-182', out: '183-198', bits: 16, slant: 'down'},
            mux0: {type: 'mux', pos: [670, 125], orient: 's', in: '255-287', out: '288-303', from: 32, to: 16, bottom: true},
            },
            wires: [[24, 18, {via: [[120, 130, 's']]}], [25, 17, {via: [[140, 140, 's']]}], [26, 16, {via: [[160, 150, 's']]}], [27, 40], [28, 41], [29, 42], [30, 663, {via: [[1245, 200], [1245, 290, 's']]}], [31, 662, {via: [[1235, 210], [1235, 310, 's']]}], [32, 661, {via: [[1225, 220], [1225, 330, 's']]}], [33, 660, {via: [[1215, 230], [1215, 350, 's']]}], [34, 659, {via: [[1205, 240], [1205, 370, 's']]}], [664, 43], [665, 44], [666, 45], [667, 46], [668, 47], [669, 48], [670, 49], [671, 50], [672, 51], [673, 52], [674, 53], [675, 54], [676, 55], [677, 56], [678, 57], [679, 58], [19, 135], [664, 79], [665, 80], [666, 81], [667, 82], [668, 99], [669, 108], [670, 109], [671, 110], [672, 134], [673, 136], [674, 137], [675, 138], [676, 139], [677, 140], [678, 141], [679, 142], [143, 167], [144, 168], [145, 169], [146, 170], [147, 171], [148, 172], [149, 173], [150, 174], [151, 175], [152, 176], [153, 177], [154, 178], [155, 179], [156, 180], [157, 181], [158, 182], [183, 0], [184, 1], [185, 2], [186, 3], [187, 4], [188, 5], [189, 6], [190, 7], [191, 8], [192, 9], [193, 10], [194, 11], [195, 12], [196, 13], [197, 14], [198, 15], [83, 100], [84, 101], [85, 102], [86, 103], [87, 104], [88, 105], [89, 106], [90, 107], [91, 115], [92, 116], [93, 117], [94, 118], [95, 119], [96, 120], [97, 121], [98, 594], [288, 595], [289, 596], [290, 597], [291, 646], [292, 647], [293, 648], [294, 649], [295, 650], [296, 651], [297, 652], [298, 653], [299, 654], [300, 655], [301, 656], [302, 657], [303, 658], [36, 287, {via: [[490, 250]]}], [63, 255], [64, 256], [65, 257], [66, 258], [67, 259], [68, 260], [69, 261], [70, 262], [71, 263], [72, 264], [73, 265], [74, 266], [75, 267], [76, 268], [77, 269], [78, 270], [111, 271], [112, 272], [113, 273], [114, 274], [122, 275], [123, 276], [124, 277], [125, 278], [126, 279], [127, 280], [128, 281], [129, 282], [130, 283], [131, 284], [132, 285], [133, 286]]
        }
        </script>
    </logic-editor>
    </div>

#### c. Unité de contrôle (CU)

Le bit 15 de `I` (`ci`: computation instruction) indique s'il s'agit d'une instruction d'ALU (`ci=1`) ou d'une adresse (`ci=0`, à destination de `A`).

=== "Solution"

    <div style="width: 100%; height: 608px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true, zoom: 70},
            defs: [
            {id: 'inv16', caption: 'Inv16', circuit: {
                components: {
                not0: {type: 'not', pos: [775, 190], in: 120, out: 121},
                not1: {type: 'not', pos: [745, 220], in: 122, out: 123},
                not2: {type: 'not', pos: [775, 245], in: 124, out: 125},
                not3: {type: 'not', pos: [745, 270], in: 126, out: 127},
                not4: {type: 'not', pos: [775, 300], in: 128, out: 129},
                not15: {type: 'not', pos: [745, 325], in: 150, out: 151},
                not5: {type: 'not', pos: [770, 355], in: 130, out: 131},
                not6: {type: 'not', pos: [740, 380], in: 132, out: 133},
                not7: {type: 'not', pos: [770, 410], in: 134, out: 135},
                not8: {type: 'not', pos: [740, 440], in: 136, out: 137},
                not9: {type: 'not', pos: [775, 470], in: 138, out: 139},
                not10: {type: 'not', pos: [740, 500], in: 140, out: 141},
                not11: {type: 'not', pos: [770, 530], in: 142, out: 143},
                not12: {type: 'not', pos: [740, 560], in: 144, out: 145},
                not13: {type: 'not', pos: [770, 585], in: 146, out: 147},
                not14: {type: 'not', pos: [740, 615], in: 148, out: 149},
                out0: {type: 'out', pos: [960, 400], id: '0-15', bits: 16, name: '/A'},
                in1: {type: 'in', pos: [595, 405], id: [16, 17, '19-32'], name: 'A', bits: 16, val: '1110000000000000'},
                },
                wires: [[121, 0], [123, 1], [125, 2], [127, 3], [129, 4], [151, 5], [131, 6], [133, 7], [135, 8], [137, 9], [139, 10], [141, 11], [143, 12], [145, 13], [147, 14], [149, 15], [16, 120], [17, 122], [19, 124], [20, 126], [21, 128], [22, 150], [23, 130], [24, 132], [25, 134], [26, 136], [27, 138], [28, 140], [29, 142], [30, 144], [31, 146], [32, 148]]
            }},
            {id: 'ul', caption: 'ul', circuit: {
                components: {
                mux0: {type: 'mux', pos: [1240, 545], in: '391-423', out: '424-439', from: 32, to: 16},
                mux1: {type: 'mux', pos: [940, 455], in: '447-479', out: '480-495', from: 32, to: 16},
                mux2: {type: 'mux', pos: [990, 990], in: '503-535', out: '536-551', from: 32, to: 16},
                pass5: {type: 'pass', pos: [165, 990], in: ['138-141', '238-241', '274-281'], out: ['314-321', '386-390', '440-442'], bits: 16, slant: 'up'},
                pass0: {type: 'pass', pos: [345, 455], in: '282-297', out: '298-313', bits: 16, slant: 'up'},
                pass11: {type: 'pass', pos: [1025, 990], in: ['661-668', '741-748'], out: '749-764', bits: 16, slant: 'up'},
                pass6: {type: 'pass', pos: [185, 285], in: '581-596', out: '597-612', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [185, 635], in: '621-636', out: '637-652', bits: 16, slant: 'up'},
                array2: {type: 'xor-array', pos: [695, 905], in: ['12-17', '19-36', '102-109'], out: ['142-151', '184-189'], bits: 16},
                pass1: {type: 'pass', pos: [365, 105], in: ['322-328', 345, '362-369'], out: '370-385', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [365, 815], in: ['8-11', '110-121'], out: '122-137', bits: 16, slant: 'up'},
                pass4: {type: 'pass', pos: [365, 1255], in: '242-257', out: '258-273', bits: 16, slant: 'up'},
                array1: {type: 'or-array', pos: [565, 545], in: '190-221', out: '222-237', bits: 16},
                pass12: {type: 'pass', pos: [1045, 630], in: '773-788', out: '789-804', bits: 16, slant: 'up'},
                array0: {type: 'and-array', pos: [680, 195], in: ['37-50', 52, '69-85'], out: '86-101', bits: 16},
                inv160: {type: 'custom-inv16', pos: [605, 1255], in: '152-167', out: '168-183'},
                pass9: {type: 'pass', pos: [735, 195], in: ['573-580', '613-620'], out: ['653-660', '693-700'], bits: 16, slant: 'down'},
                pass2: {type: 'pass', pos: [745, 1255], in: ['443-446', '496-502', '552-556'], out: '557-572', bits: 16, slant: 'up'},
                pass10: {type: 'pass', pos: [755, 370], in: '709-724', out: '725-740', bits: 16, slant: 'down'},
                pass8: {type: 'pass', pos: [765, 1075], in: '669-684', out: ['685-692', '701-708'], bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [1330, 545], id: '346-361', bits: 16, name: 'S'},
                in1: {type: 'in', pos: [135, 455], id: '329-344', name: 'A', bits: 16, val: '1111111011111111'},
                in3: {type: 'in', pos: [135, 990], id: '53-68', name: 'B', bits: 16, val: '1111111111111111'},
                in0: {type: 'in', pos: [1240, 250], orient: 's', id: 18, name: 'Op1', val: 1},
                in2: {type: 'in', pos: [995, 245], orient: 's', id: 51, name: 'Op0'},
                },
                wires: [[424, 346], [425, 347], [426, 348], [427, 349], [428, 350], [429, 351], [430, 352], [431, 353], [432, 354], [433, 355], [434, 356], [435, 357], [436, 358], [437, 359], [438, 360], [439, 361], [480, 391], [481, 392], [482, 393], [483, 394], [484, 395], [485, 396], [486, 397], [487, 398], [488, 399], [489, 400], [490, 401], [491, 402], [492, 403], [493, 404], [494, 405], [495, 406], [789, 407], [790, 408], [791, 409], [792, 410], [793, 411], [794, 412], [795, 413], [796, 414], [797, 415], [798, 416], [799, 417], [800, 418], [801, 419], [802, 420], [803, 421], [804, 422], [18, 423], [725, 447], [726, 448], [727, 449], [728, 450], [729, 451], [730, 452], [731, 453], [732, 454], [733, 455], [734, 456], [735, 457], [736, 458], [737, 459], [738, 460], [739, 461], [740, 462], [222, 463], [223, 464], [224, 465], [225, 466], [226, 467], [227, 468], [228, 469], [229, 470], [230, 471], [231, 472], [232, 473], [233, 474], [234, 475], [235, 476], [236, 477], [237, 478], [51, 479], [142, 503], [143, 504], [144, 505], [145, 506], [146, 507], [147, 508], [148, 509], [149, 510], [150, 511], [151, 512], [184, 513], [185, 514], [186, 515], [187, 516], [188, 517], [189, 518], [685, 519], [686, 520], [687, 521], [688, 522], [689, 523], [690, 524], [691, 525], [692, 526], [701, 527], [702, 528], [703, 529], [704, 530], [705, 531], [706, 532], [707, 533], [708, 534], [51, 535], [53, 138], [54, 139], [55, 140], [56, 141], [57, 238], [58, 239], [59, 240], [60, 241], [61, 274], [62, 275], [63, 276], [64, 277], [65, 278], [66, 279], [67, 280], [68, 281], [329, 282], [330, 283], [331, 284], [332, 285], [333, 286], [334, 287], [335, 288], [336, 289], [337, 290], [338, 291], [339, 292], [340, 293], [341, 294], [342, 295], [343, 296], [344, 297], [536, 661], [537, 662], [538, 663], [539, 664], [540, 665], [541, 666], [542, 667], [543, 668], [544, 741], [545, 742], [546, 743], [547, 744], [548, 745], [549, 746], [550, 747], [551, 748], [314, 581], [315, 582], [316, 583], [317, 584], [318, 585], [319, 586], [320, 587], [321, 588], [386, 589], [387, 590], [388, 591], [389, 592], [390, 593], [440, 594], [441, 595], [442, 596], [314, 621], [315, 622], [316, 623], [317, 624], [318, 625], [319, 626], [320, 627], [321, 628], [386, 629], [387, 630], [388, 631], [389, 632], [390, 633], [440, 634], [441, 635], [442, 636], [122, 12], [123, 13], [124, 14], [125, 15], [126, 16], [127, 17], [128, 19], [129, 20], [130, 21], [131, 22], [132, 23], [133, 24], [134, 25], [135, 26], [136, 27], [137, 28], [314, 29], [315, 30], [316, 31], [317, 32], [318, 33], [319, 34], [320, 35], [321, 36], [386, 102], [387, 103], [388, 104], [389, 105], [390, 106], [440, 107], [441, 108], [442, 109], [298, 322], [299, 323], [300, 324], [301, 325], [302, 326], [303, 327], [304, 328], [305, 345], [306, 362], [307, 363], [308, 364], [309, 365], [310, 366], [311, 367], [312, 368], [313, 369], [298, 8], [299, 9], [300, 10], [301, 11], [302, 110], [303, 111], [304, 112], [305, 113], [306, 114], [307, 115], [308, 116], [309, 117], [310, 118], [311, 119], [312, 120], [313, 121], [298, 242], [299, 243], [300, 244], [301, 245], [302, 246], [303, 247], [304, 248], [305, 249], [306, 250], [307, 251], [308, 252], [309, 253], [310, 254], [311, 255], [312, 256], [313, 257], [298, 190], [299, 191], [300, 192], [301, 193], [302, 194], [303, 195], [304, 196], [305, 197], [306, 198], [307, 199], [308, 200], [309, 201], [310, 202], [311, 203], [312, 204], [313, 205], [637, 206], [638, 207], [639, 208], [640, 209], [641, 210], [642, 211], [643, 212], [644, 213], [645, 214], [646, 215], [647, 216], [648, 217], [649, 218], [650, 219], [651, 220], [652, 221], [749, 773], [750, 774], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [370, 37], [371, 38], [372, 39], [373, 40], [374, 41], [375, 42], [376, 43], [377, 44], [378, 45], [379, 46], [380, 47], [381, 48], [382, 49], [383, 50], [384, 52], [385, 69], [597, 70], [598, 71], [599, 72], [600, 73], [601, 74], [602, 75], [603, 76], [604, 77], [605, 78], [606, 79], [607, 80], [608, 81], [609, 82], [610, 83], [611, 84], [612, 85], [258, 152], [259, 153], [260, 154], [261, 155], [262, 156], [263, 157], [264, 158], [265, 159], [266, 160], [267, 161], [268, 162], [269, 163], [270, 164], [271, 165], [272, 166], [273, 167], [86, 573], [87, 574], [88, 575], [89, 576], [90, 577], [91, 578], [92, 579], [93, 580], [94, 613], [95, 614], [96, 615], [97, 616], [98, 617], [99, 618], [100, 619], [101, 620], [168, 443], [169, 444], [170, 445], [171, 446], [172, 496], [173, 497], [174, 498], [175, 499], [176, 500], [177, 501], [178, 502], [179, 552], [180, 553], [181, 554], [182, 555], [183, 556], [653, 709], [654, 710], [655, 711], [656, 712], [657, 713], [658, 714], [659, 715], [660, 716], [693, 717], [694, 718], [695, 719], [696, 720], [697, 721], [698, 722], [699, 723], [700, 724], [557, 669], [558, 670], [559, 671], [560, 672], [561, 673], [562, 674], [563, 675], [564, 676], [565, 677], [566, 678], [567, 679], [568, 680], [569, 681], [570, 682], [571, 683], [572, 684]]
            }},
            {id: 'opp16', caption: 'Opp16', circuit: {
                components: {
                in0: {type: 'in', pos: [140, 725], orient: 'n', id: 223, name: 'In0', val: 1, isConstant: true},
                cnot0: {type: 'cnot-array', pos: [155, 380], in: '73-89', out: ['90-96', '113-116', '133-137'], bits: 16},
                adder1: {type: 'adder-array', pos: [285, 470], in: '152-184', out: '185-201', bits: 16},
                out1: {type: 'out', pos: [365, 470], id: ['4-13', '64-69'], bits: 16, name: '-B'},
                in2: {type: 'in', pos: [50, 380], id: '207-222', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[185, 4], [186, 5], [187, 6], [188, 7], [189, 8], [190, 9], [191, 10], [192, 11], [193, 12], [194, 13], [195, 64], [196, 65], [197, 66], [198, 67], [199, 68], [200, 69], [207, 73], [208, 74], [209, 75], [210, 76], [211, 77], [212, 78], [213, 79], [214, 80], [215, 81], [216, 82], [217, 83], [218, 84], [219, 85], [220, 86], [221, 87], [222, 88], [223, 89], [90, 152], [91, 153], [92, 154], [93, 155], [94, 156], [95, 157], [96, 158], [113, 159], [114, 160], [115, 161], [116, 162], [133, 163], [134, 164], [135, 165], [136, 166], [137, 167], [223, 168]]
            }},
            {id: 'sous16', caption: 'Sous16', circuit: {
                components: {
                adder0: {type: 'adder-array', pos: [525, 350], in: '14-46', out: '47-63', bits: 16},
                comp20: {type: 'custom-opp16', pos: [370, 515], in: '64-79', out: '80-95'},
                out0: {type: 'out', pos: [695, 350], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [165, 265], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                in0: {type: 'in', pos: [165, 515], id: '150-165', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[47, 117], [48, 118], [49, 119], [50, 120], [51, 121], [52, 122], [53, 123], [54, 124], [55, 125], [56, 126], [57, 127], [58, 128], [59, 129], [60, 130], [61, 131], [62, 132], [97, 14], [98, 15], [99, 16], [100, 17], [101, 18], [102, 19], [103, 20], [104, 21], [105, 22], [106, 23], [107, 24], [108, 25], [109, 26], [110, 27], [111, 28], [112, 29], [80, 30], [81, 31], [82, 32], [83, 33], [84, 34], [85, 35], [86, 36], [87, 37], [88, 38], [89, 39], [90, 40], [91, 41], [92, 42], [93, 43], [94, 44], [95, 45], [150, 64], [151, 65], [152, 66], [153, 67], [154, 68], [155, 69], [156, 70], [157, 71], [158, 72], [159, 73], [160, 74], [161, 75], [162, 76], [163, 77], [164, 78], [165, 79]]
            }},
            {id: 'al', caption: 'al', circuit: {
                components: {
                in2: {type: 'in', pos: [85, 840], id: 638, name: 'In0', val: 1, isConstant: true},
                pass0: {type: 'pass', pos: [90, 300], in: '510-525', out: '526-541', bits: 16, slant: 'up'},
                mux1: {type: 'mux', pos: [135, 830], in: '353-385', out: '386-401', from: 32, to: 16},
                mux0: {type: 'mux', pos: [885, 335], in: '297-329', out: '330-345', from: 32, to: 16},
                pass1: {type: 'pass', pos: [110, 150], in: '550-565', out: '566-581', bits: 16, slant: 'up'},
                pass2: {type: 'pass', pos: [110, 510], in: '590-605', out: '606-621', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [275, 830], in: '647-662', out: '663-678', bits: 16, slant: 'up'},
                adder0: {type: 'adder-array', pos: [500, 245], in: '192-224', out: '225-241', bits: 16},
                sous160: {type: 'custom-sous16', pos: [570, 670], in: '242-273', out: '274-289'},
                pass4: {type: 'pass', pos: [295, 335], in: '687-702', out: '703-718', bits: 16, slant: 'up'},
                pass6: {type: 'pass', pos: [660, 670], in: '735-750', out: '751-766', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [680, 420], in: '775-790', out: '791-806', bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [990, 335], id: '622-637', bits: 16, name: 'S'},
                in0: {type: 'in', pos: [40, 300], id: '470-485', name: 'A', bits: 16, val: '0000000000000110'},
                in1: {type: 'in', pos: [40, 745], id: '486-501', name: 'B', bits: 16, val: '0000000000000010'},
                in3: {type: 'in', pos: [885, 50], orient: 's', id: 807, name: 'Op1', val: 1},
                in4: {type: 'in', pos: [135, 590], orient: 's', id: 808, name: 'Op0', val: 1},
                },
                wires: [[330, 622], [331, 623], [332, 624], [333, 625], [334, 626], [335, 627], [336, 628], [337, 629], [338, 630], [339, 631], [340, 632], [341, 633], [342, 634], [343, 635], [344, 636], [345, 637], [470, 510], [471, 511], [472, 512], [473, 513], [474, 514], [475, 515], [476, 516], [477, 517], [478, 518], [479, 519], [480, 520], [481, 521], [482, 522], [483, 523], [484, 524], [485, 525], [486, 353], [487, 354], [488, 355], [489, 356], [490, 357], [491, 358], [492, 359], [493, 360], [494, 361], [495, 362], [496, 363], [497, 364], [498, 365], [499, 366], [500, 367], [501, 368], [638, 369], [808, 385], [225, 297], [226, 298], [227, 299], [228, 300], [229, 301], [230, 302], [231, 303], [232, 304], [233, 305], [234, 306], [235, 307], [236, 308], [237, 309], [238, 310], [239, 311], [240, 312], [791, 313], [792, 314], [793, 315], [794, 316], [795, 317], [796, 318], [797, 319], [798, 320], [799, 321], [800, 322], [801, 323], [802, 324], [803, 325], [804, 326], [805, 327], [806, 328], [807, 329], [526, 550], [527, 551], [528, 552], [529, 553], [530, 554], [531, 555], [532, 556], [533, 557], [534, 558], [535, 559], [536, 560], [537, 561], [538, 562], [539, 563], [540, 564], [541, 565], [526, 590], [527, 591], [528, 592], [529, 593], [530, 594], [531, 595], [532, 596], [533, 597], [534, 598], [535, 599], [536, 600], [537, 601], [538, 602], [539, 603], [540, 604], [541, 605], [386, 647], [387, 648], [388, 649], [389, 650], [390, 651], [391, 652], [392, 653], [393, 654], [394, 655], [395, 656], [396, 657], [397, 658], [398, 659], [399, 660], [400, 661], [401, 662], [566, 192], [567, 193], [568, 194], [569, 195], [570, 196], [571, 197], [572, 198], [573, 199], [574, 200], [575, 201], [576, 202], [577, 203], [578, 204], [579, 205], [580, 206], [581, 207], [703, 208], [704, 209], [705, 210], [706, 211], [707, 212], [708, 213], [709, 214], [710, 215], [711, 216], [712, 217], [713, 218], [714, 219], [715, 220], [716, 221], [717, 222], [718, 223], [606, 242], [607, 243], [608, 244], [609, 245], [610, 246], [611, 247], [612, 248], [613, 249], [614, 250], [615, 251], [616, 252], [617, 253], [618, 254], [619, 255], [620, 256], [621, 257], [663, 258], [664, 259], [665, 260], [666, 261], [667, 262], [668, 263], [669, 264], [670, 265], [671, 266], [672, 267], [673, 268], [674, 269], [675, 270], [676, 271], [677, 272], [678, 273], [663, 687], [664, 688], [665, 689], [666, 690], [667, 691], [668, 692], [669, 693], [670, 694], [671, 695], [672, 696], [673, 697], [674, 698], [675, 699], [676, 700], [677, 701], [678, 702], [274, 735], [275, 736], [276, 737], [277, 738], [278, 739], [279, 740], [280, 741], [281, 742], [282, 743], [283, 744], [284, 745], [285, 746], [286, 747], [287, 748], [288, 749], [289, 750], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [765, 789], [766, 790]]
            }},
            {id: 'alu', caption: 'ALU', circuit: {
                components: {
                mux0: {type: 'mux', pos: [715, 280], in: '258-290', out: '291-306', from: 32, to: 16},
                pass4: {type: 'pass', pos: [460, 200], in: '327-342', out: '343-358', bits: 16, slant: 'down'},
                mux2: {type: 'mux', pos: [660, 1265], in: ['122-135', '171-178', '211-218', '251-253'], out: ['254-257', '307-318'], from: 32, to: 16},
                pass6: {type: 'pass', pos: [270, 1185], in: '407-422', out: '423-438', bits: 16, slant: 'up'},
                mux1: {type: 'mux', pos: [1475, 615], in: ['58-61', '78-81', '108-114', '136-153'], out: '154-169', from: 32, to: 16},
                mux3: {type: 'mux', pos: [860, 365], in: [319, '513-527', '560-576'], out: '577-592', from: 32, to: 16},
                ul0: {type: 'custom-ul', pos: [1115, 525], in: ['0-7', '765-772', '805-822'], out: '823-838'},
                al0: {type: 'custom-al', pos: [1195, 1105], in: '8-41', out: '42-57'},
                pass5: {type: 'pass', pos: [480, 1350], in: '367-382', out: '383-398', bits: 16, slant: 'down'},
                pass9: {type: 'pass', pos: [720, 1265], in: '528-543', out: '544-559', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [290, 365], in: '447-462', out: '463-478', bits: 16, slant: 'up'},
                pass10: {type: 'pass', pos: [905, 365], in: '481-496', out: '497-512', bits: 16, slant: 'down'},
                pass3: {type: 'pass', pos: [1265, 1105], in: '219-234', out: '235-250', bits: 16, slant: 'up'},
                pass11: {type: 'pass', pos: [740, 685], in: '608-623', out: '624-639', bits: 16, slant: 'up'},
                pass8: {type: 'pass', pos: [925, 945], in: ['320-326', '359-366', 399], out: ['400-406', '439-446', 480], bits: 16, slant: 'down'},
                pass2: {type: 'pass', pos: [1285, 700], in: '179-194', out: '195-210', bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [1555, 615], id: ['598-607', '640-645'], bits: 16, name: 'S'},
                in0: {type: 'in', pos: [205, 200], id: '62-77', name: 'A', bits: 16, val: '0000000000000100'},
                in1: {type: 'in', pos: [220, 1185], id: '82-97', name: 'B', bits: 16, val: '0000000000000001'},
                in3: {type: 'in', pos: [1470, 80], orient: 's', id: 170, name: 'u'},
                in6: {type: 'in', pos: [1200, 95], orient: 's', id: 99, name: 'Op1'},
                in2: {type: 'in', pos: [1165, 95], orient: 's', id: 98, name: 'Op0'},
                in5: {type: 'in', pos: [860, 75], orient: 's', id: 593, name: 'zx'},
                in4: {type: 'in', pos: [660, 70], orient: 's', id: 479, name: 'sw'},
                },
                wires: [[154, 598], [155, 599], [156, 600], [157, 601], [158, 602], [159, 603], [160, 604], [161, 605], [162, 606], [163, 607], [164, 640], [165, 641], [166, 642], [167, 643], [168, 644], [169, 645], [62, 258], [63, 259], [64, 260], [65, 261], [66, 262], [67, 263], [68, 264], [69, 265], [70, 266], [71, 267], [72, 268], [73, 269], [74, 270], [75, 271], [76, 272], [77, 273], [463, 274], [464, 275], [465, 276], [466, 277], [467, 278], [468, 279], [469, 280], [470, 281], [471, 282], [472, 283], [473, 284], [474, 285], [475, 286], [476, 287], [477, 288], [478, 289], [479, 290], [62, 327], [63, 328], [64, 329], [65, 330], [66, 331], [67, 332], [68, 333], [69, 334], [70, 335], [71, 336], [72, 337], [73, 338], [74, 339], [75, 340], [76, 341], [77, 342], [82, 122], [83, 123], [84, 124], [85, 125], [86, 126], [87, 127], [88, 128], [89, 129], [90, 130], [91, 131], [92, 132], [93, 133], [94, 134], [95, 135], [96, 171], [97, 172], [383, 173], [384, 174], [385, 175], [386, 176], [387, 177], [388, 178], [389, 211], [390, 212], [391, 213], [392, 214], [393, 215], [394, 216], [395, 217], [396, 218], [397, 251], [398, 252], [479, 253], [82, 407], [83, 408], [84, 409], [85, 410], [86, 411], [87, 412], [88, 413], [89, 414], [90, 415], [91, 416], [92, 417], [93, 418], [94, 419], [95, 420], [96, 421], [97, 422], [823, 58], [824, 59], [825, 60], [826, 61], [827, 78], [828, 79], [829, 80], [830, 81], [831, 108], [832, 109], [833, 110], [834, 111], [835, 112], [836, 113], [837, 114], [838, 136], [195, 137], [196, 138], [197, 139], [198, 140], [199, 141], [200, 142], [201, 143], [202, 144], [203, 145], [204, 146], [205, 147], [206, 148], [207, 149], [208, 150], [209, 151], [210, 152], [170, 153], [291, 319], [292, 513], [293, 514], [294, 515], [295, 516], [296, 517], [297, 518], [298, 519], [299, 520], [300, 521], [301, 522], [302, 523], [303, 524], [304, 525], [305, 526], [306, 527], [593, 576], [577, 0], [578, 1], [579, 2], [580, 3], [581, 4], [582, 5], [583, 6], [584, 7], [585, 765], [586, 766], [587, 767], [588, 768], [589, 769], [590, 770], [591, 771], [592, 772], [624, 805], [625, 806], [626, 807], [627, 808], [628, 809], [629, 810], [630, 811], [631, 812], [632, 813], [633, 814], [634, 815], [635, 816], [636, 817], [637, 818], [638, 819], [639, 820], [99, 821, {via: [[1200, 190, 'w']]}], [98, 822], [400, 8], [401, 9], [402, 10], [403, 11], [404, 12], [405, 13], [406, 14], [439, 15], [440, 16], [441, 17], [442, 18], [443, 19], [444, 20], [445, 21], [446, 22], [480, 23], [254, 24], [255, 25], [256, 26], [257, 27], [307, 28], [308, 29], [309, 30], [310, 31], [311, 32], [312, 33], [313, 34], [314, 35], [315, 36], [316, 37], [317, 38], [318, 39], [99, 40], [98, 41], [343, 367], [344, 368], [345, 369], [346, 370], [347, 371], [348, 372], [349, 373], [350, 374], [351, 375], [352, 376], [353, 377], [354, 378], [355, 379], [356, 380], [357, 381], [358, 382], [254, 528], [255, 529], [256, 530], [257, 531], [307, 532], [308, 533], [309, 534], [310, 535], [311, 536], [312, 537], [313, 538], [314, 539], [315, 540], [316, 541], [317, 542], [318, 543], [423, 447], [424, 448], [425, 449], [426, 450], [427, 451], [428, 452], [429, 453], [430, 454], [431, 455], [432, 456], [433, 457], [434, 458], [435, 459], [436, 460], [437, 461], [438, 462], [577, 481], [578, 482], [579, 483], [580, 484], [581, 485], [583, 487], [584, 488], [586, 490], [588, 492], [589, 493], [42, 219], [43, 220], [44, 221], [45, 222], [46, 223], [47, 224], [48, 225], [49, 226], [50, 227], [51, 228], [52, 229], [53, 230], [54, 231], [55, 232], [56, 233], [57, 234], [544, 608], [545, 609], [546, 610], [547, 611], [548, 612], [549, 613], [550, 614], [551, 615], [552, 616], [553, 617], [554, 618], [555, 619], [556, 620], [557, 621], [558, 622], [559, 623], [497, 320], [498, 321], [499, 322], [500, 323], [501, 324], [502, 325], [503, 326], [504, 359], [505, 360], [506, 361], [507, 362], [508, 363], [509, 364], [510, 365], [511, 366], [512, 399], [235, 179], [236, 180], [237, 181], [238, 182], [239, 183], [240, 184], [241, 185], [242, 186], [243, 187], [244, 188], [245, 189], [246, 190], [247, 191], [248, 192], [249, 193], [250, 194]]
            }},
            {id: 'iszero', caption: 'isZero', circuit: {
                components: {
                or0: {type: 'or', pos: [305, 95], in: [39, 40], out: 41},
                or1: {type: 'or', pos: [305, 140], in: [42, 43], out: 44},
                or2: {type: 'or', pos: [305, 185], in: [45, 46], out: 47},
                or3: {type: 'or', pos: [305, 230], in: [48, 49], out: 50},
                or4: {type: 'or', pos: [305, 270], in: [51, 52], out: 53},
                or5: {type: 'or', pos: [305, 310], in: [54, 55], out: 56},
                or7: {type: 'or', pos: [305, 350], in: [60, 61], out: 62},
                or6: {type: 'or', pos: [305, 390], in: [57, 58], out: 59},
                or8: {type: 'or', pos: [410, 120], in: [63, 64], out: 65},
                or9: {type: 'or', pos: [410, 205], in: [66, 67], out: 68},
                or10: {type: 'or', pos: [410, 290], in: [69, 70], out: 71},
                or11: {type: 'or', pos: [405, 375], in: [72, 73], out: 74},
                or12: {type: 'or', pos: [510, 165], in: [75, 76], out: 77},
                or13: {type: 'or', pos: [510, 330], in: [78, 79], out: 80},
                or14: {type: 'or', pos: [610, 235], in: [81, 82], out: 83},
                not0: {type: 'not', pos: [690, 235], in: 84, out: 85},
                out1: {type: 'out', pos: [760, 235], id: 88, name: 'nul?'},
                in0: {type: 'in', pos: [185, 215], id: '4-19', name: 'A', bits: 16, val: '0000000000000010'},
                },
                wires: [[85, 88], [4, 39], [5, 40], [6, 42], [7, 43], [8, 45], [9, 46], [10, 48], [11, 49], [12, 51], [13, 52], [14, 54], [15, 55], [16, 60], [17, 61], [18, 57], [19, 58], [41, 63], [44, 64], [47, 66], [50, 67], [53, 69], [56, 70], [62, 72], [59, 73], [65, 75], [68, 76], [71, 78], [74, 79], [77, 81], [80, 82], [83, 84]]
            }},
            {id: 'isneg', caption: 'isNeg', circuit: {
                components: {
                out1: {type: 'out', pos: [570, 660], id: 118, name: 'Out0'},
                in0: {type: 'in', pos: [405, 635], id: '119-134', name: 'In0', bits: 16, val: '0000000001001010'},
                },
                wires: [[134, 118]]
            }},
            {id: 'condition', caption: 'Condition', circuit: {
                components: {
                and0: {type: 'and', pos: [550, 350], in: [252, 253], out: 254},
                and1: {type: 'and', pos: [550, 405], in: [255, 256], out: 257},
                and2: {type: 'and', pos: [715, 750], in: [266, 267], out: 268},
                isneg0: {type: 'custom-isneg', pos: [395, 180], in: '135-150', out: 151},
                pass0: {type: 'pass', pos: [165, 180], in: '180-195', out: '196-211', bits: 16, slant: 'down'},
                or0: {type: 'or', pos: [655, 375], in: [258, 259], out: 260},
                or1: {type: 'or', pos: [835, 510], in: [269, 270], out: 271},
                nor0: {type: 'or', pos: [545, 565], in: [261, 262], out: 263},
                pass1: {type: 'pass', pos: [185, 575], in: '220-235', out: '236-251', bits: 16, slant: 'down'},
                not0: {type: 'not', pos: [625, 565], in: 264, out: 265},
                iszero0: {type: 'custom-iszero', pos: [400, 575], in: '89-104', out: 105},
                out0: {type: 'out', pos: [905, 510], id: 23, name: 'Verif'},
                in0: {type: 'in', pos: [120, 180], id: '156-171', name: 'A', bits: 16},
                in1: {type: 'in', pos: [50, 360], id: 20, name: 'lt'},
                in2: {type: 'in', pos: [50, 395], id: 21, name: 'eq', val: 1},
                in3: {type: 'in', pos: [70, 760], id: 22, name: 'gt'},
                },
                wires: [[271, 23], [151, 252], [20, 253], [21, 255], [105, 256], [265, 266], [22, 267], [156, 135], [157, 136], [158, 137], [159, 138], [160, 139], [161, 140], [162, 141], [163, 142], [164, 143], [165, 144], [166, 145], [167, 146], [168, 147], [169, 148], [170, 149], [171, 150], [156, 180], [157, 181], [158, 182], [159, 183], [160, 184], [161, 185], [162, 186], [163, 187], [164, 188], [165, 189], [166, 190], [167, 191], [168, 192], [169, 193], [170, 194], [171, 195], [254, 258], [257, 259], [260, 269], [268, 270], [151, 261], [105, 262], [196, 220], [197, 221], [198, 222], [199, 223], [200, 224], [201, 225], [202, 226], [203, 227], [204, 228], [205, 229], [206, 230], [207, 231], [208, 232], [209, 233], [210, 234], [211, 235], [263, 264], [236, 89], [237, 90], [238, 91], [239, 92], [240, 93], [241, 94], [242, 95], [243, 96], [244, 97], [245, 98], [246, 99], [247, 100], [248, 101], [249, 102], [250, 103], [251, 104]]
            }},
            {id: 'aluinstruction', caption: 'ALU instruction', circuit: {
                components: {
                condition0: {type: 'custom-condition', pos: [300, 660], orient: 's', in: '0-18', out: 19},
                alu0: {type: 'custom-alu', pos: [830, 330], orient: 's', in: ['100-107', '115-121', '594-597', '646-663'], out: '664-679'},
                mux0: {type: 'mux', pos: [670, 125], orient: 's', in: '255-287', out: '288-303', from: 32, to: 16, bottom: true},
                pass0: {type: 'pass', pos: [830, 410], orient: 's', in: ['79-82', 99, '108-110', 134, '136-142'], out: '143-158', bits: 16, slant: 'down'},
                pass1: {type: 'pass', pos: [330, 430], orient: 's', in: '167-182', out: '183-198', bits: 16, slant: 'down'},
                out0: {type: 'out', pos: [1275, 155], id: 40, name: 'a'},
                out1: {type: 'out', pos: [1340, 170], id: 41, name: 'd'},
                out2: {type: 'out', pos: [1275, 185], id: 42, name: '✱a'},
                in0: {type: 'in', pos: [40, 205], id: '24-39', name: 'I', bits: 16, val: '0000000000000010'},
                in2: {type: 'in', pos: [990, 55], orient: 's', id: '83-98', name: 'D', bits: 16, val: '0000000000000100'},
                in1: {type: 'in', pos: [755, 55], orient: 's', id: '63-78', name: 'A', bits: 16, val: '0000000001000010'},
                in3: {type: 'in', pos: [585, 55], orient: 's', id: ['111-114', '122-133'], name: '✱A', bits: 16, val: '0000000000000010'},
                out3: {type: 'out', pos: [830, 705], orient: 's', id: '43-58', bits: 16, name: 'R'},
                out4: {type: 'out', pos: [300, 755], orient: 's', id: 135, name: 'j'},
                },
                wires: [[27, 40], [28, 41], [29, 42], [664, 43], [665, 44], [666, 45], [667, 46], [668, 47], [669, 48], [670, 49], [671, 50], [672, 51], [673, 52], [674, 53], [675, 54], [676, 55], [677, 56], [678, 57], [679, 58], [19, 135], [183, 0], [184, 1], [185, 2], [186, 3], [187, 4], [188, 5], [189, 6], [190, 7], [191, 8], [192, 9], [193, 10], [194, 11], [195, 12], [196, 13], [197, 14], [198, 15], [26, 16, {via: [[160, 150, 's']]}], [25, 17, {via: [[140, 140, 's']]}], [24, 18, {via: [[120, 130, 's']]}], [83, 100], [84, 101], [85, 102], [86, 103], [87, 104], [88, 105], [89, 106], [90, 107], [91, 115], [92, 116], [93, 117], [94, 118], [95, 119], [96, 120], [97, 121], [98, 594], [288, 595], [289, 596], [290, 597], [291, 646], [292, 647], [293, 648], [294, 649], [295, 650], [296, 651], [297, 652], [298, 653], [299, 654], [300, 655], [301, 656], [302, 657], [303, 658], [34, 659, {via: [[1205, 240], [1205, 370, 's']]}], [33, 660, {via: [[1215, 230], [1215, 350, 's']]}], [32, 661, {via: [[1225, 220], [1225, 330, 's']]}], [31, 662, {via: [[1235, 210], [1235, 310, 's']]}], [30, 663, {via: [[1245, 200], [1245, 290, 's']]}], [63, 255], [64, 256], [65, 257], [66, 258], [67, 259], [68, 260], [69, 261], [70, 262], [71, 263], [72, 264], [73, 265], [74, 266], [75, 267], [76, 268], [77, 269], [78, 270], [111, 271], [112, 272], [113, 273], [114, 274], [122, 275], [123, 276], [124, 277], [125, 278], [126, 279], [127, 280], [128, 281], [129, 282], [130, 283], [131, 284], [132, 285], [133, 286], [36, 287, {via: [[490, 250]]}], [664, 79], [665, 80], [666, 81], [667, 82], [668, 99], [669, 108], [670, 109], [671, 110], [672, 134], [673, 136], [674, 137], [675, 138], [676, 139], [677, 140], [678, 141], [679, 142], [143, 167], [144, 168], [145, 169], [146, 170], [147, 171], [148, 172], [149, 173], [150, 174], [151, 175], [152, 176], [153, 177], [154, 178], [155, 179], [156, 180], [157, 181], [158, 182]]
            }}
            ],
            components: {
            aluinstruction0: {type: 'custom-aluinstruction', pos: [790, 305], in: ['20-23', '59-62', '159-166', '199-246'], out: ['247-254', '304-315']},
            in0: {type: 'in', pos: [60, 305], id: '320-335', name: 'I', bits: 16, val: '0000000000000010'},
            in1: {type: 'in', pos: [1110, 60], orient: 's', id: '340-355', name: 'D', bits: 16, val: '0000000000000010'},
            in2: {type: 'in', pos: [790, 60], orient: 's', id: '360-375', name: 'A', bits: 16, val: '0000000000000100'},
            in3: {type: 'in', pos: [470, 60], orient: 's', id: '380-395', name: '✱A', bits: 16, val: '0000000000000100'},
            out1: {type: 'out', pos: [640, 805], orient: 's', id: 416, name: 'j'},
            out2: {type: 'out', pos: [1515, 275], id: 417, name: 'a'},
            out3: {type: 'out', pos: [1515, 350], id: 418, name: 'd'},
            out4: {type: 'out', pos: [1515, 420], id: 419, name: '✱a'},
            mux0: {type: 'mux', pos: [885, 690], orient: 's', in: '476-508', out: '509-524', from: 32, to: 16, bottom: true},
            pass0: {type: 'pass', pos: [100, 305], in: '533-548', out: '549-564', bits: 16, slant: 'up'},
            pass1: {type: 'pass', pos: [120, 570], in: '573-588', out: '589-604', bits: 16, slant: 'up'},
            pass2: {type: 'pass', pos: [970, 505], orient: 's', in: '613-628', out: '629-644', bits: 16, slant: 'up'},
            out0: {type: 'out', pos: [885, 800], orient: 's', id: '1-16', bits: 16, name: 'R'},
            in4: {type: 'in', pos: [1385, 200], orient: 's', id: 104, isConstant: true},
            in5: {type: 'in', pos: [1330, 200], orient: 's', id: 105, val: 1, isConstant: true},
            mux2: {type: 'mux', pos: [640, 755], orient: 's', in: '110-112', out: 113, from: 2, to: 1, bottom: true},
            in6: {type: 'in', pos: [650, 705], orient: 's', id: 0, isConstant: true},
            pass3: {type: 'pass', pos: [1455, 115], in: 17, out: 18},
            mux3: {type: 'mux', pos: [1465, 420], in: [19, 24, 25], out: 26, from: 2, to: 1},
            mux4: {type: 'mux', pos: [1425, 350], in: '27-29', out: 30, from: 2, to: 1},
            mux1: {type: 'mux', pos: [1350, 275], in: '31-33', out: 34, from: 2, to: 1},
            },
            wires: [[320, 20], [321, 21], [322, 22], [323, 23], [324, 59], [325, 60], [326, 61], [327, 62], [328, 159], [329, 160], [330, 161], [331, 162], [332, 163], [333, 164], [334, 165], [335, 166], [340, 199], [341, 200], [342, 201], [343, 202], [344, 203], [345, 204], [346, 205], [347, 206], [348, 207], [349, 208], [350, 209], [351, 210], [352, 211], [353, 212], [354, 213], [355, 214], [360, 215], [361, 216], [362, 217], [363, 218], [364, 219], [365, 220], [366, 221], [367, 222], [368, 223], [369, 224], [370, 225], [371, 226], [372, 227], [373, 228], [374, 229], [375, 230], [380, 231], [381, 232], [382, 233], [383, 234], [384, 235], [385, 236], [386, 237], [387, 238], [388, 239], [389, 240], [390, 241], [391, 242], [392, 243], [393, 244], [394, 245], [395, 246], [335, 508, {via: [[80, 695]]}], [250, 492], [251, 493], [252, 494], [253, 495], [254, 496], [304, 497], [305, 498], [306, 499], [307, 500], [308, 501], [309, 502], [310, 503], [311, 504], [312, 505], [313, 506], [314, 507], [320, 533], [321, 534], [322, 535], [323, 536], [324, 537], [325, 538], [326, 539], [327, 540], [328, 541], [329, 542], [330, 543], [331, 544], [332, 545], [333, 546], [334, 547], [335, 548], [549, 573], [550, 574], [551, 575], [552, 576], [553, 577], [554, 578], [555, 579], [556, 580], [557, 581], [558, 582], [559, 583], [560, 584], [561, 585], [562, 586], [563, 587], [564, 588], [589, 613], [590, 614], [591, 615], [592, 616], [593, 617], [594, 618], [595, 619], [596, 620], [597, 621], [598, 622], [599, 623], [600, 624], [601, 625], [602, 626], [603, 627], [604, 628], [629, 476], [630, 477], [631, 478], [632, 479], [633, 480], [634, 481], [635, 482], [636, 483], [637, 484], [638, 485], [639, 486], [640, 487], [641, 488], [642, 489], [643, 490], [644, 491], [509, 1], [510, 2], [511, 3], [512, 4], [513, 5], [514, 6], [515, 7], [516, 8], [517, 9], [518, 10], [519, 11], [520, 12], [521, 13], [522, 14], [523, 15], [524, 16], [247, 32], [34, 417], [248, 28, {via: [[1310, 305], [1310, 360]]}], [30, 418], [105, 31], [104, 27, {via: [[1385, 340, 's']]}], [26, 419], [104, 19, {via: [[1385, 410, 's']]}], [249, 24, {via: [[1285, 430]]}], [335, 112, {via: [[80, 760]]}], [315, 111], [113, 416], [0, 110], [335, 17, {via: [[95, 380, 'n'], [95, 115, 'n']]}], [18, 25], [18, 29, {via: [[1425, 115, 's']]}], [18, 33, {via: [[1350, 115, 's']]}]]
        }
        </script>
    </logic-editor>
    </div>

#### d. Ordinateur

L'architecture mise en œuvre est de type *Harvard* (programme et données sont dans des mémoires distinctes) avec uniquement 2 registres internes (`A` et `D`).

- La ROM (instructions du programme) est parcourue séquentiellement grâce à un compteur (adresses incrémentales) où par saut (adresse donnée par `A`).
- Chaque instruction est analysée par l'Unité de Contrôle (UC) pour être:
    - soit traitée par l'ALU
    - fournir une donnée à la mémoire combinée (utilisation des bits `a`, `d` et `✱a`).

=== "Solution"

    <div style="width: 100%; height: 822.25px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true, zoom: 65},
            defs: [
            {id: 'inv16', caption: 'Inv16', circuit: {
                components: {
                not0: {type: 'not', pos: [775, 190], in: 120, out: 121},
                not1: {type: 'not', pos: [745, 220], in: 122, out: 123},
                not2: {type: 'not', pos: [775, 245], in: 124, out: 125},
                not3: {type: 'not', pos: [745, 270], in: 126, out: 127},
                not4: {type: 'not', pos: [775, 300], in: 128, out: 129},
                not15: {type: 'not', pos: [745, 325], in: 150, out: 151},
                not5: {type: 'not', pos: [770, 355], in: 130, out: 131},
                not6: {type: 'not', pos: [740, 380], in: 132, out: 133},
                not7: {type: 'not', pos: [770, 410], in: 134, out: 135},
                not8: {type: 'not', pos: [740, 440], in: 136, out: 137},
                not9: {type: 'not', pos: [775, 470], in: 138, out: 139},
                not10: {type: 'not', pos: [740, 500], in: 140, out: 141},
                not11: {type: 'not', pos: [770, 530], in: 142, out: 143},
                not12: {type: 'not', pos: [740, 560], in: 144, out: 145},
                not13: {type: 'not', pos: [770, 585], in: 146, out: 147},
                not14: {type: 'not', pos: [740, 615], in: 148, out: 149},
                out0: {type: 'out', pos: [960, 400], id: '0-15', bits: 16, name: '/A'},
                in1: {type: 'in', pos: [595, 405], id: [16, 17, '19-32'], name: 'A', bits: 16, val: '1110000000000000'},
                },
                wires: [[121, 0], [123, 1], [125, 2], [127, 3], [129, 4], [151, 5], [131, 6], [133, 7], [135, 8], [137, 9], [139, 10], [141, 11], [143, 12], [145, 13], [147, 14], [149, 15], [16, 120], [17, 122], [19, 124], [20, 126], [21, 128], [22, 150], [23, 130], [24, 132], [25, 134], [26, 136], [27, 138], [28, 140], [29, 142], [30, 144], [31, 146], [32, 148]]
            }},
            {id: 'ul', caption: 'ul', circuit: {
                components: {
                mux0: {type: 'mux', pos: [1240, 545], in: '391-423', out: '424-439', from: 32, to: 16},
                mux1: {type: 'mux', pos: [940, 455], in: '447-479', out: '480-495', from: 32, to: 16},
                mux2: {type: 'mux', pos: [990, 990], in: '503-535', out: '536-551', from: 32, to: 16},
                pass5: {type: 'pass', pos: [165, 990], in: ['138-141', '238-241', '274-281'], out: ['314-321', '386-390', '440-442'], bits: 16, slant: 'up'},
                pass0: {type: 'pass', pos: [345, 455], in: '282-297', out: '298-313', bits: 16, slant: 'up'},
                pass11: {type: 'pass', pos: [1025, 990], in: ['661-668', '741-748'], out: '749-764', bits: 16, slant: 'up'},
                pass6: {type: 'pass', pos: [185, 285], in: '581-596', out: '597-612', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [185, 635], in: '621-636', out: '637-652', bits: 16, slant: 'up'},
                array2: {type: 'xor-array', pos: [695, 905], in: ['12-17', '19-36', '102-109'], out: ['142-151', '184-189'], bits: 16},
                pass1: {type: 'pass', pos: [365, 105], in: ['322-328', 345, '362-369'], out: '370-385', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [365, 815], in: ['8-11', '110-121'], out: '122-137', bits: 16, slant: 'up'},
                pass4: {type: 'pass', pos: [365, 1255], in: '242-257', out: '258-273', bits: 16, slant: 'up'},
                array1: {type: 'or-array', pos: [565, 545], in: '190-221', out: '222-237', bits: 16},
                pass12: {type: 'pass', pos: [1045, 630], in: '773-788', out: '789-804', bits: 16, slant: 'up'},
                array0: {type: 'and-array', pos: [680, 195], in: ['37-50', 52, '69-85'], out: '86-101', bits: 16},
                inv160: {type: 'custom-inv16', pos: [605, 1255], in: '152-167', out: '168-183'},
                pass9: {type: 'pass', pos: [735, 195], in: ['573-580', '613-620'], out: ['653-660', '693-700'], bits: 16, slant: 'down'},
                pass2: {type: 'pass', pos: [745, 1255], in: ['443-446', '496-502', '552-556'], out: '557-572', bits: 16, slant: 'up'},
                pass10: {type: 'pass', pos: [755, 370], in: '709-724', out: '725-740', bits: 16, slant: 'down'},
                pass8: {type: 'pass', pos: [765, 1075], in: '669-684', out: ['685-692', '701-708'], bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [1330, 545], id: '346-361', bits: 16, name: 'S'},
                in1: {type: 'in', pos: [135, 455], id: '329-344', name: 'A', bits: 16, val: '1111111011111111'},
                in3: {type: 'in', pos: [135, 990], id: '53-68', name: 'B', bits: 16, val: '1111111111111111'},
                in0: {type: 'in', pos: [1240, 250], orient: 's', id: 18, name: 'Op1', val: 1},
                in2: {type: 'in', pos: [995, 245], orient: 's', id: 51, name: 'Op0'},
                },
                wires: [[424, 346], [425, 347], [426, 348], [427, 349], [428, 350], [429, 351], [430, 352], [431, 353], [432, 354], [433, 355], [434, 356], [435, 357], [436, 358], [437, 359], [438, 360], [439, 361], [480, 391], [481, 392], [482, 393], [483, 394], [484, 395], [485, 396], [486, 397], [487, 398], [488, 399], [489, 400], [490, 401], [491, 402], [492, 403], [493, 404], [494, 405], [495, 406], [789, 407], [790, 408], [791, 409], [792, 410], [793, 411], [794, 412], [795, 413], [796, 414], [797, 415], [798, 416], [799, 417], [800, 418], [801, 419], [802, 420], [803, 421], [804, 422], [18, 423], [725, 447], [726, 448], [727, 449], [728, 450], [729, 451], [730, 452], [731, 453], [732, 454], [733, 455], [734, 456], [735, 457], [736, 458], [737, 459], [738, 460], [739, 461], [740, 462], [222, 463], [223, 464], [224, 465], [225, 466], [226, 467], [227, 468], [228, 469], [229, 470], [230, 471], [231, 472], [232, 473], [233, 474], [234, 475], [235, 476], [236, 477], [237, 478], [51, 479], [142, 503], [143, 504], [144, 505], [145, 506], [146, 507], [147, 508], [148, 509], [149, 510], [150, 511], [151, 512], [184, 513], [185, 514], [186, 515], [187, 516], [188, 517], [189, 518], [685, 519], [686, 520], [687, 521], [688, 522], [689, 523], [690, 524], [691, 525], [692, 526], [701, 527], [702, 528], [703, 529], [704, 530], [705, 531], [706, 532], [707, 533], [708, 534], [51, 535], [53, 138], [54, 139], [55, 140], [56, 141], [57, 238], [58, 239], [59, 240], [60, 241], [61, 274], [62, 275], [63, 276], [64, 277], [65, 278], [66, 279], [67, 280], [68, 281], [329, 282], [330, 283], [331, 284], [332, 285], [333, 286], [334, 287], [335, 288], [336, 289], [337, 290], [338, 291], [339, 292], [340, 293], [341, 294], [342, 295], [343, 296], [344, 297], [536, 661], [537, 662], [538, 663], [539, 664], [540, 665], [541, 666], [542, 667], [543, 668], [544, 741], [545, 742], [546, 743], [547, 744], [548, 745], [549, 746], [550, 747], [551, 748], [314, 581], [315, 582], [316, 583], [317, 584], [318, 585], [319, 586], [320, 587], [321, 588], [386, 589], [387, 590], [388, 591], [389, 592], [390, 593], [440, 594], [441, 595], [442, 596], [314, 621], [315, 622], [316, 623], [317, 624], [318, 625], [319, 626], [320, 627], [321, 628], [386, 629], [387, 630], [388, 631], [389, 632], [390, 633], [440, 634], [441, 635], [442, 636], [122, 12], [123, 13], [124, 14], [125, 15], [126, 16], [127, 17], [128, 19], [129, 20], [130, 21], [131, 22], [132, 23], [133, 24], [134, 25], [135, 26], [136, 27], [137, 28], [314, 29], [315, 30], [316, 31], [317, 32], [318, 33], [319, 34], [320, 35], [321, 36], [386, 102], [387, 103], [388, 104], [389, 105], [390, 106], [440, 107], [441, 108], [442, 109], [298, 322], [299, 323], [300, 324], [301, 325], [302, 326], [303, 327], [304, 328], [305, 345], [306, 362], [307, 363], [308, 364], [309, 365], [310, 366], [311, 367], [312, 368], [313, 369], [298, 8], [299, 9], [300, 10], [301, 11], [302, 110], [303, 111], [304, 112], [305, 113], [306, 114], [307, 115], [308, 116], [309, 117], [310, 118], [311, 119], [312, 120], [313, 121], [298, 242], [299, 243], [300, 244], [301, 245], [302, 246], [303, 247], [304, 248], [305, 249], [306, 250], [307, 251], [308, 252], [309, 253], [310, 254], [311, 255], [312, 256], [313, 257], [298, 190], [299, 191], [300, 192], [301, 193], [302, 194], [303, 195], [304, 196], [305, 197], [306, 198], [307, 199], [308, 200], [309, 201], [310, 202], [311, 203], [312, 204], [313, 205], [637, 206], [638, 207], [639, 208], [640, 209], [641, 210], [642, 211], [643, 212], [644, 213], [645, 214], [646, 215], [647, 216], [648, 217], [649, 218], [650, 219], [651, 220], [652, 221], [749, 773], [750, 774], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [370, 37], [371, 38], [372, 39], [373, 40], [374, 41], [375, 42], [376, 43], [377, 44], [378, 45], [379, 46], [380, 47], [381, 48], [382, 49], [383, 50], [384, 52], [385, 69], [597, 70], [598, 71], [599, 72], [600, 73], [601, 74], [602, 75], [603, 76], [604, 77], [605, 78], [606, 79], [607, 80], [608, 81], [609, 82], [610, 83], [611, 84], [612, 85], [258, 152], [259, 153], [260, 154], [261, 155], [262, 156], [263, 157], [264, 158], [265, 159], [266, 160], [267, 161], [268, 162], [269, 163], [270, 164], [271, 165], [272, 166], [273, 167], [86, 573], [87, 574], [88, 575], [89, 576], [90, 577], [91, 578], [92, 579], [93, 580], [94, 613], [95, 614], [96, 615], [97, 616], [98, 617], [99, 618], [100, 619], [101, 620], [168, 443], [169, 444], [170, 445], [171, 446], [172, 496], [173, 497], [174, 498], [175, 499], [176, 500], [177, 501], [178, 502], [179, 552], [180, 553], [181, 554], [182, 555], [183, 556], [653, 709], [654, 710], [655, 711], [656, 712], [657, 713], [658, 714], [659, 715], [660, 716], [693, 717], [694, 718], [695, 719], [696, 720], [697, 721], [698, 722], [699, 723], [700, 724], [557, 669], [558, 670], [559, 671], [560, 672], [561, 673], [562, 674], [563, 675], [564, 676], [565, 677], [566, 678], [567, 679], [568, 680], [569, 681], [570, 682], [571, 683], [572, 684]]
            }},
            {id: 'opp16', caption: 'Opp16', circuit: {
                components: {
                in0: {type: 'in', pos: [140, 725], orient: 'n', id: 223, name: 'In0', val: 1, isConstant: true},
                cnot0: {type: 'cnot-array', pos: [155, 380], in: '73-89', out: ['90-96', '113-116', '133-137'], bits: 16},
                adder1: {type: 'adder-array', pos: [285, 470], in: '152-184', out: '185-201', bits: 16},
                out1: {type: 'out', pos: [365, 470], id: ['4-13', '64-69'], bits: 16, name: '-B'},
                in2: {type: 'in', pos: [50, 380], id: '207-222', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[185, 4], [186, 5], [187, 6], [188, 7], [189, 8], [190, 9], [191, 10], [192, 11], [193, 12], [194, 13], [195, 64], [196, 65], [197, 66], [198, 67], [199, 68], [200, 69], [207, 73], [208, 74], [209, 75], [210, 76], [211, 77], [212, 78], [213, 79], [214, 80], [215, 81], [216, 82], [217, 83], [218, 84], [219, 85], [220, 86], [221, 87], [222, 88], [223, 89], [90, 152], [91, 153], [92, 154], [93, 155], [94, 156], [95, 157], [96, 158], [113, 159], [114, 160], [115, 161], [116, 162], [133, 163], [134, 164], [135, 165], [136, 166], [137, 167], [223, 168]]
            }},
            {id: 'sous16', caption: 'Sous16', circuit: {
                components: {
                adder0: {type: 'adder-array', pos: [525, 350], in: '14-46', out: '47-63', bits: 16},
                comp20: {type: 'custom-opp16', pos: [370, 515], in: '64-79', out: '80-95'},
                out0: {type: 'out', pos: [695, 350], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [165, 265], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                in0: {type: 'in', pos: [165, 515], id: '150-165', name: 'B', bits: 16, val: '0000000000000001'},
                },
                wires: [[47, 117], [48, 118], [49, 119], [50, 120], [51, 121], [52, 122], [53, 123], [54, 124], [55, 125], [56, 126], [57, 127], [58, 128], [59, 129], [60, 130], [61, 131], [62, 132], [97, 14], [98, 15], [99, 16], [100, 17], [101, 18], [102, 19], [103, 20], [104, 21], [105, 22], [106, 23], [107, 24], [108, 25], [109, 26], [110, 27], [111, 28], [112, 29], [80, 30], [81, 31], [82, 32], [83, 33], [84, 34], [85, 35], [86, 36], [87, 37], [88, 38], [89, 39], [90, 40], [91, 41], [92, 42], [93, 43], [94, 44], [95, 45], [150, 64], [151, 65], [152, 66], [153, 67], [154, 68], [155, 69], [156, 70], [157, 71], [158, 72], [159, 73], [160, 74], [161, 75], [162, 76], [163, 77], [164, 78], [165, 79]]
            }},
            {id: 'al', caption: 'al', circuit: {
                components: {
                in2: {type: 'in', pos: [85, 840], id: 638, name: 'In0', val: 1, isConstant: true},
                pass0: {type: 'pass', pos: [90, 300], in: '510-525', out: '526-541', bits: 16, slant: 'up'},
                mux1: {type: 'mux', pos: [135, 830], in: '353-385', out: '386-401', from: 32, to: 16},
                mux0: {type: 'mux', pos: [885, 335], in: '297-329', out: '330-345', from: 32, to: 16},
                pass1: {type: 'pass', pos: [110, 150], in: '550-565', out: '566-581', bits: 16, slant: 'up'},
                pass2: {type: 'pass', pos: [110, 510], in: '590-605', out: '606-621', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [275, 830], in: '647-662', out: '663-678', bits: 16, slant: 'up'},
                adder0: {type: 'adder-array', pos: [500, 245], in: '192-224', out: '225-241', bits: 16},
                sous160: {type: 'custom-sous16', pos: [570, 670], in: '242-273', out: '274-289'},
                pass4: {type: 'pass', pos: [295, 335], in: '687-702', out: '703-718', bits: 16, slant: 'up'},
                pass6: {type: 'pass', pos: [660, 670], in: '735-750', out: '751-766', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [680, 420], in: '775-790', out: '791-806', bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [990, 335], id: '622-637', bits: 16, name: 'S'},
                in0: {type: 'in', pos: [40, 300], id: '470-485', name: 'A', bits: 16, val: '0000000000000110'},
                in1: {type: 'in', pos: [40, 745], id: '486-501', name: 'B', bits: 16, val: '0000000000000010'},
                in3: {type: 'in', pos: [885, 50], orient: 's', id: 807, name: 'Op1', val: 1},
                in4: {type: 'in', pos: [135, 590], orient: 's', id: 808, name: 'Op0', val: 1},
                },
                wires: [[330, 622], [331, 623], [332, 624], [333, 625], [334, 626], [335, 627], [336, 628], [337, 629], [338, 630], [339, 631], [340, 632], [341, 633], [342, 634], [343, 635], [344, 636], [345, 637], [470, 510], [471, 511], [472, 512], [473, 513], [474, 514], [475, 515], [476, 516], [477, 517], [478, 518], [479, 519], [480, 520], [481, 521], [482, 522], [483, 523], [484, 524], [485, 525], [486, 353], [487, 354], [488, 355], [489, 356], [490, 357], [491, 358], [492, 359], [493, 360], [494, 361], [495, 362], [496, 363], [497, 364], [498, 365], [499, 366], [500, 367], [501, 368], [638, 369], [808, 385], [225, 297], [226, 298], [227, 299], [228, 300], [229, 301], [230, 302], [231, 303], [232, 304], [233, 305], [234, 306], [235, 307], [236, 308], [237, 309], [238, 310], [239, 311], [240, 312], [791, 313], [792, 314], [793, 315], [794, 316], [795, 317], [796, 318], [797, 319], [798, 320], [799, 321], [800, 322], [801, 323], [802, 324], [803, 325], [804, 326], [805, 327], [806, 328], [807, 329], [526, 550], [527, 551], [528, 552], [529, 553], [530, 554], [531, 555], [532, 556], [533, 557], [534, 558], [535, 559], [536, 560], [537, 561], [538, 562], [539, 563], [540, 564], [541, 565], [526, 590], [527, 591], [528, 592], [529, 593], [530, 594], [531, 595], [532, 596], [533, 597], [534, 598], [535, 599], [536, 600], [537, 601], [538, 602], [539, 603], [540, 604], [541, 605], [386, 647], [387, 648], [388, 649], [389, 650], [390, 651], [391, 652], [392, 653], [393, 654], [394, 655], [395, 656], [396, 657], [397, 658], [398, 659], [399, 660], [400, 661], [401, 662], [566, 192], [567, 193], [568, 194], [569, 195], [570, 196], [571, 197], [572, 198], [573, 199], [574, 200], [575, 201], [576, 202], [577, 203], [578, 204], [579, 205], [580, 206], [581, 207], [703, 208], [704, 209], [705, 210], [706, 211], [707, 212], [708, 213], [709, 214], [710, 215], [711, 216], [712, 217], [713, 218], [714, 219], [715, 220], [716, 221], [717, 222], [718, 223], [606, 242], [607, 243], [608, 244], [609, 245], [610, 246], [611, 247], [612, 248], [613, 249], [614, 250], [615, 251], [616, 252], [617, 253], [618, 254], [619, 255], [620, 256], [621, 257], [663, 258], [664, 259], [665, 260], [666, 261], [667, 262], [668, 263], [669, 264], [670, 265], [671, 266], [672, 267], [673, 268], [674, 269], [675, 270], [676, 271], [677, 272], [678, 273], [663, 687], [664, 688], [665, 689], [666, 690], [667, 691], [668, 692], [669, 693], [670, 694], [671, 695], [672, 696], [673, 697], [674, 698], [675, 699], [676, 700], [677, 701], [678, 702], [274, 735], [275, 736], [276, 737], [277, 738], [278, 739], [279, 740], [280, 741], [281, 742], [282, 743], [283, 744], [284, 745], [285, 746], [286, 747], [287, 748], [288, 749], [289, 750], [751, 775], [752, 776], [753, 777], [754, 778], [755, 779], [756, 780], [757, 781], [758, 782], [759, 783], [760, 784], [761, 785], [762, 786], [763, 787], [764, 788], [765, 789], [766, 790]]
            }},
            {id: 'alu', caption: 'ALU', circuit: {
                components: {
                mux0: {type: 'mux', pos: [715, 280], in: '258-290', out: '291-306', from: 32, to: 16},
                pass4: {type: 'pass', pos: [460, 200], in: '327-342', out: '343-358', bits: 16, slant: 'down'},
                mux2: {type: 'mux', pos: [660, 1265], in: ['122-135', '171-178', '211-218', '251-253'], out: ['254-257', '307-318'], from: 32, to: 16},
                pass6: {type: 'pass', pos: [270, 1185], in: '407-422', out: '423-438', bits: 16, slant: 'up'},
                mux1: {type: 'mux', pos: [1475, 615], in: ['58-61', '78-81', '108-114', '136-153'], out: '154-169', from: 32, to: 16},
                mux3: {type: 'mux', pos: [860, 365], in: [319, '513-527', '560-576'], out: '577-592', from: 32, to: 16},
                ul0: {type: 'custom-ul', pos: [1115, 525], in: ['0-7', '765-772', '805-822'], out: '823-838'},
                al0: {type: 'custom-al', pos: [1195, 1105], in: '8-41', out: '42-57'},
                pass5: {type: 'pass', pos: [480, 1350], in: '367-382', out: '383-398', bits: 16, slant: 'down'},
                pass9: {type: 'pass', pos: [720, 1265], in: '528-543', out: '544-559', bits: 16, slant: 'up'},
                pass7: {type: 'pass', pos: [290, 365], in: '447-462', out: '463-478', bits: 16, slant: 'up'},
                pass10: {type: 'pass', pos: [905, 365], in: '481-496', out: '497-512', bits: 16, slant: 'down'},
                pass3: {type: 'pass', pos: [1265, 1105], in: '219-234', out: '235-250', bits: 16, slant: 'up'},
                pass11: {type: 'pass', pos: [740, 685], in: '608-623', out: '624-639', bits: 16, slant: 'up'},
                pass8: {type: 'pass', pos: [925, 945], in: ['320-326', '359-366', 399], out: ['400-406', '439-446', 480], bits: 16, slant: 'down'},
                pass2: {type: 'pass', pos: [1285, 700], in: '179-194', out: '195-210', bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [1555, 615], id: ['598-607', '640-645'], bits: 16, name: 'S'},
                in0: {type: 'in', pos: [205, 200], id: '62-77', name: 'A', bits: 16, val: '0000000000000100'},
                in1: {type: 'in', pos: [220, 1185], id: '82-97', name: 'B', bits: 16, val: '0000000000000001'},
                in3: {type: 'in', pos: [1470, 80], orient: 's', id: 170, name: 'u'},
                in6: {type: 'in', pos: [1200, 95], orient: 's', id: 99, name: 'Op1'},
                in2: {type: 'in', pos: [1165, 95], orient: 's', id: 98, name: 'Op0'},
                in5: {type: 'in', pos: [860, 75], orient: 's', id: 593, name: 'zx'},
                in4: {type: 'in', pos: [660, 70], orient: 's', id: 479, name: 'sw'},
                },
                wires: [[154, 598], [155, 599], [156, 600], [157, 601], [158, 602], [159, 603], [160, 604], [161, 605], [162, 606], [163, 607], [164, 640], [165, 641], [166, 642], [167, 643], [168, 644], [169, 645], [62, 258], [63, 259], [64, 260], [65, 261], [66, 262], [67, 263], [68, 264], [69, 265], [70, 266], [71, 267], [72, 268], [73, 269], [74, 270], [75, 271], [76, 272], [77, 273], [463, 274], [464, 275], [465, 276], [466, 277], [467, 278], [468, 279], [469, 280], [470, 281], [471, 282], [472, 283], [473, 284], [474, 285], [475, 286], [476, 287], [477, 288], [478, 289], [479, 290], [62, 327], [63, 328], [64, 329], [65, 330], [66, 331], [67, 332], [68, 333], [69, 334], [70, 335], [71, 336], [72, 337], [73, 338], [74, 339], [75, 340], [76, 341], [77, 342], [82, 122], [83, 123], [84, 124], [85, 125], [86, 126], [87, 127], [88, 128], [89, 129], [90, 130], [91, 131], [92, 132], [93, 133], [94, 134], [95, 135], [96, 171], [97, 172], [383, 173], [384, 174], [385, 175], [386, 176], [387, 177], [388, 178], [389, 211], [390, 212], [391, 213], [392, 214], [393, 215], [394, 216], [395, 217], [396, 218], [397, 251], [398, 252], [479, 253], [82, 407], [83, 408], [84, 409], [85, 410], [86, 411], [87, 412], [88, 413], [89, 414], [90, 415], [91, 416], [92, 417], [93, 418], [94, 419], [95, 420], [96, 421], [97, 422], [823, 58], [824, 59], [825, 60], [826, 61], [827, 78], [828, 79], [829, 80], [830, 81], [831, 108], [832, 109], [833, 110], [834, 111], [835, 112], [836, 113], [837, 114], [838, 136], [195, 137], [196, 138], [197, 139], [198, 140], [199, 141], [200, 142], [201, 143], [202, 144], [203, 145], [204, 146], [205, 147], [206, 148], [207, 149], [208, 150], [209, 151], [210, 152], [170, 153], [291, 319], [292, 513], [293, 514], [294, 515], [295, 516], [296, 517], [297, 518], [298, 519], [299, 520], [300, 521], [301, 522], [302, 523], [303, 524], [304, 525], [305, 526], [306, 527], [593, 576], [577, 0], [578, 1], [579, 2], [580, 3], [581, 4], [582, 5], [583, 6], [584, 7], [585, 765], [586, 766], [587, 767], [588, 768], [589, 769], [590, 770], [591, 771], [592, 772], [624, 805], [625, 806], [626, 807], [627, 808], [628, 809], [629, 810], [630, 811], [631, 812], [632, 813], [633, 814], [634, 815], [635, 816], [636, 817], [637, 818], [638, 819], [639, 820], [99, 821, {via: [[1200, 190, 'w']]}], [98, 822], [400, 8], [401, 9], [402, 10], [403, 11], [404, 12], [405, 13], [406, 14], [439, 15], [440, 16], [441, 17], [442, 18], [443, 19], [444, 20], [445, 21], [446, 22], [480, 23], [254, 24], [255, 25], [256, 26], [257, 27], [307, 28], [308, 29], [309, 30], [310, 31], [311, 32], [312, 33], [313, 34], [314, 35], [315, 36], [316, 37], [317, 38], [318, 39], [99, 40], [98, 41], [343, 367], [344, 368], [345, 369], [346, 370], [347, 371], [348, 372], [349, 373], [350, 374], [351, 375], [352, 376], [353, 377], [354, 378], [355, 379], [356, 380], [357, 381], [358, 382], [254, 528], [255, 529], [256, 530], [257, 531], [307, 532], [308, 533], [309, 534], [310, 535], [311, 536], [312, 537], [313, 538], [314, 539], [315, 540], [316, 541], [317, 542], [318, 543], [423, 447], [424, 448], [425, 449], [426, 450], [427, 451], [428, 452], [429, 453], [430, 454], [431, 455], [432, 456], [433, 457], [434, 458], [435, 459], [436, 460], [437, 461], [438, 462], [577, 481], [578, 482], [579, 483], [580, 484], [581, 485], [583, 487], [584, 488], [586, 490], [588, 492], [589, 493], [42, 219], [43, 220], [44, 221], [45, 222], [46, 223], [47, 224], [48, 225], [49, 226], [50, 227], [51, 228], [52, 229], [53, 230], [54, 231], [55, 232], [56, 233], [57, 234], [544, 608], [545, 609], [546, 610], [547, 611], [548, 612], [549, 613], [550, 614], [551, 615], [552, 616], [553, 617], [554, 618], [555, 619], [556, 620], [557, 621], [558, 622], [559, 623], [497, 320], [498, 321], [499, 322], [500, 323], [501, 324], [502, 325], [503, 326], [504, 359], [505, 360], [506, 361], [507, 362], [508, 363], [509, 364], [510, 365], [511, 366], [512, 399], [235, 179], [236, 180], [237, 181], [238, 182], [239, 183], [240, 184], [241, 185], [242, 186], [243, 187], [244, 188], [245, 189], [246, 190], [247, 191], [248, 192], [249, 193], [250, 194]]
            }},
            {id: 'iszero', caption: 'isZero', circuit: {
                components: {
                or0: {type: 'or', pos: [305, 95], in: [39, 40], out: 41},
                or1: {type: 'or', pos: [305, 140], in: [42, 43], out: 44},
                or2: {type: 'or', pos: [305, 185], in: [45, 46], out: 47},
                or3: {type: 'or', pos: [305, 230], in: [48, 49], out: 50},
                or4: {type: 'or', pos: [305, 270], in: [51, 52], out: 53},
                or5: {type: 'or', pos: [305, 310], in: [54, 55], out: 56},
                or7: {type: 'or', pos: [305, 350], in: [60, 61], out: 62},
                or6: {type: 'or', pos: [305, 390], in: [57, 58], out: 59},
                or8: {type: 'or', pos: [410, 120], in: [63, 64], out: 65},
                or9: {type: 'or', pos: [410, 205], in: [66, 67], out: 68},
                or10: {type: 'or', pos: [410, 290], in: [69, 70], out: 71},
                or11: {type: 'or', pos: [405, 375], in: [72, 73], out: 74},
                or12: {type: 'or', pos: [510, 165], in: [75, 76], out: 77},
                or13: {type: 'or', pos: [510, 330], in: [78, 79], out: 80},
                or14: {type: 'or', pos: [610, 235], in: [81, 82], out: 83},
                not0: {type: 'not', pos: [690, 235], in: 84, out: 85},
                out1: {type: 'out', pos: [760, 235], id: 88, name: 'nul?'},
                in0: {type: 'in', pos: [185, 215], id: '4-19', name: 'A', bits: 16, val: '0000000000000010'},
                },
                wires: [[85, 88], [4, 39], [5, 40], [6, 42], [7, 43], [8, 45], [9, 46], [10, 48], [11, 49], [12, 51], [13, 52], [14, 54], [15, 55], [16, 60], [17, 61], [18, 57], [19, 58], [41, 63], [44, 64], [47, 66], [50, 67], [53, 69], [56, 70], [62, 72], [59, 73], [65, 75], [68, 76], [71, 78], [74, 79], [77, 81], [80, 82], [83, 84]]
            }},
            {id: 'isneg', caption: 'isNeg', circuit: {
                components: {
                out1: {type: 'out', pos: [570, 660], id: 118, name: 'Out0'},
                in0: {type: 'in', pos: [405, 635], id: '119-134', name: 'In0', bits: 16, val: '0000000001001010'},
                },
                wires: [[134, 118]]
            }},
            {id: 'condition', caption: 'Condition', circuit: {
                components: {
                and0: {type: 'and', pos: [550, 350], in: [252, 253], out: 254},
                and1: {type: 'and', pos: [550, 405], in: [255, 256], out: 257},
                and2: {type: 'and', pos: [715, 750], in: [266, 267], out: 268},
                isneg0: {type: 'custom-isneg', pos: [395, 180], in: '135-150', out: 151},
                pass0: {type: 'pass', pos: [165, 180], in: '180-195', out: '196-211', bits: 16, slant: 'down'},
                or0: {type: 'or', pos: [655, 375], in: [258, 259], out: 260},
                or1: {type: 'or', pos: [835, 510], in: [269, 270], out: 271},
                nor0: {type: 'or', pos: [545, 565], in: [261, 262], out: 263},
                pass1: {type: 'pass', pos: [185, 575], in: '220-235', out: '236-251', bits: 16, slant: 'down'},
                not0: {type: 'not', pos: [625, 565], in: 264, out: 265},
                iszero0: {type: 'custom-iszero', pos: [400, 575], in: '89-104', out: 105},
                out0: {type: 'out', pos: [905, 510], id: 23, name: 'Verif'},
                in0: {type: 'in', pos: [120, 180], id: '156-171', name: 'A', bits: 16},
                in1: {type: 'in', pos: [50, 360], id: 20, name: 'lt'},
                in2: {type: 'in', pos: [50, 395], id: 21, name: 'eq', val: 1},
                in3: {type: 'in', pos: [70, 760], id: 22, name: 'gt'},
                },
                wires: [[271, 23], [151, 252], [20, 253], [21, 255], [105, 256], [265, 266], [22, 267], [156, 135], [157, 136], [158, 137], [159, 138], [160, 139], [161, 140], [162, 141], [163, 142], [164, 143], [165, 144], [166, 145], [167, 146], [168, 147], [169, 148], [170, 149], [171, 150], [156, 180], [157, 181], [158, 182], [159, 183], [160, 184], [161, 185], [162, 186], [163, 187], [164, 188], [165, 189], [166, 190], [167, 191], [168, 192], [169, 193], [170, 194], [171, 195], [254, 258], [257, 259], [260, 269], [268, 270], [151, 261], [105, 262], [196, 220], [197, 221], [198, 222], [199, 223], [200, 224], [201, 225], [202, 226], [203, 227], [204, 228], [205, 229], [206, 230], [207, 231], [208, 232], [209, 233], [210, 234], [211, 235], [263, 264], [236, 89], [237, 90], [238, 91], [239, 92], [240, 93], [241, 94], [242, 95], [243, 96], [244, 97], [245, 98], [246, 99], [247, 100], [248, 101], [249, 102], [250, 103], [251, 104]]
            }},
            {id: 'aluinstruction', caption: 'ALU instruction', circuit: {
                components: {
                condition0: {type: 'custom-condition', pos: [300, 660], orient: 's', in: '0-18', out: 19},
                alu0: {type: 'custom-alu', pos: [830, 330], orient: 's', in: ['100-107', '115-121', '594-597', '646-663'], out: '664-679'},
                mux0: {type: 'mux', pos: [670, 125], orient: 's', in: '255-287', out: '288-303', from: 32, to: 16, bottom: true},
                pass0: {type: 'pass', pos: [830, 410], orient: 's', in: ['79-82', 99, '108-110', 134, '136-142'], out: '143-158', bits: 16, slant: 'down'},
                pass1: {type: 'pass', pos: [330, 430], orient: 's', in: '167-182', out: '183-198', bits: 16, slant: 'down'},
                out0: {type: 'out', pos: [1275, 155], id: 40, name: '✱a'},
                out1: {type: 'out', pos: [1340, 170], id: 41, name: 'd'},
                out2: {type: 'out', pos: [1275, 185], id: 42, name: 'a'},
                in0: {type: 'in', pos: [40, 205], id: '24-39', name: 'I', bits: 16, val: '0000000000000010'},
                in2: {type: 'in', pos: [990, 55], orient: 's', id: '83-98', name: 'D', bits: 16, val: '0000000000000100'},
                in1: {type: 'in', pos: [755, 55], orient: 's', id: '63-78', name: 'A', bits: 16, val: '0000000001000010'},
                in3: {type: 'in', pos: [585, 55], orient: 's', id: ['111-114', '122-133'], name: '✱A', bits: 16, val: '0000000000000010'},
                out3: {type: 'out', pos: [830, 705], orient: 's', id: '43-58', bits: 16, name: 'R'},
                out4: {type: 'out', pos: [300, 755], orient: 's', id: 135, name: 'j'},
                },
                wires: [[27, 40], [28, 41], [29, 42], [664, 43], [665, 44], [666, 45], [667, 46], [668, 47], [669, 48], [670, 49], [671, 50], [672, 51], [673, 52], [674, 53], [675, 54], [676, 55], [677, 56], [678, 57], [679, 58], [19, 135], [183, 0], [184, 1], [185, 2], [186, 3], [187, 4], [188, 5], [189, 6], [190, 7], [191, 8], [192, 9], [193, 10], [194, 11], [195, 12], [196, 13], [197, 14], [198, 15], [26, 16, {via: [[160, 150, 's']]}], [25, 17, {via: [[140, 140, 's']]}], [24, 18, {via: [[120, 130, 's']]}], [83, 100], [84, 101], [85, 102], [86, 103], [87, 104], [88, 105], [89, 106], [90, 107], [91, 115], [92, 116], [93, 117], [94, 118], [95, 119], [96, 120], [97, 121], [98, 594], [288, 595], [289, 596], [290, 597], [291, 646], [292, 647], [293, 648], [294, 649], [295, 650], [296, 651], [297, 652], [298, 653], [299, 654], [300, 655], [301, 656], [302, 657], [303, 658], [34, 659, {via: [[1205, 240], [1205, 370, 's']]}], [33, 660, {via: [[1215, 230], [1215, 350, 's']]}], [32, 661, {via: [[1225, 220], [1225, 330, 's']]}], [31, 662, {via: [[1235, 210], [1235, 310, 's']]}], [30, 663, {via: [[1245, 200], [1245, 290, 's']]}], [63, 255], [64, 256], [65, 257], [66, 258], [67, 259], [68, 260], [69, 261], [70, 262], [71, 263], [72, 264], [73, 265], [74, 266], [75, 267], [76, 268], [77, 269], [78, 270], [111, 271], [112, 272], [113, 273], [114, 274], [122, 275], [123, 276], [124, 277], [125, 278], [126, 279], [127, 280], [128, 281], [129, 282], [130, 283], [131, 284], [132, 285], [133, 286], [36, 287, {via: [[490, 250]]}], [664, 79], [665, 80], [666, 81], [667, 82], [668, 99], [669, 108], [670, 109], [671, 110], [672, 134], [673, 136], [674, 137], [675, 138], [676, 139], [677, 140], [678, 141], [679, 142], [143, 167], [144, 168], [145, 169], [146, 170], [147, 171], [148, 172], [149, 173], [150, 174], [151, 175], [152, 176], [153, 177], [154, 178], [155, 179], [156, 180], [157, 181], [158, 182]]
            }},
            {id: 'controlunit', caption: 'Control Unit', circuit: {
                components: {
                in4: {type: 'in', pos: [1385, 200], orient: 's', id: 104, name: 'In0', isConstant: true},
                in5: {type: 'in', pos: [1330, 200], orient: 's', id: 105, name: 'In1', val: 1, isConstant: true},
                in6: {type: 'in', pos: [650, 705], orient: 's', id: 0, name: 'In2', isConstant: true},
                mux4: {type: 'mux', pos: [1425, 350], in: '27-29', out: 30, from: 2, to: 1},
                mux3: {type: 'mux', pos: [1465, 205], in: [19, 24, 25], out: 26, from: 2, to: 1},
                mux1: {type: 'mux', pos: [1360, 480], in: '31-33', out: 34, from: 2, to: 1},
                mux2: {type: 'mux', pos: [640, 755], orient: 's', in: '110-112', out: 113, from: 2, to: 1, bottom: true},
                aluinstruction0: {type: 'custom-aluinstruction', pos: [790, 305], in: ['20-23', '59-62', '159-166', '199-246'], out: ['247-254', '304-315']},
                pass0: {type: 'pass', pos: [100, 305], in: '533-548', out: '549-564', bits: 16, slant: 'up'},
                mux0: {type: 'mux', pos: [885, 690], orient: 's', in: '476-508', out: '509-524', from: 32, to: 16, bottom: true},
                pass3: {type: 'pass', pos: [1455, 115], in: 17, out: 18},
                pass1: {type: 'pass', pos: [120, 570], in: '573-588', out: '589-604', bits: 16, slant: 'up'},
                pass2: {type: 'pass', pos: [970, 505], orient: 's', in: '613-628', out: '629-644', bits: 16, slant: 'up'},
                out4: {type: 'out', pos: [1530, 205], id: 419, name: '✱a'},
                out3: {type: 'out', pos: [1515, 350], id: 418, name: 'd'},
                out2: {type: 'out', pos: [1515, 480], id: 417, name: 'a'},
                in0: {type: 'in', pos: [60, 305], id: '320-335', name: 'I', bits: 16, val: '0000000000000010'},
                in1: {type: 'in', pos: [1110, 60], orient: 's', id: '340-355', name: 'D', bits: 16, val: '0000000000000010'},
                in3: {type: 'in', pos: [470, 60], orient: 's', id: '380-395', name: '✱A', bits: 16, val: '0000000000000100'},
                in2: {type: 'in', pos: [220, 60], orient: 's', id: '360-375', name: 'A', bits: 16, val: '0000000000000100'},
                out0: {type: 'out', pos: [885, 800], orient: 's', id: '1-16', bits: 16, name: 'R'},
                out1: {type: 'out', pos: [640, 805], orient: 's', id: 416, name: 'j'},
                },
                wires: [[34, 417], [30, 418], [26, 419], [509, 1], [510, 2], [511, 3], [512, 4], [513, 5], [514, 6], [515, 7], [516, 8], [517, 9], [518, 10], [519, 11], [520, 12], [521, 13], [522, 14], [523, 15], [524, 16], [113, 416], [104, 27, {via: [[1385, 340, 's']]}], [248, 28, {via: [[1310, 305], [1310, 360]]}], [18, 29, {via: [[1425, 115, 's']]}], [104, 19, {via: [[1385, 410, 's']]}], [249, 24, {via: [[1285, 430]]}], [18, 25], [105, 31], [247, 32], [18, 33, {via: [[1350, 115, 's']]}], [0, 110], [315, 111], [335, 112, {via: [[80, 760]]}], [320, 20], [321, 21], [322, 22], [323, 23], [324, 59], [325, 60], [326, 61], [327, 62], [328, 159], [329, 160], [330, 161], [331, 162], [332, 163], [333, 164], [334, 165], [335, 166], [340, 199], [341, 200], [342, 201], [343, 202], [344, 203], [345, 204], [346, 205], [347, 206], [348, 207], [349, 208], [350, 209], [351, 210], [352, 211], [353, 212], [354, 213], [355, 214], [360, 215], [361, 216], [362, 217], [363, 218], [364, 219], [365, 220], [366, 221], [367, 222], [368, 223], [369, 224], [370, 225], [371, 226], [372, 227], [373, 228], [374, 229], [375, 230], [380, 231], [381, 232], [382, 233], [383, 234], [384, 235], [385, 236], [386, 237], [387, 238], [388, 239], [389, 240], [390, 241], [391, 242], [392, 243], [393, 244], [394, 245], [395, 246], [320, 533], [321, 534], [322, 535], [323, 536], [324, 537], [325, 538], [326, 539], [327, 540], [328, 541], [329, 542], [330, 543], [331, 544], [332, 545], [333, 546], [334, 547], [335, 548], [629, 476], [630, 477], [631, 478], [632, 479], [633, 480], [634, 481], [635, 482], [636, 483], [637, 484], [638, 485], [639, 486], [640, 487], [641, 488], [642, 489], [643, 490], [644, 491], [250, 492], [251, 493], [252, 494], [253, 495], [254, 496], [304, 497], [305, 498], [306, 499], [307, 500], [308, 501], [309, 502], [310, 503], [311, 504], [312, 505], [313, 506], [314, 507], [335, 508, {via: [[80, 695]]}], [335, 17, {via: [[95, 380, 'n'], [95, 115, 'n']]}], [549, 573], [550, 574], [551, 575], [552, 576], [553, 577], [554, 578], [555, 579], [556, 580], [557, 581], [558, 582], [559, 583], [560, 584], [561, 585], [562, 586], [563, 587], [564, 588], [589, 613], [590, 614], [591, 615], [592, 616], [593, 617], [594, 618], [595, 619], [596, 620], [597, 621], [598, 622], [599, 623], [600, 624], [601, 625], [602, 626], [603, 627], [604, 628]]
            }},
            {id: 'bascule', caption: 'bascule', circuit: {
                components: {
                mux0: {type: 'mux', pos: [105, 75], in: '9-11', out: 12, from: 2, to: 1, bottom: true},
                pass1: {type: 'pass', pos: [125, 35], orient: 'n', in: 2, out: 3},
                pass2: {type: 'pass', pos: [80, 25], orient: 'w', in: 4, out: 5},
                pass3: {type: 'pass', pos: [70, 55], orient: 's', in: 6, out: 7},
                out0: {type: 'out', pos: [170, 75], id: 8, name: 'Out0'},
                in1: {type: 'in', pos: [40, 85], id: 19, name: 'd'},
                in0: {type: 'in', pos: [105, 135], orient: 'n', id: 18, name: 'st'},
                },
                wires: [[12, 8], [7, 9], [19, 10], [18, 11], [12, 2], [3, 4], [5, 6]]
            }},
            {id: 'bff', caption: 'b-ff', circuit: {
                components: {
                bascule1: {type: 'custom-bascule', pos: [315, 70], in: [0, 1], out: 2},
                not0: {type: 'not', pos: [145, 205], in: 5, out: 6},
                and1: {type: 'and', pos: [400, 240], in: [11, 12], out: 13},
                and0: {type: 'and', pos: [225, 195], in: [8, 9], out: 10},
                bascule0: {type: 'custom-bascule', pos: [470, 70], in: [15, 16], out: 17},
                out0: {type: 'out', pos: [600, 70], id: 14, name: 'Out0'},
                in0: {type: 'in', pos: [70, 70], id: 3, name: 'd', val: 1},
                in2: {type: 'in', pos: [70, 145], id: 7, name: 'st'},
                in1: {type: 'in', pos: [105, 280], orient: 'n', id: 4, name: 'clk'},
                },
                wires: [[17, 14], [3, 0], [10, 1, {via: [[315, 195, 'n']]}], [4, 5], [7, 11, {via: [[360, 140]]}], [4, 12], [7, 8, {via: [[185, 145]]}], [6, 9], [2, 15], [13, 16, {via: [[470, 240, 'n']]}]]
            }},
            {id: 'reg16', caption: 'Reg16', circuit: {
                components: {
                bff0: {type: 'custom-bff', pos: [670, 55], in: '18-20', out: 21},
                bff1: {type: 'custom-bff', pos: [670, 135], in: '0-2', out: 3},
                bff2: {type: 'custom-bff', pos: [670, 215], in: '4-6', out: 7},
                bff3: {type: 'custom-bff', pos: [670, 295], in: '8-10', out: 11},
                bff4: {type: 'custom-bff', pos: [670, 375], in: '12-14', out: 15},
                bff5: {type: 'custom-bff', pos: [670, 455], in: [16, 17, 22], out: 23},
                bff6: {type: 'custom-bff', pos: [670, 535], in: '24-26', out: 27},
                bff7: {type: 'custom-bff', pos: [670, 615], in: '28-30', out: 31},
                bff8: {type: 'custom-bff', pos: [670, 695], in: '32-34', out: 35},
                bff9: {type: 'custom-bff', pos: [670, 775], in: '36-38', out: 39},
                bff10: {type: 'custom-bff', pos: [670, 855], in: '40-42', out: 43},
                bff11: {type: 'custom-bff', pos: [670, 935], in: '44-46', out: 47},
                bff12: {type: 'custom-bff', pos: [670, 1015], in: '48-50', out: 51},
                bff13: {type: 'custom-bff', pos: [670, 1095], in: '52-54', out: 55},
                bff14: {type: 'custom-bff', pos: [670, 1175], in: '56-58', out: 59},
                bff15: {type: 'custom-bff', pos: [670, 1255], in: '60-62', out: 63},
                out0: {type: 'out', pos: [915, 565], id: [64, 65, '86-99'], bits: 16, name: 'Out0'},
                in0: {type: 'in', pos: [435, 565], id: '70-85', name: 'd', bits: 16, val: '0000000000000010'},
                in1: {type: 'in', pos: [395, 980], id: 66, name: 'st', val: 1},
                in2: {type: 'in', pos: [600, 1370], orient: 'n', id: 67, name: 'clk', val: 1},
                },
                wires: [[21, 64], [3, 65], [7, 86], [11, 87], [15, 88], [23, 89], [27, 90], [31, 91], [35, 92], [39, 93], [43, 94], [47, 95], [51, 96], [55, 97], [59, 98], [63, 99], [70, 18], [66, 19], [67, 20, {via: [[600, 95, 'n']]}], [71, 0], [66, 1], [67, 2, {via: [[600, 175, 'n']]}], [72, 4], [66, 5], [67, 6, {via: [[600, 255, 'n']]}], [73, 8], [66, 9], [67, 10, {via: [[600, 335, 'n']]}], [74, 12], [66, 13], [67, 14, {via: [[600, 415, 'n']]}], [75, 16], [66, 17], [67, 22, {via: [[600, 495, 'n']]}], [76, 24], [66, 25], [67, 26, {via: [[600, 575, 'n']]}], [77, 28], [66, 29], [67, 30, {via: [[600, 655, 'n']]}], [78, 32], [66, 33], [67, 34, {via: [[600, 735, 'n']]}], [79, 36], [66, 37], [67, 38, {via: [[600, 815, 'n']]}], [80, 40], [66, 41], [67, 42, {via: [[600, 895, 'n']]}], [81, 44], [66, 45], [67, 46, {via: [[600, 975, 'n']]}], [82, 48], [66, 49], [67, 50, {via: [[600, 1055, 'n']]}], [83, 52], [66, 53], [67, 54, {via: [[600, 1135, 'n']]}], [84, 56], [66, 57], [67, 58, {via: [[600, 1215, 'n']]}], [85, 60], [66, 61], [67, 62, {via: [[600, 1295]]}]]
            }},
            {id: 'inc16', caption: 'Inc16', circuit: {
                components: {
                in0: {type: 'in', pos: [300, 335], id: 223, name: 'In0', val: 1, isConstant: true},
                adder0: {type: 'adder-array', pos: [360, 320], in: '147-179', out: '180-196', bits: 16},
                out0: {type: 'out', pos: [470, 320], id: '117-132', bits: 16, name: 'S'},
                in3: {type: 'in', pos: [235, 230], id: '97-112', name: 'A', bits: 16, val: '0000000000000011'},
                },
                wires: [[180, 117], [181, 118], [182, 119], [183, 120], [184, 121], [185, 122], [186, 123], [187, 124], [188, 125], [189, 126], [190, 127], [191, 128], [192, 129], [193, 130], [194, 131], [195, 132], [97, 147], [98, 148], [99, 149], [100, 150], [101, 151], [102, 152], [103, 153], [104, 154], [105, 155], [106, 156], [107, 157], [108, 158], [109, 159], [110, 160], [111, 161], [112, 162], [223, 163]]
            }},
            {id: 'compteur', caption: 'compteur', circuit: {
                components: {
                in3: {type: 'in', pos: [285, 530], id: 47, name: 'In0', val: 1, isConstant: true},
                reg160: {type: 'custom-reg16', pos: [360, 370], in: '100-117', out: '118-133'},
                mux0: {type: 'mux', pos: [255, 360], in: ['32-45', '95-99', '134-136', '169-176', '209-211'], out: ['212-216', '249-256', '289-291'], from: 32, to: 16, bottom: true},
                inc160: {type: 'custom-inc16', pos: [510, 370], in: '0-15', out: '16-31'},
                pass0: {type: 'pass', pos: [580, 370], in: '137-152', out: '153-168', bits: 16, slant: 'up'},
                pass1: {type: 'pass', pos: [730, 100], orient: 'w', in: '177-192', out: '193-208', bits: 16, slant: 'down'},
                pass2: {type: 'pass', pos: [215, 100], orient: 'w', in: '217-232', out: '233-248', bits: 16, slant: 'up'},
                pass3: {type: 'pass', pos: [130, 190], orient: 's', in: '257-272', out: '273-288', bits: 16, slant: 'up'},
                out0: {type: 'out', pos: [810, 370], id: '52-67', bits: 16, name: 'Out'},
                in1: {type: 'in', pos: [60, 445], id: '294-309', name: 'X', bits: 16, val: '0000000000000001'},
                in0: {type: 'in', pos: [255, 610], orient: 'n', id: 292, name: 'st', val: 1},
                in2: {type: 'in', pos: [170, 605], orient: 'n', id: 46, name: 'clk'},
                },
                wires: [[16, 52], [17, 53], [18, 54], [19, 55], [20, 56], [21, 57], [22, 58], [23, 59], [24, 60], [25, 61], [26, 62], [27, 63], [28, 64], [29, 65], [30, 66], [31, 67], [212, 100], [213, 101], [214, 102], [215, 103], [216, 104], [249, 105], [250, 106], [251, 107], [252, 108], [253, 109], [254, 110], [255, 111], [256, 112], [289, 113], [290, 114], [291, 115], [47, 116], [46, 117], [273, 32], [274, 33], [275, 34], [276, 35], [277, 36], [278, 37], [279, 38], [280, 39], [281, 40], [282, 41], [283, 42], [284, 43], [285, 44], [286, 45], [287, 95], [288, 96], [294, 97], [295, 98], [296, 99], [297, 134], [298, 135], [299, 136], [300, 169], [301, 170], [302, 171], [303, 172], [304, 173], [305, 174], [306, 175], [307, 176], [308, 209], [309, 210], [292, 211, {via: [[255, 580]]}], [118, 0], [119, 1], [120, 2], [121, 3], [122, 4], [123, 5], [124, 6], [125, 7], [126, 8], [127, 9], [128, 10], [129, 11], [130, 12], [131, 13], [132, 14], [133, 15], [16, 137], [17, 138], [18, 139], [19, 140], [20, 141], [21, 142], [22, 143], [23, 144], [24, 145], [25, 146], [26, 147], [27, 148], [28, 149], [29, 150], [30, 151], [31, 152], [153, 177], [154, 178], [155, 179], [156, 180], [157, 181], [158, 182], [159, 183], [160, 184], [161, 185], [162, 186], [163, 187], [164, 188], [165, 189], [166, 190], [167, 191], [168, 192], [193, 217], [194, 218], [195, 219], [196, 220], [197, 221], [198, 222], [199, 223], [200, 224], [201, 225], [202, 226], [203, 227], [204, 228], [205, 229], [206, 230], [207, 231], [208, 232], [233, 257], [234, 258], [235, 259], [236, 260], [237, 261], [238, 262], [239, 263], [240, 264], [241, 265], [242, 266], [243, 267], [244, 268], [245, 269], [246, 270], [247, 271], [248, 272]]
            }},
            {id: 'memoiresadram', caption: 'mémoires (A,     D,     RAM)', circuit: {
                components: {
                ram0: {type: 'ram', pos: [645, 470], in: [20, '208-211', '260-263', '280-300'], out: '301-316', bits: 16, lines: 2048},
                pass0: {type: 'pass', pos: [130, 470], in: ['28-33', '102-111'], out: ['112-116', '156-166'], bits: 16, slant: 'up'},
                reg160: {type: 'custom-reg16', pos: [380, 190], in: '34-51', out: '52-67'},
                reg161: {type: 'custom-reg16', pos: [470, 800], in: '68-85', out: '86-101'},
                pass1: {type: 'pass', pos: [150, 180], in: '175-190', out: '191-206', bits: 16, slant: 'up'},
                pass2: {type: 'pass', pos: [150, 790], in: '215-230', out: '231-246', bits: 16, slant: 'up'},
                out2: {type: 'out', pos: [810, 180], id: '264-279', bits: 16, name: 'D'},
                out1: {type: 'out', pos: [795, 470], id: ['212-214', '247-259'], bits: 16, name: '✱A'},
                out0: {type: 'out', pos: [795, 835], id: ['21-27', '167-174', 207], bits: 16, name: 'A'},
                in0: {type: 'in', pos: [45, 470], id: '4-19', name: 'X', bits: 16, val: '0000000000000100'},
                in1: {type: 'in', pos: [60, 1010], id: 0, name: 'clk'},
                in4: {type: 'in', pos: [550, 45], orient: 's', id: 3, name: '✱a'},
                in3: {type: 'in', pos: [430, 50], orient: 's', id: 2, name: 'd'},
                in2: {type: 'in', pos: [315, 55], orient: 's', id: 1, name: 'a'},
                },
                wires: [[86, 264], [87, 265], [88, 266], [89, 267], [90, 268], [91, 269], [92, 270], [93, 271], [94, 272], [95, 273], [96, 274], [97, 275], [98, 276], [99, 277], [100, 278], [101, 279], [301, 212], [302, 213], [303, 214], [304, 247], [305, 248], [306, 249], [307, 250], [308, 251], [309, 252], [310, 253], [311, 254], [312, 255], [313, 256], [314, 257], [315, 258], [316, 259], [52, 21], [53, 22], [54, 23], [55, 24], [56, 25], [57, 26], [58, 27], [59, 167], [60, 168], [61, 169], [62, 170], [63, 171], [64, 172], [65, 173], [66, 174], [67, 207], [3, 208, {via: [[555, 595, 's'], [625, 595]]}], [0, 209, {via: [[665, 1010]]}], [4, 210], [5, 211], [6, 260], [7, 261], [8, 262], [9, 263], [10, 280], [11, 281], [12, 282], [13, 283], [14, 284], [15, 285], [16, 286], [17, 287], [18, 288], [19, 289], [52, 290, {via: [[695, 115]]}], [53, 291, {via: [[685, 125, 's']]}], [54, 292, {via: [[675, 135, 's']]}], [55, 293, {via: [[665, 145, 's']]}], [56, 294, {via: [[655, 155]]}], [57, 295, {via: [[645, 165]]}], [58, 296, {via: [[635, 175]]}], [59, 297, {via: [[625, 185]]}], [60, 298, {via: [[615, 195]]}], [61, 299, {via: [[605, 205]]}], [62, 300, {via: [[595, 215]]}], [4, 28], [5, 29], [6, 30], [7, 31], [8, 32], [9, 33], [10, 102], [11, 103], [12, 104], [13, 105], [14, 106], [15, 107], [16, 108], [17, 109], [18, 110], [19, 111], [191, 34], [192, 35], [193, 36], [194, 37], [195, 38], [196, 39], [197, 40], [198, 41], [199, 42], [200, 43], [201, 44], [202, 45], [203, 46], [204, 47], [205, 48], [206, 49], [1, 50, {via: [[315, 350, 's']]}], [0, 51, {via: [[380, 1010, 'n']]}], [231, 68], [232, 69], [233, 70], [234, 71], [235, 72], [236, 73], [237, 74], [238, 75], [239, 76], [240, 77], [241, 78], [242, 79], [243, 80], [244, 81], [245, 82], [246, 83], [2, 84, {via: [[410, 960, 's']]}], [0, 85, {via: [[470, 1010]]}], [112, 175], [113, 176], [114, 177], [115, 178], [116, 179], [156, 180], [157, 181], [158, 182], [159, 183], [160, 184], [161, 185], [162, 186], [163, 187], [164, 188], [165, 189], [166, 190], [112, 215], [113, 216], [114, 217], [115, 218], [116, 219], [156, 220], [157, 221], [158, 222], [159, 223], [160, 224], [161, 225], [162, 226], [163, 227], [164, 228], [165, 229], [166, 230]]
            }}
            ],
            components: {
            ram0: {type: 'rom', pos: [260, 895], in: '140-150', out: '151-166', bits: 16, lines: 2048},
            pass0: {type: 'pass', pos: [235, 495], orient: 's', in: '703-718', out: '719-734', bits: 16, slant: 'down'},
            pass1: {type: 'pass', pos: [590, 475], orient: 's', in: '743-758', out: '759-774', bits: 16, slant: 'down'},
            pass2: {type: 'pass', pos: [920, 30], orient: 's', in: '42-57', out: '58-73', bits: 16, slant: 'down'},
            pass3: {type: 'pass', pos: [1450, 1165], in: '82-97', out: '98-113', bits: 16, slant: 'up'},
            pass4: {type: 'pass', pos: [1535, 180], orient: 'n', in: [138, 139, '167-180'], out: '181-196', bits: 16, slant: 'up'},
            pass5: {type: 'pass', pos: [920, 1080], orient: 's', in: '221-236', out: '237-252', bits: 16, slant: 'up'},
            clock0: {type: 'clock', pos: [30, 160], id: 257, period: 2000},
            memoire0: {type: 'custom-memoiresadram', pos: [910, 330], orient: 's', in: '494-513', out: '514-561'},
            controlunit0: {type: 'custom-controlunit', pos: [910, 895], in: '562-625', out: '626-645'},
            compteur0: {type: 'custom-compteur', pos: [235, 715], orient: 's', in: '778-795', out: '796-811'},
            label0: {type: 'label', pos: [360, 1000], text: 'Bus'},
            label1: {type: 'label', pos: [365, 1025], text: 'instructions'},
            label2: {type: 'label', pos: [690, 535], text: 'Bus'},
            label3: {type: 'label', pos: [710, 555], text: 'Adresses'},
            label4: {type: 'label', pos: [1055, 575], text: 'Bus'},
            label5: {type: 'label', pos: [1055, 595], text: 'Données'},
            label6: {type: 'label', pos: [1050, 615], text: 'Out'},
            label7: {type: 'label', pos: [1200, 1080], text: 'Bus Données In'},
            },
            wires: [[796, 140], [797, 141], [798, 142], [799, 143], [800, 144], [801, 145], [802, 146], [803, 147], [804, 148], [805, 149], [806, 150], [151, 562], [152, 563], [153, 564], [154, 565], [155, 566], [156, 567], [157, 568], [158, 569], [159, 570], [160, 571], [161, 572], [162, 573], [163, 574], [164, 575], [165, 576], [166, 577], [514, 578], [515, 579], [516, 580], [517, 581], [518, 582], [519, 583], [520, 584], [521, 585], [522, 586], [523, 587], [524, 588], [525, 589], [526, 590], [527, 591], [528, 592], [529, 593], [530, 594], [531, 595], [532, 596], [533, 597], [534, 598], [535, 599], [536, 600], [537, 601], [538, 602], [539, 603], [540, 604], [541, 605], [542, 606], [543, 607], [544, 608], [545, 609], [546, 610], [547, 611], [548, 612], [549, 613], [550, 614], [551, 615], [552, 616], [553, 617], [554, 618], [555, 619], [556, 620], [557, 621], [558, 622], [559, 623], [560, 624], [561, 625], [546, 743], [547, 744], [548, 745], [549, 746], [550, 747], [551, 748], [552, 749], [553, 750], [554, 751], [555, 752], [556, 753], [557, 754], [558, 755], [559, 756], [560, 757], [561, 758], [759, 703], [760, 704], [761, 705], [762, 706], [763, 707], [764, 708], [765, 709], [766, 710], [767, 711], [768, 712], [769, 713], [770, 714], [771, 715], [772, 716], [773, 717], [774, 718], [719, 778], [720, 779], [721, 780], [722, 781], [723, 782], [724, 783], [725, 784], [726, 785], [727, 786], [728, 787], [729, 788], [730, 789], [731, 790], [732, 791], [733, 792], [734, 793], [629, 221], [630, 222], [631, 223], [632, 224], [633, 225], [634, 226], [635, 227], [636, 228], [637, 229], [638, 230], [639, 231], [640, 232], [641, 233], [642, 234], [643, 235], [644, 236], [237, 82], [238, 83], [239, 84], [240, 85], [241, 86], [242, 87], [243, 88], [244, 89], [245, 90], [246, 91], [247, 92], [248, 93], [249, 94], [250, 95], [251, 96], [252, 97], [98, 138], [99, 139], [100, 167], [101, 168], [102, 169], [103, 170], [104, 171], [105, 172], [106, 173], [107, 174], [108, 175], [109, 176], [110, 177], [111, 178], [112, 179], [113, 180], [181, 42], [182, 43], [183, 44], [184, 45], [185, 46], [186, 47], [187, 48], [188, 49], [189, 50], [190, 51], [191, 52], [192, 53], [193, 54], [194, 55], [195, 56], [196, 57], [58, 494], [59, 495], [60, 496], [61, 497], [62, 498], [63, 499], [64, 500], [65, 501], [66, 502], [67, 503], [68, 504], [69, 505], [70, 506], [71, 507], [72, 508], [73, 509], [628, 513, {via: [[1440, 915, 'n'], [1440, 310, 'n']]}], [645, 794, {via: [[750, 1095, 'w'], [60, 1095, 'w']]}], [257, 510, {via: [[750, 165]]}], [257, 795], [627, 512, {via: [[1425, 895, 'n'], [1425, 330, 'n']]}], [626, 511]]
        }
        </script>
    </logic-editor>
    </div>

    !!! warning "Attention"

        Le simulateur ne supporte pas plus de 4096 lignes dans la RAM (adressage sur 12 bits).

#### e. Entrée et sortie

Exemple:

- Entrée : bouton poussoir modifiant le bit 15 d'un mot de 16 bits (uniquement si `st` et `clk` valent `1`).
- Sortie : lampe contrôlée par les bits 0 (éteindre) et 1 (allumer) d'un mot de 16 bits.

=== "Solution"

    <div style="width: 100%; height: 362px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            components: {
            in0: {type: 'in', pos: [530, 315], id: 4, name: 'bp', isPushButton: true},
            out0: {type: 'out', pos: [640, 240], id: '17-32', bits: 16, name: 'out'},
            in1: {type: 'in', pos: [185, 250], id: '37-52', name: 'in', bits: 16, val: '0000000000000001'},
            disp0: {type: 'display', pos: [515, 105], id: '53-55', bits: 3, name: 'lampe'},
            in2: {type: 'in', pos: [85, 45], id: 56, name: 'st'},
            in3: {type: 'in', pos: [85, 90], id: 57, name: 'clk'},
            and0: {type: 'and', pos: [235, 55], in: [58, 59], out: 60},
            mux0: {type: 'mux', pos: [440, 85], in: '75-77', out: 78, from: 2, to: 1},
            mux1: {type: 'mux', pos: [440, 170], in: '67-69', out: 70, from: 2, to: 1},
            in4: {type: 'in', pos: [325, 75], id: 71, isConstant: true},
            label0: {type: 'label', pos: [540, 155], text: '1:  off,  2:  on'},
            },
            wires: [[4, 32], [56, 58], [57, 59, {via: [[115, 65]]}], [78, 53], [60, 77], [37, 76, {via: [[205, 95]]}], [38, 68], [60, 69, {via: [[390, 55], [390, 140]]}], [70, 54, {via: [[460, 105, 'n']]}], [71, 75], [71, 67, {via: [[355, 160, 's']]}]]
        }
        </script>
    </logic-editor>
    </div>
