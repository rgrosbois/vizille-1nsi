!!! danger "Rappel"

    **Un notebook se lit en exécutant toutes les cellules de code au fur et à mesure.**  Pour cela il y a le bouton ![Exécuter](img/01-executer.png) dans la barre d'outils ou le raccourci clavier ++shift+enter++
    

# Logique combinatoire

Une valeur logique ne peut prendre que 2 valeurs: `True`/`False`, `0`/`1`

## 1. Table de vérité

C'est un tableau qui indique les valeurs de sortie d'une fonction logique pour toutes les combinaisons possibles des entrées.

![Fonction logique](img/02-Fonction_logique.png)

Comme chaque entrée peut prendre 2 valeurs, la table d'une fonction à *n* entrées contient *2n* lignes de valeurs. 

!!! example "Exemple"

    | e1 | e2 | e3 | s |
    |----|----|----|---|
    |  0 |  0 |  0 | 1 |
    |  0 |  0 |  1 | 0 |
    |  0 |  1 |  0 | 1 |
    |  0 |  1 |  1 | 0 |
    |  1 |  0 |  0 | 0 |
    |  1 |  0 |  1 | 1 |
    |  1 |  1 |  0 | 0 |
    |  1 |  1 |  1 | 1 |

    *(peu importe l'ordre des lignes, ici il se fait selon l'ordre croissant du code binaire naturel: 000, 001, 010, 011...)* 

## 2. Python

### 2.1. Valeurs logiques

Elles apparaissent sous 2 formes dans le langage:

- le type *boolean* (valeurs `True` ou `False`) pour le résultat d'un test/comparaison:
  ```python
  a = (5 > 2) 
  b = (3 == 2) 
  ```
  > `a` et `b` contiennent respectivement `True` et `False`.
  
- Le *bit* dans la représentation binaire d'une donnée (entier, flottant, texte...)
  ```python
  a = 0b01001011
  b = 75
  ```
  > `a` et `b` contiennent ici la même valeur. 

!!! danger "Note"
    
    L'ordinateur ne travaille qu'en binaire. L'utilisation de la base décimale (ici pour la valeur `75`) est permise dans le code-source pour simplifier la tâche du développeur (habituée à travailler en base 10), mais cette valeur est automatiquement convertie en binaire (`0...01001011`) une fois que s'exécute le programme.
    
### 2.2. Opérateurs logiques

Ils ne sont pas les mêmes, selon qu'on travaille sur des *bits* ou des *booléens* :

| Fonction logique  | Binaire   | Booléen          |  
|-------------------|-----------|------------------|
| ET                | `0 & 1`   | `False and True` | 
| OU                | `0 | 1`   | `False or True`  |
| Complément        | `~0`      | `not False`      |
| Ou exclusif       | `0^1`     | `False ^ True`   |
| ...               | ...       | ...              |

!!! danger "Opérations bit à bit"    
    Les opérateurs binaires sont qualifiés de *bit à bit* (il s'appliquent entre les bits de même poids): 

    - l'opération `3 & 2` réalise un *ET* logique entre les 2 valeurs binaires `0...0 1 1` et `0...0 1 0` et donne `0&0...0&0 1&1 1&0`=`0...0 1 0` (soit `2`)

    - l'opération `~0` complémente **tous les bits** de la représentation binaire de `0` et donne `1...111` (soit `-1` en représentation complément à 2).
    
    !!! tip "Astuce"

        Pour obtenir le bit complémentaire, on peut utiliser l'astuce suivante (on suppose que `a` vaut `0` ou `1`):
        ```python
        b = 1-a
        ```

??? question     
    Exécuter les 2 cellules suivantes et expliquer les résultats.

    ```python
    75 & 15
    ```


    ```python
    ~1
    ```

### 2.3. Affichage d'une table de vérité

- Fonction `NON ET` (`NAND`), version booléenne :
   - les valeurs possibles sont stockées dans une liste `[False, True]` parcourue par une boucle `for`
   - Il y a autant de boucle `for` imbriquées que d'entrées dans la table de vérité.


```python
for e1 in [False, True]:
    for e2 in [False, True]:
        s = not (e1 and e2)
        print(e1, e2, s)
```

??? question

    Écrire la version (binaire) du programme précédent.

    Vous devez obtenir:
        
    | e1| e2|  s|
    |---|---|---|
    | 0 | 0 | 1 | 
    | 0 | 1 | 1 |
    | 1 | 0 | 1 |
    | 1 | 1 | 0 |
    

??? question

    Écrire un programme (version *binaire* ou *booléenne*) qui affiche la table de vérité de l'opération `OU EXCLUSIF` à 3 entrées (e1, e2, e3)
