# Découverte et utilisation du shell Linux

(source: TP Cisco NetAcad du cours de cybersécurité)

---

!!! info "Objectif"

    Se familiariser avec le shell pour gérer des fichiers et des répertoires.

## I. Notions de base sur le shell

Le terme « shell » désigne l'interpréteur de commandes sous Linux. 

Généralement confondu avec les termes *Terminal*, *Ligne de commande* et *Invite de commande* (ou *prompt*), le shell permet d'interagir de manière efficace avec un système d'exploitation Linux.


### 1. Accéder à la ligne de commande

Vous devez lancer une application de terminal (*gnome-terminal*, *xterm*, ...).

![Terminal](img/03-terminal.png)

### 2. Afficher les pages de manuel à partir de la ligne de commande

Vous pouvez afficher l'aide de la ligne de commande à l'aide de la commande `man`. 

Une page *man*, abréviation de page de manuel, est une documentation intégrée sur les commandes Linux. Les pages man offrent des informations détaillées sur les commandes et leurs options.

??? info "Pages de manuel"

    Pour en savoir plus sur les pages de manuel, saisissez :

    ```{.console .rgconsole}
    [nsi@localhost:~]$ man man
    ```

    Tapez ++q++ pour quitter la page man.

- Utilisez la commande `man` pour en savoir plus sur la commande `cp`:
    ```{.console .rgconsole}
    [nsi@localhost:~]$ man cp
    ```

??? tip "Touche clavier ++tab++"
    ++tab++ peut s'avérer extrêmement utile dans le shell Linux, elle permet :

    - de compléter automatiquement la fin d'une commande ou d'un nom de fichier (ou de répertoire).
    - d'afficher les diverses complétions possibles (en appuyant 2 fois sur ++tab++).

    !!! example "Exemple" 
    
        Tapez `ma` et appuyer 2 fois sur ++tab++ pour tester.

### 3. Créer et modifier des répertoires

Au cours de cette étape, vous utiliserez les commandes de changement de répertoire (`cd`), de création de répertoire (`mkdir`) et de liste de répertoire (`ls`).

??? info "Remarque"
    « Répertoire » est synonyme de « Dossier ». Les termes dossier et répertoire sont souvent utilisés de manière interchangeable dans ces travaux pratiques.

- Tapez `pwd` dans l'invite

    ```{.console .rgconsole}
    [nsi@localhost:~]$ pwd
    ```
    Quel est le répertoire actif ?

- Accédez au répertoire `/home/nsi` si ce n'est pas le répertoire actif en saisissant la commande `cd /home/nsi`
    ```{.console .rgconsole}
    [nsi@localhost:~]$ cd /home/nsi
    ```

- Saisissez `ls -l` dans l'invite de commande pour obtenir la liste des fichiers et des dossiers qui se trouvent dans le dossier actif.

    ??? info 
    
        L'option `-l` (pour liste) affiche la taille du fichier, ses autorisations, ses paramètres de propriété, sa date de création et bien plus encore.

    ```{.console .rgconsole}
    [nsi@localhost:~]$ ls -l
    ```

- Dans le répertoire actif, utilisez la commande `mkdir` pour créer le sous-dossiers `rep1`
    ```{.console .rgconsole}
    [nsi@localhost:~]$ mkdir rep1
    ```

- Répétez ces étapes pour créer 2 autres sous-dossiers: `rep2` et `rep3`.

    Saisissez `ls -l` pour vérifier que les dossiers ont bien été créés.

- Saisissez `cd /home/nsi/rep3` dans l'invite de commande et appuyez sur ++enter++.
    Dans quel répertoire vous trouvez-vous maintenant ?

    ??? tip "Invite de commande"

        L'invite de base indique `[nsi@localhost ~]$`

        === "`~`"
    
            `~` représente le répertoire de base de l'utilisateur.       
            Dans cet exemple, le répertoire de base de l'utilisateur est `/home/nsi`. 

        === "`$`"
            
            `$` (dollar) indique des privilèges d'utilisateur standard. Si un signe `#` (hashtag) s'affiche dans l'invite de commande, celui-ci indique des privilèges élevés (utilisateur *root*).

        Si ces symboles, conventions et concepts de base ne changent pas, l'invite d'une fenêtre de terminal sous Linux reste tout de même hautement personnalisable. Par conséquent, la structure de l'invite observée sur le poste de travail virtuel sera probablement différente de l'invite sur les autres installations Linux.

    Après exécution de la commande `cd /home/nsi/rep3`, le répertoire de base de l'utilisateur actuel devient `/home/nsi/rep3`.

- Dans quel répertoire vous trouvez vous si vous tapez la commande `cd ~` ? (le symbole `~` est un tilde)

- Retournez dans le répertoire `rep3` et utilisez la commande `mkdir` pour créer un nouveau sous-dossier nommé `rep4` à l'intérieur du dossier `rep3`.
    
    Utilisez la commande `ls -l` pour vérifier que le dossier a été créé.
    
Jusqu'ici, nous avons utilisé des chemins d'accès absolus. Les chemins absolus désignent les chemins qui commencent à la racine (`/`). Il est également possible d'utiliser des chemins relatifs. Les chemins d'accès relatifs réduisent le volume de texte à taper. Pour comprendre comment sont formés les chemins d'accès relatifs, vous devez savoir à quoi correspondent les répertoires `.` et `..` (*point* et *double-point*).

- Depuis le répertoire `rep3`, taper une commande `ls – la`
    ```{.console .rgconsole}
    [nsi@localhost:~]$ ls -la /home/nsi/rep3
    ```

    ??? tip "En savoir plus" 
    
        L'option `-a` indique à `ls` d'afficher tous les fichiers. Notez les listes `.` et `..` affichées par `ls`. Ces listes sont utilisées par le système d'exploitation pour assurer le suivi de l'utilisation du répertoire actuel (`.`) et du répertoire parent (`..`). Vous pouvez voir comment sont utilisés les répertoires `.` et `..` lorsque vous utilisez la commande `cd` pour changer de répertoire.
    
- Tapez la commande `cd .`
    
    ??? tip "Précisions"
    
        Aucun changement de répertoire visible ne se produit, car `.` désigne le répertoire actuel.
    
- Tapez la commande `cd ..`
    ```{.console .rgconsole}
    [nsi@localhost:~]$ cd ..
    ```
    Dans quel répertoire vous trouvez-vous ?

- Tapez la commande `cd rep3/rep4`
    ```{.console .rgconsole}
    [nsi@localhost:~]$ cd rep3/rep4
    ```
    Dans quel répertoire vous trouvez-vous ?

- Tapez la commande `cd ../../rep1`
    ```{.console .rgconsole}
    [nsi@localhost ~]$ cd ..
    ```
    Dans quel répertoire vous trouvez-vous ?

### 4. Rediriger les sorties.

Il existe un autre opérateur de ligne de commande puissant dans Linux appelé *redirect*. Représenté par le symbole `>`, cet opérateur permet de rediriger la sortie d'une commande vers un emplacement autre que la fenêtre de terminal actuelle (par défaut).

- Utilisez la commande `cd` pour passer au répertoire `/home/nsi/` (`~`) :
    ```{.console .rgconsole}
    [nsi@localhost:/]$ cd /home/nsi
    ```

- Utilisez la commande `echo` pour envoyer un message par écho. Étant donné qu'aucune sortie n'a été définie, l'écho sera transmis à la fenêtre de terminal actuelle :
    ```{.console .rgconsole}
    [nsi@localhost:~]$ echo "Il s'agit d'un message envoye au terminal par echo."
    ```

- Utilisez l'opérateur `>` pour transmettre la sortie de l'écho à un fichier de texte plutôt qu'à l'écran :
    ```{.console .rgconsole}
    [nsi@localhost:~]$ echo "Il s'agit d'un message envoye au terminal par echo." > fichier_texte.txt
    ```
    Aucune sortie ne s'affiche. Ce résultat était-il prévisible ?

    ??? tip "En savoir plus"
    
        Notez que même si le fichier `fichier_texte.txt` n'existait pas, il a été créé automatiquement pour recevoir la sortie générée par la commande echo. Utilisez la commande `ls -l` pour vérifier si le fichier a été créé.

- Utilisez la commande `cat` pour afficher le contenu du fichier texte `fichier_texte.txt` :
    ```{.console .rgconsole}
    [nsi@localhost:~]$ cat fichier_texte.txt
    ```

- Utilisez de nouveau l'opérateur `>` pour transmettre une autre sortie d'écho au fichier texte `fichier_texte.txt` :
    ```{.console .rgconsole}
    [nsi@localhost:~]$ echo "Il s'agit d'un message DIFFERENT envoye de nouveau au terminal par echo." > fichier_texte.txt
    ```

    Utilisez de nouveau la commande `cat` pour afficher le contenu du fichier texte `fichier_texte.txt`.

### 5. Rediriger un fichier texte et y ajouter des données.

Semblable à l'opérateur `>`, l'opérateur `>>` permet également de rediriger les données vers les fichiers. La différence est que `>>` ajoute les données à la fin du fichier ciblé, sans modifier le contenu actuel. Pour ajouter un message au fichier `fichier_texte.txt`, émettez la commande ci-dessous :

```{.console .rgconsole}
[nsi@localhost:~] $ echo "Il s'agit d'une autre ligne de texte. Elle sera AJOUTEE au fichier de sortie." >> fichier_texte.txt
```

Utilisez la commande cat pour afficher de nouveau le contenu du fichier texte fichier_texte.txt
    
    
??? tip "Éditeurs de texte"

    Il existe aussi des éditeurs de texte qui fonctionne dans la commande en ligne (`nano`, `vim`, &hellip;). 
    
    Pour éditer le fichier (identifier les commandes qui apparaissent sur la dernière ligne du terminal et taper ++ctrl+c++ pour sortir):
    ```{.console .rgconsole}
    [nsi@localhost:~]$ nano fichier_texte.txt
    ```

### 6. Utiliser des fichiers cachés dans Linux.

Dans Linux, les fichiers dont le nom commence par un `.` (*point*) ne sont pas affichés par défaut. Bien que les fichiers `.` (point) n'aient pas d'autre particularité, ils sont appelés fichiers cachés pour cette raison. Par exemple, dees fichiers se nommant `.fichier5`, `.fichier6`, `.fichier7` sont des fichiers cachés.

??? info "Remarque" 

    Ne confondez pas les fichiers `.` (point) avec le symbole `.` qui est l'indicateur de répertoire. Le nom des fichiers masqués commence par un point, suivi de plusieurs caractères, tandis que le répertoire `.` (point) est un répertoire masqué dont le nom comporte uniquement un point.

- Utilisez la commande `ls -l` pour afficher les fichiers stockés dans le répertoire de base de l'analyste.
    Combien de fichiers sont affichés ?

- Utilisez la commande `ls -la` pour afficher tous les fichiers dans le répertoire de base de l'utilisateur analyst, y compris les fichiers cachés.
    ```{.console .rgconsole}
    [nsi@localhost:~]$ ls –la
    ```

- Tapez la commande `man ls` à l'invite pour en savoir plus sur la commande `ls`.

    ??? tip "Astuce"
        Utilisez ++arrow-down++ (une seule ligne à la fois) ou ++space++ (une page à la fois) pour faire
        défiler la page vers le bas jusqu'à ce que vous trouviez la commande -a utilisée ci-dessus. Lisez sa description pour vous familiariser avec la commande ls -a.

## II Copier, supprimer et déplacer des fichiers

### 1. Copier des fichiers

La commande `cp` sert à copier des fichiers dans le système de fichiers local. Lorsque vous utilisez la commande `cp`, une nouvelle copie du fichier est créée et placée dans l'emplacement spécifié, sans modifier le fichier d'origine. Le premier paramètre correspond au fichier source et le second à la destination. 

- Lancez la commande ci-dessous pour copier `fichier_texte.txt` du répertoire de base vers le dossier `rep2` :
    ```{.console .rgconsole}
    [nsi@localhost:~]$ cp fichier_texte.txt rep2/
    ```

- Identifier les paramètres dans la commande `cp` ci-dessus. Quels sont les fichiers source et de destination ? (Utilisez les chemins d'accès complets pour représenter les paramètres.)

- Utilisez la commande `ls` pour vérifier que `fichier_texte.txt` est maintenant dans le dossier `rep2`.

### 2. Supprimer des fichiers et des répertoires

- Utilisez la commande `rm` pour supprimer des fichiers. Lancez la commande ci-dessous pour supprimer le fichier fichier_texte.txt du répertoire de base. :
    ```{.console .rgconsole}
    [nsi@localhost:~]$ rm fichier_texte.txt
    ```

    Vérifier avec la commande `ls` que le fichier `fichier_texte.txt` a été supprimé du répertoire de base.

    ??? tip "En savoir plus"
    
        Dans Linux, les répertoires sont considérés comme un type de fichier. À ce titre, la commande `rm` sert également à supprimer des répertoires, mais l'option `-r` (récursive) doit être utilisée.
        Notez que tous les fichiers et tous les répertoires dans un répertoire donné sont également supprimés lorsque vous supprimez un répertoire parent.

- Exécutez la commande ci-dessous pour supprimer le dossier `rep1` et son contenu :
    ```{.console .rgconsole}
    [nsi@localhost:~]$ rm -r rep1
    ```

- Vérifier avec la commande `ls` que le répertoire a bien été supprimé.

    ??? tip "Précisions"
        Le processus utilisé pour déplacer des fichiers est similaire à celui utilisé pour les copier. La différence est que lorsqu'un fichier est déplacé il est supprimé de son emplacement d'origine.
    
- Utilisez les commandes `mv` pour déplacer des fichiers dans le système de fichiers local.
    ??? tip "Astuce"
        Comme les commandes `cp`, la commande `mv` requiert également des paramètres de fichier source et de destination.

- Lancez la commande ci-dessous pour déplacer le fichier `fichier_texte.txt` du répertoire `/home/nsi/rep2` vers le répertoire de base :
    ```{.console .rgconsole}
    [nsi@localhost:~]$ mv rep2/fichier_texte.txt .
    ```

    Vérifier le résultat avec les commandes `ls` appropriées.

    ??? tip "Astuce"
        La commande `mv` peut également être utilisée pour déplacer des répertoires complets et les fichiers qu'ils contiennent. Pour déplacer `rep3` (et tous les fichiers et répertoires qu'il contient) dans `rep2`, utilisez la commande suivante :
        ```{.console .rgconsole}
        [nsi@localhost ~]$ mv rep3/ rep2/
        ```

        Vérifier le résultat avec une commande `ls`