---
hide:
  - navigation
---

# L'ordinateur à transistors

## 1. Repères historiques

=== "Avant 1943"

    Il existait quelques systèmes programmables, mais ils 
    étaient électromécaniques et incapables d'exécuter l'ensemble des 
    instructions attendues pour un ordinateur (on parle plutôt de *calculateur*).

    <figure>

    ![Collossus](img/01-Colossus.jpg)

    <figcaption>la famille des Colossus en Grande-Bretagne qui a servi à casser les 
    communications chiffrées des nazis.</figcaption>
    </figure>

=== "1943"

    1er ordinateur électronique complet (à tubes à vide).

    L'[ENIAC](https://dailygeekshow.com/eniac-premier-ordinateur/) est développé 
    en secret aux États-Unis initialement pour mettre au point la bombe H (puis
    finalement pour calculer des trajectoires de missiles).

    ![Eniac](img/01-eniac.jpg){ width=75% }
    ![Tube à vide](img/01-lampe_a_vide.jpg){ width=15% }

    - 30 tonnes, 170 m2, 160 kW
    - 70 000 résistances, 10 000 condensateurs, 6 000 commutateurs, 1 500 relais, 18 000 tubes à vide.
    - 5 000 calculs à la seconde en système décimal signé
    - Dispose d'une instruction de branchement conditionnel (équivalent des instructions if..elif..else)
    - La (re)programmation s'effectue grâce à des câbles

    *[ENIAC]: Electronical Numerical Integrator and Computor

=== "1947"

    1er transistor à semi-conducteur[^1] mis au point par les laboratoire Bell.

    |    |    |
    |:--:|:--:|
    | ![transistor](img/01-transistor1.png) | ![point-contact](img/01-transistor-point_contact.png)

    [^1]: technologie point-contact (germanium + contacts en or).

=== "1971"

    ??? tip inline end "Circuits intégrés"

        Grâce à sa faculté de miniaturisation extrême, il est possible 
        d'intégrer un très grand nombre de transistors sur une petite surface 
        de semi-conducteur et réaliser des fonctions spécifiques.

        ![Wafer](img/01-wafer.png)

        De nos jours, un smartphone contient environ 2 milliards de transistors.

    1er processeur (C4004, ~2 300 transistors) mis au point par Intel.

    ![C4004](img/01-C4004.png){ width=25% }


## 2. De l'interrupteur à la porte NAND

### 2.1. L'interrupteur commandé

=== "Relais mécanique (*préhistoire*)"

    Il possède 1 entrée signal (`in`), 1 entrée de commande (`c`) et une sortie. Il existe 2 variantes :

    <figure>

    ![Relais mécanique](img/01-relais.png)

    <figcaption>(source: [https://nandgame.com](https://nandgame.com))</figcaption>
    </figure>

    === "Default off"
    
        ou Normalement Ouvert (NO): la sortie ne recopie l'entrée que si la commande vaut `1`.

        | c | in | out |
        |:-:|:--:|:---:|
        | 0 |  0 |  0  |
        | 0 |  1 |  0  |
        | 1 |  0 |  0  |
        | 1 |  1 |  1  |

        (il s'agit d'une porte ET: `out = c and in`)

    === "Default on"

        ou Normalement Fermé (NF): la sortie recopie l'entrée tant que la commande vaut `0`.

        | c | in | out |
        |:-:|:--:|:---:|
        | 0 |  0 |  0  |
        | 0 |  1 |  1  |
        | 1 |  0 |  0  |
        | 1 |  1 |  0  |

        (si on fixe `in=1`, on obtient une porte inverseuse: `out = not c`)

    Le relais se comporte comme un interrupteur commandé par une courant appliqué à son entrée.

=== "Transistor (MOSFET)"

    De nos jours, les transistors sont essentiellement des MOSFET réalisés en silicium et miniaturisables à une taille inférieure à 10 nm. 
        
    ??? info inline end "Détails"
    
        Le transistor possède 3 (voire 4) bornes :
        
        - la Grille (G), Source (S) et Drain (D).
        - 1 substrat (souvent relié à la source)

        Le courant ne circule pas au repos. Si on applique une tension $V_{GS}\ge V_{GSTh} > 0$, il se crée un canal sous l'oxyde où peut passer le courant.

    ![MOSFET](img/src/01-MOSFET.svg)

    *[MOSFET]: Metal Oxyde Semiconductor Field Effect Transistor

    ??? info inline end "Détails"

        Ce transistor nécessite une tension $V_{GS}\le V_{GSTh} < 0$ pour que soit créé le canal.

    Il existe un transistor MOSFET complémentaire: 

    ![Transistor P](img/src/01-mosfetp.svg){ width=15% }

    === "Interrupteur Ouvert au repos"

        - tension `e` nulle (état bas, `0` ou `#!python False`) : interrupteur ouvert

            <object data="../img/src/01-interrupteur_mosfet.svg" width=60%></object>

        - tension `e` positive (état haut, `1` ou `#!python True`) : interrupteur fermé

    === "Interrupteur Fermé au repos"

        On a le comportement inverse avec le transistor MOSFET complémentaire:

        - tension `e` nulle (état bas, `0` ou `#!python False`) : interrupteur fermé

        <object data="../img/src/01-interrupteur_mosfetp2.svg" width=60%></object>

        - tension `e` positive (état haut, `1` ou `#!python True`) : interrupteur ouvert

    Le transistor se comporte comme un interrupteur commandé par une tension *e* appliquée à son entrée.

### 2.2 Porte NAND

L'association de plusieurs *interrupteurs* permet de réaliser des opérations logiques.

=== "Exemples de réalisation"

    | Avec relais | Avec transistors |
    |:-----------:|:----------------:|
    | ![Porte NAND](img/01-NAND_relais.png){ width=50%} | ![Porte NAND](img/src/01-porte_nand.svg){ width=100% } | 

=== "Symbole" 

    <div style="width: 50%; height: 125px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
                "v": 3,
                "opts": {"showGateTypes": true},
                "in": [
                    {"pos": [50, 20], "id": 2, "name": "e1", "val": 1}, 
                    {"pos": [50, 80], "id": 21, "name": "e2", "val": 0}
                ],
                "out": [{"pos": [270, 50], "id": 3, "name": "s"}],
                "gates": [{"type": "NAND", "pos": [160, 50], "in": [18, 19], "out": 20}],
                "wires": [[2, 18], [21, 19], [20, 3]]
            }
            </script>
        </logic-editor>
    </div>

La table de vérité[^2] du **NON ET** est la suivante :

[^2]: Une table de vérité est un tableau qui indique l'état logique de la sortie pour toutes les combinaisons possibles des entrées.

| a | b | s  |
|:-:|:-:|:--:|
| 0 | 0 |  1 |
| 0 | 1 |  1 |
| 1 | 0 |  1 |
| 1 | 1 |  0 |

Nous verrons (en TP) qu'il est possible de construire toute l'électronique d'un processeur à partir de cette unique porte.

??? info "Cellule CMOS"

    === "Schéma"

        2 transistors complémentaires:

        <object data="../img/src/01-cellule_CMOS.svg" width="60%"></object>

    === "Table de vérité"

        ??? info inline end "Équation logique"

            ```py
            s = not(e)
            ```

        | e | s |
        |:-:|:-:|
        | 0 | 1 |
        | 1 | 0 |


    === "Symbole" 

        Il s'agit de l'opérateur booléen NOT (non) 

        <div style="width: 75%; height: 80px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
                "v": 3,
                "opts": {"showGateTypes": true},
                "gates": [{"type": "NOT", "pos": [140, 40], "in": 0, "out": 1}],
                "in": [{"pos": [70, 40], "id": 2, "name": "e", "val": 0}],
                "out": [{"pos": [210, 40], "name": "s", "id": 7}],
                "wires": [[2, 0], [1, 7]]
            }
            </script>
        </logic-editor>
        </div>

    ??? tip "Autre utilisation"

        La cellule CMOS est aussi à la base des capteurs photo/vidéo actuels.

## 3. ALU

*[ALU]: Arithmetic and Logic Unit

(*UAL* en français)

### 3.1. Opérateurs logiques

#### a. Tables

En plus des portes NAND et NOT, l'UAL comporte généralement toute une panoplie d'autres portes. Compléter leur table de vérité ci-après:

??? tip "Aide: simulateur"

    <div style="width: 100%; height: 340px">
    <logic-editor mode="tryout">
        <script type="application/json">
        {
            "v": 3,
            "opts": {"showGateTypes": true},
            "in": [
                {"pos": [60, 90], "id": 2, "val": 1, "name": "e1"}, 
                {"pos": [60, 240], "id": 19, "val": 0, "name": "e2"}
            ],
            "out": [
            {"pos": [280, 40], "id": 3},
            {"pos": [280, 100], "id": 20},
            {"pos": [280, 160], "id": 21},
            {"pos": [280, 230], "id": 22},
            {"pos": [280, 300], "id": 23}
            ],
            "gates": [
            {"type": "AND", "pos": [180, 40], "in": [4, 5], "out": 6},
            {"type": "OR", "pos": [180, 100], "in": [7, 8], "out": 9},
            {"type": "NOR", "pos": [180, 160], "in": [10, 11], "out": 12},
            {"type": "XOR", "pos": [180, 230], "in": [13, 14], "out": 15},
            {"type": "XNOR", "pos": [180, 300], "in": [16, 17], "out": 18}
            ],
            "wires": [[2, 4], [2, 7], [2, 10], [2, 13], [2, 16], [19, 5], [19, 8], [19, 11], [19, 14], [19, 17], [6, 3], [9, 20], [12, 21], [15, 22], [18, 23]]
        }
        </script>
    </logic-editor>
    </div>

=== "AND (ET)"

    | e1 | e2 |	s  |
    |:--:|:--:|:--:|
    | 0	 | 0  | {{ rg_qnum(0,0) }} |
    | 0	 | 1  | {{ rg_qnum(0,0) }} |
    | 1	 | 0  | {{ rg_qnum(0,0) }} |
    | 1	 | 1  | {{ rg_qnum(1,0) }} |

=== "OR (OU)"

    | e1 | e2 |	s  |
    |:--:|:--:|:--:|
    | 0	 | 0  | {{ rg_qnum(0,0) }} |
    | 0	 | 1  | {{ rg_qnum(1,0) }} |
    | 1	 | 0  | {{ rg_qnum(1,0) }} |
    | 1	 | 1  | {{ rg_qnum(1,0) }} |

=== "NOR (NON OU)"

    | e1 | e2 |	s  |
    |:--:|:--:|:--:|
    | 0	 | 0  | {{ rg_qnum(1,0) }} |
    | 0	 | 1  | {{ rg_qnum(0,0) }} |
    | 1	 | 0  | {{ rg_qnum(0,0) }} |
    | 1	 | 1  | {{ rg_qnum(0,0) }} |

=== "XOR (OU EXCLUSIF)"

    | e1 | e2 |	s  |
    |:--:|:--:|:--:|
    | 0	 | 0  | {{ rg_qnum(0,0) }} |
    | 0	 | 1  | {{ rg_qnum(1,0) }} |
    | 1	 | 0  | {{ rg_qnum(1,0) }} |
    | 1	 | 1  | {{ rg_qnum(0,0) }} |

=== "XNOR (NON OU EXCLUSIF)"

    | e1 | e2 |	s  |
    |:--:|:--:|:--:|
    | 0	 | 0  | {{ rg_qnum(1,0) }} |
    | 0	 | 1  | {{ rg_qnum(0,0) }} |
    | 1	 | 0  | {{ rg_qnum(0,0) }} |
    | 1	 | 1  | {{ rg_qnum(1,0) }} |

#### b. Équations 

La lecture d'une table de vérité permet d'établir une **équation logique** qui relie la sortie à l'entrée. Celle-ci peut s'écrire avec des `OU logiques` des combinaisons correspondantes.

Exemple de la porte NAND :

| e1 | e2 | s  |
|:--:|:--:|:--:|
| 0  | 0  |  1 |
| 0  | 1  |  1 |
| 1  | 0  |  1 |
| 1  | 1  |  0 |

=== "Méthode 1"

    On identifie les cas où la variable de sortie vaut 1. 

    ??? info inline end "Détails"

        La sortie est vraie si:
        
        - e1 est faux **et** e2 est faux,
        - **ou** si e1 est faux **et** e2 est vrai,
        - **ou** si e1 est vrai **et** e2 est faux. 

    ```py
    s = (not(e1) and not(e2)) \ 
        or (not(e1) and e2) \ 
        or (e1 and not(e2))
    ```

=== "Méthode 2"

    On identifie les cas où la variable de sortie vaut 0. 
    
    ??? info inline end "Détails"

        La sortie est fausse si e1 est vrai **et** e2 est vrai. 

    ```py
    not(s) = e1 and e2
    # ou
    s = not(e1 and e2)
    ```

#### c. Schémas

Les équations logiques obtenues précédemment peuvent être réalisées à l'aide de 3 portes : *AND*, *OR* et *NOT*.

Exemple de réalisation de la porte NAND :

=== "Méthode 1"

    <div style="width: 100%; height: 270px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [70, 45], id: 3, name: 'e1'},
            in1: {type: 'in', pos: [70, 215], id: 4, name: 'e2'},
            not0: {type: 'not', pos: [205, 45], in: 5, out: 6},
            not1: {type: 'not', pos: [195, 215], in: 7, out: 8},
            and1: {type: 'and', pos: [325, 135], in: [9, 10], out: 11},
            or0: {type: 'or', pos: [490, 135], in: '15-17', out: 18, bits: 3},
            and0: {type: 'and', pos: [330, 55], in: [19, 20], out: 21},
            and2: {type: 'and', pos: [325, 225], in: [22, 23], out: 24},
            out0: {type: 'out', pos: [605, 135], id: 25, name: 's'},
            },
            wires: [[3, 5], [4, 7], [6, 9], [8, 10], [6, 19], [4, 20], [8, 22], [3, 23], [24, 17], [21, 15], [11, 16], [18, 25]]
        }
        </script>
    </logic-editor>
    </div>

=== "Méthode 2"

    <div style="width: 100%; height: 165px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            opts: {showGateTypes: true},
            components: {
            in0: {type: 'in', pos: [55, 35], id: 3, name: 'e1', val: 1},
            in1: {type: 'in', pos: [55, 130], id: 4, name: 'e2'},
            out0: {type: 'out', pos: [435, 80], id: 25, name: 's'},
            not0: {type: 'not', pos: [325, 80], in: 26, out: 27},
            and0: {type: 'and', pos: [200, 80], in: [28, 29], out: 30},
            },
            wires: [[3, 28], [4, 29], [30, 26], [27, 25]]
        }
        </script>
    </logic-editor>
    </div>

### 3.2. Opérateurs arithmétiques

En associant les portes logiques de manière appropriée, il est possible de 
construire des fonctions arithmétiques :

- Une cellule d'addition binaire (avec propagation d'une retenue `C`) :

    On additionne 3 bits `A`, `B` et `Cin` (retenue entrante) et on récupère
    la sortie `S` et une éventuelle retenue sortante `Cout`.

    `(Cout, S) = A + B + Cin` 

    === "Table logique"

        | Cin | A | B | Cout | S |
        |:---:|:-:|:-:|:----:|:-:|
        | 0   | 0 | 0 |  0   | 0 |
        | 0   | 0 | 1 |  0   | 1 |
        | 0   | 1 | 0 |  0   | 1 |
        | 0   | 1 | 1 |  1   | 0 |
        | 1   | 0 | 0 |  0   | 1 |
        | 1   | 0 | 1 |  1   | 0 |
        | 1   | 1 | 0 |  1   | 0 |
        | 1   | 1 | 1 |  1   | 1 |

    === "Réalisation"

        <div style="width: 100%; height: 300px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
                "v": 3,
                "opts": {"showGateTypes": true},
                "in": [
                    {"pos": [70, 140], "id": 36, "val": 0, "name": "A"}, 
                    {"pos": [70, 250], "id": 37, "val": 1, "name": "B"}, 
                    {"pos": [310, 50], "orient": "s", "id": 38, "val": 0, "name": "Cin"}],
                "out": [
                    {"pos": [580, 260], "orient": "s", "id": 48, "name": "Cout"}, 
                    {"pos": [610, 110], "id": 49, "name": "S"}],
                "gates": [
                {"type": "XOR", "pos": [260, 150], "in": [30, 31], "out": 32},
                {"type": "AND", "pos": [400, 180], "in": [33, 34], "out": 35},
                {"type": "XOR", "pos": [400, 110], "in": [39, 40], "out": 41},
                {"type": "AND", "pos": [400, 240], "in": [42, 43], "out": 44},
                {"type": "OR", "pos": [510, 210], "in": [45, 46], "out": 47}
                ],
                "wires": [[36, 30], [37, 31], [38, 33], [32, 34], [38, 39], [32, 40], [36, 42], [37, 43], [35, 45], [44, 46], [47, 48], [41, 49]]
            }
            </script>
        </logic-editor>
        </div>

    === "Symbole"

        <div style="width: 100%; height: 300px">
            <logic-editor mode="tryout">
                <script type="application/json">
                {
                    "v": 3,
                    "opts": {"showGateTypes": true},
                    "in": [
                    {"pos": [200, 50], "orient": "s", "id": 36, "val": 0, "name": "B"},
                    {"pos": [160, 50], "orient": "s", "id": 37, "val": 0, "name": "A"},
                    {"pos": [290, 160], "orient": "w", "id": 38, "val": 0, "name": "Cin"}
                    ],
                    "out": [
                        {"pos": [80, 160], "orient": "w", "id": 48, "name": "Cout"}, 
                        {"pos": [180, 250], "orient": "s", "id": 49, "name": "S"}],
                    "components": [{"type": "adder", "pos": [180, 160], "in": [57, 58, 59], "out": [60, 61]}],
                    "wires": [[37, 57], [36, 58], [38, 59], [60, 49], [61, 48]]
                }
                </script>
            </logic-editor>
        </div>

- Un additionneur *4* bits (pour entiers en complément à 2) obtenu en cascadant 
*4* additionneurs 1 bit :

    === "Détails"

        <table>
        <caption>additionneur binaire 4 bits</caption>
        <tr style="font-size: x-small;"><td></td><td>1</td><td>1</td><td>1</td><td></td><tr>
        <tr><td></td><td>0</td><td>1</td><td>0</td><td>1</td><tr>
        <tr><td>+</td><td>0</td><td>1</td><td>1</td><td>1</td><tr>
        <tr><td>=</td><td>1</td><td>1</td><td>0</td><td>0</td><tr>
        </table>


    === "Réalisation"

        <div style="width: 100%; height: 320px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
            "v": 3,
            "opts": {"showGateTypes": true},
            "in": [
                {"pos": [660, 180], "orient": "w", "id": 38, "val": 0},
                {"type": "nibble", "pos": [280, 20], "orient": "s", "id": [62, 63, 64, 65], "val": [0, 0, 1, 0]},
                {"type": "nibble", "pos": [490, 20], "orient": "s", "id": [70, 71, 72, 73], "val": [0, 1, 1, 0]}
            ],
            "out": [
                {"pos": [100, 180], "orient": "w", "id": 48, "name": "overflow"},
                {"type": "nibble", "pos": [380, 300], "orient": "s", "id": [66, 67, 68, 69]},
                {"type": "nibble", "pos": [640, 50], "id": [93, 94, 95, 96]},
                {"type": "nibble", "pos": [110, 50], "orient": "w", "id": [97, 98, 99, 100]}
            ],
            "components": [
                {"type": "adder", "pos": [560, 180], "in": [57, 58, 59], "out": [60, 61]},
                {"type": "adder", "pos": [440, 180], "in": [74, 75, 76], "out": [77, 78]},
                {"type": "adder", "pos": [320, 180], "in": [79, 80, 81], "out": [82, 83]},
                {"type": "adder", "pos": [200, 180], "in": [84, 85, 86], "out": [87, 88]}
            ],
            "wires": [
                [38, 59],
                [70, 58],
                [62, 57],
                [61, 76],
                [78, 81],
                [83, 86],
                [88, 48],
                [71, 75],
                [72, 80],
                [73, 85],
                [63, 74],
                [64, 79],
                [65, 84],
                [87, 69],
                [82, 68],
                [77, 67],
                [60, 66],
                [70, 93],
                [71, 94],
                [72, 95],
                [73, 96],
                [64, 99],
                [65, 100],
                [63, 98],
                [62, 97]
            ]
            }
            </script>
        </logic-editor>
        </div>

- D'autres opérateurs de base : soustracteur, incrémenteur, décrémenteur...

### 3.3. Sélection d'opération

!!! info "Sélecteur"

    Aussi appelé multiplexeur, il permet de choisir un signal de sortie parmi 
    plusieurs signaux d'entrée.

    === "Principe"

        Le choix de l'entrée s'effectue à l'aide de *Op*.

        <div style="width: 100%; height: 210px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
            "v": 3,
            "opts": {"showGateTypes": true},
            "in": [
                {"pos": [60, 40], "id": 29, "name": "e1", "val": 0},
                {"pos": [60, 120], "id": 30, "name": "e2", "val": 1},
                {"pos": [160, 170], "orient": "n", "id": 31, "name": "Op", "val": 0}
            ],
            "out": [{"pos": [240, 80], "id": 32, "name": "s"}],
            "components": [{"type": "mux-2to1", "pos": [160, 80], "in": [12, 13, 14], "out": 15}],
            "wires": [[31, 14], [15, 32], [29, 12], [30, 13]]
            }
            </script>
        </logic>
        </div>

    === "Choix d'un opérateur"

        Les 2 opérations sont effectuées en parallèle mais seul le résultat
        sélectionné par *Op* est récupéré en sortie.

        <div style="width: 100%; height: 230px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
            "v": 3,
            "opts": {"showGateTypes": true},
            "components": [{"type": "mux-2to1", "pos": [310, 100], "in": [12, 13, 14], "out": 15}],
            "gates": [{"type": "AND", "pos": [180, 50], "in": [23, 24], "out": 25}, {"type": "OR", "pos": [180, 140], "in": [26, 27], "out": 28}],
            "in": [
                {"pos": [60, 40], "id": 29, "name": "e1", "val": 1},
                {"pos": [60, 150], "id": 30, "name": "e2", "val": 0},
                {"pos": [310, 190], "orient": "n", "id": 31, "name": "Op", "val": 0}
            ],
            "out": [{"pos": [390, 100], "id": 32, "name": "s"}],
            "wires": [[29, 23], [30, 24], [29, 26], [30, 27], [25, 12], [28, 13], [31, 14], [15, 32]]
            }
            </script>
        </logic>
        </div>

Une Unité Arithmétique et Logique (UAL) est composant qui, en fonction de la valeur
d'une commande (ici `Op1, Op0`), réalise soit une opération arithmétique, soit une opération 
logique sur ces entrées :

=== "Opérations"

    | Op1 | Op0 | Opération effectuée |
    |:---:|:---:|---------------------|
    |  0  |  0  | Addition  A+B       |
    |  0  |  1  | Soustraction A-B    |
    |  1  |  0  | OU logique          |
    |  1  |  1  | ET logique          | 

=== "Réalisation"

    <div style="width: 100%; height: 280px">
    <logic-editor mode="tryout">
        <script type="application/json">
        { // JSON5
            v: 6,
            components: {
            alu0: {type: 'alu', pos: [280, 150], orient: 's', in: '0-10', out: '11-17'},
            in0: {type: 'in', pos: [230, 20], orient: 's', id: '19-22', bits: 4, val: '0010'},
            in1: {type: 'in', pos: [330, 20], orient: 's', id: '24-27', bits: 4, val: '0101'},
            disp0: {type: 'display', pos: [105, 70], orient: 'w', id: '28-31'},
            disp1: {type: 'display', pos: [465, 70], id: '32-35'},
            out0: {type: 'out', pos: [105, 135], orient: 'w', id: 37, name: 'Overflow?'},
            out1: {type: 'out', pos: [105, 175], orient: 'w', id: 38, name: 'Nul?'},
            in2: {type: 'in', pos: [465, 130], orient: 'w', id: 39, name: 'Cin'},
            in3: {type: 'in', pos: [465, 165], orient: 'w', id: 40, name: 'Op1'},
            in4: {type: 'in', pos: [465, 195], orient: 'w', id: 41, name: 'Op0'},
            disp2: {type: 'display', pos: [280, 250], orient: 's', id: '42-45'},
            },
            wires: [[19, 4], [20, 5], [21, 6], [22, 7], [24, 0], [25, 1], [26, 2], [27, 3], [19, 28], [20, 29], [21, 30], [22, 31], [24, 32], [25, 33], [26, 34], [27, 35], [15, 37], [16, 38], [39, 10], [40, 9], [41, 8], [11, 42], [12, 43], [13, 44], [14, 45]]
        }
        </script>
    </logic-editor>
    </div>

Des sorties spécifiques peuvent fournir des informations supplémentaires 
(résultat nul, dépassement de capacité &hellip;).
 
## 4. Mémoires

### 4.1. Registre

- Commençons par créer le verrou `SR` (`Set/Reset`) :

    === "Réalisation"

        <div style="width: 75%; height: 140px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
                "v": 3,
                "opts": {"showGateTypes": true},
                "in": [{"pos": [50, 30], "id": 31, "name": "S", "val": 1}, {"pos": [50, 110], "id": 32, "name": "R", "val": 0}],
                "out": [{"pos": [250, 40], "id": 33, "name": "Q"}, {"pos": [250, 100], "id": 34, "name": "not(Q)"}],
                "gates": [{"type": "NOR", "pos": [150, 40], "in": [44, 45], "out": 46}, {"type": "NOR", "pos": [150, 100], "in": [47, 48], "out": 49}],
                "wires": [[46, 33], [49, 34], [46, 47], [49, 45], [32, 44], [31, 48]]
            }
            </script>
        </logic-editor>
        </div>

    === "Symbole"

        <div style="width: 75%; height: 100px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
                "v": 3,
                "opts": {"showGateTypes": true},
                "in": [{"pos": [50, 30], "id": 31, "name": "S", "val": 1}, {"pos": [50, 70], "id": 32, "name": "R", "val": 0}],
                "out": [{"pos": [250, 30], "id": 33, "name": "Q"}, {"pos": [250, 70], "id": 34, "name": "not(Q)"}],
                "components": [{"type": "latch-sr", "pos": [160, 50], "in": [35, 36], "out": [37, 38], "state": 1}],
                "wires": [[31, 35], [32, 36], [37, 33], [38, 34]]
            }
            </script>
        </logic-editor>
        </div>

    Ce dispositif peut stocker le bit `1` (si `S` passe à `1`) ou `0` (si `R` passe à `1`)
    mais son état est indéterminé si `S` et `R` valent tous les deux `1`.

- L'étape suivante consiste à transformer ce verrou en une bascule `D` (`Data`)
synchronisée par une horloge (`Clock`) :

    Ce composant ne stocke la valeur de la donnée `D` que lorsque l'horloge 
    passe à `1`.

    === "Réalisation"

        <div style="width: 100%; height: 200px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
                "v": 3,
                "opts": {"showGateTypes": true},
                "in": [
                    {"pos": [80, 60], "id": 31, "name": "D", "val": 0}, 
                    {"pos": [80, 110], "id": 32, "name": "Clock", "val": 0, "isPushButton": true}],
                "out": [{"pos": [480, 80], "id": 33, "name": "Q"}, {"pos": [480, 120], "id": 34, "name": "not(Q)"}],
                "components": [{"type": "latch-sr", "pos": [390, 100], "in": [50, 51], "out": [52, 53], "state": 0}],
                "gates": [
                {"type": "AND", "pos": [260, 70], "in": [54, 55], "out": 56},
                {"type": "AND", "pos": [260, 130], "in": [57, 58], "out": 59},
                {"type": "NOT", "pos": [180, 140], "in": 60, "out": 61}
                ],
                "wires": [[52, 33], [53, 34], [31, 54], [59, 51], [32, 55], [32, 57], [56, 50], [61, 58], [31, 60]]
            }
            </script>
        </logic-editor>
        </div>

        Le circuit en amont évite que les 2 entrées du verrou ne soient dans
        un état indéterminé.


    === "Symbole"

        <div style="width: 100%; height: 100px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
                "v": 3,
                "opts": {"showGateTypes": true},
                "in": [
                    {"pos": [50, 30], "id": 31, "name": "D", "val": 1}, 
                    {"pos": [130, 70], "id": 32, "name": "Clock", "val": 0, "isPushButton": true}],
                "out": [{"pos": [300, 30], "id": 33, "name": "Q"}, {"pos": [300, 70], "id": 34, "name": "not(Q)"}],
                "components": [{"type": "flipflop-d", "pos": [200, 50], "in": [63, 64, 65, 66], "out": [67, 68], "state": 0}],
                "wires": [[31, 66], [32, 63], [67, 33], [68, 34]]
            }
            </script>
        </logic-editor>
        </div>

        On trouve parfois 2 entrées supplémentaires qui permettent de forcer le bit stocké à la 
        valeur `0` (`Clr`: `clear`) ou `1` (`Pst`: `preset`).

- En utilisant plusieurs bascules `D`, il devient possible de stocker plusieurs 
bits à la fois et d'obtenir un **registre**.

    === "Réalisation"

        <div style="width: 100%; height: 420px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
                "v": 3,
                "opts": {"showGateTypes": true},
                "components": [
                {"type": "flipflop-d", "pos": [280, 60], "in": [69, 70, 71, 72], "out": [73, 74], "state": 1},
                {"type": "flipflop-d", "pos": [280, 160], "in": [75, 76, 77, 78], "out": [79, 80], "state": 0},
                {"type": "flipflop-d", "pos": [280, 260], "in": [81, 82, 83, 84], "out": [85, 86], "state": 0},
                {"type": "flipflop-d", "pos": [280, 360], "in": [87, 88, 89, 90], "out": [91, 92], "state": 1}
                ],
                "in": [
                    {"type": "nibble", "pos": [80, 70], "id": [93, 94, 95, 96], "val": [0, 1, 1, 0]}, 
                    {"pos": [70, 240], "id": 97, "name": "Clock", "val": 0, "isPushButton": true}],
                "out": [{"type": "nibble", "pos": [450, 70], "id": [99, 100, 101, 102]}],
                "wires": [[93, 72], [94, 78], [95, 84], [96, 90], [97, 69], [97, 75], [97, 81], [97, 87], [73, 99], [79, 100], [85, 101], [91, 102]]
            }
            </script>
        </logic-editor>
        </div>

    === "Symbole"

        <div style="width: 100%; height: 380px">
        <logic-editor mode="tryout">
            <script type="application/json">
            {
                "v": 3,
                "opts": {"showGateTypes": true},
                "in": [
                {"type": "nibble", "pos": [70, 100], "id": [93, 94, 95, 96], "val": [1, 0, 0, 1]},
                {"pos": [70, 280], "id": 97, "name": "Clock", "val": 0, "isPushButton": true},
                {"pos": [200, 270], "orient": "n", "id": 114, "name": "clear", "val": 0}
                ],
                "out": [{"type": "nibble", "pos": [320, 100], "id": [99, 100, 101, 102]}],
                "components": [
                {"type": "register", "pos": [200, 100], "in": [103, 104, 105, 106, 107, 108, 109], "out": [110, 111, 112, 113], "state": [0, 1, 1, 0]}
                ],
                "wires": [[93, 106], [94, 107], [95, 108], [96, 109], [97, 103], [110, 99], [111, 100], [112, 101], [113, 102], [114, 105]]
            }
            </script>
        </logic-editor>
        </div>    

        On note que l'entrée `Clr` (`clear`) permet d'effacer le contenu du registre.

Le registre est un compagnon indispensable de l'UAL. C'est une mémoire extrêmement rapide (directement intégrée au processeur), mais en faible quantité et qui s'efface en cas de perte d'alimentation.

??? example "Exemple : Addition de plusieurs nombres"

    ??? tip inline end "Liste d'instructions"

        1. Effacer le registre.
        2. Charger `1` sur l'entrée `B`.
        3. Effectuer l'addition (`Op=0,0`, front montant de l'horloge).
        4. Charger `3` sur l'entrée `B`.
        5. Effectuer l'addition.
        6. Charger `2` sur l'entrée `B`.
        7. Effectuer l'addition.
        9. Charger `7` sur l'entrée `B`.
        10. Effectuer l'addition.

    Mise en œuvre de l'addition `1 + 3 + 2 + 7` à l'aide d'un registre *résultat*.

    <div style="width: 60%; height: 440px">
    <logic-editor mode="tryout">
        <script type="application/json">
        {
            "v": 3,
            "opts": {"showGateTypes": true, "wireStyle": "straight"},
            "components": [
            {
                "type": "alu",
                "pos": [230, 160],
                "orient": "s",
                "in": [202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212],
                "out": [213, 214, 215, 216, 217, 218]
            },
            {
                "type": "register",
                "pos": [230, 280],
                "orient": "s",
                "in": [243, 244, 245, 246, 247, 248, 249],
                "out": [250, 251, 252, 253],
                "state": [0, 0, 1, 0],
                "showContent": false
            }
            ],
            "in": [
            {"type": "nibble", "pos": [180, 20], "orient": "s", "id": [227, 228, 229, 230], "val": [0, 0, 0, 0]},
            {"pos": [70, 280], "id": 254, "name": "clear", "val": 0, "isPushButton": true},
            {"pos": [70, 230], "id": 255, "name": "clock", "val": 0, "isPushButton": true}
            ],
            "out": [
            {"type": "nibble", "pos": [60, 80], "orient": "w", "id": [231, 232, 233, 234]},
            {"type": "nibble", "pos": [150, 390], "orient": "w", "id": [239, 240, 241, 242]}
            ],
            "wires": [
            [227, 206],
            [228, 207],
            [229, 208],
            [230, 209],
            [227, 231, {"via": [[210, 110, "w"]]}],
            [228, 232, {"via": [[190, 90, "w"]]}],
            [229, 233, {"via": [[170, 70, "w"]]}],
            [230, 234, {"via": [[150, 50, "w"]]}],
            [213, 246],
            [214, 247],
            [215, 248],
            [216, 249],
            [254, 245],
            [255, 243],
            [253, 242, {"via": [[200, 360, "w"]]}],
            [252, 241, {"via": [[220, 380, "w"]]}],
            [251, 240, {"via": [[240, 400, "w"]]}],
            [250, 239, {"via": [[260, 420, "w"]]}],
            [250, 202, {"via": [[360, 330, "n"], [360, 120, "n"]]}],
            [251, 203, {"via": [[240, 340, "n"], [370, 340, "n"], [370, 110, "n"], [290, 110, "w"]]}],
            [252, 204, {"via": [[220, 350, "n"], [380, 350, "n"], [380, 100, "n"], [270, 100, "w"]]}],
            [253, 205, {"via": [[200, 360, "n"], [390, 360, "n"], [390, 90, "n"], [250, 90, "w"]]}]
            ]
        }
        </script>
    </logic-editor>
    </div>

### 4.2. RAM

*[RAM]: Random Access Memory

Ce composant se construit aussi à l'aide de transistors :

- Il dispose de multiples emplacements mémoire repérés par des adresses.
- Il permet de stocker (écrire) ou récupérer (lire) des données (ou des 
instructions de programme).

??? question inline end "Exercice"

    Stocker les valeurs `1`, `3`, `2` et `7` aux 4 premiers emplacements de 
    cette RAM. 

<div style="width: 60%; height: 350px">
  <logic-editor mode="tryout">
    <script type="application/json">
      {
        "v": 3,
        "opts": {"showGateTypes": true, "wireStyle": "straight"},
        "components": [{"type": "ram-16x4", "pos": [260, 180], "in": [256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266], "out": [267, 268, 269, 270]}],
        "in": [
          {"type": "nibble", "pos": [130, 60], "orient": "s", "id": [271, 272, 273, 274], "val": [1, 0, 0, 1]},
          {"type": "nibble", "pos": [260, 60], "orient": "s", "id": [275, 276, 277, 278], "val": [0, 0, 0, 0]},
          {"pos": [160, 290], "id": 279, "name": "Clock", "val": 0, "isPushButton": true},
          {"pos": [240, 290], "orient": "n", "id": 280, "name": "Write", "val": 0},
          {"pos": [280, 310], "orient": "n", "id": 281, "name": "clear", "val": 0, "isPushButton": true}
        ],
        "out": [{"type": "nibble", "pos": [390, 180], "id": [282, 283, 284, 285]}],
        "labels": [{"pos": [260, 40], "text": "Adresse"}, {"pos": [130, 20], "text": "Données"}, {"pos": [130, 40], "text": "(ou instructions)"}],
        "wires": [
          [278, 266],
          [277, 265],
          [276, 264],
          [275, 263],
          [279, 256],
          [280, 257],
          [281, 258],
          [274, 262, {"via": [[100, 210, "s"]]}],
          [273, 261, {"via": [[120, 190, "s"]]}],
          [272, 260, {"via": [[140, 170, "s"]]}],
          [271, 259, {"via": [[160, 150, "s"]]}],
          [270, 285],
          [269, 284],
          [268, 283],
          [267, 282]
        ]
      }
    </script>
  </logic-editor>
</div>

??? tip "Lecture mémoire 8 bits avec données ASCII"

    On utilise un compteur pour faire défiler les adresses mémoire dans l'ordre
    croissant. Les données stockées ici sont des codes ASCII.

    <div style="width: 100%; height: 410px">
    <logic-editor mode="static">
        <script type="application/json">
        {
            "v": 3,
            "opts": {"showGateTypes": true},
            "in": [
            {"pos": [140, 160], "orient": "n", "id": 79, "name": "clear", "val": 0, "isPushButton": true},
            {"pos": [250, 340], "orient": "n", "id": 89, "name": "Write", "val": 0},
            {"type": "clock", "pos": [70, 110], "id": 80, "period": 500}
            ],
            "out": [
            {"type": "ascii", "pos": [410, 250], "orient": "s", "id": [4, 5, 6, 7, 8, 9, 10]},
            {"type": "16seg", "pos": [600, 190], "id": [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 78]}
            ],
            "components": [
            {
                "type": "ram-16x8",
                "pos": [270, 200],
                "in": [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62],
                "out": [63, 64, 65, 66, 67, 68, 69, 70],
                "content": [
                "00110001",
                "01100101",
                "01110010",
                "01100101",
                "00100000",
                "01001110",
                "01010011",
                "01001001",
                "00100000",
                "01010110",
                "01101001",
                "01111010",
                "01101001",
                "01101100",
                "01101100",
                "01100101"
                ]
            },
            {"type": "counter", "pos": [140, 70], "in": [71, 72], "out": [73, 74, 75, 76, 77]},
            {
                "type": "decoder-16seg",
                "pos": [510, 190],
                "in": [1, 2, 3, 11, 12, 13, 14],
                "out": [15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
            }
            ],
            "wires": [
            [73, 59, {"via": [[300, 30]]}],
            [79, 72],
            [74, 60, {"via": [[280, 50]]}],
            [75, 61, {"via": [[260, 70]]}],
            [76, 62, {"via": [[240, 90]]}],
            [89, 49],
            [63, 4, {"via": [[440, 160]]}],
            [64, 5, {"via": [[430, 170]]}],
            [65, 6, {"via": [[420, 180]]}],
            [66, 7, {"via": [[410, 190]]}],
            [67, 8, {"via": [[400, 200]]}],
            [68, 9, {"via": [[390, 210]]}],
            [69, 10],
            [63, 1],
            [64, 2],
            [65, 3],
            [66, 11],
            [67, 12],
            [68, 13],
            [69, 14],
            [15, 32],
            [16, 33],
            [17, 34],
            [18, 35],
            [19, 36],
            [20, 37],
            [21, 38],
            [22, 39],
            [23, 40],
            [24, 41],
            [25, 42],
            [26, 43],
            [27, 44],
            [28, 45],
            [29, 46],
            [30, 47],
            [31, 78],
            [80, 71]
            ]
        }
        </script>
    </logic-editor>
    </div>


Cette mémoire permet de stocker une grande quantité de donnés, mais est 
beaucoup plus lente qu'un registre[^3] et s'efface en cas de perte 
d'alimentation.

[^3]: La RAM se situe sur la carte mère et est reliée au processeur via les bus respectifs de données et d'adresses.)

??? tip "Autres mémoires à transistors"

    - *Cache* : semblable à de la RAM, directement intégrée dans 
    le processeur.
    - ROM : mémoire en lecture seule.
    - SSD : remplace progressivement les disques durs magnétiques.
    - Flash (clé USB, carte mémoire).
    - &hellip;

*[ROM]: Read Only Memory
*[SSD]: Solid-State Drive

## 5. CPU

(*Control Processing Unit*, ou unité centrale de traitement, ou (*micro-*)*processeur*)

Il intègre, sur même puce de semiconducteur, toutes les fonctions précédentes 
(avec d'autres).

![Processeur](img/01-processeur.jpg)

Les puces actuelles travaillent sur des données et instructions de 64 bits et 
sont cadencées par des horloges de fréquence de l'ordre du gigahertz.

