---
hide:
  - navigation
---

# Shell et fichers avec GNU/Linux

*[Linux]: Linux est le noyau du système d'exploitation GNU/Linux.

??? danger "Le terminal (simulateur)"

    Il nécessite une application spécifique (exemple: *LXTerminal*, *xterm*, *gnome-terminal*&hellip;).

    <iframe loading="lazy" 
        width="100%"
        scrolling="no"
        height="275"
        title="JSLinux"
        allow="fullscreen"
        src="https://bellard.org/jslinux/vm.html?url=alpine-x86.cfg&mem=192&rows=10">
        </iframe>

    Taper ci-dessus les commandes suivantes pour obtenir une configuration de base dans le shell de cet émulateur :

    ```{.console}
    adduser -D nsi
    su -l nsi
    export PS1='[\u@\h:\W]\$ '
    clear
    ```

## 1. Interpréteur de commandes (*shell*)


*Exemple de shell : bash, tcsh&hellip;*

!!! info "Invite de commande du shell (prompt)"

    ??? tip inline end "Détails"

        - `nsi` : nom de l'utilisateur actuel
        - `localhost` : nom de l'ordinateur
        - `~` : nom du répertoire actuel (ici le répertoire personnel)
        - `$` : l'utilisateur actuel n'a pas de privilèges.


    ```{.console .rgconsole}
    [nsi@localhost:~]$ 
    ```

Il permet d'interagir directement avec le noyau du système d'exploitation ou de lancer des programmes :

```
nom_du_programme -option1 -option2 argument1 argument2
```

Deux cas peuvent se présenter[^1] :

[^1]: La commande `type` permet de savoir le programme est une primitive du shell ou un fichier exécutable:

    ```{.console rgconsole}
    [nsi@localhost:~]$ type -a cd
    cd est une primitive du shell
    ```

1. Le programme est une *primitive* (commande) du shell :

    ```{.console .rgconsole}
    [nsi@localhost:~]$ echo -n Bonjour
    ```

2. Le programme est un exécutable stocké dans l'arborescence des fichiers. Deux méthodes sont possibles :

    1. Fournir le **chemin** du programme :

        ```{.console .rgconsole}
        [nsi@localhost:~]$ /usr/bin/ls -al ~/Documents
        ...
        [nsi@localhost:~]$ ./lancer_sauvegarde Photos/
        ```

    2. Indiquer uniquement le **nom** du programme (mais il faut que le répertoire le contenant 
    soit spécifié dans la variable `PATH`).

        ```{.console .rgconsole}
        [nsi@localhost ~]$ lancer_sauvegarde Photos/
        ```

        ??? tip "Variable d'environnement PATH"
            ```{.console .rgconsole}
            [nsi@localhost:~]$ echo $PATH
            /usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin
            ```

            ??? tip "Modifier PATH"
                ```{.console .rgconsole}
                [nsi@localhost:~]$ export PATH=$PATH:/home/nsi/bin/
                ```

??? tip "Quelques commandes/programmes utiles"

    | Commande | Signification |
    |:--------:|:--------------|
    | `man`    | Manuel d'utilisation d'une commande |
    | `pwd`    | Afficher le nom du répertoire de travail actuel |
    | `ls`     | Afficher le contenu de répertoires |
    | `cd`     | Changer de répertoire de travail |
    | `cp`     | Copier un fichier |
    | `rm`     | Supprimer un fichier |
    | `mv`     | Déplacer/renommer un fichier ou répertoire |
    | `mkdir`  | Créer un répertoire |
    | `rmdir`  | Supprimer un répertoire vide |
    | `echo`   | Afficher une ligne de texte |
    | `cat`    | Afficher sur la sortie standard |
    | `less`   | Affichage avancé d'un fichier texte |
    | `nano`   | Un éditeur de texte fonctionnant dans le terminal | 

## 2. Utilisateurs

Il existe 2 catégories d'utilisateurs :

- Classique (ou *sans privilège*) : `nsi`&hellip;
    ```{.console .rgconsole}
    [nsi@localhost ~]$
    ```
- Administrateur : 
    - super-utilisateur (ou **root**)
        ```{.console .rgconsole}
        [root@localhost ~]# 
        ```
    - avec privilèges temporaires (*sudoers*)

        ??? tip inline end "sudo"
        
            `sudo` indique que le reste de la ligne est une commande à exécuter avec des privilèges (nécessite de taper son mot de passe)

        ```{.console .rgconsole}
        [nsi@localhost ~]$ sudo less /etc/shadow
        ```


## 3. Système de fichiers

Le stockage de données sur n'importe quel périphérique se fait via des fichiers (blocs
d'octets non nécessairement contigus) qui possèdent un nom et souvent une
extension (indicative).

```{.console .rgconsole}
[nsi@localhost:~]$ ls -F
Documents/   Musique/   Consignes.pdf   Readme.txt  
```

!!! warning "GNU/Linux"

    Tout est fichier dans le système d'exploitation GNU/Linux : fichier standard, 
    répertoire, périphérique, partition&hellip;

### 1. Permissions

On peut visualiser les propriétés d'un fichier à l'aide de la commande `ls`
et de son option `-l` :

??? tip inline end "Interprétation"

    Les droits sur le fichier `monfichier.txt` (utilisateur : `grosbois` 
    groupe : `nsi`),  d'une taille de 250 octets et modifié la dernière 
    fois le 10 septembre à 21h56, sont :

    - utilisateur : lecture et écriture (mais pas d'exécution)
    - groupe : lecture seule
    - autre : lecture seule

    Les droits sur le fichier `monscript.sh` (utilisateur : `grosbois` 
    groupe : `nsi`),  d'une taille de 130 octets et modifié la dernière 
    fois le 11 septembre à 11h32, sont : 

    - utilisateur : lecture, écriture et exécution
    - groupe : lecture et exécution (pas d'écriture)
    - autre : lecture seule.

```{.console .rgconsole}
[nsi@localhost:Documents]$ ls -l
-rw-r--r--. 1 grosbois nsi      250 10 sept. 21:56 monfichier.txt
-rwxr-xr--. 1 grosbois nsi      130 11 sept. 11:32 monscript.sh
```

Chaque fichier possède au minimum 9 permissions : 

![permissions](img/src/03-permissions_fichiers.svg){ width=600px }

- 3 types de permissions d'accès :

    - `r` : droit en lecture.
    - `w` : droit en écriture.
    - `x` : droit en exécution (pour un programme ou un répertoire).

- et 3 catégories d'utilisateurs :

    - `u` : le propriétaire du fichier (par défaut, la personne qui a créé le fichier),
    - `g` : le groupe d'utilisateurs qui possèdent des droits particuliers sur ce fichier. 
    - `o` : tous les autres utilisateurs.


!!! info "Modifier les permissions d'un fichier"

    La commande `chmod` permet d'ajouter ou supprimer des droits sur un fichier : 

    `chmod <categorie_utilisateur>[+-]<type de droit> nom_de_fichier`

    ??? tip inline end "Interprétation"

        Le droit d'écriture sur le fichier `monfichier.txt` est rajouté au groupe.

    Exemple:
    ```{.console .rgconsole}
    [nsi@localhost:Documents]$ chmod g+w monfichier.txt
    ```

### 2. Structure hiérarchique

FHS : *Filesystem Hierarchy Standard*

Tous les fichiers s'organisent selon une *arborescence* avec un unique parent : la *racine* (**root**, notée `/`).

```mermaid
flowchart 

    root["/"] --- dev["dev/"] & etc["etc/"] & home["home/"] & proc["proc/"]
    root --- root2["root/"] & run["run/"] & var["var/"]
    root --- tmp["tmp/"] & usr["usr/"] 
    home --- nsi["nsi/"] & grosbois["grosbois/"]
    usr --- share["share/"] --- Python2["Python"] --- HelloNSI2["HelloNSI.py"]
    var --- www["www/"] --- html["html/"] --- index.html
    subgraph "~/"
        nsi --- Documents["Documents/"] & Download["Téléchargements/"] 
        Download --- escgame["escape_game/"]
        nsi --- Seance01["Seance01/"] --- Python["Python/"] --- HelloNSI.py
    end
    grosbois --- Seance013["Seance01/"] --- Python3["Python/"] --- Hello3["HelloNSI.py"]

```

*(Un répertoire peut contenir un ou plusieurs (sous-)répertoires et fichiers)*

??? example "Exercice"

    1. Depuis le répertoire `Téléchargements`, déterminer les chemins absolu
    et relatifs pour aller dans le répertoire `Documents`.

    2. Depuis le répertoire `www`, déterminer les chemins absolu
    et relatifs pour aller dans le répertoire `share`.

    3. Dessiner l'arbre des fichiers/répertoires (connus) après les 
    commandes suivantes :

        ```{.console .rgconsole}
        [nsi@localhost:~]$ cd /home/nsi
        [nsi@localhost:~]$ mkdir Photos
        [nsi@localhost:~]$ mkdir Vidéo
        [nsi@localhost:~]$ cd Vidéo
        [nsi@localhost:Vidéo]$ mkdir Copines
        [nsi@localhost:Vidéo]$ cd ..
        [nsi@localhost:~]$ mkdir Vacances
        [nsi@localhost:~]$ mkdir Vacances/Avril
        [nsi@localhost:~]$ mkdir Vacances/Juillet
        [nsi@localhost:~]$ mkdir /home/nsi/Vidéo/Danse
        ```

    4. Citer un avantage d'un chemin relatif par rapport à un chemin absolu

    5. Citer un avantage d'un chemin absolu par rapport à un chemin relatif


??? info "Répertoires particuliers"

    - `/`: la **racine**  (*root*) est un répertoire (fichier) contient tous les autres.
    - `~/`: le répertoire personnel (`/home/nsi/` pour l'utilisateur `nsi`).
    - `./`: répertoire courant
    - `../`: répertoire parent.

- Chemins d'accès d'un fichier :

    ??? example inline end "Exemples"

        ```
        /home/nsi/Seance01/Python/HelloNSI.py
        /usr/share/Python/HelloNSI.py
        ```

    - Chemin **absolu** : liste des répertoires (séparés par `/`) 
    rencontrés depuis la racine (`/`) jusqu'à la destination. 
    
        > Il débute obligatoirement par le caractère `/`.

    
    ??? example inline end "Exemples" 
    
        (depuis le répertoire `/home/nsi`) :
        ```
        Seance01/Python/HelloNSI.py
        ../grosbois/Seance01/Python/HelloNSI.py
        ../../var/www/html/index.html
        ```

    - Chemin **relatif** : liste des répertoires (séparés par `/`)
    rencontrés depuis le répertoire courant (`./`). La notation `../` permet
    d'accéder à un répertoire parent.

        > Il débute par `./` (facultatif) ou `../`.

