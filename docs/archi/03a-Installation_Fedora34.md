# Installation de Fedora 34 LXDE sur un PC

!!! info 

    Cette installation s'effectue sur le disque dur SSD d'une tour de PC Optiplex 360 de la salle MC03.

    Elle nécessite une clé USB amorçable construite depuis l'image ISO de démarrage `Fedora-LXDE-Live-x86_64-34-1.2.iso`.

## I. Installation

1. Connecter la clé USB amorçable sur le PC et le mettre sous tension.
2. Une fois le clavier détecté et initialisé (la LED s'allume), appuyer sur la touche ++f12++ et sélectionner l'amorçage sur la clé USB.
3. Choisir l'option `Start Fedora-LXDE-Live 34` et attendre la fin du démarrage du système d'exploitation *Live*.

    ![Boot USB](img/03-boot_USB.png){ width=50% }

    ??? tip "Suppression des partitions existantes sur le disque SSD:"

        1. Le clavier utilise par défaut un agencement américain. Une astuce pour le passer en mode français est de:
            1. Lancer le programme `Install to Hard Drive`.
            2. Sélectionner la langue française pour l'installation

                ![Langue française](img/03-langue_francais.png){ width=75% }

            3. Quitter le programme d'installation.
        2. Ouvrir le *terminal* (`lxterminal`) et installer le programme `gparted` sur l'OS Live à l'aide de la commande suivante:

            ```{.console .rgconsole}
            [liveuser@localhost:~]$ sudo dnf install gparted
            ```
        3. Langer le programme `gparted` et supprimer les partitions qui se trouvent déjà sur le disque SSD (**attention :** ne rien supprimer l'autre disque dur).

4. (Re-)lancer le programme `Install to Hard Drive` et aller jusqu'au bout de la configuration:

    ![Configuration de l'installation](img/03-Config_install.png){ width=75% }

    - Installation destination : sélectionner le disque dur SSD (taille ~200 Go) et vérifier que la `configuration du stockage` est `Automatique`, puis cliquer sur le bouton `Fait`.
    - **ne pas créer d'administrateur** mais un utilisateur `nsi` (idem pour le nom complet et le mot de passe) et cocher `Faire de cet utilisateur un administrateur`.
    - Cliquer sur le bouton `Commencer l'installation`.

        ![Commencer l'installation](img/03-commencer_installation.png){ width=75% }

    - Une fois l'installation terminée, cliquer sur le bouton `Terminer l'installation`.

5. Arrêter le système Live (`Shutdown`), **enlever la clé USB**, redémarrer le PC et laisser s'exécuter l'amorçage automatique sur le nouveau système `Fedora 34`.

    ![Boot Fedora 34](img/03-boot_f34.png){ width=50% }

6. Se connecter en tant que l'utilisateur `nsi`:

    ![Fedora 34 LXDE](img/03-lxde_f34.png){ width=75% }


## II. Notions de configuration du système Linux

### 1. Généralités

1. Mise à jour du système depuis un terminal (à exécuter régulièrement) :

    ```{.console .rgconsole}
    [nsi@fedora:~]$ sudo dnf update
    ```

2. Ajout du raccourci clavier (++ctrl+alt+t++) pour lancer le terminal :

    - éditer le fichier `~/.config/openbox/lxde-rc.xml`.
    - ajouter le code HTML suivant:
        ```XML
        <keybind key="C-A-t">
            <action name="Execute">
                <command>lxterminal</command>
            </action>
        </keybind>
        ```

        ??? tip "Démo"

            Ouvrir l'image suivante dans une autre fenêtre pour une démo.
        
            <img src="../img/03-lxde_shortcut.svg" alt="raccourci clavier" width="300px" >

    - redémarrer le gestionnaire de fenêtre:

        ```{.console .rgconsole}
        [nsi@fedora:~]$ openbox --restart
        ```

### 2. Éditeur pour le développement Web

Nous allons utiliser le logiciel [Sublime Text](https://www.sublimetext.com/) qui est fourni sur un dépôt tiers :

- installation de la clé de chiffrement GPG :

    ```{.console .rgconsole}
    [nsi@fedora:~]$ sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
    ```

- ajout du dépôt :

    ```{.console .rgconsole}
    [nsi@fedora:~]$ sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
    ```

- installation du paquet :

    ```{.console .rgconsole}
    [nsi@fedora:~]$ sudo dnf install sublime-text
    ```
