---
hide:
  - navigation
---

# Généralités sur le Web

Le Web actuel est né en 1989 avec le projet WWW du chercheur 
britannique Tim Berners Lee[^1] et du belge Robert Cailliau alors qu'ils 
travaillaient au CERN.

*[WWW]: World Wide Web

| 1er site Web | Tim Berners-Lee |
| ------------ | --------------- |
| [![Premier site Web](img/01-premier_siteweb.png){ width=300px}](https://line-mode.cern.ch/www/hypertext/WWW/TheProject.html){ target=_blank } | ![Tim Berners Lee](img/01-Tim_Berners-Lee.jpg){ width=60%, class="rgimage" } |

Ils ont créé et associé l'URL, le protocole HTTP et langage HTML pour gérer la documentation du CERN et pour faciliter la collaboration entre scientifiques d'instituts et universités du monde entier, via le réseau (initialement ARPANET puis Internet).

[^1]: Tim Berners Lee est devenu en 1994 le président du consortium international W3C qui s'occupe de promouvoir les technologies du Web.

## 1. HTML

*[HTML]: HyperText Markup Language (langage à balisage et hypertexte)

Ce langage sert à décrire le contenu des documents (*pages*) qui se trouvent sur le Web (`#!html <!DOCTYPE html>`).

!!! danger "Définition"

    Une page Web est constituée d'**éléments HTML**, *imbriqués* les uns dans les autres (structure arborescente).

### a. Élément HTML

Il indique la nature (la *sémantique*) d'un contenu (ex : paragraphe, titre, tableau, image…)[^2]. 

\[
    \underset{\text{(obligatoire)}}{\underset
        {\text{balise ouvrante}}{\underbrace{\text{<nomdebalise>}}}}
    \text{contenu}
    \underset{\text{élément sans contenu)}}{\underset{\text{(absente si}}{\underset
        {\text{balise fermante}}
        {\underbrace{\text{</nomdebalise>}}}}}
        \]

- Le contenu est soit du texte, soit un ou plusieurs autres éléments HTML.

    > Certains éléments, sans contenu, n'ont pas de balise fermante. La balise ouvrante est alors qualifiée d'*auto-fermante*.

- Les noms de balises sont normalisés : 

    ??? example "exemples de noms de balise"

        | Nom de balise | Signification | Attributs spécifiques | Exemple | 
        |---------------|---------------|-----------------------|---------|
        | `h1`, `h2`, `h3` | Titre de niveau 1, 2 ou 3 |        | `#!html <h1>Le Web</h1>` |
        | `p` | paragraphe | | `#!html <p>Un paragraphe d'explication</p>` |
        | `a` | hyperlien  | `href`=URL | `#!html <a href="https://snlpdo.fr">Lien vers Moodle</a>` |
        | `img` | image | `src`=emplacement `alt`=descriptif | `#!html <img src="lpdo.jpg" alt="Logo du lycée">`

    (une liste complète est fournie à cette [page](https://developer.mozilla.org/fr/docs/Web/HTML/Reference))


- Une balise ouvrante peut avoir un, plusieurs (ou aucun) attributs :

    \[
    \text{<nomdebalise attribut1="valeur1" attribut2="valeur2">}
    \]

    (quelques attributs sont universels, mais bien souvent, ils sont propres à chaque élément HTML)

??? info "DOM (Document Object Model)"

    Lorsque le contenu d’un élément est un autre élément HTML, on obtient une structure hiérarchique : un *arbre* constitué de *n&oelig;uds* parent et enfants reliés par des branches.

    Le DOM est la représentation, sous forme d'*objets*, des données d'une page Web. Il définit la façon dont cette page peut être modifiée, en termes de style et de contenu, par des scripts ou programmes (orientés objet). Pour cela chaque n&oelig;ud :

    - possède des propriétés et des méthodes.
    - peut être associé à un gestionnaire d'évènements.


[^2]: Les balises HTML n’ont pas pour objectif de spécifier la manière dont le client (navigateur) doit afficher ce contenu. Ce rôle est dévolu au langage CSS.


### b. Structure minimale d'un document HTML 

=== "Code-source"

    ```html linenums="1"
    <!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>Titre de la page</title>
        </head>
        <body>

        </body>
    </html>
    ```

=== "DOM"

    ```mermaid
    flowchart 
        html --- head & body
        head --- meta & title 
        title --- text["titre de la page"]

        style text stroke:none,fill:none;
    ```

Les prochains éléments seront ajoutés :

- Sous l'élément `head` : s'ils contiennent des métadonnées ou tout autre contenu 
non affichable (feuille de style, scripts&hellip;).
- Sous l'élément `body` : s'ils ont du contenu affichable.

## 2. Page Web avancée

### a. Langage CSS

*[CSS]: Cascading Style Sheet

Le style d'affichage d'une page Web (agencement, couleurs, espacements&hellip;) 
dépend du navigateur, mais il est contrôlable grâce au langage CSS.

=== "Format d'instruction"

    ```
    propriété1: valeur1;
    propriété2: valeur2;
    ```

    (une liste des propriétés et valeurs utilisables est fournie sur cette 
    [page](https://developer.mozilla.org/fr/docs/Web/CSS/Reference))

=== "Exemples"

    ```css linenums="1"
    font-size: 48pt;
    text-align: center;
    border: 1px solid red;
    ```

- Les instructions s'écrivent :

    - dans la balise ouvrante de l'élément HTML, grâce à l'attribut `style` :

        ```html
        <h1 style="color: red; font-size: 32pt;">Mon titre</h1>
        ```

    - de façon regroupée dans une feuille de style (succession de **règles**) :

        === "Format d'une règle"

            ```css
            selecteur {
                propriete1: valeur1;
                propriete2: valeur2;
            }
            ```

            où [`selecteur`](https://delmas-rigoutsos.nom.fr/outils/explain-expression/) 
            indique le ou les éléments HTML concernés.

        === "Exemple"

            ```css
            h1 {
                color: red;
                font-size: 48pt;
            }
            ```

            (ici toutes les éléments `h1` sont modifiés)

        Cette feuille de style est écrite soit :

        - sous un élément `style` de la page HTML (de préférence sous l'élément `head`):

            ```html
            <style>
                body {
                    background-color: white;
                }
                h1 {
                    color: red;
                    font-size: 48pt;
                }
            </style>
            ```

        - dans un fichier externe (exemple : `feuilledestyle.css`) chargé avec l'élément `link` (de préférence sous l'élément head) :

            ```html
            <link rel="stylesheet" type="text/css" href="chemin/relatif/vers/feuilledestyle.css">
            ```

### b. Identifiant et classes

Ils facilitent la sélection d'un ou plusieurs éléments d'une page HTML :

- un **seul élément** : on lui associe un identifiant unique grâce à l'attribut `id` (placé dans sa balise ouvrante) :

    === "HTML"

        ```html
        <nomDeBalise id="mon-id-unique">...
        ```

    === "CSS"

        ```css 
        #mon-id-unique {
            color: red;
        }
        ```

        !!! danger "Attention"
        
            Le nom de l'identifiant doit être précédé de `#` dans le sélecteur 
            CSS.

- un **groupe d'éléments** : utilisation de l'attribut `class` (la même valeur est réutilisée dans chaque élément) :

    === "HTML"

        ```html
        <nomDeBalise1 class="ma-classe">
            ...
        <nomDeBalise2 class="ma-classe">
        ```

    === "CSS"

        ```css
        .ma-classe {
            border: 1px black solid;
        }
        ```

        !!! danger "Attention"
        
            Le nom de l'identifiant doit être précédé de `.` dans le sélecteur 
            CSS.

!!! info "Remarque"

    Un élément ne peut avoir qu'un seul identifiant, mais il peut appartenir à plusieurs classes.

    ```html
    <nomdeBalise id="mon-identifiant" class="ma-classe1 ma-classe2">
    ```

### c. Éléments HTML neutres

L'élément HTML `div` ne décrit aucune sémantique, mais il est énormément utilisé pour *encadrer* des groupes d'éléments et leur appliquer un style.

??? info "span"

    Cet élément sert essentiellement à *encadrer* des portions de texte.

Exemple :

=== "HTML"

    ```html
    <div class="cadre">
        <p>Un premier <span class="en-vert">paragraphe</span></p>
        <p>Un deuxième paragraphe</p>
    </div>
    ```

=== "CSS"

    ```css
    .cadre {
        border:  1px red solid;
        background-color:  beige;
        padding:  5px;
        margin:  10px;
    }

    .en-vert {
        color: green;
    }
    ```

## 3. URL

*[URL]: Uniform Resource Locator

Le Web est un *maillage* de ressources en réseau basé sur la notion d'hypertexte
 numérique[^6].
 
[^6]: 1965, Ted Nelson (sociologue américain): projet Xanadu.

![Hypertexte](img/src/01-hypertexte.svg){ width=60%, class="rgimage" }

Le type d'adresse utilisé par le Web s'appelle une URL : elle se décompose en plusieurs fragments qui indiquent à la fois l'emplacement d'une ressource et la façon d'y accéder. 

Fragments d'une URL simple :

    <protocole>://<serveur>/<chemin sur le serveur>

!!! example "Exemple"

    ![URL avec https](img/01-https_via_URL.svg){ width=80%, class="rgimage" }

    ![Décomposition URL](img/src/01-decomposition_URL.svg){ width=50%, class="rgimage" }

    1. Le protocole d'accès: `https` (existe aussi : `http`, `ftp`, `file`, `mailto`&hellip;)
    2. Le nom de domaine (serveur), ou son adresse IP, qui fournit la ressource[^7] : `snlpdo.fr`

    [^7]: Ce fragment n'est pas présent pour le protocole `file`.

    3. Le chemin vers la ressource (parfois appelé *route*): `/NSI/presentation.html`:
        - Répertoire : `/NSI/` (utilisation du séparateur `/` pour les répertoires) 
        - Fichier : `presentation.html`


??? info "Aller plus loin"

    D'autres fragments (optionnels) peuvent apparaître dans une URL :

        <protocole>://<utilisateur>:<motdepasse>@<serveur>:<port>/<chemin>?cle1=val1&cle2=val2#<ancre>

    - Une information sur l'utilisateur qui peut contenir un nom et un mot de passe.

    - Un numéro de `port` : *porte* à utiliser pour accéder à la ressource (généralement `80` pour http, `443` pour https&hellip;)

    - Des paramètres à fournir au serveur : après le caractère `?`, sous la forme de couples `clé=valeur`, séparés par le caractère `&`.

    - Une ancre pour préciser un emplacement dans la ressource.

    !!! example "Exemples"

        - `ftp://admin:azerty@ftp.snlpdo.fr/NSI/1nsi/archive.zip`
        - `https://snlpdo.fr/meteo?jour=15&mois=septembre&annee=2022`
        - `https://snlpdo.fr/1nsi/Web#URL`


## 4. HTTP

*[HTTP]: HyperText Transfer Protocol

??? info "Communications clients-serveurs"

    Un **serveur** est un hôte qui fournit des **services** à d'autres hôtes (les *clients*) en **répondant** à leurs **requêtes**.
    
    Différents types de serveurs peuvent être sollicités lorsqu'un client souhaite accéder à une ressource Web :

    ```mermaid 
    sequenceDiagram
        autonumber
        actor Client
        participant Web as Serveur Web
        participant DNS as Serveur DNS
        participant MDR as Moteur Recherche

        Client->>+MDR: Météo à Vizille ?
        Note over Client,MDR: Requête pour obtenir une URL
        MDR-->>-Client: https://meteofrance.com/previsions-meteo-france/vizille/38220

        Client->>+DNS: meteofrance.com ?
        Note over Client,DNS: Requête pour obtenir une adresse IP
        DNS-->>-Client: 185.86.168.137

        Client->>+Web: GET #hellip;
        Note over Client,Web: Connexion et requête HTTP
        Web-->>-Client: 200 #hellip;
    ```

HTTP est le protocole utilisé pour les communications entre client et serveur Web :

![Modèle client-serveur](img/01-modele_client_serveur.svg){ class="rgimage" width="80%" }

Exemple : le client HTTP veut obtenir la ressource *statique* `PageWeb.html` 
auprès du serveur Web `monserveur.fr`.
> Les données sont stockées sur le serveur et le traitement (mise en page) est réalisé par le navigateur.


=== "1. Requête"

    Le navigateur (client) se connecte au serveur Web[^3] et envoie une **requête** `GET`[^8] pour demander la ressource qui a pour URL: `http://monserver.fr/PageWeb.html` :

    [^8]: Autres [requêtes](https://developer.mozilla.org/fr/docs/Web/HTTP/Methods) HTTP possibles: `POST`, `DELETE`&hellip;

    ```http title="Contenu (minimal) de la requête"
    GET /PageWeb.html HTTP/1.1
    Host: monserveur.fr
    ```

    [^3]: L'établissement de la communication avec le serveur distant s'effectue à l'aide du protocole TCP (non abordé ici).

=== "2. Réponse"

    Le serveur renvoie le **code de retour** `200` (car la ressource existe)[^4]  suivi du contenu de la page HTML[^5] :

    ```http
    HTTP/1.1 200 OK
    Date: Mon, 18 Sep 2022 21:04:27 GMT
    Server: Apache/2.4.51 (Fedora)
    Last-Modified: Mon, 18 Sep 2022 21:03:24 GMT
    ETag: "a9-5cea6e027bcc0"
    Accept-Ranges: bytes
    Content-Length: 169
    Content-Type: text/html; charset=UTF-8

    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Page de Test</title>
    </head>
    <body>
        <h1>Bienvenu chez moi</h1>
    </body>
    </html>
    ```

    [^4]: Autres codes possibles: `30x` en cas de redirection, `404` si la ressource n'existe pas, `50x` en cas d'erreur sur le serveur&hellip;

    [^5]: Plusieurs requêtes `GET` peuvent être lancées par le client pour une même ressource, notamment pour récupérer des images, fichiers CSS&hellip;

    Le code HTML reçu est ensuite interprété par le navigateur pour réaliser un affichage (qui peut différer d'un navigateur à un autre).


??? tip "Pour aller plus loin : HTTPS"

    Le serveur et le client définissent initialement une clé secrète pour chiffrer les communications.

    ```{.console .rgconsole}
    [nsi@localhost:~]$ openssl  s_client -connect snlpdo.fr:443
    ...
    GET /NSI/index.html HTTP/1.1
    Host: snlpdo.fr
    ...
    ```

