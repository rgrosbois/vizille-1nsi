---
hide:
  - navigation
---

# Introduction aux formulaires

Un formulaire Web permet de récupérer des **données**
saisies par l'utilisateur dans des éléments HTML spécifiques.

!!! danger "Définition"

    Dans toute cette leçon, une donnée correspond à un couple : (**nom**, **valeur**).

??? tip inline end "Données associées"

    | Noms        | Valeurs      |
    |-------------|:------------:|
    | pseudo      | `rgrosbois`  |
    | motdepasse  | `bidon`      |
    | specialite  | `NSI`        |
    | naissance   | `1970-04-01` |

![Formulaire Web](img/src/02-exemple_formulaire.svg){ width=40% style="margin-left: 150px;"}


## 1. Éléments HTML

Le code HTML utilise des [balises spécifiques](https://developer.mozilla.org/fr/docs/Web/HTML/Element#formulaires). Quelques exemples :

??? example inline end "Exemple d'arbre HTML"

    ```mermaid
    flowchart
        form --- input1["input"] & input2["input"] & select & input3["input"]
        select --- option1["option"] & option2["option"] & option3["option"]
    ```

| Nom de balise | Description |
|---------------|-------------|
| `form`        | Nom de balise de l'élément parent |
| `input`       | Champ de saisie, différents [types](https://developer.mozilla.org/fr/docs/Web/HTML/Element/input#les_diff%C3%A9rents_types_de_champs_input) possibles |
| `select`      | Pour une liste d'`option`s.


> Chaque élément de saisie est généralement accompagné d'un `label` qui fournit des 
> indications à l'utilisateur.

??? tip inline end "Données générées"

    | Noms        | Valeurs |
    |-------------|---------|
    | nom         | `...`   |
    | prenom      | `...`   |
    | nationalite | `...`   | 

=== "Exemple"

    ![Exemple de formulaire](img/src/02-exemple_formulaire2.svg){ width=40% }

=== "Code HTML"

    ```html
    <form>
        <label for="nomId">Nom</label>
        <input type="text" id="nomId" name="nom">

        <label for="prenomId">Prénom</label>
        <input type="text" id="prenomId" name="prenom">

        <label for="nationaliteId">Nationalité</label>
        <select id="nationaliteId" name="nationalite">
            <option>Française</option>
            <option>Anglaise</option>
            <option>...</option>
        </select>
        <input type="submit" value="Envoyer">
    </form>
    ```

Correspondances :

![Correspondance attributs](img/src/02-explications_attributs.svg){ width=100% }

??? tip "Précisions sur certains attributs"

    | Balise  | Attribut | Précisions                                    |
    |---------|----------|-----------------------------------------------|
    | `*`     | `name`   | Fixe le nom de la donnée                      |
    | `*`     | `value`  | Fixe la valeur (initiale) de la donnée        |
    | `label` | `for`    | Indique l'`id` de la balise de champ associée |


## 2. Transmission de données

<center>

![Transmission vers servlet](img/src/02-transmission_servlet.svg){ width=75% }

</center>

??? tip inline end "Langages"

    Une servlet peut être écrite à l'aide d'un des divers langages de programmation suivants[^1] :

    - `PHP` : langage le plus utilisé à ce jour, directement supporté par la plupart des serveurs HTTP.
    - `JavaScript` (`Nodejs`)
    - `Python` : langage qui nécessite (généralement) de charger un module d'adaptation spécifique sur le serveur Web.
    - `C`, `Java`...

Une *servlet* est un programme exécuté sur un serveur HTTP et qui, dans le cas présent, va :

1. Traiter les données récupérées par un formulaire et 
2. Renvoyer un résultat au client (exemple : une nouvelle page Web créée dynamiquement).

[^1]: Nous utiliserons Python (avec le framework `Flask`) pour écrire des servlets en NSI car ce langage est déja mis en œuvre dans d'autres parties du programme.

Les attributs `action` et `method` de la balise `form` ouvrante permettent de gérer cette transmission.

=== "`action`"

    Cet attribut contient l'URL de la servlet cible.

    ```html
    <form action="http://monserveur.fr/servlet">
        ...
    </form>
    ```

    ??? tip "Rappel: fragments d'URL (simple)"

        `<protocole>://<serveur>/<chemin sur le serveur>`

        Il est possible de n'indiquer que la partie *chemin* de l'URL de la servlet si cette dernière se trouve sur le même serveur que le formulaire.

        Exemple : `action="/gestion/login"`

=== "`method`"

    Pour spécifier l'une des 2 méthodes HTTP permettant de transmettre des données :

    ??? tip inline end "Remarques"
    
        - Avantage : les données peuvent être envoyées/modifiées même sans utiliser de formulaire (on modifie directement l'URL)
        - Inconvénients : les données sont directement visibles dans le champ d'adresse du navigateur et il existe des restrictions (taille, format...)

    - `GET` : les données sont ajoutées automatiquement à la fin de l'URL[^2] lors de l'appel de la servlet.
    
    [^2]: Les données se trouvent après le caractère `?`, selon le format `nom=valeur` et séparées par le caractère `&`.

        ```html
        <form action="http://monserveur.fr/servlet" method="GET">
            ...
        </form>
        ```

        ??? tip "Fragments d'URL"
        
            L'URL possède alors les fragments suivants : 

            `<protocole>://<serveur>/<chemin sur le serveur>?nom1=valeur1&nom2=valeur2`

    ??? tip inline end "Remarques"

        - Avantage : les données n'ont quasiment aucune restriction et ne sont pas directement visibles.

        - Inconvénient : il faut obligatoirement un formulaire HTML (ou équivalent) pour les générer.

    - `POST` : elles sont fournies en tant que données supplémentaires lors de l'appel de la servlet.

        ```html
        <form action="http://monserveur.fr/servlet" method="POST">
            ...
        </form>
        ```

## 3. Traitement de données (Python)

Le module `request` du framework `Flask` permet de récupérer directement les données dans un **dictionnaire** :

=== "Depuis la méthode `GET`"

    ```py
    @app.route('/traitement', methods=['GET'])
    def traitement():
        # Récupération des données dans un dictionnaire
        donnees = dict(request.args)
        ...
    ```

=== "Depuis la méthode `POST`"

    ```py
    @app.route('/traitement', methods=['POST'])
    def traitement():
        # Récupération des données dans un dictionnaire
        donnees = dict(request.form)
        ...
    ```

À chaque couple (nom, valeur) de donnée correspond un couple (clé, valeur) dans le dictionnaire (ici `donnees`) :

=== "Exemple de données"

    | Noms         | Valeurs     |
    |--------------|-------------|
    | `pseudo`     | `john`      |
    | `motdepasse` | `bidon`     |
    | `age`        | `15`        |

=== "Dictionnaire Python récupéré"

    ```py
    # Contenu de la dictionnaire donnees
    >>> print(donnees)
    {
        'pseudo'     : 'john',
        'motdepasse' : 'bidon',
        'age'        : '15'
    }
    ```

Pour accéder à la valeur d'une donnée, par exemple le mot de passe de l'utilisateur, il faut écrire :

```py
mot_de_passe = donnees['motdepasse']
```

??? danger "Type des valeurs"

    Les valeurs reçues dans le dictionnaires sont des chaînes de caractères (type `str`). Il faudra parfois
    les convertir dans d'autres types suivants le traitement à effectuer.

    Par exemple, pour effectuer un traitement différentié selon l'age d'un utilisateur :

    ```py
    age = int(donnees['age'])

    if age >= 18 : # utilisateur majeur
        ...
    else : # utilisateur mineur
        ...
    ```

Une fois le traitement effectué, la fonction peut renvoyer :

- une simple chaîne de caractère :

    ```py
    @app.route('/traitement', methods=['POST'])
        def traitement():
            donnees = dict(request.form)
            ...
            # traitement intermédiaire
            ...
            return "Utilisateur valide"
    ```

- une page HTML complète (utilisation d'un *template*) :

    ```py
    @app.route('/traitement', methods=['POST'])
        def traitement():
            donnees = dict(request.form)
            ...
            # traitement intermédiaire
            ...
            return render_template('resultat.html')
    ```
