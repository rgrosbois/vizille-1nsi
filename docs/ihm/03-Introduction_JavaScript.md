---
hide:
  - navigation
---

# Introduction au langage JavaScript

JavaScript (ou ECMAScript) est un langage de script à l'origine conçu pour être incorporé (ou associé) à une page Web et être exécuté par le navigateur (client Web). Il permet d'ajouter de l'interactivité et du dynamisme à une page Web.

## I. Pour débuter

### 1. Console du navigateur

C'est un outil qui sert au développeur pour :

- Visualiser les divers messages d'exécution des scripts (erreurs, débogage...).
- Tester sans risque des instructions (les effets disparaissent en rechargeant la page).

Pour accéder à cette console, il faut ouvrir l'*inspecteur* et cliquer sur l'onglet *Console* :

![Console Firefox](img/03-Console_Firefox.png)

### 2. Intégration de scripts

Il y a 2 façons de faire :

=== "En externe (à préférer)"

    Les instructions sont écrites dans un fichier qui porte l'extension '.js' (par convention) et son chargement s'effectue
    grâce à l'attribut `src` d'une la balise `script` ouvrante :

    ```html
    <script src="monscript.js"></script>
    ```

=== "En interne"

    Les instructions sont directement écrites dans le fichier HTML, à l'intérieur d'un élément `script` :

    ```html
    <script>
        // succession d'instructions 
        // en JavaScript
        ...
    </script>
    ```

??? warning "Position préférable d'un élément script"

    Comme l'exécution du code JavaScript nécessite généralement que la page HTML soit complètement chargée,
    il est préférable de placer ce(s) éléments :

    - soit à la fin du corps (juste avant la balise `body` fermante )

        ```html
            ...
            <script src="monscript.js"></script>
          </body>
        </html>
        ```

    - soit à la fin de l'en-tête (juste avant la balise `head` fermante) avec le mot-clé `defer` :

        ```html
            <script src="monscript.js" defer></script>
        </head>
        <body>
            ...
        ```        

## II. Quelques bases

### 1. Généralités

- Les commentaires : indications pour le développeur (ces instructions ne sont jamais exécutées)

    - jusqu'à la fin d'une ligne : débute par `// `) 

        ```js
        // Un commentaire sur une seule ligne
        ```

    - sur plusieurs lignes : débute par `/*` et se termine par `*/`

        ```js
        /* Un commentaire qui
        s'étend sur plusieurs
        lignes */
        ```

- Fin d'instruction : une ligne contient une seule instruction et se termine par `;`

- Bloc d'instructions (d'une boucle, fonction...) : débute par `{`, se termine par `}` et chaque ligne d'instruction est indentée.

### 2. Variables

Contrairement à Python, les variables sont **déclarées** utilisation :
mot clé `let` (`,`  pour séparer plusieurs variables) :

```js
let x, y; // 2 nouvelles variables
```

L'initialisation peut se fait à l'initialisation ou ultérieurement :

```js
let z = 5.2; 
x = 4; // variable déclarée précédemment
```

### 3. Types

- `Number` : les nombres, sans distinction entre *entiers* et *flottants*.

    ```js
    let x = 4;
    let y = 5.2;
    ```

- `boolean` : les booléens (`true`, `false`, résultat d'un test ou d'une expression logique) 

- `string` : les chaînes de caractères

    ```js
    let discipline = "NSI";
    ```

- `Array` : les tableaux (semblables aux `list` de Python)

    ```js
    let jours = ['lundi', 'mardi'];
    ```

    longueur du tableau : `jours.length`

### 4. Exécution conditionnelle 

Avec les mots clé `if`, `else if` et `else`, les 2 premiers étant accompagnés d'une condition (entre parenthèses) :

```js
if (age<13) {
    categorie = "enfant";
} else if (age<18) {
    categorie = "adolescent";
} else {
    categorie = "adulte";
}
```

### 5. Boucle (for)

Il faut fournir 3 informations séparées par des `;` et placées entre parenthèses :

```js
for (let i=0; i<4; i=i+1) { 
  // Bloc d'instructions
  // exécuté 4 fois
}
```

- la déclaration/initialisation : `let i=0`
- la condition de poursuite des itérations : `i<4`
- l'instruction de fin de chaque itération : `i=i+1`

??? tip "Autre type de boucle"

    Il existe aussi la boucle `while` :

    ```js
    while (a>0) {
        // Bloc à exécuter
    }
    ```

### 6. Fonction

La déclaration d'une nouvelle fonction se fait avec le mot clé `function` :

```js
function moyenne(tab, iMin=0, iMax=-1) {
    let max = iMax;
    if (iMax === -1) {
        max = tab.length-1;
    }
    
    let somme = 0;
    for (let i=iMin; i<=max; i=i+1) {
        somme += tab[i];
    }
    
    return somme/(max-iMin+1);
}
```

Pour appeler cette fonction avec son argument obligatoire :

```js
moyenne([10, 11, 12]);
```

## III. Interactions avec l'utilisateur

### 1. Sorties (affichage)

Il existe 3 façons d'afficher des informations à destination de l'utilisateur : dans la console, la boîte de dialogue ou directement dans la page Web. 

=== "Page HTML (à préférer)" 

    On modifie, a posteriori, le contenu de la page (voir la partie *Modification du DOM*).

    - Récupérer d'abord l'élément HTML avec `document.querySelecteur` et un 
    sélecteur similaire au CSS :

        ```js
        let baliseH1 = document.querySelector('h1')
        ```

    - Modifier le contenu ou le style de cet élément :

        ```js
        baliseH1.textContent = 'Nouveau titre';
        baliseH1.style.color = "red";
        ```

=== "Console"

    Méthode à n'utiliser que durant le développement (l'utilisateur basique n'a pas connaissance de l'existence de cette console).

    ```js
    console.log('Un message classique'); // équivalent du print en Python
    console.error("Un message d'erreur")
    ```

=== "Boîte de dialogue"
    
    À n'utiliser qu'avec parcimonie :

    ```js
    alert("Le message à afficher")
    reponse = prompt("Un message pour une boîte attendant une réponse");
    ```


### 2. Entrées (évènement)

La plupart des interactions de l'utilisateur avec l'IHM génèrent des [évènements](https://developer.mozilla.org/fr/docs/Web/Events). Quelques exemples :

| Noms        | Commentaires                            |
|-------------|-----------------------------------------|
| `click`     | Clic de souris sur un élément de l'IHM  |
| `keydown`   | Appui sur une touche de clavier         |
| `mousemove` | Déplacement de la souris sur un élément |
| &hellip;    | &hellip;                                |

Il existe 2 façons de spécifier une fonction JavaScript à exécuter lorsque survient un tel évènement : depuis le code JavaScript ou depuis le code HTML.

=== "JavaScript (à préférer)" 

    Il faut ajouter un *écouteur* de l'évènement (`addEventListener`) pour l'élément cible.

    ```js
    let image = document.querySelector("#mon-image"); // élément avec id='mon-image'
    image.addEventListener('click', codeAExecuter); // écouteur de clic sur l'image
    ```

=== "HTML" 

    Attributs spécifiques (de la forme `on`+*nom de l'évènement*) dans la balise ouvrante de l'élément :

    - `onsubmit` pour élément `form` (code JavaScript exécuté juste avant d'envoyer les données au serveur)

        ```html
        <form onsubmit="codeAExecuter2" method="..." action="...">
            ...
        </form>
        ```
        
    - Attribut `onclick` de l'élément `button` :

        ```html
        <button onclick="codeAExecuter3">Valider<button>
        ```

La fonction JavaScript à exécuter se déclare de la manière suivante :

```js
function codeAExecuter(e) { // e contient l'évènement correspondant
    ...
}
```

## IV. Dessiner dans un canvas

Un [Canvas](https://developer.mozilla.org/fr/docs/Web/API/CanvasRenderingContext2D) est une zone rectangulaire dans laquelle il est possible de dessiner à l'aide du langage JavaScript.

!!! info "Canvas et son répère" 

    ![Canvas](img/src/03-repere.svg){ width=60% }

    Attention : l'axe des ordonnées est (presque) toujours orienté vers le
    bas en informatique.

=== "Code HTML"

    Il faut ajouter un élément `canvas` et spécifier 
    
    - ses dimensions et 
    - identifiant (pour accéder à ce canvas depuis le code JavaScript)

    ```html
    <canvas width="400" height="300" id="canvas">
        Canvas non supporté !
    </canvas>
    ```

=== "Code JavaScript (initial)"

    Avant toute chose, il faut récupérer l'outil de *dessin* de ce canvas :

    ```js
    let canvas = document.querySelector("#canvas");
    let pinceau = canvas.getContext("2d");
    ```

    (à placer dans élément `script` ou un fichier JavaScript externe)

### 1. Couleur

??? tip inline end "Formats de couleurs"

    On peut aussi utiliser de l'hexadécimal (`"#FFA500"`), une couleur
    RGB (`"rgb(255, 165, 0)"`), ou RGBA (`"rgba(255, 165, 0, 0.5)"`)&hellip;
    

- `pinceau.fillStyle` permet de changer la couleur de remplissage :

    ```js
    pinceau.fillStyle = "red";
    ```

- `pinceau.strokeStyle` permet de changer la couleur des traits :

    ```js
    pinceau.strokeStyle = "green";
    ```


### 2. Formes géométriques simples

Seuls les lignes droites et rectangles sont supportés nativement.

=== "Ligne droite"

    Une droite est définie par 2 points de coordonnés `(x1, y1)` et `(x2, y2)`:

    ![Rectangle](img/src/03-repere-droite.svg){ width=80% }

    ```js
    pinceau.lineWidth = 1.0; // épaisseur du trait
    pinceau.moveTo(x1, y1); // point de départ
    pinceau.lineTo(x2, y2); // point d'arrivée
    ```

    > `moveTo` déplace le *pinceau* sans laisser de trace alors que `lineTo` effectue un déplacement en ligne droite en traçant un trait.

=== "Rectangle"

    Il faut spécifier les coordonnées (`x` puis `y`) de son coin supérieur gauche, puis sa `largeur` et enfin sa `hauteur`.

    === "Coutour"

        ```js
        pinceau.strokeRect(50, 50, 150, 75); // uniquement le contour 
        ```

        ![Rectangle](img/src/03-repere-rectangle.svg){ width=80% }

    === "Remplissage"

        ```js
        pinceau.fillRect(50, 50, 150, 75); // uniquement l'intérieur
        ```

        ![Rectangle](img/src/03-repere-rectangle-fill.svg){ width=80% }

### 3. Forme composée

Il faut créer un *chemin* :

```js
pinceau.beginPath();
/**
 * Placer ici les instructions de dessin du chemin
 **/
pinceau.closePath();
```

et appeler ensuite l'instruction de dessin adaptée :

```js
pinceau.stroke(); // uniquement le contour
pinceau.fill(); // uniquement le remplissage
```

??? tip "(arc de) cercle"

    Pour dessiner un [arc/cercle](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/arcTo), il faut spécifier les coordonnées du centre `(x, y)` , puis le `rayon`, les angles de début et de fin (`0` et `2*Math.PI` pour un 
    cercle) et enfin `True` pour le parcourir dans le sens anti-horaire.

    === "Contour"

        ```js
        pinceau.beginPath()
        pinceau.arc(75, 75, 50, 0, 2*Math.PI, true);
        pinceau.closePath()

        pinceau.stroke();
        ```

        ![Rectangle](img/src/03-repere-cercle.svg){ width=80% }

    === "Remplissage"

        ```js
        pinceau.beginPath()
        pinceau.arc(75, 75, 50, 0, 2*Math.PI, true);
        pinceau.closePath()

        pinceau.fill();
        ```

        ![Rectangle](img/src/03-repere-cercle-fill.svg){ width=80% }


    (pour plus d'[informations](https://developer.mozilla.org/fr/docs/Web/API/CanvasRenderingContext2D/arc))


### 4. Transformations

Toutes les instructions suivant une transformation sont affectées par cette dernière. Pour appliquer la transformation uniquement sur quelques instructions, il faut respecter la structure suivante :

1. Sauvegarder le repère initial :

    ```js
    pinceau.save()
    ```

2. Appliquer la transformation 

3. Restaurer le repère initial :

    ```js
    pinceau.restore();
    ```

=== "Translation"

    Elle s'effectue à l'aide de la méthode [`translate`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/translate):

    ```js
    // x: décalage selon les abscisses (vers la droite)
    // y: décalage selon les ordonnées (vers le bas)
    pinceau.translate(x, y);
    ```

    ??? info "Exemple : grille de jeu"

        ```js
        for(let i=0; i<4; i++) {
        for(let j=0; j<5; j++) {
            pinceau.save();
        
            pinceau.translate(j*100, i*100);
            pinceau.fillRect(5, 5, 90, 90);
        
            pinceau.restore();
        }
        }
        ```

=== "Rotation"

    Elle s'effectue à l'aide de la méthode [`rotate`](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/rotate):

    ```js
    // angle selon le sens-antihoraire, en radian.
    pinceau.rotate(angle);
    ```

    !!! warning "Attention"

        La rotation s'effectue autour de l'origine `(0, 0)` du repère. Il est nécessaire d'effectuer une translation pour touner autour d'un autre point.

### 5. Exemple d'animation

À l'aide de la méthode [`setInterval`](https://developer.mozilla.org/en-US/docs/Web/API/Window/setInterval) qui permet d'appeler périodiquement la fonction de dessin.

```js
function monDessin() {
    ...
}

setInterval(monDessin, 500); // toutes les 500 ms
```

!!! warning "Attention"

    Il faut généralement effacer la zone couverte par le dessin précédent avant de dessiner le nouveau, grâce à l'instruction `clearRect(x, y, largeur, hauteur)`.

## V. Modification du DOM

*(Informations à venir)*